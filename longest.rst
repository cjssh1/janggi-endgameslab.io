================
Longest endgames
================

Records
~~~~~~~

.. list-table::

    * - n pieces
      - endgame
      - longest mate
    * - 2
      - NA
      - No mate
    * - 3
      - NA
      - No mate
    * - 4
      - :ref:`漢兵兵楚`
      - 21
    * - 5
      - :ref:`漢象兵楚卒`
      - 52
    * - 6 (so far)
      - :ref:`漢馬馬兵楚馬`
      - 125

All endgames
~~~~~~~~~~~~

.. list-table::

    * - Endgame
      - 漢
      - 楚
    * - 漢楚
      - No mate
      - No mate
    * - 漢車楚
      - No mate
      - No mate
    * - 漢包楚
      - No mate
      - No mate
    * - 漢馬楚
      - No mate
      - No mate
    * - 漢象楚
      - No mate
      - No mate
    * - 漢兵楚
      - No mate
      - No mate
    * - 漢士楚
      - No mate
      - No mate
    * - :ref:`漢車車楚`
      - 7
      - No mate
    * - :ref:`漢車包楚`
      - 16
      - No mate
    * - :ref:`漢車馬楚`
      - 11
      - No mate
    * - :ref:`漢車象楚`
      - 10
      - No mate
    * - :ref:`漢車兵楚`
      - 14
      - No mate
    * - 漢車士楚
      - No mate
      - No mate
    * - :ref:`漢車楚車`
      - 1
      - 1
    * - :ref:`漢車楚包`
      - 1
      - No mate
    * - 漢車楚馬
      - No mate
      - No mate
    * - :ref:`漢車楚象`
      - 1
      - No mate
    * - 漢車楚卒
      - No mate
      - No mate
    * - :ref:`漢車楚士`
      - 1
      - No mate
    * - 漢包包楚
      - No mate
      - No mate
    * - :ref:`漢包馬楚`
      - 1
      - No mate
    * - 漢包象楚
      - No mate
      - No mate
    * - :ref:`漢包兵楚`
      - 2
      - No mate
    * - 漢包士楚
      - No mate
      - No mate
    * - 漢包楚包
      - No mate
      - No mate
    * - 漢包楚馬
      - No mate
      - No mate
    * - 漢包楚象
      - No mate
      - No mate
    * - 漢包楚卒
      - No mate
      - No mate
    * - 漢包楚士
      - No mate
      - No mate
    * - :ref:`漢馬馬楚`
      - 1
      - No mate
    * - :ref:`漢馬象楚`
      - 1
      - No mate
    * - :ref:`漢馬兵楚`
      - 18
      - No mate
    * - 漢馬士楚
      - No mate
      - No mate
    * - 漢馬楚馬
      - No mate
      - No mate
    * - 漢馬楚象
      - No mate
      - No mate
    * - 漢馬楚卒
      - No mate
      - No mate
    * - 漢馬楚士
      - No mate
      - No mate
    * - :ref:`漢象象楚`
      - 1
      - No mate
    * - :ref:`漢象兵楚`
      - 20
      - No mate
    * - 漢象士楚
      - No mate
      - No mate
    * - 漢象楚象
      - No mate
      - No mate
    * - 漢象楚卒
      - No mate
      - No mate
    * - 漢象楚士
      - No mate
      - No mate
    * - :ref:`漢兵兵楚`
      - 21
      - No mate
    * - 漢兵士楚
      - No mate
      - No mate
    * - 漢兵楚卒
      - No mate
      - No mate
    * - 漢兵楚士
      - No mate
      - No mate
    * - 漢士士楚
      - No mate
      - No mate
    * - 漢士楚士
      - No mate
      - No mate
    * - :ref:`漢車車包楚`
      - 14
      - No mate
    * - :ref:`漢車車馬楚`
      - 7
      - No mate
    * - :ref:`漢車車象楚`
      - 10
      - No mate
    * - :ref:`漢車車兵楚`
      - 7
      - No mate
    * - :ref:`漢車車士楚`
      - 8
      - No mate
    * - :ref:`漢車車楚車`
      - 21
      - 1
    * - :ref:`漢車車楚包`
      - 8
      - No mate
    * - :ref:`漢車車楚馬`
      - 12
      - No mate
    * - :ref:`漢車車楚象`
      - 10
      - No mate
    * - :ref:`漢車車楚卒`
      - 9
      - No mate
    * - :ref:`漢車車楚士`
      - 9
      - No mate
    * - :ref:`漢車包包楚`
      - 16
      - No mate
    * - :ref:`漢車包馬楚`
      - 15
      - No mate
    * - :ref:`漢車包象楚`
      - 15
      - No mate
    * - :ref:`漢車包兵楚`
      - 16
      - No mate
    * - :ref:`漢車包士楚`
      - 13
      - No mate
    * - :ref:`漢車包楚車`
      - 17
      - 2
    * - :ref:`漢車包楚包`
      - 27
      - No mate
    * - :ref:`漢車包楚馬`
      - 51
      - 1
    * - :ref:`漢車包楚象`
      - 33
      - 1
    * - :ref:`漢車包楚卒`
      - 25
      - No mate
    * - :ref:`漢車包楚士`
      - 18
      - No mate
    * - :ref:`漢車馬馬楚`
      - 11
      - No mate
    * - :ref:`漢車馬象楚`
      - 11
      - No mate
    * - :ref:`漢車馬兵楚`
      - 16
      - No mate
    * - :ref:`漢車馬士楚`
      - 11
      - No mate
    * - :ref:`漢車馬楚車`
      - 17
      - 1
    * - :ref:`漢車馬楚包`
      - 13
      - No mate
    * - :ref:`漢車馬楚馬`
      - 22
      - 1
    * - :ref:`漢車馬楚象`
      - 16
      - No mate
    * - :ref:`漢車馬楚卒`
      - 15
      - No mate
    * - :ref:`漢車馬楚士`
      - 15
      - No mate
    * - :ref:`漢車象象楚`
      - 10
      - No mate
    * - :ref:`漢車象兵楚`
      - 20
      - No mate
    * - :ref:`漢車象士楚`
      - 11
      - No mate
    * - :ref:`漢車象楚車`
      - 14
      - 2
    * - :ref:`漢車象楚包`
      - 17
      - No mate
    * - :ref:`漢車象楚馬`
      - 32
      - 1
    * - :ref:`漢車象楚象`
      - 19
      - 1
    * - :ref:`漢車象楚卒`
      - 14
      - No mate
    * - :ref:`漢車象楚士`
      - 18
      - No mate
    * - :ref:`漢車兵兵楚`
      - 19
      - No mate
    * - :ref:`漢車兵士楚`
      - 15
      - No mate
    * - :ref:`漢車兵楚車`
      - 15
      - 1
    * - :ref:`漢車兵楚包`
      - 18
      - No mate
    * - :ref:`漢車兵楚馬`
      - 37
      - No mate
    * - :ref:`漢車兵楚象`
      - 22
      - No mate
    * - :ref:`漢車兵楚卒`
      - 18
      - No mate
    * - :ref:`漢車兵楚士`
      - 23
      - No mate
    * - 漢車士士楚
      - No mate
      - No mate
    * - :ref:`漢車士楚車`
      - 1
      - 1
    * - :ref:`漢車士楚包`
      - 1
      - No mate
    * - :ref:`漢車士楚馬`
      - No mate
      - 1
    * - :ref:`漢車士楚象`
      - 1
      - 1
    * - 漢車士楚卒
      - No mate
      - No mate
    * - :ref:`漢車士楚士`
      - 1
      - No mate
    * - :ref:`漢包包馬楚`
      - 37
      - No mate
    * - :ref:`漢包包象楚`
      - 45
      - No mate
    * - :ref:`漢包包兵楚`
      - 32
      - No mate
    * - :ref:`漢包包士楚`
      - 25
      - No mate
    * - :ref:`漢包包楚車`
      - No mate
      - 2
    * - 漢包包楚包
      - No mate
      - No mate
    * - :ref:`漢包包楚馬`
      - No mate
      - 1
    * - :ref:`漢包包楚象`
      - No mate
      - 1
    * - 漢包包楚卒
      - No mate
      - No mate
    * - 漢包包楚士
      - No mate
      - No mate
    * - :ref:`漢包馬馬楚`
      - 17
      - No mate
    * - :ref:`漢包馬象楚`
      - 20
      - No mate
    * - :ref:`漢包馬兵楚`
      - 31
      - No mate
    * - :ref:`漢包馬士楚`
      - 1
      - No mate
    * - :ref:`漢包馬楚車`
      - 1
      - 2
    * - :ref:`漢包馬楚包`
      - 6
      - No mate
    * - :ref:`漢包馬楚馬`
      - 2
      - 1
    * - :ref:`漢包馬楚象`
      - 2
      - 1
    * - :ref:`漢包馬楚卒`
      - 2
      - No mate
    * - :ref:`漢包馬楚士`
      - 2
      - No mate
    * - :ref:`漢包象象楚`
      - 27
      - No mate
    * - :ref:`漢包象兵楚`
      - 35
      - No mate
    * - :ref:`漢包象士楚`
      - 1
      - No mate
    * - :ref:`漢包象楚車`
      - 1
      - 3
    * - :ref:`漢包象楚包`
      - 14
      - No mate
    * - :ref:`漢包象楚馬`
      - 1
      - 1
    * - :ref:`漢包象楚象`
      - 1
      - 1
    * - 漢包象楚卒
      - No mate
      - No mate
    * - :ref:`漢包象楚士`
      - 1
      - No mate
    * - :ref:`漢包兵兵楚`
      - 25
      - No mate
    * - :ref:`漢包兵士楚`
      - 10
      - No mate
    * - :ref:`漢包兵楚車`
      - 2
      - 2
    * - :ref:`漢包兵楚包`
      - 11
      - No mate
    * - :ref:`漢包兵楚馬`
      - 4
      - No mate
    * - :ref:`漢包兵楚象`
      - 4
      - No mate
    * - :ref:`漢包兵楚卒`
      - 4
      - No mate
    * - :ref:`漢包兵楚士`
      - 3
      - No mate
    * - 漢包士士楚
      - No mate
      - No mate
    * - :ref:`漢包士楚車`
      - No mate
      - 1
    * - :ref:`漢包士楚包`
      - 1
      - No mate
    * - :ref:`漢包士楚馬`
      - No mate
      - 1
    * - :ref:`漢包士楚象`
      - 1
      - 1
    * - 漢包士楚卒
      - No mate
      - No mate
    * - 漢包士楚士
      - No mate
      - No mate
    * - :ref:`漢馬馬象楚`
      - 23
      - No mate
    * - :ref:`漢馬馬兵楚`
      - 20
      - No mate
    * - :ref:`漢馬馬士楚`
      - 1
      - No mate
    * - :ref:`漢馬馬楚車`
      - 1
      - 1
    * - :ref:`漢馬馬楚包`
      - 25
      - No mate
    * - :ref:`漢馬馬楚馬`
      - 3
      - 1
    * - :ref:`漢馬馬楚象`
      - 5
      - No mate
    * - :ref:`漢馬馬楚卒`
      - 1
      - No mate
    * - :ref:`漢馬馬楚士`
      - 2
      - No mate
    * - :ref:`漢馬象象楚`
      - 17
      - No mate
    * - :ref:`漢馬象兵楚`
      - 21
      - No mate
    * - :ref:`漢馬象士楚`
      - 1
      - No mate
    * - :ref:`漢馬象楚車`
      - 1
      - 2
    * - :ref:`漢馬象楚包`
      - 40
      - No mate
    * - :ref:`漢馬象楚馬`
      - 1
      - 1
    * - :ref:`漢馬象楚象`
      - 3
      - 1
    * - :ref:`漢馬象楚卒`
      - 1
      - No mate
    * - :ref:`漢馬象楚士`
      - 1
      - No mate
    * - :ref:`漢馬兵兵楚`
      - 24
      - No mate
    * - :ref:`漢馬兵士楚`
      - 19
      - No mate
    * - :ref:`漢馬兵楚車`
      - 19
      - No mate
    * - :ref:`漢馬兵楚包`
      - 27
      - No mate
    * - :ref:`漢馬兵楚馬`
      - 27
      - No mate
    * - :ref:`漢馬兵楚象`
      - 34
      - No mate
    * - :ref:`漢馬兵楚卒`
      - 29
      - No mate
    * - :ref:`漢馬兵楚士`
      - 16
      - No mate
    * - 漢馬士士楚
      - No mate
      - No mate
    * - :ref:`漢馬士楚車`
      - No mate
      - 1
    * - 漢馬士楚包
      - No mate
      - No mate
    * - :ref:`漢馬士楚馬`
      - No mate
      - 1
    * - :ref:`漢馬士楚象`
      - No mate
      - 1
    * - 漢馬士楚卒
      - No mate
      - No mate
    * - 漢馬士楚士
      - No mate
      - No mate
    * - :ref:`漢象象兵楚`
      - 22
      - No mate
    * - :ref:`漢象象士楚`
      - 1
      - No mate
    * - :ref:`漢象象楚車`
      - 1
      - 3
    * - :ref:`漢象象楚包`
      - 29
      - No mate
    * - :ref:`漢象象楚馬`
      - 1
      - 1
    * - :ref:`漢象象楚象`
      - 1
      - 1
    * - :ref:`漢象象楚卒`
      - 1
      - No mate
    * - :ref:`漢象象楚士`
      - 1
      - No mate
    * - :ref:`漢象兵兵楚`
      - 23
      - No mate
    * - :ref:`漢象兵士楚`
      - 21
      - No mate
    * - :ref:`漢象兵楚車`
      - 21
      - 2
    * - :ref:`漢象兵楚包`
      - 38
      - No mate
    * - :ref:`漢象兵楚馬`
      - 21
      - No mate
    * - :ref:`漢象兵楚象`
      - 21
      - No mate
    * - :ref:`漢象兵楚卒`
      - 52
      - No mate
    * - :ref:`漢象兵楚士`
      - 18
      - No mate
    * - 漢象士士楚
      - No mate
      - No mate
    * - :ref:`漢象士楚車`
      - No mate
      - 1
    * - 漢象士楚包
      - No mate
      - No mate
    * - :ref:`漢象士楚馬`
      - No mate
      - 1
    * - :ref:`漢象士楚象`
      - No mate
      - 1
    * - 漢象士楚卒
      - No mate
      - No mate
    * - 漢象士楚士
      - No mate
      - No mate
    * - :ref:`漢兵兵士楚`
      - 24
      - No mate
    * - :ref:`漢兵兵楚車`
      - 22
      - No mate
    * - :ref:`漢兵兵楚包`
      - 25
      - No mate
    * - :ref:`漢兵兵楚馬`
      - 22
      - No mate
    * - :ref:`漢兵兵楚象`
      - 22
      - No mate
    * - :ref:`漢兵兵楚卒`
      - 28
      - No mate
    * - :ref:`漢兵兵楚士`
      - 11
      - No mate
    * - 漢兵士士楚
      - No mate
      - No mate
    * - :ref:`漢兵士楚車`
      - No mate
      - 1
    * - 漢兵士楚包
      - No mate
      - No mate
    * - 漢兵士楚馬
      - No mate
      - No mate
    * - 漢兵士楚象
      - No mate
      - No mate
    * - 漢兵士楚卒
      - No mate
      - No mate
    * - 漢兵士楚士
      - No mate
      - No mate
    * - :ref:`漢士士楚車`
      - No mate
      - 1
    * - 漢士士楚包
      - No mate
      - No mate
    * - :ref:`漢士士楚馬`
      - No mate
      - 1
    * - :ref:`漢士士楚象`
      - No mate
      - 1
    * - 漢士士楚卒
      - No mate
      - No mate
    * - 漢士士楚士
      - No mate
      - No mate
    * - :ref:`漢車車包士楚`
      - 9
      - No mate
    * - :ref:`漢車車包楚士`
      - 20
      - No mate
    * - :ref:`漢車車馬士楚`
      - 8
      - No mate
    * - :ref:`漢車車馬楚士`
      - 16
      - No mate
    * - :ref:`漢車車象士楚`
      - 10
      - No mate
    * - :ref:`漢車車象楚士`
      - 18
      - No mate
    * - :ref:`漢車車兵兵楚`
      - 8
      - No mate
    * - :ref:`漢車車兵士楚`
      - 9
      - No mate
    * - :ref:`漢車車兵楚卒`
      - 17
      - No mate
    * - :ref:`漢車車兵楚士`
      - 22
      - No mate
    * - :ref:`漢車車士士楚`
      - 8
      - No mate
    * - :ref:`漢車車士楚車`
      - 20
      - 2
    * - :ref:`漢車車士楚包`
      - 13
      - 1
    * - :ref:`漢車車士楚馬`
      - 14
      - 1
    * - :ref:`漢車車士楚象`
      - 11
      - 1
    * - :ref:`漢車車士楚卒`
      - 11
      - 1
    * - :ref:`漢車車士楚士`
      - 10
      - No mate
    * - :ref:`漢車車楚車士`
      - 22
      - 1
    * - :ref:`漢車車楚包士`
      - 13
      - 1
    * - :ref:`漢車車楚馬士`
      - 15
      - No mate
    * - :ref:`漢車車楚象士`
      - 13
      - No mate
    * - :ref:`漢車車楚卒卒`
      - 13
      - 17
    * - :ref:`漢車車楚卒士`
      - 11
      - No mate
    * - :ref:`漢車車楚士士`
      - 12
      - No mate
    * - :ref:`漢車包包士楚`
      - 14
      - No mate
    * - :ref:`漢車包包楚士`
      - 26
      - No mate
    * - :ref:`漢車包馬士楚`
      - 14
      - No mate
    * - :ref:`漢車包馬楚士`
      - 20
      - No mate
    * - :ref:`漢車包象士楚`
      - 14
      - No mate
    * - :ref:`漢車包象楚士`
      - 21
      - No mate
    * - :ref:`漢車包兵兵楚`
      - 23
      - No mate
    * - :ref:`漢車包兵士楚`
      - 15
      - No mate
    * - :ref:`漢車包兵楚卒`
      - 26
      - No mate
    * - :ref:`漢車包兵楚士`
      - 27
      - No mate
    * - :ref:`漢車包士士楚`
      - 13
      - No mate
    * - :ref:`漢車包士楚車`
      - 19
      - 2
    * - :ref:`漢車包士楚包`
      - 27
      - 1
    * - :ref:`漢車包士楚馬`
      - 51
      - 1
    * - :ref:`漢車包士楚象`
      - 36
      - 1
    * - :ref:`漢車包士楚卒`
      - 25
      - 1
    * - :ref:`漢車包士楚士`
      - 28
      - No mate
    * - :ref:`漢車包楚車士`
      - 18
      - 2
    * - :ref:`漢車包楚包士`
      - 31
      - 2
    * - :ref:`漢車包楚馬士`
      - 51
      - 1
    * - :ref:`漢車包楚象士`
      - 33
      - 1
    * - :ref:`漢車包楚卒卒`
      - 43
      - 25
    * - :ref:`漢車包楚卒士`
      - 25
      - No mate
    * - :ref:`漢車包楚士士`
      - 17
      - No mate
    * - :ref:`漢車馬馬士楚`
      - 11
      - No mate
    * - :ref:`漢車馬馬楚士`
      - 16
      - No mate
    * - :ref:`漢車馬象士楚`
      - 11
      - No mate
    * - :ref:`漢車馬象楚士`
      - 20
      - No mate
    * - :ref:`漢車馬兵兵楚`
      - 19
      - No mate
    * - :ref:`漢車馬兵士楚`
      - 16
      - No mate
    * - :ref:`漢車馬兵楚卒`
      - 30
      - No mate
    * - :ref:`漢車馬兵楚士`
      - 23
      - No mate
    * - :ref:`漢車馬士士楚`
      - 11
      - No mate
    * - :ref:`漢車馬士楚車`
      - 22
      - 2
    * - :ref:`漢車馬士楚包`
      - 16
      - 1
    * - :ref:`漢車馬士楚馬`
      - 22
      - 1
    * - :ref:`漢車馬士楚象`
      - 16
      - 1
    * - :ref:`漢車馬士楚卒`
      - 15
      - 1
    * - :ref:`漢車馬士楚士`
      - 14
      - No mate
    * - :ref:`漢車馬楚車士`
      - 18
      - 1
    * - :ref:`漢車馬楚包士`
      - 27
      - 1
    * - :ref:`漢車馬楚馬士`
      - 39
      - 1
    * - :ref:`漢車馬楚象士`
      - 25
      - No mate
    * - :ref:`漢車馬楚卒卒`
      - 23
      - 19
    * - :ref:`漢車馬楚卒士`
      - 21
      - No mate
    * - :ref:`漢車馬楚士士`
      - 42
      - No mate
    * - :ref:`漢車象象士楚`
      - 12
      - No mate
    * - :ref:`漢車象象楚士`
      - 19
      - No mate
    * - :ref:`漢車象兵兵楚`
      - 20
      - No mate
    * - :ref:`漢車象兵士楚`
      - 19
      - No mate
    * - :ref:`漢車象兵楚卒`
      - 52
      - No mate
    * - :ref:`漢車象兵楚士`
      - 23
      - No mate
    * - :ref:`漢車象士士楚`
      - 11
      - No mate
    * - :ref:`漢車象士楚車`
      - 18
      - 2
    * - :ref:`漢車象士楚包`
      - 16
      - 1
    * - :ref:`漢車象士楚馬`
      - 32
      - 1
    * - :ref:`漢車象士楚象`
      - 19
      - 1
    * - :ref:`漢車象士楚卒`
      - 15
      - 1
    * - :ref:`漢車象士楚士`
      - 18
      - No mate
    * - :ref:`漢車象楚車士`
      - 26
      - 2
    * - :ref:`漢車象楚包士`
      - 57
      - 2
    * - :ref:`漢車象楚馬士`
      - 109
      - 1
    * - :ref:`漢車象楚象士`
      - 34
      - 1
    * - :ref:`漢車象楚卒卒`
      - 24
      - 20
    * - :ref:`漢車象楚卒士`
      - 24
      - No mate
    * - :ref:`漢車象楚士士`
      - 28
      - No mate
    * - :ref:`漢車兵兵士楚`
      - 15
      - No mate
    * - :ref:`漢車兵兵楚車`
      - 35
      - 1
    * - :ref:`漢車兵兵楚包`
      - 25
      - No mate
    * - :ref:`漢車兵兵楚馬`
      - 41
      - No mate
    * - :ref:`漢車兵兵楚象`
      - 24
      - No mate
    * - :ref:`漢車兵兵楚卒`
      - 26
      - No mate
    * - :ref:`漢車兵兵楚士`
      - 25
      - No mate
    * - :ref:`漢車兵士士楚`
      - 15
      - No mate
    * - :ref:`漢車兵士楚車`
      - 28
      - 2
    * - :ref:`漢車兵士楚包`
      - 23
      - No mate
    * - :ref:`漢車兵士楚馬`
      - 39
      - 1
    * - :ref:`漢車兵士楚象`
      - 22
      - 1
    * - :ref:`漢車兵士楚卒`
      - 19
      - No mate
    * - :ref:`漢車兵士楚士`
      - 23
      - No mate
    * - :ref:`漢車兵楚車士`
      - 24
      - 1
    * - :ref:`漢車兵楚包士`
      - 27
      - No mate
    * - :ref:`漢車兵楚馬士`
      - 37
      - No mate
    * - :ref:`漢車兵楚象士`
      - 32
      - No mate
    * - :ref:`漢車兵楚卒卒`
      - 25
      - 28
    * - :ref:`漢車兵楚卒士`
      - 30
      - No mate
    * - :ref:`漢車兵楚士士`
      - 27
      - No mate
    * - :ref:`漢車士士楚車`
      - 1
      - 2
    * - :ref:`漢車士士楚包`
      - 1
      - 1
    * - :ref:`漢車士士楚馬`
      - No mate
      - 1
    * - :ref:`漢車士士楚象`
      - 1
      - 1
    * - :ref:`漢車士士楚卒`
      - No mate
      - 1
    * - :ref:`漢車士士楚士`
      - 1
      - No mate
    * - :ref:`漢車士楚車士`
      - 1
      - 1
    * - :ref:`漢車士楚包包`
      - 3
      - No mate
    * - :ref:`漢車士楚包馬`
      - 2
      - 3
    * - :ref:`漢車士楚包象`
      - 4
      - 1
    * - :ref:`漢車士楚包卒`
      - 2
      - 3
    * - :ref:`漢車士楚包士`
      - 1
      - 1
    * - :ref:`漢車士楚馬馬`
      - 1
      - 5
    * - :ref:`漢車士楚馬象`
      - 2
      - 6
    * - :ref:`漢車士楚馬卒`
      - No mate
      - 16
    * - :ref:`漢車士楚馬士`
      - 1
      - 1
    * - :ref:`漢車士楚象象`
      - 3
      - 2
    * - :ref:`漢車士楚象卒`
      - 2
      - 17
    * - :ref:`漢車士楚象士`
      - 1
      - 1
    * - :ref:`漢車士楚卒卒`
      - No mate
      - 8
    * - :ref:`漢車士楚卒士`
      - 1
      - No mate
    * - :ref:`漢車士楚士士`
      - 1
      - No mate
    * - :ref:`漢包包馬士楚`
      - 28
      - No mate
    * - :ref:`漢包包馬楚士`
      - 62
      - No mate
    * - :ref:`漢包包象士楚`
      - 27
      - No mate
    * - :ref:`漢包包象楚士`
      - 38
      - No mate
    * - :ref:`漢包包兵兵楚`
      - 54
      - No mate
    * - :ref:`漢包包兵士楚`
      - 35
      - No mate
    * - :ref:`漢包包兵楚卒`
      - 69
      - No mate
    * - :ref:`漢包包兵楚士`
      - 42
      - No mate
    * - :ref:`漢包包士士楚`
      - 18
      - No mate
    * - :ref:`漢包包士楚車`
      - 25
      - 4
    * - :ref:`漢包包士楚包`
      - 26
      - 1
    * - :ref:`漢包包士楚馬`
      - 25
      - 1
    * - :ref:`漢包包士楚象`
      - 26
      - 1
    * - :ref:`漢包包士楚卒`
      - 25
      - 1
    * - :ref:`漢包包士楚士`
      - 22
      - No mate
    * - :ref:`漢包包楚包卒`
      - No mate
      - 17
    * - :ref:`漢包包楚包士`
      - No mate
      - 2
    * - :ref:`漢包包楚馬馬`
      - 1
      - 66
    * - :ref:`漢包包楚馬士`
      - 1
      - 1
    * - :ref:`漢包包楚象士`
      - 1
      - 1
    * - :ref:`漢包包楚卒卒`
      - No mate
      - 35
    * - 漢包包楚卒士
      - No mate
      - No mate
    * - 漢包包楚士士
      - No mate
      - No mate
    * - :ref:`漢包馬馬士楚`
      - 17
      - No mate
    * - :ref:`漢包馬馬楚士`
      - 30
      - No mate
    * - :ref:`漢包馬象士楚`
      - 17
      - No mate
    * - :ref:`漢包馬象楚士`
      - 34
      - No mate
    * - :ref:`漢包馬兵兵楚`
      - 30
      - No mate
    * - :ref:`漢包馬兵士楚`
      - 25
      - No mate
    * - :ref:`漢包馬兵楚卒`
      - 48
      - No mate
    * - :ref:`漢包馬兵楚士`
      - 44
      - No mate
    * - :ref:`漢包馬士士楚`
      - 1
      - No mate
    * - :ref:`漢包馬士楚車`
      - 3
      - 3
    * - :ref:`漢包馬士楚包`
      - 46
      - 1
    * - :ref:`漢包馬士楚馬`
      - 2
      - 1
    * - :ref:`漢包馬士楚象`
      - 4
      - 1
    * - :ref:`漢包馬士楚卒`
      - 3
      - 1
    * - :ref:`漢包馬士楚士`
      - 3
      - No mate
    * - :ref:`漢包馬楚包卒`
      - 9
      - 11
    * - :ref:`漢包馬楚包士`
      - 5
      - 2
    * - :ref:`漢包馬楚馬士`
      - 5
      - 1
    * - :ref:`漢包馬楚象士`
      - 5
      - 1
    * - :ref:`漢包馬楚卒卒`
      - 2
      - 25
    * - :ref:`漢包馬楚卒士`
      - 5
      - No mate
    * - :ref:`漢包馬楚士士`
      - 3
      - No mate
    * - :ref:`漢包象象士楚`
      - 18
      - No mate
    * - :ref:`漢包象象楚士`
      - 49
      - No mate
    * - :ref:`漢包象兵兵楚`
      - 41
      - No mate
    * - :ref:`漢包象兵士楚`
      - 27
      - No mate
    * - :ref:`漢包象兵楚士`
      - 35
      - No mate
    * - :ref:`漢包象士士楚`
      - 1
      - No mate
    * - :ref:`漢包象士楚車`
      - 2
      - 4
    * - :ref:`漢包象士楚包`
      - 21
      - 1
    * - :ref:`漢包象士楚馬`
      - 2
      - 1
    * - :ref:`漢包象士楚象`
      - 2
      - 1
    * - :ref:`漢包象士楚卒`
      - 2
      - 1
    * - :ref:`漢包象士楚士`
      - 2
      - No mate
    * - :ref:`漢包象楚包士`
      - 12
      - 2
    * - :ref:`漢包象楚馬士`
      - 2
      - 1
    * - :ref:`漢包象楚象士`
      - 5
      - 1
    * - :ref:`漢包象楚卒卒`
      - No mate
      - 25
    * - :ref:`漢包象楚卒士`
      - 1
      - No mate
    * - :ref:`漢包象楚士士`
      - 1
      - No mate
    * - :ref:`漢包兵兵士楚`
      - 32
      - No mate
    * - :ref:`漢包兵兵楚車`
      - 25
      - 3
    * - :ref:`漢包兵兵楚包`
      - 49
      - No mate
    * - :ref:`漢包兵兵楚馬`
      - 42
      - No mate
    * - :ref:`漢包兵兵楚象`
      - 70
      - No mate
    * - :ref:`漢包兵兵楚卒`
      - 44
      - No mate
    * - :ref:`漢包兵兵楚士`
      - 40
      - No mate
    * - :ref:`漢包兵士士楚`
      - 8
      - No mate
    * - :ref:`漢包兵士楚車`
      - 10
      - 2
    * - :ref:`漢包兵士楚包`
      - 14
      - No mate
    * - :ref:`漢包兵士楚馬`
      - 12
      - 1
    * - :ref:`漢包兵士楚象`
      - 12
      - 1
    * - :ref:`漢包兵士楚卒`
      - 15
      - No mate
    * - :ref:`漢包兵士楚士`
      - 10
      - No mate
    * - :ref:`漢包兵楚包卒`
      - 11
      - 11
    * - :ref:`漢包兵楚包士`
      - 7
      - 2
    * - :ref:`漢包兵楚馬卒`
      - 5
      - 67
    * - :ref:`漢包兵楚馬士`
      - 5
      - No mate
    * - :ref:`漢包兵楚象卒`
      - 6
      - 82
    * - :ref:`漢包兵楚象士`
      - 5
      - No mate
    * - :ref:`漢包兵楚卒卒`
      - 6
      - 38
    * - :ref:`漢包兵楚卒士`
      - 5
      - No mate
    * - :ref:`漢包兵楚士士`
      - 3
      - No mate
    * - :ref:`漢包士士楚車`
      - No mate
      - 2
    * - :ref:`漢包士士楚包`
      - 1
      - 1
    * - :ref:`漢包士士楚馬`
      - No mate
      - 1
    * - :ref:`漢包士士楚象`
      - 1
      - 1
    * - :ref:`漢包士士楚卒`
      - No mate
      - 1
    * - 漢包士士楚士
      - No mate
      - No mate
    * - :ref:`漢包士楚包士`
      - 1
      - 1
    * - :ref:`漢包士楚馬馬`
      - No mate
      - 25
    * - :ref:`漢包士楚馬象`
      - 2
      - 37
    * - :ref:`漢包士楚馬卒`
      - No mate
      - 27
    * - :ref:`漢包士楚馬士`
      - 1
      - 1
    * - :ref:`漢包士楚象象`
      - 2
      - 27
    * - :ref:`漢包士楚象卒`
      - 2
      - 37
    * - :ref:`漢包士楚象士`
      - 1
      - 1
    * - :ref:`漢包士楚卒卒`
      - No mate
      - 21
    * - 漢包士楚卒士
      - No mate
      - No mate
    * - :ref:`漢包士楚士士`
      - 1
      - No mate
    * - :ref:`漢馬馬象士楚`
      - 18
      - No mate
    * - :ref:`漢馬馬象楚車`
      - 24
      - 3
    * - :ref:`漢馬馬象楚包`
      - 40
      - 1
    * - :ref:`漢馬馬象楚馬`
      - 77
      - 1
    * - :ref:`漢馬馬象楚象`
      - 52
      - 1
    * - :ref:`漢馬馬象楚卒`
      - 29
      - 1
    * - :ref:`漢馬馬象楚士`
      - 24
      - No mate
    * - :ref:`漢馬馬兵兵楚`
      - 23
      - No mate
    * - :ref:`漢馬馬兵士楚`
      - 19
      - No mate
    * - :ref:`漢馬馬兵楚車`
      - 21
      - 2
    * - :ref:`漢馬馬兵楚包`
      - 33
      - No mate
    * - :ref:`漢馬馬兵楚馬`
      - 125
      - 1
    * - :ref:`漢馬馬兵楚象`
      - 36
      - No mate
    * - :ref:`漢馬馬兵楚卒`
      - 32
      - No mate
    * - :ref:`漢馬馬兵楚士`
      - 47
      - No mate
    * - :ref:`漢馬馬士士楚`
      - 1
      - No mate
    * - :ref:`漢馬馬士楚車`
      - 1
      - 2
    * - :ref:`漢馬馬士楚包`
      - 26
      - 1
    * - :ref:`漢馬馬士楚馬`
      - 3
      - 1
    * - :ref:`漢馬馬士楚象`
      - 7
      - 1
    * - :ref:`漢馬馬士楚卒`
      - 1
      - 1
    * - :ref:`漢馬馬士楚士`
      - 2
      - No mate
    * - :ref:`漢馬馬楚馬士`
      - 7
      - 1
    * - :ref:`漢馬馬楚象士`
      - 7
      - No mate
    * - :ref:`漢馬馬楚卒卒`
      - 1
      - 19
    * - :ref:`漢馬馬楚卒士`
      - 2
      - No mate
    * - :ref:`漢馬馬楚士士`
      - 5
      - No mate
    * - :ref:`漢馬象象士楚`
      - 17
      - No mate
    * - :ref:`漢馬象象楚士`
      - 29
      - No mate
    * - :ref:`漢馬象兵兵楚`
      - 24
      - No mate
    * - :ref:`漢馬象兵士楚`
      - 21
      - No mate
    * - :ref:`漢馬象兵楚卒`
      - 54
      - No mate
    * - :ref:`漢馬象兵楚士`
      - 36
      - No mate
    * - :ref:`漢馬象士士楚`
      - 1
      - No mate
    * - :ref:`漢馬象士楚車`
      - 1
      - 2
    * - :ref:`漢馬象士楚包`
      - 39
      - 1
    * - :ref:`漢馬象士楚馬`
      - 1
      - 1
    * - :ref:`漢馬象士楚象`
      - 5
      - 1
    * - :ref:`漢馬象士楚卒`
      - 1
      - 1
    * - :ref:`漢馬象士楚士`
      - 1
      - No mate
    * - :ref:`漢馬象楚馬象`
      - 5
      - 5
    * - :ref:`漢馬象楚馬士`
      - 4
      - 1
    * - :ref:`漢馬象楚象象`
      - 4
      - 3
    * - :ref:`漢馬象楚象士`
      - 5
      - 1
    * - :ref:`漢馬象楚卒卒`
      - 1
      - 20
    * - :ref:`漢馬象楚卒士`
      - 1
      - No mate
    * - :ref:`漢馬象楚士士`
      - 5
      - No mate
    * - :ref:`漢馬兵兵士楚`
      - 25
      - No mate
    * - :ref:`漢馬兵兵楚車`
      - 25
      - No mate
    * - :ref:`漢馬兵兵楚包`
      - 29
      - No mate
    * - :ref:`漢馬兵兵楚馬`
      - 109
      - No mate
    * - :ref:`漢馬兵兵楚象`
      - 46
      - No mate
    * - :ref:`漢馬兵兵楚卒`
      - 38
      - No mate
    * - :ref:`漢馬兵兵楚士`
      - 28
      - No mate
    * - :ref:`漢馬兵士士楚`
      - 19
      - No mate
    * - :ref:`漢馬兵士楚車`
      - 20
      - 2
    * - :ref:`漢馬兵士楚包`
      - 29
      - No mate
    * - :ref:`漢馬兵士楚馬`
      - 26
      - 1
    * - :ref:`漢馬兵士楚象`
      - 40
      - 1
    * - :ref:`漢馬兵士楚卒`
      - 26
      - No mate
    * - :ref:`漢馬兵士楚士`
      - 16
      - No mate
    * - :ref:`漢馬兵楚馬士`
      - 27
      - No mate
    * - :ref:`漢馬兵楚象卒`
      - 40
      - 52
    * - :ref:`漢馬兵楚象士`
      - 31
      - No mate
    * - :ref:`漢馬兵楚卒卒`
      - 56
      - 28
    * - :ref:`漢馬兵楚卒士`
      - 29
      - No mate
    * - :ref:`漢馬兵楚士士`
      - 9
      - No mate
    * - :ref:`漢馬士士楚車`
      - No mate
      - 2
    * - :ref:`漢馬士士楚包`
      - No mate
      - 1
    * - :ref:`漢馬士士楚馬`
      - No mate
      - 1
    * - :ref:`漢馬士士楚象`
      - No mate
      - 1
    * - :ref:`漢馬士士楚卒`
      - No mate
      - 1
    * - 漢馬士士楚士
      - No mate
      - No mate
    * - :ref:`漢馬士楚馬士`
      - 1
      - 1
    * - :ref:`漢馬士楚象象`
      - 1
      - 3
    * - :ref:`漢馬士楚象卒`
      - No mate
      - 14
    * - :ref:`漢馬士楚象士`
      - 1
      - 1
    * - :ref:`漢馬士楚卒卒`
      - No mate
      - 12
    * - 漢馬士楚卒士
      - No mate
      - No mate
    * - :ref:`漢馬士楚士士`
      - 1
      - No mate
    * - :ref:`漢象象兵兵楚`
      - 22
      - No mate
    * - :ref:`漢象象兵士楚`
      - 22
      - No mate
    * - :ref:`漢象象兵楚卒`
      - 80
      - No mate
    * - :ref:`漢象象兵楚士`
      - 34
      - No mate
    * - :ref:`漢象象士士楚`
      - 1
      - No mate
    * - :ref:`漢象象士楚車`
      - 1
      - 4
    * - :ref:`漢象象士楚包`
      - 32
      - 1
    * - :ref:`漢象象士楚馬`
      - 1
      - 1
    * - :ref:`漢象象士楚象`
      - 1
      - 1
    * - :ref:`漢象象士楚卒`
      - 1
      - 1
    * - :ref:`漢象象士楚士`
      - 1
      - No mate
    * - :ref:`漢象象楚象士`
      - 3
      - 1
    * - :ref:`漢象象楚卒卒`
      - 1
      - 20
    * - :ref:`漢象象楚卒士`
      - 1
      - No mate
    * - :ref:`漢象象楚士士`
      - 3
      - No mate
    * - :ref:`漢象兵兵士楚`
      - 24
      - No mate
    * - :ref:`漢象兵兵楚車`
      - 23
      - 3
    * - :ref:`漢象兵兵楚包`
      - 47
      - No mate
    * - :ref:`漢象兵兵楚馬`
      - 105
      - No mate
    * - :ref:`漢象兵兵楚象`
      - 75
      - No mate
    * - :ref:`漢象兵兵楚卒`
      - 57
      - No mate
    * - :ref:`漢象兵兵楚士`
      - 33
      - No mate
    * - :ref:`漢象兵士士楚`
      - 21
      - No mate
    * - :ref:`漢象兵士楚車`
      - 22
      - 2
    * - :ref:`漢象兵士楚包`
      - 42
      - No mate
    * - :ref:`漢象兵士楚馬`
      - 28
      - 1
    * - :ref:`漢象兵士楚象`
      - 34
      - 1
    * - :ref:`漢象兵士楚卒`
      - 50
      - No mate
    * - :ref:`漢象兵士楚士`
      - 18
      - No mate
    * - :ref:`漢象兵楚象士`
      - 17
      - No mate
    * - :ref:`漢象兵楚卒卒`
      - 65
      - 28
    * - :ref:`漢象兵楚卒士`
      - 50
      - No mate
    * - :ref:`漢象兵楚士士`
      - 9
      - No mate
    * - :ref:`漢象士士楚車`
      - No mate
      - 2
    * - :ref:`漢象士士楚包`
      - No mate
      - 1
    * - :ref:`漢象士士楚馬`
      - No mate
      - 1
    * - :ref:`漢象士士楚象`
      - No mate
      - 1
    * - :ref:`漢象士士楚卒`
      - No mate
      - 1
    * - 漢象士士楚士
      - No mate
      - No mate
    * - :ref:`漢象士楚象士`
      - 1
      - 1
    * - :ref:`漢象士楚卒卒`
      - No mate
      - 13
    * - 漢象士楚卒士
      - No mate
      - No mate
    * - :ref:`漢象士楚士士`
      - 1
      - No mate
    * - :ref:`漢兵兵士士楚`
      - 23
      - No mate
    * - :ref:`漢兵兵士楚車`
      - 24
      - 1
    * - :ref:`漢兵兵士楚包`
      - 30
      - No mate
    * - :ref:`漢兵兵士楚馬`
      - 24
      - No mate
    * - :ref:`漢兵兵士楚象`
      - 24
      - No mate
    * - :ref:`漢兵兵士楚卒`
      - 33
      - No mate
    * - :ref:`漢兵兵士楚士`
      - 11
      - No mate
    * - :ref:`漢兵兵楚卒卒`
      - 29
      - 29
    * - :ref:`漢兵兵楚卒士`
      - 14
      - No mate
    * - :ref:`漢兵兵楚士士`
      - 4
      - No mate
    * - :ref:`漢兵士士楚車`
      - No mate
      - 2
    * - 漢兵士士楚包
      - No mate
      - No mate
    * - :ref:`漢兵士士楚馬`
      - No mate
      - 1
    * - :ref:`漢兵士士楚象`
      - No mate
      - 1
    * - 漢兵士士楚卒
      - No mate
      - No mate
    * - 漢兵士士楚士
      - No mate
      - No mate
    * - 漢兵士楚卒士
      - No mate
      - No mate
    * - 漢兵士楚士士
      - No mate
      - No mate
    * - 漢士士楚士士
      - No mate
      - No mate


.. _漢車車楚:

漢車車楚
~~~~~~~~

.. janggi-board::

    HK48,HR9,HR19,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HK40-50
    8. CK31-41 big
    9. HR19-49 check
    10. CK41-51
    11. HR9-8
    12. HR8-58 mate

.. _漢車包楚:

漢車包楚
~~~~~~~~

.. janggi-board::

    HK48,HR10,HC32,CK31

.. janggi-moves::

    1. HR10-40
    2. CK31-41 big
    3. HK48-38
    4. CK41-31
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-42
    9. HR40-37
    10. CK42-51 big
    11. HK60-50
    12. CK51-41 big
    13. HK50-40
    14. CK41-42
    15. HC32-38
    16. HR37-47 check
    17. CK42-31
    18. HK40-49
    19. HK49-58
    20. HC38-68
    21. HC68-48
    22. HK58-49
    23. HC48-50
    24. HR47-37 mate

.. _漢車馬楚:

漢車馬楚
~~~~~~~~

.. janggi-board::

    HK48,HR2,HH20,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HH20-39
    8. HK40-50
    9. CK31-41 big
    10. HH39-47
    11. HR2-32
    12. HK50-40
    13. HH47-66
    14. HH66-54
    15. HR32-42 mate

.. _漢車象楚:

漢車象楚
~~~~~~~~

.. janggi-board::

    HK48,HR30,HE10,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. HK49-60
    5. HR30-40 check
    6. CK31-42
    7. HR40-58
    8. HE10-38
    9. HE38-66
    10. HE66-83
    11. HR58-51 check
    12. CK42-32
    13. HE83-55 mate

.. _漢車兵楚:

漢車兵楚
~~~~~~~~

.. janggi-board::

    HK48,HR40,HP7,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49 check
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. CK51-41 big
    9. HR40-49 check
    10. CK41-31
    11. HR49-43
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-36
    16. HP36-35
    17. HP35-34
    18. HP34-33
    19. HR43-42 mate

.. _漢車楚車:

漢車楚車
~~~~~~~~

.. janggi-board::

    HK38,HR51,CK31,CR41

.. janggi-moves::

    1. HR51-33 mate

.. _漢車楚包:

漢車楚包
~~~~~~~~

.. janggi-board::

    HK38,HR51,CK31,CC41

.. janggi-moves::

    1. HR51-33 mate

.. _漢車楚象:

漢車楚象
~~~~~~~~

.. janggi-board::

    HK38,HR51,CK31,CE41

.. janggi-moves::

    1. HR51-33 mate

.. _漢車楚士:

漢車楚士
~~~~~~~~

.. janggi-board::

    HK38,HR52,CK31,CO32

.. janggi-moves::

    1. HR52-51 mate

.. _漢包馬楚:

漢包馬楚
~~~~~~~~

.. janggi-board::

    HK48,HC2,HH31,CK32

.. janggi-moves::

    1. HH31-12 mate

.. _漢包兵楚:

漢包兵楚
~~~~~~~~

.. janggi-board::

    HK48,HC49,HP23,CK31

.. janggi-moves::

    1. HP23-33
    2. HP33-42 mate

.. _漢馬馬楚:

漢馬馬楚
~~~~~~~~

.. janggi-board::

    HK48,HH11,HH53,CK31

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬象楚:

漢馬象楚
~~~~~~~~

.. janggi-board::

    HK48,HH53,HE42,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢馬兵楚:

漢馬兵楚
~~~~~~~~

.. janggi-board::

    HK48,HH80,HP7,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HH80-59
    7. HH59-67
    8. HH67-75
    9. HH75-54
    10. HP7-17
    11. HP17-27
    12. HP27-37
    13. HP37-36
    14. HP36-35
    15. HP35-34
    16. CK41-51
    17. HP34-33
    18. CK51-52
    19. HK60-50
    20. HH54-73 check
    21. CK52-51
    22. HH73-61
    23. HP33-42 mate

.. _漢象象楚:

漢象象楚
~~~~~~~~

.. janggi-board::

    HK48,HE42,HE64,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢象兵楚:

漢象兵楚
~~~~~~~~

.. janggi-board::

    HK48,HE90,HP7,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HE90-58
    8. HP7-17
    9. HP17-27
    10. HK60-50
    11. HK50-40
    12. HE58-86
    13. HE86-54
    14. HE54-82
    15. HE82-65
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. HP34-33
    21. CK51-52
    22. HE65-42
    23. HE42-25
    24. HP33-42 mate

.. _漢兵兵楚:

漢兵兵楚
~~~~~~~~

.. janggi-board::

    HK38,HP7,HP77,CK41

.. janggi-moves::

    1. HP77-67
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HP7-17
    7. HP17-27
    8. HP27-37
    9. CK41-51 big
    10. HK60-50
    11. HP37-47
    12. HP47-57
    13. HK50-60
    14. HP57-56
    15. HP56-55
    16. HP55-54
    17. HP54-53
    18. HP67-57
    19. HP53-43
    20. HP57-56
    21. HP56-55
    22. HP55-54
    23. HP54-53
    24. HP43-42 mate

.. _漢車車包楚:

漢車車包楚
~~~~~~~~~~

.. janggi-board::

    HK50,HR3,HR10,HC32,CK42

.. janggi-moves::

    1. HK50-60
    2. CK42-52 big
    3. HK60-49
    4. CK52-42 big
    5. HR3-43 check
    6. CK42-43 big
    7. HK49-39
    8. CK43-33 big
    9. HC32-34
    10. HR10-50
    11. HR50-41
    12. CK33-32
    13. HK39-49
    14. HR41-51
    15. HC34-31
    16. HC31-61
    17. HC61-41
    18. HC41-50
    19. HR51-42 mate

.. _漢車車馬楚:

漢車車馬楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HR19,HH1,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HK40-50
    8. CK31-41 big
    9. HR19-49 check
    10. CK41-51
    11. HR9-8
    12. HR8-58 mate

.. _漢車車象楚:

漢車車象楚
~~~~~~~~~~

.. janggi-board::

    HK38,HR72,HR10,HE82,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HR72-52 check
    6. CK51-52 big
    7. HE82-54
    8. HR10-40
    9. HK60-50
    10. HR40-33
    11. HE54-86
    12. HE86-63
    13. HE63-35 mate

.. _漢車車兵楚:

漢車車兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HR19,HP1,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HK40-50
    8. CK31-41 big
    9. HR19-49 check
    10. CK41-51
    11. HR9-8
    12. HR8-58 mate

.. _漢車車士楚:

漢車車士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HR3,HO40,CK42

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-52 big
    7. HK60-50
    8. CK52-42 big
    9. HO40-49
    10. HR1-11
    11. HR11-12 check
    12. CK42-31
    13. HR3-1 mate

.. _漢車車楚車:

漢車車楚車
~~~~~~~~~~

.. janggi-board::

    HK38,HR78,HR9,CK51,CR64

.. janggi-moves::

    1. HR9-59 check
    2. CK51-42
    3. HR78-48 check
    4. CK42-32 big
    5. HK38-49
    6. CR64-62
    7. HR48-78
    8. CK32-42 big
    9. HK49-40
    10. CR62-63
    11. HR59-49 check
    12. CK42-32 big
    13. HR49-38 check
    14. CK32-42
    15. HR78-79
    16. HK40-39
    17. HR38-37
    18. CR63-53
    19. HR79-49 check
    20. CK42-51
    21. HK39-40
    22. HR37-38
    23. HR49-29
    24. CK51-42
    25. HR29-22 check
    26. CK42-51
    27. HR22-21 check
    28. CK51-52
    29. HR21-61
    30. CR53-42
    31. HR61-62 check
    32. CK52-51
    33. HR38-58 check
    34. CK51-41
    35. HR62-61 check
    36. CR42-51
    37. HR61-51 mate

.. janggi-board::

    HK39,HR38,HR40,CK41,CR1

.. janggi-moves::

    1. CR1-9 mate

.. _漢車車楚包:

漢車車楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HR40,HR60,CK42,CC41

.. janggi-moves::

    1. HK48-38
    2. CK42-31 big
    3. HK38-49 check
    4. CK31-42 big
    5. HK49-39
    6. HR40-30
    7. HR30-22 check
    8. CK42-33 big
    9. HR60-38 check
    10. CK33-43
    11. HR22-32
    12. HR32-33 mate

.. _漢車車楚馬:

漢車車楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HR23,HR30,CK42,CH34

.. janggi-moves::

    1. HK48-38
    2. CH34-46 check
    3. HK38-39
    4. CH46-58 check
    5. HK39-40
    6. CK42-32 big
    7. HK40-49
    8. CK32-42 big
    9. HK49-59
    10. CH58-46
    11. HR23-22 check
    12. CK42-41
    13. HR30-50
    14. CK41-51 big
    15. HK59-49
    16. CK51-41
    17. HR22-24
    18. HR24-44 check
    19. CK41-31
    20. HR44-46
    21. HR50-40 mate

.. _漢車車楚象:

漢車車楚象
~~~~~~~~~~

.. janggi-board::

    HK40,HR3,HR80,CK32,CE54

.. janggi-moves::

    1. HK40-50
    2. CK32-42 big
    3. HK50-60
    4. CE54-37 check
    5. HK60-59
    6. CK42-52 big
    7. HK59-49
    8. CK52-42 big
    9. HK49-40
    10. CE37-65
    11. HR3-4
    12. HR4-34
    13. HR80-50 check
    14. CE65-48
    15. HR50-48 check
    16. CK42-51
    17. HR34-54 mate

.. _漢車車楚卒:

漢車車楚卒
~~~~~~~~~~

.. janggi-board::

    HK38,HR69,HR70,CK41,CP28

.. janggi-moves::

    1. HK38-39
    2. CP28-29 check
    3. HK39-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-42 big
    7. HK49-60
    8. CP29-39
    9. HR69-59
    10. CP39-40
    11. HR70-62 check
    12. CK42-41
    13. HR59-53
    14. CP40-50 check
    15. HK60-59
    16. HR53-42 mate

.. _漢車車楚士:

漢車車楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HR50,CK31,CO42

.. janggi-moves::

    1. HK38-48
    2. CK31-41
    3. HR49-59
    4. HK48-49
    5. HK49-40
    6. HR50-70
    7. CK41-31 big
    8. HR59-39 check
    9. CK31-41
    10. HR70-61 check
    11. CO42-51
    12. HR39-33
    13. HR33-51 mate

.. _漢車包包楚:

漢車包包楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR50,HC31,HC42,CK41

.. janggi-moves::

    1. HR50-40
    2. CK41-42 big
    3. HK48-58
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-60
    8. CK41-42
    9. HR40-37
    10. CK42-51 big
    11. HK60-50
    12. CK51-41 big
    13. HK50-40
    14. CK41-42
    15. HC31-38
    16. HR37-47 check
    17. CK42-31
    18. HK40-49
    19. HK49-58
    20. HC38-68
    21. HC68-48
    22. HK58-49
    23. HC48-50
    24. HR47-37 mate

.. _漢車包馬楚:

漢車包馬楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR60,HC2,HH31,CK42

.. janggi-moves::

    1. HK48-38
    2. CK42-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HK40-50
    8. CK31-41 big
    9. HR60-49 check
    10. CK41-31
    11. HK50-60
    12. HR49-9
    13. HR9-8
    14. HC2-10
    15. CK31-41
    16. HR8-48 check
    17. CK41-51 big
    18. HK60-50
    19. HK50-40
    20. HC10-50
    21. HK40-49
    22. HR48-58 mate

.. _漢車包象楚:

漢車包象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR50,HC2,HE42,CK41

.. janggi-moves::

    1. HR50-40
    2. CK41-42 big
    3. HK48-38
    4. CK42-31 big
    5. HK38-49 check
    6. CK31-41 big
    7. HK49-60
    8. CK41-42
    9. HR40-49 check
    10. CK42-51 big
    11. HK60-50
    12. HK50-40
    13. HR49-9
    14. CK51-41
    15. HC2-10
    16. CK41-31 big
    17. HR9-39 check
    18. CK31-41
    19. HR39-38
    20. HC10-50
    21. HR38-48 check
    22. CK41-31 big
    23. HK40-49
    24. HR48-38 mate

.. _漢車包兵楚:

漢車包兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HC33,HP51,CK32

.. janggi-moves::

    1. HR10-40
    2. CK32-42 big
    3. HK48-58
    4. CK42-51 big
    5. HK58-49
    6. CK51-42 big
    7. HK49-60
    8. HR40-37
    9. CK42-51 big
    10. HK60-50
    11. CK51-42 big
    12. HK50-40
    13. HC33-38
    14. HR37-47 check
    15. CK42-31
    16. HK40-49
    17. HK49-58
    18. HC38-68
    19. HC68-48
    20. HK58-49
    21. HC48-50
    22. HR47-37 mate

.. _漢車包士楚:

漢車包士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR21,HC22,HO40,CK42

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-52 big
    7. HK60-50
    8. CK52-42 big
    9. HO40-49
    10. HR21-61
    11. HR61-62 check
    12. CK42-33
    13. HC22-72
    14. HC72-42
    15. HO49-48
    16. CK33-43
    17. HC42-44
    18. HC44-49 check
    19. CK43-33
    20. HR62-42 mate

.. _漢車包楚車:

漢車包楚車
~~~~~~~~~~

.. janggi-board::

    HK38,HR39,HC62,CK52,CR32

.. janggi-moves::

    1. HC62-32
    2. CK52-42
    3. HC32-62
    4. HR39-59
    5. CK42-31 big
    6. HK38-49
    7. CK31-41 big
    8. HK49-40
    9. CK41-31 big
    10. HK40-50
    11. CK31-41 big
    12. HR59-49 check
    13. CK41-51
    14. HK50-40
    15. HR49-69
    16. HR69-68
    17. HC62-70
    18. CK51-41
    19. HR68-48 check
    20. CK41-31 big
    21. HK40-50
    22. HK50-60
    23. HC70-50
    24. HK60-49
    25. HR48-38 mate

.. _漢車包楚包:

漢車包楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HC43,CK33,CC44

.. janggi-moves::

    1. HR10-3 check
    2. CK33-42
    3. HK48-58
    4. CK42-51 big
    5. HK58-49
    6. CK51-42
    7. HK49-50
    8. HR3-13
    9. HC43-3
    10. CC44-41 big
    11. HK50-60
    12. HK60-59
    13. CC41-43
    14. HR13-12 check
    15. CK42-53 big
    16. HK59-49
    17. HK49-40
    18. CC43-63
    19. HR12-32
    20. CK53-43
    21. HR32-31
    22. CC63-33
    23. HR31-41 check
    24. CK43-53
    25. HR41-44
    26. CC33-63
    27. HR44-4
    28. CC63-43
    29. HC3-10
    30. CK53-42
    31. HR4-2 check
    32. CK42-31 big
    33. HK40-50
    34. HK50-60
    35. CK31-41
    36. HR2-52
    37. HR52-53
    38. HC10-70
    39. HC70-50
    40. HK60-49
    41. HR53-43 check
    42. CK41-31
    43. HR43-33 mate

.. _漢車包楚馬:

漢車包楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HR14,HC52,CK32,CH27

.. janggi-moves::

    1. HK48-58
    2. CH27-46 check
    3. HK58-49
    4. CK32-31
    5. HK49-60
    6. CK31-41
    7. HR14-11 check
    8. CK41-42
    9. HC52-2
    10. CH46-27
    11. HR11-14
    12. CH27-39 check
    13. HK60-59
    14. CH39-47 check
    15. HK59-49
    16. CH47-35 big
    17. HR14-44 check
    18. CK42-33
    19. HR44-34 check
    20. CK33-43 big
    21. HK49-40
    22. CH35-27
    23. HR34-14
    24. CK43-33 big
    25. HK40-49
    26. CH27-35
    27. HR14-12
    28. CK33-43 big
    29. HK49-38
    30. CH35-47
    31. HR12-13 check
    32. CK43-42
    33. HR13-19
    34. CH47-26 check
    35. HK38-39
    36. CH26-47 check
    37. HK39-49
    38. CK42-41
    39. HK49-60
    40. CK41-51 big
    41. HK60-50
    42. CH47-28
    43. HR19-15
    44. HR15-45
    45. HR45-48
    46. CH28-20
    47. HK50-49
    48. HK49-38
    49. CH20-28
    50. HR48-47
    51. CH28-9
    52. HR47-7
    53. CH9-30 check
    54. HK38-39
    55. CK51-41
    56. HK39-40
    57. CK41-31 big
    58. HK40-50
    59. CH30-18
    60. HR7-9
    61. CH18-30
    62. HR9-8
    63. CK31-41 big
    64. HR8-48 check
    65. CK41-51
    66. HK50-40
    67. HR48-8
    68. HC2-10
    69. CK51-41
    70. HR8-48 check
    71. CK41-51
    72. HK40-39
    73. CH30-9
    74. HR48-8
    75. CH9-30
    76. HC10-70
    77. CK51-41
    78. HK39-40
    79. CK41-31 big
    80. HK40-50
    81. CH30-9
    82. HK50-60
    83. CK31-41
    84. HR8-9
    85. CK41-51 big
    86. HR9-59 check
    87. CK51-41
    88. HR59-58
    89. HC70-50
    90. HR58-48 check
    91. CK41-31
    92. HK60-49
    93. HR48-38 mate

.. _漢車包楚象:

漢車包楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HR23,HC72,CK42,CE32

.. janggi-moves::

    1. HK48-58
    2. CK42-52 big
    3. HK58-49
    4. CK52-42 big
    5. HK49-40
    6. CE32-55
    7. HR23-63
    8. CK42-31 big
    9. HK40-49
    10. CK31-42 big
    11. HK49-58
    12. CK42-52
    13. HR63-62 check
    14. CK52-51
    15. HC72-42
    16. CE55-83 big
    17. HK58-48
    18. CK51-41
    19. HR62-52
    20. CE83-66
    21. HR52-53
    22. HK48-58
    23. HK58-59
    24. HR53-52
    25. HC42-62
    26. CE66-43
    27. HK59-60
    28. HR52-54
    29. CE43-26
    30. HR54-44 check
    31. CK41-51 big
    32. HK60-50
    33. HK50-40
    34. HR44-64
    35. CK51-41
    36. HC62-70
    37. CE26-58
    38. HR64-44 check
    39. CK41-31 big
    40. HK40-50
    41. CE58-90
    42. HK50-60
    43. CE90-67
    44. HR44-54
    45. HR54-57
    46. CK31-41
    47. HR57-67
    48. CK41-51 big
    49. HR67-57 check
    50. CK51-41
    51. HC70-50
    52. HR57-47 check
    53. CK41-31
    54. HK60-49
    55. HR47-37 mate

.. _漢車包楚卒:

漢車包楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HR81,HC29,CK42,CP36

.. janggi-moves::

    1. HK48-58
    2. CP36-37
    3. HR81-82 check
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-40
    8. CP37-38
    9. HR82-89
    10. CK41-31
    11. HK40-50
    12. CK31-41 big
    13. HK50-60
    14. HR89-59
    15. CP38-48
    16. HR59-39
    17. HC29-59
    18. CP48-58
    19. HC59-57
    20. CP58-48
    21. HK60-59
    22. CK41-51
    23. HR39-19
    24. CK51-41
    25. HR19-18
    26. CP48-49 check
    27. HK59-58
    28. HR18-38
    29. HR38-37
    30. HC57-27
    31. HC27-47
    32. CP49-59
    33. HK58-48
    34. HR37-36
    35. HR36-46 check
    36. CK41-31
    37. HC47-50
    38. CP59-49
    39. HK48-49
    40. HR46-36 mate

.. _漢車包楚士:

漢車包楚士
~~~~~~~~~~

.. janggi-board::

    HK49,HR8,HC9,CK43,CO31

.. janggi-moves::

    1. HR8-48 check
    2. CK43-33
    3. HK49-59
    4. CK33-32
    5. HC9-69
    6. CO31-42
    7. HR48-38 check
    8. CO42-33
    9. HC69-39
    10. CK32-42
    11. HC39-33
    12. CK42-51 big
    13. HK59-49
    14. CK51-42 big
    15. HK49-40
    16. HR38-37
    17. HC33-38
    18. HR37-47 check
    19. CK42-31
    20. HK40-49
    21. HK49-58
    22. HC38-68
    23. HC68-48
    24. HK58-49
    25. HC48-50
    26. HR47-37 mate

.. _漢車馬馬楚:

漢車馬馬楚
~~~~~~~~~~

.. janggi-board::

    HK38,HR10,HH61,HH9,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK60-50
    8. CK52-53
    9. HR10-40
    10. CK53-42 big
    11. HR40-49 check
    12. CK42-31
    13. HH9-30
    14. HH30-38
    15. HH38-46
    16. HH46-54
    17. HR49-42 mate

.. _漢車馬象楚:

漢車馬象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HH20,HE41,CK42

.. janggi-moves::

    1. HK48-38
    2. CK42-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HH20-39
    8. HK40-50
    9. HR10-40
    10. HR40-49
    11. HH39-47
    12. HH47-66
    13. HH66-54
    14. HR49-42 mate

.. _漢車馬兵楚:

漢車馬兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HR63,HH73,HP87,CK32

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CK42-52 big
    5. HR63-53 check
    6. CK52-53 big
    7. HH73-54
    8. HP87-77
    9. HP77-67
    10. HP67-57
    11. HH54-73
    12. HH73-61 check
    13. CK53-43
    14. HP57-56
    15. HP56-55
    16. HP55-54
    17. HP54-53 check
    18. CK43-33
    19. HH61-42
    20. HH42-21 check
    21. CK33-32
    22. HP53-42 mate

.. _漢車馬士楚:

漢車馬士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HH40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HR9-8
    7. HR8-48 check
    8. CK41-51 big
    9. HK60-50
    10. HH40-19
    11. HH19-38
    12. HH38-46
    13. HH46-54
    14. HR48-42 mate

.. _漢車馬楚車:

漢車馬楚車
~~~~~~~~~~

.. janggi-board::

    HK40,HR19,HH81,CK41,CR5

.. janggi-moves::

    1. HH81-62 check
    2. CK41-51
    3. HR19-59 check
    4. CK51-42
    5. HH62-54 check
    6. CK42-31 big
    7. HR59-39 check
    8. CK31-41
    9. HR39-49 check
    10. CK41-31 big
    11. HR49-38 check
    12. CK31-41
    13. HH54-33 check
    14. CK41-51
    15. HR38-58 check
    16. CK51-42
    17. HH33-21 check
    18. CK42-31 big
    19. HR58-38 check
    20. CK31-41
    21. HR38-48 check
    22. CK41-31 big
    23. HK40-49
    24. CR5-9 check
    25. HK49-58
    26. CR9-8
    27. HR48-8
    28. CK31-41
    29. HR8-48 check
    30. CK41-51 big
    31. HK58-49
    32. HR48-42 mate

.. _漢車馬楚包:

漢車馬楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HH30,CK42,CC31

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-40
    6. CK42-32 big
    7. HK40-50
    8. CK32-42 big
    9. HH30-49
    10. HR1-3
    11. HK50-60
    12. HH49-57
    13. CK42-41
    14. HR3-33
    15. HH57-78
    16. HH78-66
    17. HH66-74
    18. HH74-62 mate

.. _漢車馬楚馬:

漢車馬楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HR73,HH16,CK42,CH34

.. janggi-moves::

    1. HK48-58
    2. CH34-46 check
    3. HK58-59
    4. CH46-67 check
    5. HK59-60
    6. CH67-48 check
    7. HK60-49
    8. CH48-27 big
    9. HK49-40
    10. CH27-48 check
    11. HK40-39
    12. CH48-27 check
    13. HH16-28
    14. CK42-32 big
    15. HK39-49
    16. CK32-42 big
    17. HK49-40
    18. CK42-32 big
    19. HK40-50
    20. CK32-42 big
    21. HH28-49
    22. CH27-48
    23. HR73-78
    24. CH48-69 check
    25. HK50-40
    26. HR78-68
    27. CK42-31 big
    28. HH49-37
    29. CH69-90
    30. HK40-39
    31. CH90-69
    32. HR68-69
    33. HK39-40
    34. HR69-39
    35. HH37-45 check
    36. CK31-41
    37. HR39-33
    38. HH45-53 mate

.. _漢車馬楚象:

漢車馬楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HR22,HH80,CK41,CE14

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CE14-37 check
    7. HK60-59
    8. CK41-51 big
    9. HK59-49
    10. CK51-41 big
    11. HK49-40
    12. CK41-31
    13. HR22-52
    14. CE37-65 big
    15. HK40-49
    16. CK31-41 big
    17. HK49-59
    18. HH80-68
    19. CE65-48
    20. HK59-58
    21. HH68-76
    22. HH76-84
    23. CK41-31
    24. HR52-53 check
    25. CK31-32
    26. HH84-72
    27. HH72-51 mate

.. _漢車馬楚卒:

漢車馬楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HR30,HH19,CK41,CP27

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HH19-38
    8. CP27-37
    9. HR30-21 check
    10. CK31-32
    11. HH38-59
    12. CP37-27 big
    13. HK40-50
    14. CK32-42 big
    15. HK50-60
    16. HR21-27
    17. HR27-47 check
    18. CK42-51
    19. HK60-50
    20. HH59-78
    21. HH78-66
    22. HH66-54
    23. HR47-42 mate

.. _漢車馬楚士:

漢車馬楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HR23,HH20,CK42,CO31

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-40
    6. CK42-32 big
    7. HH20-39
    8. CO31-41
    9. HK40-50
    10. CK32-42 big
    11. HH39-47
    12. CO41-31
    13. HK50-40
    14. CK42-32 big
    15. HH47-35
    16. CO31-42
    17. HR23-26
    18. CK32-31
    19. HR26-46
    20. CK31-32
    21. HR46-36
    22. CK32-31
    23. HH35-23 check
    24. CK31-41
    25. HR36-46
    26. CO42-43
    27. HR46-43 check
    28. CK41-51
    29. HR43-42 mate

.. _漢車象象楚:

漢車象象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR20,HE10,HE90,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HE90-58
    8. HR20-50
    9. HR50-43
    10. HR43-53 check
    11. CK51-41
    12. HE58-86
    13. HE86-63
    14. HR53-31 mate

.. _漢車象兵楚:

漢車象兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HR73,HE83,HP7,CK32

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CK42-52 big
    5. HR73-53 check
    6. CK52-53 big
    7. HE83-55
    8. HP7-17
    9. HP17-27
    10. HK60-50
    11. HK50-40
    12. HP27-37
    13. HE55-27
    14. HE27-44
    15. HE44-16
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. CK53-42
    20. HP34-33 check
    21. CK42-31
    22. HE16-48
    23. HE48-65
    24. HK40-50
    25. HP33-42 mate

.. _漢車象士楚:

漢車象士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR20,HE10,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. HR20-13
    9. HR13-33 check
    10. CK51-41 big
    11. HK50-60
    12. HE10-38
    13. HE38-70
    14. HE70-87
    15. HE87-64 mate

.. _漢車象楚車:

漢車象楚車
~~~~~~~~~~

.. janggi-board::

    HK39,HR30,HE42,CK52,CR8

.. janggi-moves::

    1. HE42-25
    2. CR8-9 check
    3. HK39-38
    4. CR9-59
    5. HR30-70
    6. CR59-58 check
    7. HK38-39
    8. CR58-59 check
    9. HK39-40
    10. CR59-58 check
    11. HK40-50
    12. CR58-53
    13. HR70-62 check
    14. CK52-51
    15. HE25-53
    16. CK51-41 big
    17. HK50-40
    18. CK41-31 big
    19. HE53-36
    20. HR62-52
    21. HR52-53 check
    22. CK31-32
    23. HK40-50
    24. HE36-64 mate

.. _漢車象楚包:

漢車象楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HE71,CK42,CC2

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CC2-72
    7. HR1-3
    8. HR3-43 check
    9. CK42-52 big
    10. HK60-50
    11. HK50-40
    12. HR43-63
    13. CK52-42
    14. HR63-62 check
    15. CK42-31 big
    16. HK40-50
    17. HK50-60
    18. CK31-41
    19. HR62-52
    20. HR52-53
    21. HE71-43
    22. HE43-11
    23. HE11-34
    24. HR53-51 mate

.. _漢車象楚馬:

漢車象楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HR78,HE50,CK41,CH17

.. janggi-moves::

    1. HK48-58
    2. CH17-29
    3. HR78-71 check
    4. CK41-42
    5. HE50-78
    6. CH29-50 check
    7. HK58-59
    8. CH50-38 check
    9. HK59-60
    10. CH38-57
    11. HE78-50
    12. CH57-69
    13. HE50-18
    14. CH69-48 check
    15. HK60-59
    16. CH48-67 check
    17. HK59-58
    18. CK42-52 big
    19. HK58-49
    20. CK52-42 big
    21. HK49-39
    22. CK42-32 big
    23. HE18-35
    24. CH67-46
    25. HK39-49
    26. CH46-27
    27. HR71-72 check
    28. CK32-31
    29. HE35-67
    30. CK31-41 big
    31. HE67-44
    32. CH27-35
    33. HR72-73
    34. CK41-42
    35. HK49-40
    36. CH35-47
    37. HR73-76
    38. CH47-35
    39. HR76-66
    40. HE44-76
    41. CH35-54
    42. HR66-56
    43. CK42-31 big
    44. HK40-50
    45. CK31-41 big
    46. HE76-48
    47. CH54-73
    48. HR56-46 check
    49. CK41-31
    50. HK50-60
    51. HR46-43
    52. CH73-61
    53. HE48-65
    54. HR43-63
    55. HR63-61 check
    56. CK31-32
    57. HR61-51
    58. HR51-42 mate

.. _漢車象楚象:

漢車象楚象
~~~~~~~~~~

.. janggi-board::

    HK40,HR80,HE53,CK33,CE34

.. janggi-moves::

    1. HR80-73
    2. CE34-17 big
    3. HK40-50
    4. CK33-42 big
    5. HK50-60
    6. CK42-52
    7. HR73-63
    8. HK60-59
    9. HK59-58
    10. CE17-40
    11. HK58-49
    12. CE40-8
    13. HE53-76
    14. CK52-42 big
    15. HK49-60
    16. CE8-36
    17. HR63-65
    18. CK42-51 big
    19. HK60-50
    20. CK51-41 big
    21. HR65-45 check
    22. CK41-51
    23. HR45-35
    24. CK51-41 big
    25. HE76-48
    26. CE36-68
    27. HR35-38
    28. HR38-49
    29. HE48-65 check
    30. CE68-45
    31. HR49-45 check
    32. CK41-31
    33. HR45-42 mate

.. _漢車象楚卒:

漢車象楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HR26,HE41,CK31,CP36

.. janggi-moves::

    1. HR26-21 check
    2. CK31-42 big
    3. HK48-58
    4. CK42-51 big
    5. HK58-49
    6. CK51-42 big
    7. HK49-40
    8. CP36-26
    9. HE41-73
    10. CK42-32 big
    11. HK40-50
    12. CK32-42 big
    13. HK50-60
    14. CK42-52 big
    15. HE73-56
    16. CP26-36
    17. HR21-41
    18. CP36-46
    19. HR41-46
    20. HK60-50
    21. HR46-43
    22. HR43-33
    23. HE56-84 mate

.. _漢車象楚士:

漢車象楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HR41,HE81,CK33,CO32

.. janggi-moves::

    1. HK38-49
    2. CO32-42
    3. HR41-61
    4. CK33-43 big
    5. HK49-60
    6. CO42-52
    7. HR61-65
    8. CK43-42
    9. HR65-45 check
    10. CK42-32
    11. HK60-50
    12. HR45-43
    13. CO52-42
    14. HR43-23
    15. CO42-33
    16. HR23-22 check
    17. CK32-31
    18. HE81-64
    19. HR22-52
    20. CO33-42
    21. HR52-54
    22. CO42-41
    23. HR54-34 check
    24. CK31-42 big
    25. HR34-44 check
    26. CK42-33
    27. HR44-41
    28. HE64-47
    29. HE47-15
    30. HR41-51 mate

.. _漢車兵兵楚:

漢車兵兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HR1,HP3,HP87,CK42

.. janggi-moves::

    1. HR1-51 check
    2. CK42-51
    3. HP87-77
    4. CK51-41
    5. HP77-67
    6. CK41-31 big
    7. HK38-49
    8. CK31-41 big
    9. HK49-60
    10. HP3-13
    11. HP13-23
    12. CK41-42
    13. HP67-57
    14. HP57-47
    15. HK60-50
    16. HK50-40
    17. HP47-37
    18. HP37-36
    19. HP36-35
    20. HP35-34
    21. HP23-33 check
    22. CK42-31
    23. HP33-43
    24. HP34-33
    25. HP43-42 mate

.. _漢車兵士楚:

漢車兵士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HP7,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HR10-9
    7. CK41-51 big
    8. HK60-50
    9. CK51-41 big
    10. HR9-49 check
    11. CK41-31
    12. HR49-43
    13. HP7-17
    14. HP17-27
    15. HP27-37
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. HP34-33
    20. HR43-42 mate

.. _漢車兵楚車:

漢車兵楚車
~~~~~~~~~~

.. janggi-board::

    HK38,HR40,HP7,CK31,CR48

.. janggi-moves::

    1. HK38-48 check
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49 check
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. CK51-41 big
    11. HR40-49 check
    12. CK41-31
    13. HR49-43
    14. HP7-17
    15. HP17-27
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. HP34-33
    21. HR43-42 mate

.. _漢車兵楚包:

漢車兵楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HR50,HP22,CK42,CC34

.. janggi-moves::

    1. HK48-58 check
    2. CK42-51 big
    3. HK58-49
    4. CK51-42 big
    5. HK49-40 check
    6. CK42-33
    7. HR50-60
    8. CC34-32 big
    9. HK40-50
    10. CK33-42 big
    11. HR60-49 check
    12. CK42-33
    13. HP22-12
    14. HK50-60
    15. HR49-41
    16. HR41-31
    17. CK33-43
    18. HR31-32
    19. CK43-53 big
    20. HK60-50
    21. CK53-43 big
    22. HK50-40
    23. HR32-31
    24. HR31-51
    25. HP12-22
    26. HP22-32
    27. HR51-42 mate

.. _漢車兵楚馬:

漢車兵楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HR22,HP82,CK31,CH4

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HR22-32
    4. CH4-12
    5. HP82-72
    6. CH12-24
    7. HR32-52
    8. CH24-45
    9. HP72-62
    10. CH45-66 check
    11. HK58-59
    12. CH66-47 check
    13. HK59-49
    14. CH47-68 big
    15. HK49-58
    16. CH68-87
    17. HK58-59
    18. CH87-75
    19. HK59-60
    20. CH75-87
    21. HR52-57
    22. CH87-68 check
    23. HK60-59
    24. CK41-42
    25. HK59-58
    26. CH68-76
    27. HR57-54
    28. HR54-44 check
    29. CK42-53 big
    30. HK58-49
    31. CH76-57 check
    32. HK49-40
    33. CH57-65
    34. HR44-64
    35. CH65-46
    36. HR64-68
    37. CH46-25
    38. HR68-58 check
    39. CK53-42
    40. HR58-48 check
    41. CK42-53
    42. HR48-41
    43. CH25-4
    44. HR41-1
    45. CH4-16
    46. HR1-31 check
    47. CK53-43
    48. HR31-41 check
    49. CK43-53
    50. HR41-44
    51. HR44-54 check
    52. CK53-43
    53. HP62-52
    54. CH16-28 check
    55. HK40-39
    56. CH28-47 check
    57. HK39-49
    58. CH47-28 big
    59. HK49-38
    60. CH28-36
    61. HR54-14
    62. CH36-57 check
    63. HK38-39
    64. CH57-45
    65. HK39-40
    66. HR14-44 check
    67. CK43-33 big
    68. HK40-50
    69. HR44-42 mate

.. _漢車兵楚象:

漢車兵楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HR90,HP87,CK41,CE27

.. janggi-moves::

    1. HR90-60
    2. CE27-10 check
    3. HK38-39
    4. CK41-31 big
    5. HK39-49
    6. CK31-41 big
    7. HK49-40
    8. CK41-31 big
    9. HK40-50
    10. CE10-27 check
    11. HK50-49
    12. CK31-41 big
    13. HK49-40
    14. CK41-31 big
    15. HR60-38 check
    16. CK31-41
    17. HR38-37
    18. CE27-44
    19. HP87-86
    20. HP86-85
    21. HR37-32
    22. HP85-75
    23. HP75-65
    24. CE44-16
    25. HP65-64
    26. CE16-48
    27. HP64-63
    28. CE48-25
    29. HP63-62
    30. HR32-33
    31. HP62-52
    32. CE25-57 check
    33. HK40-39
    34. CE57-74
    35. HR33-53
    36. HP52-42 mate

.. _漢車兵楚卒:

漢車兵楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HR2,HP87,CK41,CP37

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-41 big
    5. HK49-40
    6. CP37-38
    7. HR2-9
    8. CP38-48
    9. HK40-50
    10. HK50-60
    11. HR9-59
    12. HP87-77
    13. HP77-67
    14. HP67-57
    15. HP57-56
    16. HP56-55
    17. HP55-54
    18. HP54-53
    19. HR59-69
    20. CK41-31
    21. HR69-62
    22. CP48-49 check
    23. HK60-49
    24. HR62-42 mate

.. _漢車兵楚士:

漢車兵楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HR44,HP54,CK33,CO42

.. janggi-moves::

    1. HR44-34 check
    2. CK33-43
    3. HR34-24
    4. CK43-33 big
    5. HK38-49
    6. CK33-43 big
    7. HR24-44 check
    8. CK43-33
    9. HR44-34 check
    10. CK33-43 big
    11. HP54-44 check
    12. CK43-53
    13. HR34-35
    14. CO42-43
    15. HR35-65
    16. CO43-33
    17. HR65-62
    18. CO33-42
    19. HK49-40
    20. CO42-33
    21. HP44-54 check
    22. CK53-43
    23. HR62-63 check
    24. CK43-42
    25. HP54-53 check
    26. CK42-31
    27. HK40-50
    28. HK50-60
    29. HR63-64
    30. HR64-34
    31. CO33-32
    32. HP53-43
    33. HR34-24
    34. CK31-41
    35. HR24-22
    36. HR22-32
    37. CK41-51 big
    38. HK60-50
    39. HR32-42 mate

.. _漢車士楚車:

漢車士楚車
~~~~~~~~~~

.. janggi-board::

    HK39,HR51,HO38,CK31,CR41

.. janggi-moves::

    1. HR51-33 mate

.. janggi-board::

    HK39,HR40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 mate

.. _漢車士楚包:

漢車士楚包
~~~~~~~~~~

.. janggi-board::

    HK39,HR51,HO38,CK31,CC41

.. janggi-moves::

    1. HR51-33 mate

.. _漢車士楚馬:

漢車士楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HR39,HO48,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢車士楚象:

漢車士楚象
~~~~~~~~~~

.. janggi-board::

    HK39,HR51,HO38,CK31,CE41

.. janggi-moves::

    1. HR51-33 mate

.. janggi-board::

    HK38,HR39,HO48,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢車士楚士:

漢車士楚士
~~~~~~~~~~

.. janggi-board::

    HK39,HR52,HO38,CK31,CO32

.. janggi-moves::

    1. HR52-51 mate

.. _漢包包馬楚:

漢包包馬楚
~~~~~~~~~~

.. janggi-board::

    HK50,HC51,HC8,HH14,CK32

.. janggi-moves::

    1. HK50-60
    2. CK32-42
    3. HC51-33
    4. HK60-59
    5. HK59-58
    6. HC8-68
    7. CK42-51 big
    8. HK58-49
    9. CK51-42 big
    10. HK49-38
    11. CK42-32
    12. HC68-28
    13. HC28-58
    14. HK38-49
    15. CK32-42 big
    16. HK49-59
    17. HC58-60
    18. CK42-32
    19. HC60-54
    20. CK32-42
    21. HC54-4
    22. HC4-44
    23. HK59-49
    24. HK49-39
    25. CK42-32
    26. HC44-4
    27. HC4-34
    28. HC34-40
    29. HC40-38
    30. HH14-35
    31. HC33-36 check
    32. CK32-42
    33. HH35-56
    34. HC36-66
    35. HC66-46
    36. HK39-49
    37. HK49-48
    38. HC38-88
    39. HH56-75
    40. HH75-83
    41. HC88-82
    42. HH83-62 check
    43. CK42-31
    44. HC46-49
    45. HH62-83
    46. HH83-64
    47. HH64-52 mate

.. _漢包包象楚:

漢包包象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC58,HC39,HE42,CK32

.. janggi-moves::

    1. HE42-65
    2. CK32-31
    3. HC58-28
    4. HK48-49
    5. HC39-59
    6. HK49-58
    7. CK31-41
    8. HC28-68
    9. HC68-48
    10. HK58-49
    11. HC59-39
    12. HC39-69
    13. HK49-50
    14. HC69-63
    15. HC63-70
    16. HC70-40
    17. HC40-60
    18. HK50-49
    19. HK49-58
    20. HC48-68
    21. HK58-59
    22. HC68-63
    23. HC63-70
    24. HC60-58
    25. HK59-60
    26. HC70-50
    27. HK60-59
    28. HC58-60
    29. HK59-58
    30. HE65-48 check
    31. CK41-31
    32. HC60-51
    33. HC51-21
    34. HC21-41
    35. HC41-49
    36. HC49-42
    37. HE48-65
    38. HK58-48
    39. CK31-41
    40. HE65-33
    41. HE33-1
    42. HE1-24 check
    43. CK41-31
    44. HC42-49
    45. HE24-7
    46. HE7-30
    47. HC50-20
    48. HC20-40
    49. HE30-7
    50. HE7-39 mate

.. _漢包包兵楚:

漢包包兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HC64,HC7,HP86,CK51

.. janggi-moves::

    1. HP86-76
    2. CK51-41
    3. HP76-66
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HP66-56
    10. HK60-50
    11. HP56-46
    12. HP46-45
    13. HP45-44
    14. HC64-34
    15. HC34-54
    16. HK50-60
    17. HP44-34
    18. HP34-24
    19. HP24-14
    20. HP14-4
    21. HC7-3
    22. HC3-10
    23. HC10-70
    24. HC70-50
    25. CK51-41
    26. HK60-59
    27. HC54-60
    28. HP4-14
    29. HP14-24
    30. HP24-34
    31. CK41-42
    32. HP34-44 check
    33. CK42-33
    34. HK59-49
    35. CK33-42
    36. HP44-43 check
    37. CK42-52
    38. HC60-38
    39. HP43-33
    40. HC38-31
    41. HP33-42 mate

.. _漢包包士楚:

漢包包士楚
~~~~~~~~~~

.. janggi-board::

    HK49,HC41,HC8,HO60,CK31

.. janggi-moves::

    1. HC41-50
    2. HO60-59
    3. HO59-58
    4. HC8-68
    5. HC68-38
    6. HC38-60
    7. HC60-57
    8. HC57-59
    9. HC59-39
    10. HK49-48
    11. HO58-59
    12. HC39-69
    13. HC69-49
    14. HO59-60
    15. HC50-70
    16. HC70-30
    17. HO60-50
    18. HO50-40
    19. HC30-50
    20. HO40-39
    21. HC49-29
    22. HK48-49
    23. HO39-38
    24. HC29-59
    25. HC59-39 mate

.. _漢包包楚車:

漢包包楚車
~~~~~~~~~~

.. janggi-board::

    HK39,HC51,HC49,CK42,CR1

.. janggi-moves::

    1. CR1-31 check
    2. HC51-33
    3. CR31-33 mate

.. _漢包包楚馬:

漢包包楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HC48,HC39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢包包楚象:

漢包包楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HC48,HC39,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢包馬馬楚:

漢包馬馬楚
~~~~~~~~~~

.. janggi-board::

    HK38,HC60,HH41,HH32,CK31

.. janggi-moves::

    1. HH41-53
    2. CK31-42
    3. HK38-39
    4. HK39-40
    5. HC60-30
    6. HC30-50
    7. HC50-41
    8. HC41-48
    9. HK40-49
    10. HK49-58
    11. HC48-68
    12. CK42-52
    13. HC68-38
    14. HK58-49
    15. CK52-42 big
    16. HK49-40
    17. HH53-34 check
    18. CK42-41
    19. HH32-53 check
    20. CK41-51
    21. HK40-49
    22. HC38-60 mate

.. _漢包馬象楚:

漢包馬象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC23,HH10,HE11,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HH10-29
    4. CK41-51 big
    5. HK58-49
    6. CK51-41 big
    7. HH29-48
    8. CK41-42
    9. HK49-40
    10. HH48-67
    11. HH67-55
    12. HE11-43
    13. CK42-33 big
    14. HK40-50
    15. HK50-60
    16. HE43-26
    17. CK33-42
    18. HC23-29
    19. HE26-54
    20. HE54-82
    21. HE82-65 check
    22. CK42-31
    23. HK60-49
    24. HC29-69
    25. HH55-43 check
    26. CK31-32
    27. HC69-39
    28. HH43-35 mate

.. _漢包馬兵楚:

漢包馬兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC1,HH70,HP42,CK33

.. janggi-moves::

    1. HP42-52
    2. CK33-43 big
    3. HK48-38
    4. CK43-33 big
    5. HK38-49
    6. CK33-43 big
    7. HK49-40
    8. CK43-33 big
    9. HK40-50
    10. CK33-43 big
    11. HH70-49
    12. HK50-40
    13. CK43-33 big
    14. HH49-37
    15. HK40-50
    16. CK33-43 big
    17. HH37-45
    18. HK50-40
    19. HH45-24 check
    20. CK43-53
    21. HP52-62
    22. HH24-16
    23. HH16-35
    24. CK53-42
    25. HH35-54 check
    26. CK42-43
    27. HP62-52
    28. HP52-42
    29. HP42-32
    30. HP32-22
    31. HP22-12
    32. CK43-53
    33. HH54-33
    34. HH33-21
    35. HP12-22
    36. HP22-32
    37. HC1-31
    38. HC31-33
    39. HC33-73
    40. HH21-42
    41. HH42-63 check
    42. CK53-52
    43. HP32-42 mate

.. _漢包馬士楚:

漢包馬士楚
~~~~~~~~~~

.. janggi-board::

    HK40,HC33,HH21,HO38,CK32

.. janggi-moves::

    1. HC33-39 mate

.. _漢包馬楚車:

漢包馬楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HC62,HH31,CK32,CR1

.. janggi-moves::

    1. HH31-52 mate

.. janggi-board::

    HK48,HC49,HH36,CK31,CR1

.. janggi-moves::

    1. CR1-8 check
    2. HH36-28
    3. CR8-28 mate

.. _漢包馬楚包:

漢包馬楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HC41,HH59,CK31,CC32

.. janggi-moves::

    1. HC41-49
    2. HH59-80
    3. HH80-88
    4. HH88-76
    5. HH76-64
    6. HH64-52 mate

.. _漢包馬楚馬:

漢包馬楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HC2,HH11,CK52,CH31

.. janggi-moves::

    1. HH11-32 check
    2. CH31-12
    3. HC2-22 mate

.. janggi-board::

    HK38,HC39,HH48,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢包馬楚象:

漢包馬楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HC46,HH31,CK41,CE22

.. janggi-moves::

    1. HH31-43 check
    2. CE22-45
    3. HC46-44 mate

.. janggi-board::

    HK38,HC48,HH39,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢包馬楚卒:

漢包馬楚卒
~~~~~~~~~~

.. janggi-board::

    HK38,HC46,HH31,CK41,CP35

.. janggi-moves::

    1. HH31-43 check
    2. CP35-45
    3. HC46-44 mate

.. _漢包馬楚士:

漢包馬楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HC8,HH33,CK43,CO42

.. janggi-moves::

    1. HH33-45
    2. HC8-48 mate

.. _漢包象象楚:

漢包象象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC13,HE1,HE49,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-42
    3. HE49-66
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-40
    8. CK41-31 big
    9. HE66-38
    10. CK31-32
    11. HK40-50
    12. CK32-42 big
    13. HK50-60
    14. CK42-51 big
    15. HE38-55
    16. CK51-42
    17. HE55-23
    18. CK42-52 big
    19. HK60-49
    20. CK52-42 big
    21. HK49-39
    22. HC13-33
    23. CK42-31
    24. HE1-24
    25. HE24-56
    26. CK31-32
    27. HE23-6
    28. HE6-34
    29. HC33-38 check
    30. CK32-42
    31. HE56-88
    32. HE34-57
    33. HC38-40
    34. HE57-74 check
    35. CK42-43
    36. HE88-65
    37. HE65-48
    38. HE48-25
    39. HE74-51
    40. HE51-83
    41. HE83-66 mate

.. _漢包象兵楚:

漢包象兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC3,HE10,HP42,CK33

.. janggi-moves::

    1. HP42-52
    2. CK33-43 big
    3. HK48-38
    4. CK43-33 big
    5. HK38-49
    6. CK33-43 big
    7. HK49-40
    8. CK43-53
    9. HP52-62
    10. CK53-42
    11. HE10-27
    12. CK42-31 big
    13. HK40-50
    14. CK31-41 big
    15. HK50-60
    16. CK41-42
    17. HE27-59
    18. HE59-36
    19. CK42-51 big
    20. HK60-50
    21. CK51-41 big
    22. HK50-40
    23. CK41-42
    24. HE36-13
    25. CK42-31 big
    26. HK40-50
    27. CK31-42 big
    28. HK50-60
    29. HC3-63
    30. HC63-61
    31. HC61-66
    32. HE13-36
    33. CK42-51 big
    34. HK60-50
    35. CK51-41 big
    36. HK50-40
    37. HC66-26
    38. CK41-31
    39. HK40-49
    40. CK31-32
    41. HC26-46
    42. CK32-42
    43. HE36-19
    44. HE19-47
    45. HE47-75
    46. HP62-52 check
    47. CK42-53
    48. HC46-50
    49. HP52-62
    50. HE75-52
    51. HE52-24
    52. HE24-41
    53. HE41-13
    54. HE13-36 mate

.. _漢包象士楚:

漢包象士楚
~~~~~~~~~~

.. janggi-board::

    HK40,HC39,HE33,HO38,CK32

.. janggi-moves::

    1. HE33-65 mate

.. _漢包象楚車:

漢包象楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE22,CK33,CR32

.. janggi-moves::

    1. HE22-5 mate

.. janggi-board::

    HK40,HC39,HE50,CK41,CR31

.. janggi-moves::

    1. CR31-38
    2. HE50-78
    3. CR38-60 check
    4. HE78-50
    5. CR60-58 mate

.. _漢包象楚包:

漢包象楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HC41,HE17,CK31,CC32

.. janggi-moves::

    1. HC41-49
    2. HE17-40
    3. HE40-68
    4. HE68-85
    5. HE85-53
    6. HE53-81
    7. HE81-64
    8. HE64-41
    9. HE41-73
    10. HE73-56
    11. HE56-88
    12. HE88-65
    13. HE65-82
    14. HE82-54 mate

.. _漢包象楚馬:

漢包象楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE22,CK33,CH32

.. janggi-moves::

    1. HE22-5 mate

.. janggi-board::

    HK38,HC39,HE48,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢包象楚象:

漢包象楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE71,CK31,CE32

.. janggi-moves::

    1. HE71-54 mate

.. janggi-board::

    HK38,HC39,HE48,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢包象楚士:

漢包象楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE35,CK31,CO32

.. janggi-moves::

    1. HE35-3 mate

.. _漢包兵兵楚:

漢包兵兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HC83,HP62,HP7,CK41

.. janggi-moves::

    1. HP62-52
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-31
    7. HP7-17
    8. CK31-32
    9. HK60-50
    10. CK32-33
    11. HP17-27
    12. CK33-43 big
    13. HK50-40
    14. CK43-33 big
    15. HP27-37
    16. CK33-43
    17. HP52-62
    18. HP62-72
    19. HP72-82
    20. HC83-81
    21. HC81-90
    22. HC90-30
    23. HC30-50
    24. HP82-72
    25. HP72-62
    26. HP37-47 check
    27. CK43-33 big
    28. HK40-49
    29. HP47-37
    30. HP62-52
    31. HP52-42
    32. HP37-36
    33. HP36-35
    34. HP35-34 mate

.. _漢包兵士楚:

漢包兵士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP44,HO49,CK41

.. janggi-moves::

    1. HP44-43
    2. CK41-31
    3. HP43-33
    4. CK31-41 big
    5. HK48-38 check
    6. CK41-31
    7. HK38-39
    8. HK39-40
    9. HO49-48
    10. HC50-47
    11. HK40-50
    12. HC47-49
    13. HP33-42 mate

.. _漢包兵楚車:

漢包兵楚車
~~~~~~~~~~

.. janggi-board::

    HK38,HC39,HP34,CK32,CR41

.. janggi-moves::

    1. HP34-33 check
    2. CK32-31
    3. HC39-37 mate

.. janggi-board::

    HK39,HC49,HP22,CK41,CR1

.. janggi-moves::

    1. CR1-31 check
    2. HP22-32
    3. CR31-32 mate

.. _漢包兵楚包:

漢包兵楚包
~~~~~~~~~~

.. janggi-board::

    HK40,HC49,HP37,CK31,CC32

.. janggi-moves::

    1. HP37-47
    2. HK40-50
    3. HK50-60
    4. HK60-59
    5. HK59-58
    6. HP47-46
    7. HP46-45
    8. HP45-44
    9. HP44-43
    10. HK58-48
    11. HP43-42 mate

.. _漢包兵楚馬:

漢包兵楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP44,CK41,CH6

.. janggi-moves::

    1. HP44-43
    2. CH6-27 check
    3. HK48-49
    4. CH27-48
    5. HK49-48
    6. HP43-42 mate

.. _漢包兵楚象:

漢包兵楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP44,CK41,CE33

.. janggi-moves::

    1. HP44-43
    2. CE33-16 check
    3. HK48-49
    4. CE16-48
    5. HK49-48
    6. HP43-42 mate

.. _漢包兵楚卒:

漢包兵楚卒
~~~~~~~~~~

.. janggi-board::

    HK49,HC50,HP23,CK31,CP29

.. janggi-moves::

    1. HP23-33
    2. CP29-39 check
    3. HK49-48
    4. CP39-49
    5. HK48-49
    6. HP33-42 mate

.. _漢包兵楚士:

漢包兵楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HP23,CK41,CO42

.. janggi-moves::

    1. HP23-33
    2. CO42-43
    3. HP33-43
    4. HP43-42 mate

.. _漢包士楚車:

漢包士楚車
~~~~~~~~~~

.. janggi-board::

    HK50,HC49,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-10 mate

.. _漢包士楚包:

漢包士楚包
~~~~~~~~~~

.. janggi-board::

    HK40,HC33,HO38,CK32,CC42

.. janggi-moves::

    1. HC33-39 mate

.. _漢包士楚馬:

漢包士楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HC48,HO39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢包士楚象:

漢包士楚象
~~~~~~~~~~

.. janggi-board::

    HK40,HC33,HO38,CK32,CE42

.. janggi-moves::

    1. HC33-39 mate

.. janggi-board::

    HK38,HC48,HO39,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢馬馬象楚:

漢馬馬象楚
~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HH32,HE73,CK33

.. janggi-moves::

    1. HK38-49
    2. CK33-42 big
    3. HK49-60
    4. HE73-56
    5. HE56-84
    6. HE84-67
    7. CK42-52 big
    8. HK60-50
    9. CK52-42 big
    10. HK50-40
    11. HE67-35
    12. HE35-58
    13. HE58-86
    14. CK42-33 big
    15. HK40-50
    16. CK33-42 big
    17. HK50-60
    18. HE86-54
    19. HE54-37
    20. CK42-52 big
    21. HK60-50
    22. CK52-42 big
    23. HK50-40
    24. CK42-33
    25. HH32-53
    26. CK33-42
    27. HH51-72
    28. CK42-31
    29. HK40-50
    30. CK31-42 big
    31. HH53-45
    32. HE37-14 check
    33. CK42-41
    34. HK50-40
    35. HH45-53 mate

.. _漢馬馬兵楚:

漢馬馬兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HH61,HH10,HP87,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK60-50
    8. CK52-53
    9. HK50-40
    10. CK53-42
    11. HH10-29
    12. HH29-37
    13. HH37-45
    14. HK40-50
    15. HP87-77
    16. HP77-67
    17. HK50-60
    18. HP67-57
    19. HP57-56
    20. HP56-55
    21. HP55-54
    22. HP54-53 check
    23. CK42-31
    24. HH45-66
    25. HH66-54
    26. HP53-42 mate

.. _漢馬馬士楚:

漢馬馬士楚
~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HH53,HO38,CK31

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬馬楚車:

漢馬馬楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HH11,HH53,CK31,CR1

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK39,HH38,HH40,CK31,CR21

.. janggi-moves::

    1. CR21-29 mate

.. _漢馬馬楚包:

漢馬馬楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HH81,HH16,CK33,CC42

.. janggi-moves::

    1. HK48-58
    2. CK33-43
    3. HH16-35 check
    4. CK43-33
    5. HH35-56
    6. CK33-32
    7. HH56-44 check
    8. CK32-33
    9. HH44-63
    10. CK33-43
    11. HH63-55 check
    12. CK43-33
    13. HH55-74
    14. HK58-59
    15. HK59-60
    16. HH81-73
    17. CK33-32
    18. HH74-53 check
    19. CK32-33
    20. HH53-45 check
    21. CK33-43
    22. HH45-24 check
    23. CK43-53 big
    24. HK60-50
    25. HH73-85
    26. CK53-52
    27. HH85-64 check
    28. CK52-51
    29. HH24-32
    30. HH32-13
    31. HH13-34
    32. HH64-85
    33. HH85-73
    34. HH73-54
    35. HH54-33
    36. HH34-55
    37. HH55-63 mate

.. _漢馬馬楚馬:

漢馬馬楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HH72,HH23,CK32,CH42

.. janggi-moves::

    1. HH72-51 check
    2. CK32-33
    3. HH23-2
    4. HH2-14 mate

.. janggi-board::

    HK38,HH48,HH39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢馬馬楚象:

漢馬馬楚象
~~~~~~~~~~

.. janggi-board::

    HK49,HH61,HH3,CK32,CE42

.. janggi-moves::

    1. HH61-53 check
    2. CK32-33
    3. HH53-45 check
    4. CK33-32
    5. HH3-11 check
    6. CK32-31
    7. HH45-53
    8. HH11-23 mate

.. _漢馬馬楚卒:

漢馬馬楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HH11,HH53,CK31,CP4

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬馬楚士:

漢馬馬楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HH43,HH53,CK32,CO42

.. janggi-moves::

    1. HH43-24 check
    2. CK32-31
    3. HH24-12 mate

.. _漢馬象象楚:

漢馬象象楚
~~~~~~~~~~

.. janggi-board::

    HK48,HH30,HE10,HE90,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. HH30-38
    7. HH38-59
    8. HH59-67
    9. HH67-75
    10. HH75-63
    11. HE10-38
    12. HE38-15
    13. HE90-67
    14. HE67-35
    15. CK41-31
    16. HK40-50
    17. HK50-60
    18. HE35-3 check
    19. CK31-41
    20. HE15-47
    21. HE47-24 mate

.. _漢馬象兵楚:

漢馬象兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HE81,HP7,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-42 big
    3. HH51-43
    4. CK42-43 big
    5. HK49-60
    6. HE81-64
    7. CK43-53 big
    8. HK60-50
    9. HK50-40
    10. HE64-36 check
    11. CK53-42
    12. HP7-17
    13. HP17-27
    14. HE36-8
    15. HE8-25 check
    16. CK42-31 big
    17. HP27-37
    18. HP37-36
    19. HE25-48
    20. HE48-65
    21. HP36-35
    22. CK31-41
    23. HP35-34
    24. CK41-51
    25. HP34-33
    26. CK51-52
    27. HE65-42
    28. HE42-25
    29. HP33-42 mate

.. _漢馬象士楚:

漢馬象士楚
~~~~~~~~~~

.. janggi-board::

    HK39,HH53,HE42,HO38,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢馬象楚車:

漢馬象楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HH53,HE42,CK31,CR1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK48,HH36,HE49,CK31,CR1

.. janggi-moves::

    1. CR1-8 check
    2. HH36-28
    3. CR8-28 mate

.. _漢馬象楚包:

漢馬象楚包
~~~~~~~~~~

.. janggi-board::

    HK38,HH1,HE9,CK41,CC42

.. janggi-moves::

    1. HH1-13
    2. CK41-51
    3. HH13-21
    4. CK51-41
    5. HE9-37
    6. CK41-31
    7. HH21-13
    8. CK31-41
    9. HE37-65
    10. CK41-51
    11. HH13-21
    12. CK51-41
    13. HE65-33
    14. CK41-51
    15. HE33-1
    16. CK51-41
    17. HH21-13
    18. CK41-31 big
    19. HE1-33
    20. CK31-41
    21. HH13-5
    22. CK41-51
    23. HE33-56
    24. CK51-41
    25. HH5-24
    26. HK38-39
    27. CK41-31 big
    28. HE56-33
    29. CK31-41
    30. HK39-40
    31. CK41-51
    32. HE33-61
    33. CK51-52
    34. HE61-84 check
    35. CK52-53
    36. HK40-50
    37. HE84-67
    38. HE67-35
    39. HE35-58
    40. CK53-52
    41. HE58-75 check
    42. CK52-51
    43. HH24-32
    44. CK51-41
    45. HH32-13
    46. CK41-31
    47. HE75-58
    48. HE58-90
    49. HE90-67
    50. HE67-84
    51. HE84-56
    52. CK31-41
    53. HH13-34
    54. CK41-51
    55. HE56-84
    56. HH34-53
    57. HH53-61
    58. HE84-56
    59. CK51-41
    60. HH61-53 check
    61. CK41-31
    62. HE56-88
    63. HE88-65
    64. HE65-37
    65. HE37-14 mate

.. _漢馬象楚馬:

漢馬象楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HH51,HE42,CK33,CH1

.. janggi-moves::

    1. HE42-65 mate

.. janggi-board::

    HK38,HH39,HE48,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢馬象楚象:

漢馬象楚象
~~~~~~~~~~

.. janggi-board::

    HK49,HH21,HE81,CK32,CE42

.. janggi-moves::

    1. HE81-64 check
    2. CK32-31
    3. HH21-2
    4. HH2-23 mate

.. janggi-board::

    HK38,HH39,HE48,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢馬象楚卒:

漢馬象楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HH11,HE64,CK31,CP4

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬象楚士:

漢馬象楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HH54,HE21,CK32,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢馬兵兵楚:

漢馬兵兵楚
~~~~~~~~~~

.. janggi-board::

    HK38,HH61,HP17,HP87,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK60-50
    8. CK52-53
    9. HK50-40
    10. HP17-27
    11. HP27-37
    12. HK40-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54 check
    19. CK53-42
    20. HP87-77
    21. HP77-67
    22. HP67-66
    23. HP66-65
    24. HP65-64
    25. HP64-63
    26. HP63-53 check
    27. CK42-31
    28. HP54-44
    29. HP44-43
    30. HP43-42 mate

.. _漢馬兵士楚:

漢馬兵士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HH89,HP7,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. HH89-77
    9. HH77-85
    10. HH85-73
    11. HH73-54
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-36
    16. HP36-35
    17. HP35-34
    18. CK51-41 big
    19. HP34-44
    20. CK41-51
    21. HP44-43
    22. CK51-52
    23. HH54-73 check
    24. CK52-51
    25. HH73-61
    26. HP43-42 mate

.. _漢馬兵楚車:

漢馬兵楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HH80,HP17,CK31,CR7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HH80-59
    9. HH59-67
    10. HH67-75
    11. HH75-54
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-36
    16. HP36-35
    17. HP35-34
    18. CK41-51
    19. HP34-33
    20. CK51-52
    21. HK60-50
    22. HH54-73 check
    23. CK52-51
    24. HH73-61
    25. HP33-42 mate

.. _漢馬兵楚包:

漢馬兵楚包
~~~~~~~~~~

.. janggi-board::

    HK50,HH18,HP17,CK41,CC31

.. janggi-moves::

    1. HK50-40
    2. CK41-42
    3. HH18-30
    4. HK40-39
    5. HK39-38
    6. CK42-32 big
    7. HK38-49
    8. CK32-42 big
    9. HK49-60
    10. CK42-51 big
    11. HK60-50
    12. CK51-41 big
    13. HH30-49
    14. CK41-42
    15. HP17-27
    16. CK42-32
    17. HK50-60
    18. HP27-26
    19. HH49-57
    20. HP26-25
    21. HH57-45
    22. HH45-66
    23. CK32-42
    24. HP25-35
    25. HP35-45
    26. HH66-74
    27. HP45-44
    28. CC31-53
    29. HH74-62
    30. CK42-52
    31. HK60-50
    32. HP44-43
    33. CC53-33
    34. HH62-54
    35. HP43-33
    36. HH54-73 check
    37. CK52-51
    38. HH73-61
    39. HP33-42 mate

.. _漢馬兵楚馬:

漢馬兵楚馬
~~~~~~~~~~

.. janggi-board::

    HK39,HH81,HP23,CK43,CH80

.. janggi-moves::

    1. HH81-62 check
    2. CK43-53
    3. HP23-33
    4. CH80-68
    5. HH62-41 check
    6. CK53-52
    7. HP33-43
    8. CK52-51
    9. HH41-33
    10. CK51-52
    11. HH33-45
    12. CH68-47 check
    13. HK39-49
    14. CK52-51
    15. HP43-53
    16. CH47-28 check
    17. HK49-50
    18. CH28-36
    19. HH45-33
    20. CH36-44
    21. HH33-54
    22. CH44-23
    23. HK50-40
    24. HP53-43
    25. CK51-52
    26. HH54-73 check
    27. CK52-51
    28. HH73-61
    29. CK51-41
    30. HP43-33
    31. CK41-31
    32. HH61-53
    33. CH23-11
    34. HK40-50
    35. HH53-61
    36. CH11-23
    37. HP33-23
    38. CK31-32
    39. HH61-53 check
    40. CK32-42 big
    41. HH53-45
    42. HK50-40
    43. HP23-33 check
    44. CK42-41
    45. HH45-53 check
    46. CK41-51
    47. HH53-61
    48. HP33-42 mate

.. _漢馬兵楚象:

漢馬兵楚象
~~~~~~~~~~

.. janggi-board::

    HK40,HH19,HP46,CK51,CE30

.. janggi-moves::

    1. HH19-38
    2. CE30-7
    3. HH38-26
    4. CE7-24
    5. HP46-45
    6. CE24-1
    7. HH26-14
    8. CE1-24
    9. HH14-22
    10. CE24-47
    11. HH22-43 check
    12. CK51-42
    13. HP45-44
    14. CE47-79
    15. HH43-64
    16. CE79-47
    17. HH64-45
    18. CE47-79
    19. HH45-37
    20. CE79-47
    21. HP44-54
    22. CE47-79
    23. HK40-39
    24. HP54-44
    25. CE79-47
    26. HH37-45
    27. CE47-79
    28. HH45-64
    29. CE79-47
    30. HH64-56
    31. CE47-19
    32. HH56-48
    33. CE19-47
    34. HH48-67
    35. HK39-49
    36. CE47-24
    37. HH67-75
    38. HH75-54 check
    39. CK42-53
    40. HP44-34
    41. CK53-43 big
    42. HK49-38
    43. CE24-1
    44. HH54-35
    45. CK43-42
    46. HP34-44
    47. CK42-52
    48. HP44-43
    49. CE1-24
    50. HH35-54
    51. CE24-56
    52. HH54-46
    53. CE56-88
    54. HH46-34
    55. CE88-65
    56. HH34-53
    57. CK52-51
    58. HH53-65
    59. HH65-53
    60. HH53-61
    61. HP43-42 mate

.. _漢馬兵楚卒:

漢馬兵楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HH21,HP5,CK32,CP14

.. janggi-moves::

    1. HK48-58
    2. HH21-2
    3. CP14-24
    4. HH2-23
    5. CK32-33
    6. HP5-4
    7. HP4-3
    8. HH23-4
    9. CK33-42
    10. HH4-16
    11. CK42-51 big
    12. HK58-49
    13. CK51-41 big
    14. HK49-40
    15. CK41-31 big
    16. HH16-37
    17. HP3-13
    18. CK31-32
    19. HP13-23
    20. CP24-34
    21. HK40-50
    22. CK32-42 big
    23. HK50-60
    24. HH37-45
    25. CK42-51 big
    26. HK60-50
    27. CP34-44
    28. HH45-26
    29. CK51-42
    30. HH26-14
    31. CP44-34 big
    32. HK50-40
    33. CP34-24
    34. HH14-26
    35. CK42-32 big
    36. HK40-50
    37. CK32-42 big
    38. HH26-45
    39. CK42-41
    40. HK50-40
    41. CP24-34
    42. HP23-33
    43. CP34-44
    44. HH45-64
    45. HH64-72
    46. HH72-53 check
    47. CK41-51
    48. HH53-61
    49. HP33-42 mate

.. _漢馬兵楚士:

漢馬兵楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HH43,HP7,CK53,CO51

.. janggi-moves::

    1. HH43-51
    2. HH51-72 check
    3. CK53-42
    4. HH72-64
    5. HH64-45
    6. HK38-49
    7. HP7-17
    8. HP17-27
    9. HK49-40
    10. HP27-37
    11. HP37-36
    12. HP36-35
    13. HP35-34
    14. HP34-33 check
    15. CK42-41
    16. HH45-53 check
    17. CK41-51
    18. HH53-61
    19. HP33-42 mate

.. _漢馬士楚車:

漢馬士楚車
~~~~~~~~~~

.. janggi-board::

    HK40,HH49,HO39,CK31,CR1

.. janggi-moves::

    1. CR1-10 mate

.. _漢馬士楚馬:

漢馬士楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HH48,HO39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢馬士楚象:

漢馬士楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HH39,HO48,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢象象兵楚:

漢象象兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE84,HP1,CK32

.. janggi-moves::

    1. HE42-65
    2. CK32-31
    3. HE84-52
    4. CK31-41 big
    5. HK48-38
    6. CK41-31 big
    7. HK38-49
    8. CK31-41 big
    9. HK49-40
    10. CK41-31 big
    11. HE52-35
    12. HP1-11
    13. CK31-41
    14. HE35-7
    15. CK41-31 big
    16. HE7-39
    17. CK31-41
    18. HE39-16
    19. CK41-31 big
    20. HK40-50
    21. HE16-48
    22. HP11-21 check
    23. CK31-41
    24. HK50-40
    25. HE48-25
    26. HE65-88
    27. HE88-56
    28. HE56-73 check
    29. CK41-51
    30. HP21-31
    31. HP31-41 check
    32. CK51-52
    33. HE73-56
    34. HE56-84 mate

.. _漢象象士楚:

漢象象士楚
~~~~~~~~~~

.. janggi-board::

    HK39,HE42,HE64,HO38,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢象象楚車:

漢象象楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE64,CK31,CR1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK40,HE39,HE50,CK41,CR31

.. janggi-moves::

    1. CR31-38
    2. HE50-78
    3. CR38-60 check
    4. HE78-50
    5. CR60-58 mate

.. _漢象象楚包:

漢象象楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HE72,HE5,CK32,CC42

.. janggi-moves::

    1. HE72-55 check
    2. CK32-31
    3. HK48-58
    4. CK31-41
    5. HE55-83
    6. CK41-31
    7. HK58-59
    8. HK59-60
    9. HE5-37
    10. HE37-9
    11. CK31-32
    12. HE83-55 check
    13. CK32-33
    14. HE9-26
    15. HE26-58
    16. CK33-43
    17. HE58-75 check
    18. CK43-33
    19. HK60-50
    20. HE55-38
    21. CK33-32
    22. HE38-15 check
    23. CK32-31
    24. HE75-58
    25. HE58-90
    26. HE90-67
    27. HE67-84
    28. HE84-56
    29. HE56-33
    30. HE33-1
    31. HE15-38
    32. HE38-55
    33. HE55-87
    34. HE87-64
    35. HE1-33
    36. HE33-65
    37. HE65-37
    38. HE37-14 mate

.. _漢象象楚馬:

漢象象楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE15,CK33,CH1

.. janggi-moves::

    1. HE42-65 mate

.. janggi-board::

    HK38,HE48,HE39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢象象楚象:

漢象象楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE64,CK31,CE1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK38,HE48,HE39,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢象象楚卒:

漢象象楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE15,CK33,CP4

.. janggi-moves::

    1. HE42-65 mate

.. _漢象象楚士:

漢象象楚士
~~~~~~~~~~

.. janggi-board::

    HK48,HE21,HE65,CK32,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢象兵兵楚:

漢象兵兵楚
~~~~~~~~~~

.. janggi-board::

    HK48,HE1,HP7,HP77,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HE1-33
    4. CK31-32
    5. HK38-49
    6. CK32-33
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. CK33-42
    11. HP27-37
    12. CK42-51 big
    13. HK60-50
    14. HP37-47
    15. HP47-57
    16. HK50-60
    17. HP57-56
    18. HP56-55
    19. HP55-54
    20. HP54-53
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢象兵士楚:

漢象兵士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HE81,HP7,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HE81-64 check
    7. CK41-51 big
    8. HK60-50
    9. HE64-36
    10. HE36-4
    11. HE4-21
    12. HE21-44
    13. HE44-16
    14. HP7-17
    15. HP17-27
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. HP34-33
    21. HE16-48
    22. CK51-41
    23. HP33-43
    24. HE48-65
    25. HP43-42 mate

.. _漢象兵楚車:

漢象兵楚車
~~~~~~~~~~

.. janggi-board::

    HK48,HE90,HP17,CK31,CR7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HE90-58
    10. HP7-17
    11. HP17-27
    12. HK60-50
    13. HK50-40
    14. HE58-86
    15. HE86-54
    16. HE54-82
    17. HE82-65
    18. HP27-37
    19. HP37-36
    20. HP36-35
    21. HP35-34
    22. HP34-33
    23. CK51-52
    24. HE65-42
    25. HE42-25
    26. HP33-42 mate

.. _漢象兵楚包:

漢象兵楚包
~~~~~~~~~~

.. janggi-board::

    HK48,HE30,HP17,CK41,CC42

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-52
    5. HK49-39
    6. CC42-62
    7. HE30-58
    8. CK52-42
    9. HE58-86
    10. HE86-54
    11. CC62-32
    12. HP17-27
    13. HP27-26
    14. CK42-33 big
    15. HK39-49
    16. CK33-42 big
    17. HK49-60
    18. CC32-52 check
    19. HE54-37
    20. CK42-53 big
    21. HK60-50
    22. CK53-42 big
    23. HK50-40
    24. CC52-32 check
    25. HE37-65 check
    26. CK42-41
    27. HE65-48
    28. CK41-42
    29. HK40-50
    30. CK42-33
    31. HK50-60
    32. HE48-65 check
    33. CK33-43
    34. HP26-36
    35. CK43-53 big
    36. HK60-50
    37. HE65-48
    38. HP36-46
    39. HE48-25 check
    40. CK53-43
    41. HK50-60
    42. HP46-56
    43. HP56-55
    44. HP55-54
    45. CK43-33
    46. HP54-53
    47. CC32-37
    48. HE25-57
    49. CC37-67
    50. HE57-25
    51. HE25-48
    52. CK33-32
    53. HP53-43
    54. HE48-65
    55. CC67-64
    56. HP43-33 check
    57. CK32-31
    58. HE65-48
    59. CK31-41
    60. HE48-25
    61. CK41-51 big
    62. HK60-50
    63. HP33-42 mate

.. _漢象兵楚馬:

漢象兵楚馬
~~~~~~~~~~

.. janggi-board::

    HK48,HE90,HP17,CK31,CH7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HE90-58
    10. HP7-17
    11. HP17-27
    12. HK60-50
    13. HK50-40
    14. HE58-86
    15. HE86-54
    16. HE54-82
    17. HE82-65
    18. HP27-37
    19. HP37-36
    20. HP36-35
    21. HP35-34
    22. HP34-33
    23. CK51-52
    24. HE65-42
    25. HE42-25
    26. HP33-42 mate

.. _漢象兵楚象:

漢象兵楚象
~~~~~~~~~~

.. janggi-board::

    HK48,HE90,HP17,CK31,CE7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HE90-58
    10. HP7-17
    11. HP17-27
    12. HK60-50
    13. HK50-40
    14. HE58-86
    15. HE86-54
    16. HE54-82
    17. HE82-65
    18. HP27-37
    19. HP37-36
    20. HP36-35
    21. HP35-34
    22. HP34-33
    23. CK51-52
    24. HE65-42
    25. HE42-25
    26. HP33-42 mate

.. _漢象兵楚卒:

漢象兵楚卒
~~~~~~~~~~

.. janggi-board::

    HK48,HE21,HP85,CK43,CP24

.. janggi-moves::

    1. HK48-58
    2. CK43-42
    3. HE21-4
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-40
    8. CK41-31 big
    9. HE4-36
    10. CP24-34
    11. HK40-50
    12. HK50-60
    13. HE36-59
    14. HP85-75
    15. CP34-44
    16. HE59-76
    17. CP44-54
    18. HP75-65
    19. HE76-48
    20. HE48-20
    21. HE20-37
    22. CP54-44
    23. HK60-50
    24. HP65-55
    25. HE37-65
    26. HE65-48
    27. HE48-16
    28. CP44-34
    29. HP55-45
    30. HE16-48
    31. HE48-80
    32. HE80-57
    33. HE57-29
    34. HE29-6
    35. CP34-24
    36. HP45-35
    37. HK50-40
    38. HE6-38
    39. HE38-70
    40. HE70-47
    41. HE47-30
    42. HE30-7
    43. CP24-14
    44. HP35-34
    45. CK31-32
    46. HE7-39
    47. CP14-15
    48. HE39-56
    49. CK32-42
    50. HE56-88
    51. CK42-51
    52. HE88-65
    53. CP15-25
    54. HK40-49
    55. HP34-44
    56. CK51-52
    57. HE65-48
    58. CP25-35
    59. HP44-43
    60. CP35-45
    61. HE48-76
    62. CK52-51
    63. HP43-53
    64. CP45-46
    65. HK49-59
    66. HE76-44
    67. HE44-16
    68. HK59-60
    69. HE16-33
    70. CP46-47
    71. HK60-50
    72. CP47-48
    73. HE33-65
    74. CP48-49 check
    75. HK50-49
    76. HP53-42 mate

.. _漢象兵楚士:

漢象兵楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HE23,HP7,CK53,CO51

.. janggi-moves::

    1. HE23-51
    2. CK53-52
    3. HE51-83
    4. CK52-42
    5. HE83-66
    6. HE66-34
    7. HP7-17
    8. HP17-27
    9. HE34-2
    10. HE2-25 check
    11. CK42-31 big
    12. HP27-37
    13. HP37-36
    14. HE25-48
    15. HE48-65
    16. HP36-35
    17. CK31-41
    18. HP35-34
    19. CK41-51
    20. HP34-33
    21. CK51-52
    22. HE65-42
    23. HE42-25
    24. HP33-42 mate

.. _漢象士楚車:

漢象士楚車
~~~~~~~~~~

.. janggi-board::

    HK50,HE49,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-10 mate

.. _漢象士楚馬:

漢象士楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HE48,HO39,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢象士楚象:

漢象士楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HE48,HO39,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢兵兵士楚:

漢兵兵士楚
~~~~~~~~~~

.. janggi-board::

    HK48,HP7,HP87,HO50,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HO50-40
    4. CK41-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP87-77
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢兵兵楚車:

漢兵兵楚車
~~~~~~~~~~

.. janggi-board::

    HK38,HP17,HP77,CK51,CR7

.. janggi-moves::

    1. HP17-7
    2. CK51-41
    3. HP77-67
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP67-57
    21. HP53-43
    22. HP57-56
    23. HP56-55
    24. HP55-54
    25. HP54-53
    26. HP43-42 mate

.. _漢兵兵楚包:

漢兵兵楚包
~~~~~~~~~~

.. janggi-board::

    HK38,HP77,HP87,CK51,CC84

.. janggi-moves::

    1. HP77-67
    2. CK51-41
    3. HP87-77
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP67-57
    9. HP57-47
    10. HK60-50
    11. HK50-40
    12. HP47-37
    13. HP37-36
    14. HP36-35
    15. HP77-67
    16. HP67-57
    17. HP57-47
    18. HP35-25
    19. HP25-24
    20. HP24-23
    21. CK41-42
    22. HP47-37
    23. HP37-36
    24. HP36-35
    25. HP35-34
    26. HP23-33 check
    27. CK42-31
    28. HP33-43
    29. HP34-33
    30. HP43-42 mate

.. _漢兵兵楚馬:

漢兵兵楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HP17,HP77,CK51,CH7

.. janggi-moves::

    1. HP17-7
    2. CK51-41
    3. HP77-67
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP67-57
    21. HP53-43
    22. HP57-56
    23. HP56-55
    24. HP55-54
    25. HP54-53
    26. HP43-42 mate

.. _漢兵兵楚象:

漢兵兵楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HP17,HP77,CK51,CE7

.. janggi-moves::

    1. HP17-7
    2. CK51-41
    3. HP77-67
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP67-57
    21. HP53-43
    22. HP57-56
    23. HP56-55
    24. HP55-54
    25. HP54-53
    26. HP43-42 mate

.. _漢兵兵楚卒:

漢兵兵楚卒
~~~~~~~~~~

.. janggi-board::

    HK38,HP7,HP87,CK51,CP86

.. janggi-moves::

    1. HP87-77
    2. CK51-41
    3. HP77-67
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CP86-76
    9. HP7-17
    10. HP17-27
    11. HP27-37
    12. CK41-51 big
    13. HK60-50
    14. HP37-47
    15. HP47-46
    16. HP67-57
    17. CP76-66
    18. HK50-40
    19. HP57-47
    20. HP46-36
    21. HP47-37
    22. HP36-26
    23. HK40-39
    24. HP26-25
    25. HP25-24
    26. HP24-23
    27. HP23-33
    28. HP33-43
    29. HP37-36
    30. CP66-56
    31. HP36-35
    32. CP56-57
    33. HP35-34
    34. CP57-58
    35. HP34-33
    36. CP58-49 check
    37. HK39-38
    38. HP43-42 mate

.. _漢兵兵楚士:

漢兵兵楚士
~~~~~~~~~~

.. janggi-board::

    HK38,HP43,HP7,CK41,CO53

.. janggi-moves::

    1. HP43-53
    2. HP53-43
    3. HK38-49
    4. HP7-17
    5. HP17-27
    6. HP27-37
    7. HP37-36
    8. HP36-35
    9. HP35-34
    10. HP34-33
    11. HP43-42 mate

.. _漢兵士楚車:

漢兵士楚車
~~~~~~~~~~

.. janggi-board::

    HK40,HP1,HO39,CK31,CR51

.. janggi-moves::

    1. CR51-60 mate

.. _漢士士楚車:

漢士士楚車
~~~~~~~~~~

.. janggi-board::

    HK39,HO40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 mate

.. _漢士士楚馬:

漢士士楚馬
~~~~~~~~~~

.. janggi-board::

    HK38,HO39,HO48,CK41,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢士士楚象:

漢士士楚象
~~~~~~~~~~

.. janggi-board::

    HK38,HO39,HO48,CK41,CE43

.. janggi-moves::

    1. CE43-66 mate

.. _漢車車包士楚:

漢車車包士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR2,HR8,HC41,HO49,CK31

.. janggi-moves::

    1. HR2-32 check
    2. CK31-32 big
    3. HK38-48
    4. CK32-31
    5. HR8-1 check
    6. CK31-32
    7. HR1-4
    8. HK48-58
    9. CK32-42
    10. HC41-45
    11. CK42-51 big
    12. HR4-54 check
    13. CK51-41
    14. HR54-53
    15. HC45-50 mate

.. _漢車車包楚士:

漢車車包楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR52,HR39,HC32,CK33,CO42

.. janggi-moves::

    1. HK38-49 check
    2. CK33-43 big
    3. HK49-60
    4. CO42-52
    5. HR39-49 check
    6. CK43-33
    7. HC32-40
    8. CK33-32
    9. HC40-70
    10. CO52-42
    11. HR49-39 check
    12. CO42-33
    13. HC70-40
    14. CK32-42
    15. HC40-33
    16. CK42-51 big
    17. HK60-50
    18. CK51-42 big
    19. HK50-40
    20. HR39-37
    21. HC33-38
    22. HR37-47 check
    23. CK42-31
    24. HK40-49
    25. HK49-58
    26. HC38-68
    27. HC68-48
    28. HK58-49
    29. HC48-50
    30. HR47-37 mate

.. _漢車車馬士楚:

漢車車馬士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HR10,HH11,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-42
    7. HR1-2 check
    8. CK42-51 big
    9. HK60-50
    10. CK51-41 big
    11. HO40-49
    12. HR10-30
    13. HR30-21 mate

.. _漢車車馬楚士:

漢車車馬楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR11,HR51,HH31,CK32,CO41

.. janggi-moves::

    1. HK38-49
    2. CO41-51
    3. HK49-60
    4. CO51-41
    5. HH31-12
    6. CK32-42
    7. HH12-24
    8. CK42-51 big
    9. HK60-50
    10. CK51-42 big
    11. HK50-40
    12. CK42-33 big
    13. HH24-36
    14. CO41-42
    15. HK40-50
    16. CK33-43 big
    17. HK50-60
    18. HR11-12
    19. HH36-55 check
    20. CK43-53
    21. HK60-50
    22. HK50-40
    23. CK53-52
    24. HH55-63
    25. CO42-32
    26. HR12-32 check
    27. CK52-53
    28. HR32-42 mate

.. _漢車車象士楚:

漢車車象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR72,HR8,HE82,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-41 big
    3. HK48-58
    4. CK41-51 big
    5. HR72-52 check
    6. CK51-52 big
    7. HE82-54
    8. HK58-59
    9. HR8-48
    10. HO49-58
    11. HE54-82
    12. HE82-65
    13. HR48-42 mate

.. _漢車車象楚士:

漢車車象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR21,HR52,HE41,CK33,CO42

.. janggi-moves::

    1. HK38-49
    2. CK33-43 big
    3. HK49-60
    4. CO42-52
    5. HR21-24
    6. CK43-42
    7. HR24-44 check
    8. CK42-53 big
    9. HK60-50
    10. CO52-42
    11. HE41-73
    12. HE73-56
    13. CK53-52
    14. HR44-64
    15. CO42-31
    16. HK50-40
    17. HR64-74
    18. HR74-72 check
    19. CK52-53
    20. HE56-88
    21. HR72-77
    22. HE88-65
    23. HR77-57 check
    24. CK53-43
    25. HR57-37
    26. CK43-53
    27. HR37-31 check
    28. CK53-43
    29. HR31-33 mate

.. _漢車車兵兵楚:

漢車車兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HR9,HP11,HP2,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. HR9-39
    7. CK41-42
    8. HP11-21
    9. HR1-11
    10. HR39-31 check
    11. CK42-43
    12. HR11-13 mate

.. _漢車車兵士楚:

漢車車兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HR3,HP2,HO40,CK42

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-52 big
    7. HK60-50
    8. CK52-42 big
    9. HO40-49
    10. HR1-11
    11. HR11-12 check
    12. CK42-31
    13. HR3-23
    14. HR23-21 mate

.. _漢車車兵楚卒:

漢車車兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR4,HR40,HP7,CK41,CP14

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49 check
    4. CK31-41 big
    5. HK49-60
    6. CP14-4
    7. HR40-34
    8. CK41-51 big
    9. HK60-50
    10. CK51-41 big
    11. HR34-44 check
    12. CK41-31
    13. HR44-4
    14. HR4-44
    15. HR44-43
    16. HP7-17
    17. HP17-27
    18. HP27-37
    19. HP37-36
    20. HP36-35
    21. HP35-34
    22. HP34-33
    23. HR43-42 mate

.. _漢車車兵楚士:

漢車車兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR32,HR8,HP64,CK43,CO31

.. janggi-moves::

    1. HK48-58
    2. CO31-32
    3. HR8-48 check
    4. CK43-33
    5. HP64-54
    6. CO32-42
    7. HR48-38 check
    8. CK33-43
    9. HP54-44 check
    10. CK43-53 big
    11. HK58-49
    12. CO42-43
    13. HR38-68
    14. CO43-33
    15. HR68-62
    16. CO33-42
    17. HK49-40
    18. CO42-33
    19. HP44-54 check
    20. CK53-43
    21. HR62-63 check
    22. CK43-42
    23. HP54-53 check
    24. CK42-31
    25. HK40-50
    26. HK50-60
    27. HR63-64
    28. HR64-34
    29. CO33-32
    30. HP53-43
    31. HR34-24
    32. CK31-41
    33. HR24-22
    34. HR22-32
    35. CK41-51 big
    36. HK60-50
    37. HR32-42 mate

.. _漢車車士士楚:

漢車車士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HR10,HO40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HR9-19
    7. HR19-13
    8. CK41-42
    9. HR10-3
    10. HR13-33 check
    11. CK42-41
    12. HR3-1 mate

.. _漢車車士楚車:

漢車車士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR14,HR44,HO59,CK52,CR2

.. janggi-moves::

    1. HR44-54 check
    2. CK52-42
    3. HR14-44 check
    4. CK42-31 big
    5. HR44-34 check
    6. CK31-42
    7. HR54-44 check
    8. CK42-52
    9. HR34-38
    10. CR2-9 check
    11. HK39-40
    12. CR9-10 check
    13. HK40-49
    14. CR10-9 check
    15. HK49-48
    16. CR9-2
    17. HR38-40
    18. CR2-42
    19. HR40-58 check
    20. CR42-53
    21. HR58-68
    22. CK52-51
    23. HR68-62
    24. CR53-33
    25. HK48-49
    26. HK49-50
    27. HO59-49
    28. CR33-53
    29. HR44-34
    30. HR62-61 check
    31. CK51-42
    32. HR61-31 check
    33. CK42-52
    34. HR34-32 check
    35. CR53-42
    36. HR31-42 mate

.. janggi-board::

    HK39,HR12,HR40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 check
    2. HR12-19
    3. CR9-19 mate

.. _漢車車士楚包:

漢車車士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR39,HR59,HO50,CK41,CC32

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49 check
    4. CK51-42 big
    5. HK49-60
    6. CC32-52 check
    7. HR59-69
    8. CK42-53 big
    9. HK60-49
    10. CK53-42 big
    11. HK49-40
    12. CC52-32 check
    13. HR39-29
    14. CK42-33 big
    15. HK40-49
    16. CK33-42 big
    17. HK49-60
    18. CC32-52
    19. HR69-63
    20. HR29-39
    21. HR39-33 check
    22. CK42-41
    23. HR63-43 mate

.. janggi-board::

    HK39,HR48,HR49,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-33 mate

.. _漢車車士楚馬:

漢車車士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR61,HR70,HO40,CK42,CH25

.. janggi-moves::

    1. HK48-38
    2. CH25-17 check
    3. HK38-39
    4. CK42-32 big
    5. HK39-49
    6. CK32-42 big
    7. HK49-60
    8. CK42-52 big
    9. HK60-50
    10. CH17-38 check
    11. HK50-49
    12. CK52-42 big
    13. HK49-39
    14. CH38-26
    15. HR61-68
    16. HR68-48 check
    17. CK42-31 big
    18. HK39-49
    19. CH26-34
    20. HK49-59
    21. HR70-50
    22. HR48-38
    23. HR38-34 mate

.. janggi-board::

    HK38,HR39,HR49,HO48,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢車車士楚象:

漢車車士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR20,HR80,HO40,CK41,CE4

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. CK51-41 big
    9. HO40-49
    10. CE4-27 check
    11. HK50-40
    12. CK41-31 big
    13. HO49-39
    14. CE27-44
    15. HR20-30
    16. HR30-22
    17. HR80-71 check
    18. CE44-61
    19. HR71-61 mate

.. janggi-board::

    HK48,HR58,HR49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢車車士楚卒:

漢車車士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR3,HR10,HO40,CK42,CP28

.. janggi-moves::

    1. HK38-39
    2. CP28-29 check
    3. HK39-38
    4. CK42-32 big
    5. HK38-49
    6. CK32-42 big
    7. HK49-60
    8. CK42-52 big
    9. HK60-50
    10. CK52-42 big
    11. HO40-49
    12. CP29-39
    13. HR3-9
    14. HR9-39
    15. HR39-29
    16. HR29-22 check
    17. CK42-31
    18. HR10-1 mate

.. _漢車車士楚士:

漢車車士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HR20,HO40,CK42,CO31

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-51 big
    7. HK60-50
    8. HR10-9
    9. HR9-39
    10. CK51-41 big
    11. HR39-49 check
    12. CK41-51
    13. HR20-11
    14. HR11-31 check
    15. CK51-52
    16. HR49-59 mate

.. _漢車車楚車士:

漢車車楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HR12,HR18,CK53,CR1,CO32

.. janggi-moves::

    1. HR12-13 check
    2. CK53-42
    3. HR18-48 check
    4. CK42-31
    5. HR13-63
    6. CO32-42 big
    7. HR48-38 check
    8. CK31-41
    9. HR63-61 check
    10. CO42-51
    11. HR38-48 check
    12. CK41-31 big
    13. HK40-49
    14. CK31-32
    15. HK49-58
    16. CR1-3
    17. HR61-51
    18. CR3-33
    19. HR51-52 check
    20. CK32-31
    21. HR48-49
    22. HK58-59
    23. HK59-60
    24. HR52-58
    25. HR49-69
    26. CK31-42
    27. HR69-62 check
    28. CK42-31
    29. HR62-61 check
    30. CK31-32
    31. HR61-21
    32. CR33-42
    33. HR58-38 check
    34. CR42-33
    35. HR21-22 check
    36. CK32-31
    37. HR38-33 check
    38. CK31-41
    39. HR33-42 mate

.. _漢車車楚包士:

漢車車楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR39,HR10,CK41,CC61,CO32

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CC61-31
    5. HR39-29
    6. CK51-41 big
    7. HK49-60
    8. CO32-42
    9. HK60-50
    10. CK41-51
    11. HR29-49
    12. CC31-61
    13. HR10-8
    14. CC61-41
    15. HR8-58 check
    16. CO42-52 check
    17. HK50-40
    18. HR49-43
    19. CC41-61
    20. HR43-33 check
    21. CK51-41
    22. HR58-48 check
    23. CO52-42
    24. HR33-42 mate

.. janggi-board::

    HK38,HR48,HR49,CK51,CC61,CO32

.. janggi-moves::

    1. CC61-31 mate

.. _漢車車楚馬士:

漢車車楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HR30,CK42,CH34,CO31

.. janggi-moves::

    1. HK48-38
    2. CH34-46 check
    3. HK38-39
    4. CH46-58 check
    5. HK39-40
    6. CK42-32 big
    7. HK40-49
    8. CK32-42 big
    9. HK49-59
    10. CH58-46
    11. HR10-8
    12. CH46-67 check
    13. HR8-68
    14. CK42-51 big
    15. HK59-49
    16. CK51-42 big
    17. HK49-40
    18. CK42-32 big
    19. HR68-38 check
    20. CK32-42
    21. HR30-25
    22. CK42-41
    23. HR38-49 check
    24. CK41-51
    25. HR25-45
    26. HR49-58 check
    27. CH67-55
    28. HR58-55 mate

.. _漢車車楚象士:

漢車車楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR49,HR40,CK41,CE17,CO31

.. janggi-moves::

    1. HK48-38 check
    2. CO31-42
    3. HR40-39
    4. CK41-31 big
    5. HK38-48 check
    6. CO42-32
    7. HR49-38
    8. CK31-41 big
    9. HK48-58
    10. CK41-51 big
    11. HK58-49
    12. HK49-50
    13. CK51-41 big
    14. HR38-48 check
    15. CK41-31
    16. HR48-43
    17. HR39-29
    18. CO32-33
    19. HR43-33 check
    20. CK31-41 big
    21. HK50-60
    22. HR29-21 mate

.. _漢車車楚卒卒:

漢車車楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR60,HR80,CK41,CP18,CP68

.. janggi-moves::

    1. HK48-38
    2. CP18-28 check
    3. HK38-39
    4. CK41-31 big
    5. HK39-49
    6. CP28-38 check
    7. HK49-59
    8. CP68-69 check
    9. HK59-58
    10. CP38-48 check
    11. HK58-48
    12. CK31-41 big
    13. HK48-38
    14. CK41-31 big
    15. HK38-49
    16. CK31-41 big
    17. HK49-40
    18. CK41-42
    19. HR60-49 check
    20. CK42-31 big
    21. HR49-38 check
    22. CK31-41
    23. HR38-33
    24. HR80-50 mate

.. _漢車車楚卒士:

漢車車楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR20,HR30,CK41,CP37,CO31

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-42 big
    5. HK49-40
    6. CP37-38
    7. HR20-19
    8. CO31-32
    9. HR19-59
    10. HR59-58
    11. HR58-38
    12. CO32-33
    13. HR30-23
    14. CO33-32
    15. HR38-48 check
    16. CK42-52
    17. HR23-43
    18. HR48-58 mate

.. _漢車車楚士士:

漢車車楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HR40,CK42,CO31,CO32

.. janggi-moves::

    1. HK48-38
    2. CK42-33 big
    3. HK38-49 check
    4. CK33-42 big
    5. HK49-60
    6. CK42-51 big
    7. HK60-50
    8. CK51-41 big
    9. HR40-49 check
    10. CK41-51
    11. HR1-3
    12. CO31-42
    13. HR3-63
    14. HR63-61 check
    15. CK51-52
    16. HR49-79
    17. CK52-53
    18. HR61-63 check
    19. CK53-52
    20. HR63-62 check
    21. CK52-51
    22. HR79-71 mate

.. _漢車包包士楚:

漢車包包士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR81,HC71,HC83,HO50,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-42
    3. HO50-40
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. CK51-41 big
    11. HO40-49
    12. CK41-42
    13. HR81-82 check
    14. CK42-53
    15. HK50-40
    16. HR82-32
    17. HR32-33 check
    18. CK53-52
    19. HC83-23
    20. HC23-43
    21. HC43-50
    22. HR33-42 mate

.. _漢車包包楚士:

漢車包包楚士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR15,HC33,HC25,CK52,CO31

.. janggi-moves::

    1. HR15-12 check
    2. CK52-53
    3. HK50-40
    4. CK53-43
    5. HR12-13
    6. CK43-42
    7. HC33-3
    8. CK42-32 big
    9. HK40-50
    10. CK32-42 big
    11. HC3-43
    12. HR13-23
    13. CO31-32
    14. HK50-40
    15. CO32-31
    16. HC25-22
    17. CK42-32 big
    18. HK40-49
    19. CK32-42
    20. HC22-29
    21. CO31-32
    22. HK49-39
    23. CO32-31
    24. HC29-59
    25. HK39-49
    26. CO31-32
    27. HK49-60
    28. HC43-13
    29. HR23-24
    30. HR24-14
    31. HC13-20
    32. HR14-44 check
    33. CK42-31
    34. HC20-70
    35. HC70-50
    36. HK60-49
    37. CO32-33
    38. HC59-39 check
    39. CO33-43
    40. HR44-43
    41. HR43-33 mate

.. _漢車包馬士楚:

漢車包馬士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR21,HC22,HH42,HO50,CK43

.. janggi-moves::

    1. HK48-38
    2. CK43-42
    3. HO50-40
    4. CK42-32 big
    5. HK38-49
    6. CK32-42 big
    7. HK49-60
    8. CK42-52 big
    9. HK60-50
    10. CK52-42 big
    11. HO40-49
    12. HR21-61
    13. HR61-62 check
    14. CK42-33
    15. HC22-72
    16. HC72-42
    17. HO49-48
    18. CK33-43
    19. HC42-44
    20. HC44-49 check
    21. CK43-33
    22. HR62-42 mate

.. _漢車包馬楚士:

漢車包馬楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR8,HC9,HH51,CK33,CO31

.. janggi-moves::

    1. HK38-49
    2. CK33-42 big
    3. HH51-43
    4. CK42-43 big
    5. HR8-48 check
    6. CK43-33
    7. HK49-59
    8. CK33-32
    9. HC9-69
    10. CO31-42
    11. HR48-38 check
    12. CO42-33
    13. HC69-39
    14. CK32-42
    15. HC39-33
    16. CK42-51 big
    17. HK59-49
    18. CK51-42 big
    19. HK49-40
    20. HR38-37
    21. HC33-38
    22. HR37-47 check
    23. CK42-31
    24. HK40-49
    25. HK49-58
    26. HC38-68
    27. HC68-48
    28. HK58-49
    29. HC48-50
    30. HR47-37 mate

.. _漢車包象士楚:

漢車包象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR21,HC23,HE43,HO50,CK42

.. janggi-moves::

    1. HK48-58
    2. CK42-52 big
    3. HK58-49
    4. CK52-42
    5. HO50-40
    6. CK42-43 big
    7. HK49-60
    8. CK43-42
    9. HR21-61
    10. HR61-63
    11. HC23-73
    12. HC73-43
    13. HK60-50
    14. HO40-49
    15. HO49-48
    16. HC43-49 check
    17. CK42-32
    18. HR63-53
    19. HR53-42 mate

.. _漢車包象楚士:

漢車包象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR60,HC69,HE43,CK31,CO52

.. janggi-moves::

    1. HK38-49
    2. CK31-42
    3. HK49-40
    4. CK42-33 big
    5. HR60-38 check
    6. CK33-43
    7. HR38-48 check
    8. CK43-33 big
    9. HK40-49
    10. CK33-32
    11. HK49-59
    12. CO52-42
    13. HR48-38 check
    14. CO42-33
    15. HC69-39
    16. CK32-42
    17. HC39-33
    18. CK42-51 big
    19. HK59-49
    20. CK51-42 big
    21. HK49-40
    22. HR38-37
    23. HC33-38
    24. HR37-47 check
    25. CK42-31
    26. HK40-49
    27. HK49-58
    28. HC38-68
    29. HC68-48
    30. HK58-49
    31. HC48-50
    32. HR47-37 mate

.. _漢車包兵兵楚:

漢車包兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR3,HC52,HP2,HP7,CK42

.. janggi-moves::

    1. HK50-40
    2. CK42-32 big
    3. HK40-49
    4. CK32-42 big
    5. HR3-43 check
    6. CK42-43 big
    7. HK49-59
    8. CK43-53 big
    9. HC52-54
    10. HP2-12
    11. HP12-22
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-47
    16. HP47-46
    17. HP46-45
    18. HP45-44
    19. CK53-42
    20. HC54-60
    21. HP44-54
    22. HP54-53 check
    23. CK42-33
    24. HK59-49
    25. HC60-52
    26. HC52-12
    27. HC12-42
    28. HC42-50
    29. HP53-43 mate

.. _漢車包兵士楚:

漢車包兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR9,HC43,HP7,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-42
    3. HO49-39
    4. CK42-43 big
    5. HK48-38
    6. HR9-8
    7. HP7-17
    8. CK43-33 big
    9. HK38-49
    10. HR8-48
    11. HR48-41
    12. HP17-27
    13. HP27-37
    14. HR41-51 check
    15. CK33-32
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. HR51-33 mate

.. _漢車包兵楚卒:

漢車包兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR81,HC29,HP51,CK43,CP36

.. janggi-moves::

    1. HK48-58
    2. CP36-37
    3. HR81-83 check
    4. CK43-42
    5. HR83-82 check
    6. CK42-51 big
    7. HK58-49
    8. CK51-41 big
    9. HK49-40
    10. CP37-38
    11. HR82-89
    12. CK41-31
    13. HK40-50
    14. CK31-41 big
    15. HK50-60
    16. HR89-59
    17. CP38-48
    18. HR59-39
    19. HC29-59
    20. CP48-58
    21. HC59-57
    22. CP58-48
    23. HK60-59
    24. CK41-51
    25. HR39-19
    26. CK51-41
    27. HR19-18
    28. CP48-49 check
    29. HK59-58
    30. HR18-38
    31. HR38-37
    32. HC57-27
    33. HC27-47
    34. CP49-59
    35. HK58-48
    36. HR37-36
    37. HR36-46 check
    38. CK41-31
    39. HC47-50
    40. CP59-49
    41. HK48-49
    42. HR46-36 mate

.. _漢車包兵楚士:

漢車包兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR69,HC21,HP31,CK33,CO51

.. janggi-moves::

    1. HK38-48
    2. CK33-42 big
    3. HK48-58
    4. CK42-53 big
    5. HK58-49
    6. CO51-42
    7. HR69-63 check
    8. CK53-52
    9. HR63-61
    10. CK52-53
    11. HK49-40
    12. HR61-71
    13. HP31-41
    14. CK53-43
    15. HR71-73 check
    16. CO42-53
    17. HR73-74
    18. CO53-42
    19. HR74-44 check
    20. CK43-33 big
    21. HK40-50
    22. HK50-60
    23. HC21-71
    24. CK33-32
    25. HR44-34 check
    26. CO42-33
    27. HP41-51
    28. HC71-41
    29. CK32-42
    30. HR34-54
    31. CK42-31
    32. HK60-49
    33. CK31-42 big
    34. HR54-44 check
    35. CK42-31
    36. HC41-45
    37. HR44-41 check
    38. CK31-32
    39. HC45-50
    40. HR41-21
    41. CO33-43
    42. HR21-23
    43. HR23-43
    44. HR43-42 mate

.. _漢車包士士楚:

漢車包士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR60,HC61,HO40,HO50,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-39
    6. CK41-31 big
    7. HR60-38 check
    8. CK31-42
    9. HR38-48 check
    10. CK42-31 big
    11. HK39-49
    12. HK49-60
    13. HR48-68
    14. HC61-70
    15. CK31-41
    16. HR68-58
    17. HR58-53
    18. HO50-49
    19. HC70-50 mate

.. _漢車包士楚車:

漢車包士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR34,HC42,HO39,CK51,CR9

.. janggi-moves::

    1. HR34-31 check
    2. CK51-52
    3. HR31-32
    4. CR9-39
    5. HC42-22 check
    6. CK52-51
    7. HR32-39
    8. CK51-41 big
    9. HK48-38
    10. HR39-59
    11. CK41-31 big
    12. HK38-49
    13. CK31-41 big
    14. HK49-60
    15. CK41-42
    16. HR59-49 check
    17. CK42-51 big
    18. HK60-50
    19. HK50-40
    20. HR49-29
    21. CK51-41
    22. HC22-30
    23. CK41-31 big
    24. HR29-39 check
    25. CK31-41
    26. HR39-38
    27. HC30-50
    28. HR38-48 check
    29. CK41-31 big
    30. HK40-49
    31. HR48-38 mate

.. _漢車包士楚包:

漢車包士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HC1,HO40,CK42,CC41

.. janggi-moves::

    1. HK48-38
    2. CK42-31 big
    3. HK38-49
    4. CK31-42 big
    5. HK49-60
    6. CK42-53 big
    7. HK60-50
    8. CK53-43 big
    9. HO40-49
    10. CC41-49
    11. HR10-3 check
    12. CK43-42
    13. HR3-2 check
    14. CK42-43
    15. HR2-32
    16. HK50-40
    17. HR32-31
    18. CC49-42
    19. HC1-51
    20. CC42-50
    21. HR31-34
    22. CC50-42
    23. HR34-54
    24. HC51-55
    25. HR54-34
    26. HR34-35
    27. HC55-25
    28. CC42-44
    29. HK40-49
    30. CK43-42
    31. HC25-45
    32. CC44-41
    33. HR35-25
    34. HR25-22 check
    35. CK42-43
    36. HR22-32
    37. HR32-31
    38. CC41-44
    39. HC45-50
    40. HR31-41 check
    41. CK43-33
    42. HR41-44
    43. HR44-34 mate

.. _漢車包士楚馬:

漢車包士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR88,HC33,HO50,CK32,CH23

.. janggi-moves::

    1. HR88-83
    2. CK32-42 big
    3. HK48-38
    4. CK42-32
    5. HR83-53
    6. CH23-15
    7. HO50-60
    8. CH15-34
    9. HR53-43
    10. CH34-55
    11. HR43-73
    12. CH55-76
    13. HK38-49
    14. CK32-42 big
    15. HK49-40
    16. CH76-64
    17. HR73-63
    18. CH64-43
    19. HC33-53
    20. CH43-55
    21. HR63-73
    22. CH55-36
    23. HC53-83
    24. CH36-28 check
    25. HK40-39
    26. CK42-32 big
    27. HC83-33
    28. CH28-47 check
    29. HK39-49
    30. CK32-42
    31. HC33-83
    32. CH47-28 big
    33. HK49-38
    34. CK42-32 big
    35. HC83-33
    36. CH28-36
    37. HK38-49
    38. CH36-28 check
    39. HK49-48
    40. CK32-42 big
    41. HK48-38
    42. CH28-7
    43. HK38-39
    44. CH7-19
    45. HO60-49
    46. HR73-63
    47. HC33-73
    48. CK42-32 big
    49. HO49-38
    50. CH19-38
    51. HK39-49
    52. CH38-57 check
    53. HK49-60
    54. CK32-42
    55. HR63-68
    56. HR68-48 check
    57. CK42-31
    58. HR48-44
    59. CH57-65
    60. HR44-64
    61. CH65-46
    62. HR64-74
    63. CK31-41
    64. HC73-80
    65. CK41-51 big
    66. HK60-50
    67. CH46-38 check
    68. HK50-49
    69. CH38-57 check
    70. HK49-40
    71. CK51-41
    72. HR74-44 check
    73. CK41-31 big
    74. HK40-50
    75. CH57-36
    76. HR44-46
    77. CH36-17
    78. HK50-60
    79. CH17-29
    80. HR46-49
    81. CH29-17
    82. HC80-50
    83. HR49-19
    84. CH17-25
    85. HR19-29
    86. CH25-17
    87. HR29-27
    88. CH17-29
    89. HR27-29
    90. CK31-41
    91. HR29-28
    92. HR28-48 check
    93. CK41-31
    94. HK60-49
    95. HR48-38 mate

.. _漢車包士楚象:

漢車包士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR20,HC12,HO49,CK31,CE54

.. janggi-moves::

    1. HK38-48
    2. CK31-41 big
    3. HK48-58
    4. CE54-86 check
    5. HK58-59
    6. CK41-51 big
    7. HO49-58
    8. CE86-58
    9. HR20-13
    10. CE58-35 big
    11. HR13-53 check
    12. CK51-41
    13. HR53-23
    14. CK41-51 big
    15. HK59-49
    16. CK51-42 big
    17. HK49-38
    18. CK42-32
    19. HR23-22 check
    20. CK32-31
    21. HC12-42
    22. CE35-3 big
    23. HK38-48
    24. CK31-41
    25. HR22-32
    26. CE3-26
    27. HR32-33
    28. HK48-38
    29. HK38-39
    30. HR33-32
    31. HC42-22
    32. CE26-43
    33. HK39-40
    34. HR32-34
    35. CE43-66
    36. HR34-44 check
    37. CK41-31 big
    38. HK40-50
    39. HK50-60
    40. HR44-24
    41. CK31-41
    42. HC22-30
    43. CE66-38
    44. HR24-44 check
    45. CK41-51 big
    46. HK60-50
    47. CE38-55
    48. HK50-40
    49. CE55-27
    50. HR44-34
    51. HR34-37
    52. CK51-41
    53. HR37-27
    54. CK41-31 big
    55. HR27-37 check
    56. CK31-41
    57. HC30-50
    58. HR37-47 check
    59. CK41-31 big
    60. HK40-49
    61. HR47-37 mate

.. _漢車包士楚卒:

漢車包士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR9,HC19,HO49,CK41,CP27

.. janggi-moves::

    1. HC19-59
    2. CP27-28 check
    3. HK38-39
    4. CK41-31 big
    5. HO49-38
    6. CP28-38 check
    7. HK39-40
    8. CK31-41
    9. HR9-19
    10. HC59-9
    11. CP38-48
    12. HR19-39
    13. HC9-69
    14. HR39-59
    15. HC69-39
    16. CP48-38
    17. HC39-37
    18. CP38-48
    19. HK40-39
    20. CK41-31
    21. HR59-79
    22. CK31-41
    23. HR79-78
    24. CP48-49 check
    25. HK39-38
    26. HR78-58
    27. HR58-57
    28. HC37-67
    29. HC67-47
    30. CP49-59
    31. HK38-48
    32. HR57-59
    33. HR59-56
    34. HR56-46 check
    35. CK41-31
    36. HC47-49
    37. HR46-36 mate

.. _漢車包士楚士:

漢車包士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR81,HC43,HO50,CK42,CO31

.. janggi-moves::

    1. HR81-83
    2. CK42-32 big
    3. HK38-48
    4. CK32-42
    5. HO50-60
    6. CO31-32
    7. HK48-38
    8. CK42-33 big
    9. HK38-49
    10. CK33-42
    11. HK49-40
    12. CO32-31
    13. HK40-50
    14. CK42-32
    15. HR83-73
    16. HC43-83
    17. HR73-74
    18. CK32-42 big
    19. HO60-49
    20. CK42-41
    21. HR74-84
    22. CO31-32
    23. HC83-90
    24. CO32-33
    25. HR84-44 check
    26. CK41-51
    27. HR44-54 check
    28. CK51-41
    29. HK50-60
    30. CO33-42
    31. HC90-50
    32. CK41-31
    33. HO49-39
    34. HR54-34 check
    35. CO42-32
    36. HO39-49
    37. HO49-59
    38. HC50-70
    39. HC70-40
    40. HC40-32
    41. HC32-40 check
    42. CK31-41
    43. HR34-44 check
    44. CK41-31
    45. HO59-49
    46. HO49-39 mate

.. _漢車包楚車士:

漢車包楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HR8,HC9,CK43,CR48,CO31

.. janggi-moves::

    1. HR8-48 check
    2. CK43-33
    3. HK49-59
    4. CK33-32
    5. HC9-69
    6. CO31-42
    7. HR48-38 check
    8. CO42-33
    9. HC69-39
    10. CK32-42
    11. HC39-33
    12. CK42-51 big
    13. HK59-49
    14. CK51-42 big
    15. HK49-40
    16. HR38-37
    17. HC33-38
    18. HR37-47 check
    19. CK42-31
    20. HK40-49
    21. HK49-58
    22. HC38-68
    23. HC68-48
    24. HK58-49
    25. HC48-50
    26. HR47-37 mate

.. _漢車包楚包士:

漢車包楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HR48,HC8,CK41,CC42,CO33

.. janggi-moves::

    1. HK49-59
    2. CO33-32
    3. HC8-58
    4. CK41-31
    5. HC58-38 check
    6. CK31-41
    7. HR48-47
    8. CO32-33
    9. HK59-58
    10. CK41-51 big
    11. HR47-57 check
    12. CK51-41
    13. HR57-53
    14. CO33-32
    15. HC38-68
    16. CC42-22
    17. HC68-48
    18. CC22-62
    19. HK58-49
    20. CC62-22
    21. HR53-43 check
    22. CC22-42
    23. HK49-58
    24. CK41-31
    25. HR43-53
    26. CK31-41
    27. HR53-52
    28. CO32-33
    29. HR52-54
    30. CK41-31
    31. HR54-34
    32. CK31-32
    33. HC48-68
    34. CC42-22
    35. HC68-38
    36. CK32-42
    37. HC38-33
    38. CK42-51 big
    39. HK58-49
    40. CK51-42 big
    41. HK49-39
    42. HC33-38
    43. HR34-44 check
    44. CK42-32
    45. HK39-49
    46. CC22-52
    47. HK49-58
    48. CK32-33
    49. HC38-68
    50. CC52-59
    51. HC68-48
    52. CC59-53
    53. HK58-49
    54. CC53-23
    55. HR44-41
    56. HC48-50
    57. HR41-31 mate

.. _漢車包楚馬士:

漢車包楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR74,HC62,CK52,CH34,CO32

.. janggi-moves::

    1. HC62-32
    2. CH34-46 check
    3. HK38-49
    4. CK52-51
    5. HK49-40
    6. CK51-41
    7. HR74-71 check
    8. CK41-42
    9. HC32-82
    10. CH46-67
    11. HR71-74
    12. CH67-59 check
    13. HK40-39
    14. CH59-47 check
    15. HK39-49
    16. CH47-55 big
    17. HR74-44 check
    18. CK42-33
    19. HR44-54
    20. CK33-43 big
    21. HK49-60
    22. CH55-67
    23. HR54-74
    24. CK43-53 big
    25. HK60-49
    26. CH67-55
    27. HR74-72
    28. CK53-43 big
    29. HK49-58
    30. CH55-47
    31. HR72-73 check
    32. CK43-42
    33. HR73-79
    34. CH47-66 check
    35. HK58-59
    36. CH66-47 check
    37. HK59-49
    38. CK42-41
    39. HK49-40
    40. CK41-31 big
    41. HK40-50
    42. CH47-68
    43. HR79-75
    44. HR75-45
    45. HR45-48
    46. CH68-80
    47. HK50-49
    48. HK49-58
    49. CH80-68
    50. HR48-47
    51. CH68-89
    52. HR47-87
    53. CH89-70 check
    54. HK58-59
    55. CK31-41
    56. HK59-60
    57. CK41-51 big
    58. HK60-50
    59. CH70-78
    60. HR87-89
    61. CH78-70
    62. HR89-88
    63. CK51-41 big
    64. HR88-48 check
    65. CK41-31
    66. HK50-60
    67. HR48-88
    68. HC82-90
    69. CK31-41
    70. HR88-48 check
    71. CK41-31
    72. HK60-50
    73. HR48-38 check
    74. CK31-41 big
    75. HK50-40
    76. CH70-89
    77. HR38-49 check
    78. CK41-31 big
    79. HR49-39 check
    80. CK31-41
    81. HR39-89
    82. HR89-49 check
    83. CK41-31 big
    84. HK40-50
    85. HK50-60
    86. HR49-48
    87. HC90-50
    88. HK60-49
    89. HR48-38 mate

.. _漢車包楚象士:

漢車包楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR31,HC12,CK52,CE35,CO33

.. janggi-moves::

    1. HR31-32 check
    2. CK52-51
    3. HR32-33 check
    4. CK51-41 big
    5. HR33-43 check
    6. CK41-31
    7. HR43-23
    8. CK31-42 big
    9. HK50-40
    10. CK42-32
    11. HR23-22 check
    12. CK32-31
    13. HC12-42
    14. CE35-3 big
    15. HK40-50
    16. CE3-26
    17. HR22-12
    18. CE26-9
    19. HK50-49
    20. CE9-26 check
    21. HK49-60
    22. CK31-41
    23. HR12-32
    24. HK60-50
    25. HK50-40
    26. HC42-22
    27. CE26-54
    28. HC22-52
    29. CE54-82
    30. HR32-33
    31. CE82-65
    32. HR33-53
    33. CE65-37
    34. HK40-39
    35. CE37-65
    36. HK39-38
    37. CE65-88
    38. HC52-58
    39. CE88-60
    40. HC58-18
    41. CE60-28
    42. HR53-55
    43. HK38-39
    44. CK41-31 big
    45. HK39-49
    46. CE28-56
    47. HR55-56
    48. HK49-58
    49. HR56-46
    50. HC18-68
    51. HC68-48
    52. HK58-49
    53. HC48-50
    54. HR46-36 mate

.. _漢車包楚卒卒:

漢車包楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR70,HC68,CK41,CP27,CP56

.. janggi-moves::

    1. HK48-38
    2. CP27-37 check
    3. HK38-39
    4. CP37-47
    5. HR70-50
    6. CK41-31 big
    7. HK39-49
    8. CK31-41
    9. HK49-60
    10. CP56-57
    11. HR50-49
    12. CK41-51
    13. HK60-50
    14. CK51-41
    15. HK50-40
    16. HR49-38
    17. HC68-28
    18. HC28-88
    19. HR38-68
    20. CP47-37
    21. HR68-66
    22. CP37-47
    23. HR66-46 check
    24. CK41-31 big
    25. HK40-49
    26. CP47-37
    27. HR46-48
    28. HC88-8
    29. HR48-28
    30. CP37-47
    31. HK49-60
    32. CK31-41
    33. HR28-26
    34. CP57-67
    35. HK60-50
    36. CP67-57
    37. HR26-46 check
    38. CK41-51
    39. HR46-6
    40. CP47-37
    41. HC8-5
    42. CP37-38
    43. HR6-36
    44. CK51-41 big
    45. HR36-46 check
    46. CK41-31
    47. HK50-60
    48. CP38-28
    49. HK60-59
    50. HR46-48
    51. CP28-29
    52. HR48-38 check
    53. CK31-41
    54. HR38-37
    55. CP57-67
    56. HR37-67
    57. HK59-58
    58. HR67-57
    59. HR57-55
    60. HR55-35
    61. HC5-45
    62. HK58-48
    63. HR35-34
    64. CP29-39
    65. HR34-44 check
    66. CK41-31
    67. HC45-50
    68. CP39-49
    69. HK48-49
    70. HR44-34 mate

.. _漢車包楚卒士:

漢車包楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR62,HC72,CK33,CP57,CO32

.. janggi-moves::

    1. HC72-32
    2. CP57-58
    3. HK50-60
    4. CK33-43
    5. HR62-69
    6. CK43-53
    7. HR69-39
    8. CP58-48 big
    9. HR39-59 check
    10. CK53-43
    11. HR59-69
    12. HK60-50
    13. HK50-40
    14. CP48-38
    15. HR69-59
    16. CK43-42
    17. HC32-62
    18. CK42-31
    19. HR59-53 check
    20. CK31-32
    21. HK40-50
    22. HC62-22
    23. HC22-42
    24. HR53-43
    25. CK32-31
    26. HR43-44
    27. HC42-45
    28. CP38-48
    29. HK50-60
    30. CP48-58
    31. HR44-54
    32. HR54-58
    33. CK31-41
    34. HK60-49
    35. HR58-54
    36. HR54-44 check
    37. CK41-31
    38. HC45-50
    39. HR44-34 mate

.. _漢車包楚士士:

漢車包楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR40,HC33,CK32,CO31,CO42

.. janggi-moves::

    1. HC33-31 check
    2. CO42-33
    3. HC31-33
    4. CK32-42 big
    5. HK48-58
    6. CK42-51 big
    7. HK58-49
    8. CK51-42 big
    9. HK49-60
    10. HR40-37
    11. CK42-51 big
    12. HK60-50
    13. CK51-42 big
    14. HK50-40
    15. HC33-38
    16. HR37-47 check
    17. CK42-31
    18. HK40-49
    19. HK49-58
    20. HC38-68
    21. HC68-48
    22. HK58-49
    23. HC48-50
    24. HR47-37 mate

.. _漢車馬馬士楚:

漢車馬馬士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR10,HH61,HH9,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-41 big
    3. HK48-58
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK58-48
    8. CK52-53
    9. HH9-28
    10. HR10-7
    11. HR7-47
    12. HH28-36
    13. HH36-55
    14. HH55-63
    15. HR47-42 mate

.. _漢車馬馬楚士:

漢車馬馬楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR11,HH31,HH51,CK32,CO41

.. janggi-moves::

    1. HK38-49
    2. CO41-51
    3. HK49-60
    4. CO51-41
    5. HH31-12
    6. CK32-42
    7. HH12-24
    8. CK42-51 big
    9. HK60-50
    10. CK51-42 big
    11. HK50-40
    12. CK42-33 big
    13. HH24-36
    14. CO41-42
    15. HK40-50
    16. CK33-43 big
    17. HK50-60
    18. HR11-12
    19. HH36-55 check
    20. CK43-53
    21. HK60-50
    22. HK50-40
    23. CK53-52
    24. HH55-63
    25. CO42-32
    26. HR12-32 check
    27. CK52-53
    28. HR32-42 mate

.. _漢車馬象士楚:

漢車馬象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR50,HH61,HE10,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-41 big
    3. HK48-58
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK58-59
    8. HE10-38
    9. HE38-66
    10. HE66-83
    11. HO49-58
    12. CK52-53
    13. HR50-41
    14. HR41-51 check
    15. CK53-43
    16. HE83-66 mate

.. _漢車馬象楚士:

漢車馬象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR82,HH72,HE1,CK32,CO31

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CK42-52 big
    5. HH72-53 check
    6. CK52-53 big
    7. HK60-50
    8. CK53-43 big
    9. HK50-40
    10. CO31-42
    11. HR82-84
    12. HR84-44 check
    13. CK43-53
    14. HE1-24
    15. HR44-54 check
    16. CK53-43
    17. HE24-56
    18. HR54-24
    19. CK43-53
    20. HR24-23 check
    21. CO42-43
    22. HR23-22
    23. CO43-42
    24. HE56-88
    25. HE88-65
    26. CO42-31
    27. HR22-23 check
    28. CK53-52
    29. HR23-21
    30. CO31-32
    31. HR21-22
    32. HR22-32 check
    33. CK52-51
    34. HR32-42 mate

.. _漢車馬兵兵楚:

漢車馬兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HH42,HP4,HP87,CK43

.. janggi-moves::

    1. HK48-38
    2. CK43-42
    3. HR1-51 check
    4. CK42-51
    5. HP4-14
    6. HP14-24
    7. HP24-34
    8. CK51-42
    9. HP34-44
    10. HK38-49
    11. HK49-60
    12. HP44-54
    13. HP87-77
    14. HP77-67
    15. HP67-66
    16. HP66-65
    17. HP65-64
    18. HP64-63
    19. HP63-53 check
    20. CK42-31
    21. HP54-44
    22. HP44-43
    23. HP43-42 mate

.. _漢車馬兵士楚:

漢車馬兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR63,HH73,HP87,HO49,CK32

.. janggi-moves::

    1. HK38-48
    2. CK32-42 big
    3. HK48-58
    4. CK42-52 big
    5. HR63-53 check
    6. CK52-53 big
    7. HH73-54
    8. HP87-77
    9. HP77-67
    10. HP67-57
    11. HH54-73
    12. HH73-61 check
    13. CK53-43
    14. HP57-56
    15. HP56-55
    16. HP55-54
    17. HP54-53 check
    18. CK43-33
    19. HH61-42
    20. HH42-21 check
    21. CK33-32
    22. HP53-42 mate

.. _漢車馬兵楚卒:

漢車馬兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR14,HH21,HP5,CK32,CP24

.. janggi-moves::

    1. HK38-49
    2. CP24-14
    3. HK49-60
    4. HH21-2
    5. CP14-24
    6. HH2-23
    7. CK32-33
    8. HP5-4
    9. HP4-3
    10. HH23-4
    11. CK33-42
    12. HH4-16
    13. CK42-51 big
    14. HK60-50
    15. CK51-41 big
    16. HK50-40
    17. CK41-31 big
    18. HH16-37
    19. HP3-13
    20. CK31-32
    21. HP13-23
    22. CP24-34
    23. HK40-50
    24. CK32-42 big
    25. HK50-60
    26. HH37-45
    27. CK42-51 big
    28. HK60-50
    29. CP34-44
    30. HH45-26
    31. CK51-42
    32. HH26-14
    33. CP44-34 big
    34. HK50-40
    35. CP34-24
    36. HH14-26
    37. CK42-32 big
    38. HK40-50
    39. CK32-42 big
    40. HH26-45
    41. CK42-41
    42. HK50-40
    43. CP24-34
    44. HP23-33
    45. CP34-44
    46. HH45-64
    47. HH64-72
    48. HH72-53 check
    49. CK41-51
    50. HH53-61
    51. HP33-42 mate

.. _漢車馬兵楚士:

漢車馬兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR68,HH51,HP65,CK41,CO31

.. janggi-moves::

    1. HH51-43
    2. CK41-42
    3. HP65-55
    4. CK42-43 big
    5. HP55-45
    6. CO31-42
    7. HP45-44 check
    8. CK43-33
    9. HR68-58
    10. CO42-43
    11. HR58-52
    12. CO43-42
    13. HR52-55
    14. CO42-43
    15. HR55-25
    16. CO43-53
    17. HR25-22
    18. CO53-42
    19. HK48-58
    20. CO42-53
    21. HP44-34 check
    22. CK33-43
    23. HR22-23 check
    24. CK43-42
    25. HP34-33 check
    26. CK42-51
    27. HK58-49
    28. HK49-40
    29. HR23-24
    30. HR24-54
    31. CO53-52
    32. HP33-43
    33. HR54-64
    34. CK51-41
    35. HR64-62
    36. HR62-52
    37. CK41-31 big
    38. HK40-50
    39. HR52-42 mate

.. _漢車馬士士楚:

漢車馬士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HH80,HO40,HO50,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HH80-59
    8. HK60-49
    9. HR10-8
    10. HR8-48
    11. HH59-78
    12. HH78-66
    13. HH66-54
    14. HR48-42 mate

.. _漢車馬士楚車:

漢車馬士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR6,HH32,HO59,CK52,CR69

.. janggi-moves::

    1. HH32-44 check
    2. CK52-53
    3. HR6-56 check
    4. CK53-43
    5. HH44-23
    6. CR69-70
    7. HR56-46 check
    8. CK43-33 big
    9. HR46-36 check
    10. CK33-43
    11. HH23-35 check
    12. CK43-53
    13. HR36-56 check
    14. CK53-42
    15. HH35-54 check
    16. CK42-53
    17. HO59-60
    18. CR70-68 check
    19. HK38-49
    20. CR68-28
    21. HR56-46
    22. CR28-29 check
    23. HK49-58
    24. CR29-22
    25. HR46-41
    26. CK53-52
    27. HK58-59
    28. CR22-32
    29. HK59-49
    30. CK52-53
    31. HK49-50
    32. HO60-49
    33. HR41-61
    34. HR61-63 check
    35. CK53-52
    36. HR63-62 check
    37. CK52-51
    38. HR62-32
    39. HR32-42 mate

.. _漢車馬士楚包:

漢車馬士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR1,HH9,HO40,CK42,CC2

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-52 big
    7. HK60-50
    8. CC2-10 check
    9. HH9-30
    10. CK52-42 big
    11. HK50-60
    12. CK42-52 big
    13. HK60-49
    14. CK52-42 big
    15. HK49-39
    16. HR1-2 check
    17. CK42-31 big
    18. HH30-38
    19. HK39-49
    20. CK31-41 big
    21. HH38-46
    22. CC10-50
    23. HK49-50
    24. HK50-60
    25. HH46-54
    26. HR2-42 mate

.. _漢車馬士楚馬:

漢車馬士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR90,HH89,HO60,CK31,CH26

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-40
    4. CK41-31 big
    5. HK40-50
    6. CH26-38 check
    7. HK50-49
    8. CH38-57 check
    9. HK49-58
    10. CH57-78
    11. HR90-80
    12. CH78-66 check
    13. HK58-49
    14. CK31-41 big
    15. HK49-40
    16. CK41-31 big
    17. HK40-50
    18. CH66-58 check
    19. HO60-59
    20. CK31-41 big
    21. HK50-60
    22. CH58-39 check
    23. HO59-49
    24. CK41-51 big
    25. HK60-50
    26. CH39-47
    27. HR80-76
    28. HR76-56 check
    29. CK51-41
    30. HR56-46 check
    31. CK41-31
    32. HR46-47
    33. HR47-43
    34. HR43-33 check
    35. CK31-41
    36. HH89-77
    37. HH77-65
    38. HH65-53 mate

.. _漢車馬士楚象:

漢車馬士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR90,HH88,HO50,CK41,CE34

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-41 big
    5. HK49-39
    6. CK41-31
    7. HO50-49
    8. CE34-66 big
    9. HO49-38
    10. CE66-38
    11. HK39-49
    12. CK31-41 big
    13. HK49-40
    14. CE38-66
    15. HH88-67
    16. CK41-31 big
    17. HK40-50
    18. CK31-41 big
    19. HH67-46
    20. CE66-34
    21. HK50-60
    22. CE34-51
    23. HR90-80
    24. HR80-73
    25. HR73-53
    26. HH46-54
    27. HR53-42 mate

.. _漢車馬士楚卒:

漢車馬士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR12,HH10,HO39,CK41,CP27

.. janggi-moves::

    1. HO39-40
    2. CP27-28 check
    3. HK38-39
    4. CK41-31 big
    5. HK39-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. CK51-41 big
    11. HO40-49
    12. CP28-38
    13. HR12-19
    14. CP38-28
    15. HR19-9
    16. HR9-8
    17. HR8-28
    18. HR28-48 check
    19. CK41-31
    20. HH10-18
    21. HH18-26
    22. HH26-34
    23. HR48-42 mate

.. _漢車馬士楚士:

漢車馬士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HH78,HO39,CK32,CO31

.. janggi-moves::

    1. HK38-48
    2. CK32-42 big
    3. HK48-58 check
    4. CK42-51 big
    5. HH78-57
    6. HK58-59
    7. HR49-58
    8. CK51-41
    9. HH57-78
    10. HH78-66
    11. CO31-42
    12. HR58-48
    13. CK41-31
    14. HH66-54
    15. CO42-51
    16. HR48-38 check
    17. CK31-41
    18. HR38-28
    19. CK41-31
    20. HR28-21 check
    21. CK31-32
    22. HR21-51
    23. HR51-42 mate

.. _漢車馬楚車士:

漢車馬楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR86,HH52,CK43,CR81,CO51

.. janggi-moves::

    1. HH52-31 check
    2. CK43-33 big
    3. HR86-36 check
    4. CK33-42
    5. HH31-23 check
    6. CK42-43
    7. HH23-35 check
    8. CK43-53
    9. HR36-56 check
    10. CK53-42
    11. HH35-54 check
    12. CK42-31 big
    13. HR56-36 check
    14. CK31-41
    15. HH54-62 check
    16. CK41-42
    17. HH62-81
    18. HH81-62
    19. HH62-54 check
    20. CK42-41
    21. HR36-46 check
    22. CK41-31 big
    23. HH54-35
    24. HR46-36
    25. CK31-41
    26. HH35-23
    27. CO51-42
    28. HR36-46
    29. CO42-43
    30. HR46-43 check
    31. CK41-51
    32. HR43-42 mate

.. _漢車馬楚包士:

漢車馬楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR10,HH11,CK42,CC33,CO31

.. janggi-moves::

    1. HK50-60
    2. CK42-53 big
    3. HK60-49
    4. CK53-43 big
    5. HK49-39
    6. CC33-53
    7. HR10-70
    8. CO31-32
    9. HR70-68
    10. CO32-42
    11. HR68-63
    12. CK43-33 big
    13. HK39-49
    14. CK33-43 big
    15. HK49-60
    16. CK43-33
    17. HH11-3
    18. CO42-52
    19. HH3-24
    20. CK33-42
    21. HH24-45
    22. HR63-66
    23. HK60-50
    24. CC53-31
    25. HR66-46
    26. CO52-53
    27. HR46-47
    28. HH45-24 check
    29. CK42-51
    30. HH24-36
    31. CO53-42
    32. HH36-55
    33. CC31-53
    34. HH55-34
    35. CO42-32
    36. HH34-53
    37. CO32-42
    38. HH53-61
    39. CO42-31
    40. HK50-40
    41. HH61-82
    42. CK51-52
    43. HH82-63
    44. CK52-53
    45. HR47-57 check
    46. CK53-43
    47. HR57-51
    48. HH63-55 mate

.. _漢車馬楚馬士:

漢車馬楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HH71,CK42,CH51,CO31

.. janggi-moves::

    1. HK48-58
    2. CK42-53 big
    3. HK58-49
    4. CH51-72
    5. HK49-50
    6. CO31-42
    7. HR9-3 check
    8. CO42-43
    9. HR3-2
    10. CO43-42
    11. HK50-40
    12. HR2-3 check
    13. CO42-43
    14. HR3-4
    15. CK53-42
    16. HR4-74
    17. CK42-31 big
    18. HK40-50
    19. CH72-53
    20. HR74-84
    21. HH71-63
    22. HH63-55
    23. CO43-33
    24. HK50-40
    25. HH55-36
    26. HR84-81 check
    27. CK31-32
    28. HR81-86
    29. CK32-31
    30. HH36-44
    31. CO33-42 big
    32. HR86-36 check
    33. CK31-41
    34. HR36-26
    35. CK41-31 big
    36. HK40-50
    37. CO42-32
    38. HR26-46
    39. CK31-42
    40. HH44-65 check
    41. CK42-52
    42. HH65-84
    43. HK50-40
    44. HH84-63
    45. CO32-31
    46. HR46-43
    47. CO31-42
    48. HR43-23
    49. CO42-31
    50. HR23-22 check
    51. CH53-32
    52. HK40-50
    53. HR22-21
    54. CH32-44
    55. HH63-44 check
    56. CK52-42
    57. HR21-25
    58. HR25-45
    59. HH44-63 check
    60. CK42-53
    61. HK50-40
    62. HR45-55 check
    63. CK53-43
    64. HR55-51
    65. HH63-55 mate

.. _漢車馬楚象士:

漢車馬楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HH71,CK31,CE44,CO42

.. janggi-moves::

    1. HK38-48
    2. CE44-72
    3. HR49-29
    4. CK31-41
    5. HK48-38
    6. CK41-31 big
    7. HK38-49
    8. CK31-41
    9. HK49-60
    10. CO42-51
    11. HR29-22
    12. CO51-42
    13. HR22-21 check
    14. CO42-31
    15. HR21-27
    16. CO31-42
    17. HR27-47
    18. HK60-50
    19. CK41-51
    20. HK50-40
    21. CK51-41
    22. HR47-77
    23. CE72-55
    24. HK40-50
    25. CE55-23
    26. HK50-60
    27. CK41-31
    28. HR77-37 check
    29. CK31-41
    30. HR37-36
    31. CK41-51 big
    32. HR36-56 check
    33. CK51-41
    34. HH71-63
    35. HR56-26
    36. CO42-53
    37. HR26-23
    38. CK41-42
    39. HR23-21
    40. CK42-32
    41. HR21-51
    42. CO53-43
    43. HH63-44
    44. HH44-23
    45. HR51-31 mate

.. _漢車馬楚卒卒:

漢車馬楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HH30,CK31,CP19,CP37

.. janggi-moves::

    1. HR10-6
    2. CP19-29
    3. HR6-36 check
    4. CK31-41 big
    5. HR36-46 check
    6. CK41-31
    7. HH30-9
    8. CP37-27
    9. HR46-16
    10. CK31-41 big
    11. HK48-58
    12. HK58-59
    13. CK41-51 big
    14. HK59-49
    15. CK51-41 big
    16. HK49-60
    17. CK41-31
    18. HK60-50
    19. HR16-36 check
    20. CK31-41 big
    21. HR36-46 check
    22. CK41-31
    23. HR46-49
    24. HR49-29
    25. CK31-41 big
    26. HR29-49 check
    27. CK41-31
    28. HR49-47
    29. HR47-27
    30. HR27-47
    31. HH9-30
    32. HH30-38
    33. HH38-46
    34. HH46-54
    35. HR47-42 mate

.. _漢車馬楚卒士:

漢車馬楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR21,HH20,CK41,CP28,CO31

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-42 big
    5. HK49-40
    6. CK42-32 big
    7. HH20-39
    8. CP28-38
    9. HH39-60
    10. CP38-28 big
    11. HK40-49
    12. CP28-38 check
    13. HK49-58
    14. CK32-42
    15. HR21-24
    16. HR24-54
    17. HH60-79
    18. HK58-59
    19. HH79-87
    20. HH87-75
    21. CO31-41
    22. HR54-64
    23. HR64-63
    24. HH75-54 check
    25. CK42-52
    26. HK59-60
    27. HK60-50
    28. HR63-33
    29. CP38-49 check
    30. HK50-49
    31. HH54-73 mate

.. _漢車馬楚士士:

漢車馬楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR50,HH86,CK52,CO31,CO41

.. janggi-moves::

    1. HK48-38
    2. CK52-51
    3. HR50-60 check
    4. CK51-42
    5. HH86-74
    6. CK42-32 big
    7. HK38-49
    8. CK32-42 big
    9. HK49-40
    10. CO41-51
    11. HR60-50 check
    12. CK42-32 big
    13. HK40-49
    14. CO31-42
    15. HR50-30
    16. CO42-33
    17. HH74-55
    18. CK32-42 big
    19. HK49-60
    20. CO33-32
    21. HR30-22
    22. HH55-63 check
    23. CK42-53 big
    24. HK60-50
    25. CK53-43 big
    26. HH63-44
    27. CO32-31
    28. HK50-60
    29. CK43-53 big
    30. HH44-56
    31. CO51-42
    32. HR22-24
    33. CO42-32
    34. HR24-64
    35. CK53-52
    36. HK60-50
    37. CK52-42 big
    38. HK50-40
    39. CK42-41
    40. HR64-61 check
    41. CK41-42
    42. HH56-35
    43. CO32-33
    44. HR61-63
    45. CK42-32
    46. HR63-64
    47. CO31-42
    48. HH35-23
    49. CO42-41
    50. HR64-14
    51. CK32-42
    52. HH23-35
    53. CK42-51
    54. HR14-64
    55. CO41-42
    56. HR64-61 check
    57. CK51-52
    58. HH35-54
    59. CO42-43
    60. HR61-41
    61. CO43-42
    62. HH54-73 check
    63. CK52-53
    64. HR41-61
    65. CK53-43
    66. HR61-66
    67. CO42-51
    68. HH73-54
    69. CO33-42
    70. HH54-35 check
    71. CK43-33
    72. HR66-36
    73. CO42-53
    74. HH35-54 check
    75. CK33-42
    76. HR36-33 check
    77. CK42-41
    78. HR33-53
    79. CO51-52
    80. HH54-33 check
    81. CK41-51
    82. HR53-52 mate

.. _漢車象象士楚:

漢車象象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HE41,HE43,HO50,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-41
    3. HR49-69
    4. CK41-42
    5. HR69-63
    6. HK48-49
    7. HO50-40
    8. HK49-50
    9. HO40-49
    10. HE43-75
    11. HR63-43 check
    12. CK42-31
    13. HR43-53 check
    14. CK31-32
    15. HE75-47
    16. HE47-64 mate

.. _漢車象象楚士:

漢車象象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR51,HE52,HE53,CK43,CO31

.. janggi-moves::

    1. HK48-38
    2. CO31-42
    3. HR51-61
    4. CO42-53
    5. HE52-84
    6. CK43-33 big
    7. HK38-49
    8. CK33-42 big
    9. HK49-60
    10. HR61-65
    11. HR65-45 check
    12. CK42-51
    13. HK60-50
    14. HE84-67
    15. HE67-35
    16. CO53-42
    17. HE35-18
    18. CK51-52
    19. HK50-40
    20. HE18-46
    21. CK52-53
    22. HE46-74
    23. CO42-31
    24. HR45-65
    25. HR65-63 check
    26. CK53-52
    27. HR63-61
    28. CK52-53
    29. HR61-31 check
    30. CK53-43
    31. HR31-42 mate

.. _漢車象兵兵楚:

漢車象兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR73,HE83,HP1,HP7,CK32

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CK42-52 big
    5. HR73-53 check
    6. CK52-53 big
    7. HE83-55
    8. HP7-17
    9. HP17-27
    10. HK60-50
    11. HK50-40
    12. HP27-37
    13. HE55-27
    14. HE27-44
    15. HE44-16
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. CK53-42
    20. HP34-33 check
    21. CK42-31
    22. HE16-48
    23. HE48-65
    24. HK40-50
    25. HP33-42 mate

.. _漢車象兵士楚:

漢車象兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR73,HE83,HP7,HO49,CK32

.. janggi-moves::

    1. HK38-48
    2. CK32-42 big
    3. HK48-58
    4. CK42-52 big
    5. HR73-53 check
    6. CK52-53 big
    7. HE83-55
    8. HK58-59
    9. HP7-17
    10. HP17-27
    11. HP27-37
    12. HO49-58
    13. HE55-27
    14. HE27-44
    15. HE44-16
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. HP34-33
    20. HE16-48
    21. HE48-25 check
    22. CK53-52
    23. HP33-42 mate

.. _漢車象兵楚卒:

漢車象兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR73,HE83,HP85,CK32,CP4

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CK42-52 big
    5. HR73-53 check
    6. CK52-53 big
    7. HE83-55
    8. CP4-14
    9. HK60-50
    10. CK53-42 big
    11. HK50-40
    12. CP14-24
    13. HE55-38
    14. CP24-34
    15. HE38-66
    16. CP34-44
    17. HP85-75
    18. HE66-49
    19. HK40-50
    20. HP75-65
    21. CP44-54
    22. HK50-60
    23. HE49-26
    24. CP54-44
    25. HP65-55
    26. HE26-58
    27. HE58-90
    28. HE90-67
    29. HE67-50
    30. HE50-27
    31. CP44-34
    32. HP55-45
    33. HK60-50
    34. HE27-55
    35. HE55-38
    36. HE38-6
    37. CP34-24
    38. HK50-40
    39. HP45-35
    40. HE6-38
    41. HE38-70
    42. HE70-47
    43. HE47-30
    44. HE30-7
    45. CP24-14
    46. HP35-34
    47. HE7-39
    48. CP14-15
    49. HE39-56
    50. HE56-88
    51. CK42-51
    52. HE88-65
    53. CP15-25
    54. HK40-49
    55. HP34-44
    56. CK51-52
    57. HE65-48
    58. CP25-35
    59. HP44-43
    60. CP35-45
    61. HE48-76
    62. CK52-51
    63. HP43-53
    64. CP45-46
    65. HK49-59
    66. HE76-44
    67. HE44-16
    68. HK59-60
    69. HE16-33
    70. CP46-47
    71. HK60-50
    72. CP47-48
    73. HE33-65
    74. CP48-49 check
    75. HK50-49
    76. HP53-42 mate

.. _漢車象兵楚士:

漢車象兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR68,HE53,HP14,CK42,CO41

.. janggi-moves::

    1. HK48-38
    2. CK42-53
    3. HP14-24
    4. CO41-42
    5. HP24-34
    6. CK53-43
    7. HR68-48 check
    8. CK43-53
    9. HR48-58 check
    10. CK53-43
    11. HP34-44 check
    12. CK43-33 big
    13. HK38-49
    14. CO42-43
    15. HR58-28
    16. CO43-53
    17. HR28-22
    18. CO53-42
    19. HK49-60
    20. CO42-53
    21. HP44-34 check
    22. CK33-43
    23. HR22-23 check
    24. CK43-42
    25. HP34-33 check
    26. CK42-51
    27. HK60-50
    28. HK50-40
    29. HR23-24
    30. HR24-54
    31. CO53-52
    32. HP33-43
    33. HR54-64
    34. CK51-41
    35. HR64-62
    36. HR62-52
    37. CK41-31 big
    38. HK40-50
    39. HR52-42 mate

.. _漢車象士士楚:

漢車象士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR2,HE10,HO40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. HR2-32
    9. CK51-41 big
    10. HO40-49
    11. HR32-33
    12. HE10-38
    13. HE38-70
    14. HE70-87
    15. HE87-64 mate

.. _漢車象士楚車:

漢車象士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR43,HE55,HO38,CK51,CR24

.. janggi-moves::

    1. HE55-83 check
    2. CK51-52
    3. HR43-41
    4. CR24-29 check
    5. HK39-40
    6. CR29-30 check
    7. HK40-49
    8. CK52-53
    9. HE83-66
    10. CK53-52
    11. HE66-34
    12. CR30-29 check
    13. HK49-48
    14. CK52-53
    15. HR41-51 check
    16. CK53-43 big
    17. HK48-58
    18. CR29-26
    19. HE34-11 check
    20. CR26-21
    21. HR51-21
    22. CK43-53 big
    23. HK58-49
    24. CK53-42 big
    25. HK49-40
    26. HR21-22 check
    27. CK42-31
    28. HE11-34
    29. HE34-51
    30. HE51-74
    31. HR22-42 mate

.. _漢車象士楚包:

漢車象士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR59,HE50,HO39,CK42,CC32

.. janggi-moves::

    1. HK48-38
    2. CK42-33 big
    3. HK38-49
    4. CK33-42 big
    5. HK49-60
    6. CC32-52 check
    7. HR59-69
    8. CK42-53 big
    9. HK60-49
    10. CK53-42 big
    11. HK49-40
    12. CC52-32 check
    13. HO39-49
    14. CK42-33 big
    15. HO49-38
    16. HK40-49
    17. HR69-67
    18. CK33-42 big
    19. HO38-48
    20. HR67-47 check
    21. CK42-31
    22. HR47-45
    23. HE50-78
    24. HE78-46
    25. HE46-14 mate

.. janggi-board::

    HK39,HR49,HE48,HO38,CK32,CC31

.. janggi-moves::

    1. CK32-42 mate

.. _漢車象士楚馬:

漢車象士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR90,HE63,HO50,CK51,CH26

.. janggi-moves::

    1. HK38-39
    2. CH26-47 check
    3. HK39-49
    4. CH47-55
    5. HE63-35
    6. CK51-42 big
    7. HK49-58
    8. CH55-47
    9. HE35-67
    10. CK42-51 big
    11. HK58-49
    12. CH47-55
    13. HE67-84
    14. CK51-41 big
    15. HK49-58
    16. CH55-76
    17. HE84-61
    18. CK41-51 big
    19. HK58-48
    20. CK51-41 big
    21. HE61-44
    22. CH76-57
    23. HR90-81 check
    24. CK41-42
    25. HR81-82 check
    26. CK42-41
    27. HK48-58
    28. CH57-65
    29. HE44-16
    30. CH65-46 check
    31. HK58-49
    32. CH46-25 big
    33. HK49-40
    34. CH25-37
    35. HE16-33
    36. CH37-45
    37. HE33-1
    38. CK41-31 big
    39. HK40-49
    40. CK31-41
    41. HR82-88
    42. CH45-37 big
    43. HK49-40
    44. CH37-25
    45. HR88-38
    46. HE1-24 check
    47. CK41-42
    48. HR38-35
    49. CH25-4
    50. HE24-56
    51. CH4-12
    52. HE56-88
    53. CK42-41
    54. HE88-65
    55. CH12-4
    56. HR35-33
    57. CH4-23
    58. HR33-23
    59. CK41-31 big
    60. HR23-33 check
    61. CK31-41
    62. HR33-42 mate

.. _漢車象士楚象:

漢車象士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR81,HE43,HO50,CK42,CE21

.. janggi-moves::

    1. HR81-83
    2. CE21-4
    3. HK48-58
    4. CK42-52 big
    5. HK58-49
    6. CK52-42
    7. HO50-40
    8. CE4-32
    9. HK49-60
    10. CK42-52 big
    11. HK60-50
    12. CK52-42
    13. HO40-49
    14. CE32-55
    15. HR83-73
    16. CE55-78 check
    17. HK50-40
    18. HK40-39
    19. HE43-26
    20. CK42-32 big
    21. HO49-38
    22. CE78-46
    23. HE26-9
    24. CK32-42
    25. HE9-37
    26. CK42-52
    27. HR73-76
    28. CE46-74
    29. HR76-74
    30. HR74-44
    31. HE37-65
    32. HR44-54 mate

.. _漢車象士楚卒:

漢車象士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR59,HE16,HO49,CK42,CP26

.. janggi-moves::

    1. HE16-39
    2. CK42-31 big
    3. HK38-48
    4. CK31-41 big
    5. HK48-58
    6. CK41-51 big
    7. HE39-56
    8. CP26-36
    9. HK58-48
    10. CK51-41 big
    11. HK48-38
    12. CP36-46
    13. HE56-79
    14. HR59-55
    15. HK38-48
    16. HR55-45 check
    17. CK41-31
    18. HR45-46
    19. HR46-43
    20. HE79-47
    21. HE47-64
    22. HR43-53 mate

.. _漢車象士楚士:

漢車象士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR60,HE81,HO40,CK42,CO31

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-39
    6. CK42-32 big
    7. HR60-38 check
    8. CK32-42
    9. HR38-48 check
    10. CK42-32 big
    11. HK39-49
    12. CO31-42
    13. HK49-50
    14. HR48-28
    15. CO42-31
    16. HR28-23
    17. CK32-42 big
    18. HO40-49
    19. CK42-41
    20. HR23-63
    21. HE81-53
    22. HR63-61 check
    23. CK41-42
    24. HE53-25 check
    25. CK42-32
    26. HR61-64
    27. CK32-33
    28. HR64-34 check
    29. CK33-43
    30. HR34-31
    31. HR31-42 mate

.. _漢車象楚車士:

漢車象楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR5,HE13,CK43,CR21,CO33

.. janggi-moves::

    1. HR5-45 check
    2. CK43-53
    3. HE13-36 check
    4. CK53-52
    5. HR45-55 check
    6. CK52-42
    7. HR55-53 check
    8. CK42-32
    9. HE36-4 check
    10. CR21-23
    11. HK38-49
    12. HK49-60
    13. HR53-51
    14. HR51-21
    15. CK32-42
    16. HR21-23
    17. CK42-51 big
    18. HK60-50
    19. CK51-41 big
    20. HK50-40
    21. CK41-42
    22. HR23-25
    23. HR25-45 check
    24. CK42-31
    25. HK40-50
    26. HE4-27
    27. HE27-55
    28. CO33-42
    29. HE55-78
    30. CK31-32
    31. HK50-60
    32. HE78-46
    33. CK32-33
    34. HE46-14
    35. CO42-51
    36. HR45-25
    37. HR25-23 check
    38. CK33-32
    39. HR23-21
    40. CK32-33
    41. HR21-51 check
    42. CK33-32
    43. HR51-31 mate

.. _漢車象楚包士:

漢車象楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR80,HE16,CK42,CC61,CO31

.. janggi-moves::

    1. HK48-38
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-40
    6. CK42-32 big
    7. HE16-39
    8. CO31-41
    9. HR80-72 check
    10. CK32-33
    11. HK40-50
    12. CK33-43 big
    13. HK50-60
    14. CK43-53 big
    15. HE39-56
    16. CC61-31
    17. HK60-50
    18. CC31-51
    19. HE56-73
    20. HE73-45
    21. HR72-74
    22. CO41-42
    23. HK50-40
    24. HR74-44
    25. HE45-68
    26. HE68-85 check
    27. CK53-52
    28. HR44-64
    29. CC51-53
    30. HE85-57
    31. CO42-33
    32. HE57-74
    33. CK52-42
    34. HR64-44 check
    35. CK42-52
    36. HR44-41
    37. HR41-31
    38. CO33-43
    39. HR31-32 check
    40. CO43-42
    41. HR32-2
    42. HR2-3
    43. HE74-46
    44. HK40-50
    45. HE46-14
    46. CK52-51
    47. HR3-1 check
    48. CC53-31
    49. HR1-2
    50. CO42-33
    51. HR2-5
    52. CO33-32
    53. HR5-55 check
    54. CK51-41 big
    55. HR55-45 check
    56. CK41-51
    57. HR45-43
    58. CC31-35
    59. HE14-37
    60. CK51-52
    61. HE37-65
    62. CC35-31
    63. HR43-63
    64. CK52-51
    65. HR63-53 check
    66. CK51-41 big
    67. HK50-60
    68. CC31-61
    69. HR53-52
    70. CO32-31
    71. HR52-72
    72. CK41-51 big
    73. HK60-50
    74. CC61-41
    75. HK50-40
    76. HE65-48
    77. CO31-42
    78. HE48-25
    79. CO42-33
    80. HE25-57
    81. CC41-61
    82. HE57-74 check
    83. CK51-41
    84. HR72-71
    85. CK41-31
    86. HR71-61 check
    87. CK31-32
    88. HR61-64
    89. HR64-34
    90. HK40-50
    91. HR34-44
    92. HE74-46
    93. HE46-14
    94. HR44-41
    95. HR41-31 mate

.. _漢車象楚馬士:

漢車象楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR81,HE85,CK42,CH78,CO33

.. janggi-moves::

    1. HK50-60
    2. CK42-52 big
    3. HK60-49
    4. CK52-42 big
    5. HK49-39
    6. CH78-66
    7. HR81-82 check
    8. CK42-43
    9. HR82-83 check
    10. CK43-42
    11. HE85-53
    12. CH66-47 check
    13. HK39-49
    14. CH47-35 big
    15. HK49-38
    16. CH35-54
    17. HE53-21
    18. CH54-46 check
    19. HK38-49
    20. CH46-25 big
    21. HK49-40
    22. CK42-31
    23. HR83-84
    24. CH25-13
    25. HR84-81 check
    26. CK31-42
    27. HR81-82 check
    28. CK42-31
    29. HR82-12
    30. CO33-42 big
    31. HK40-50
    32. CH13-25
    33. HR12-15
    34. CH25-33
    35. HR15-35
    36. HK50-40
    37. HR35-34
    38. HE21-4
    39. HE4-27
    40. HE27-44
    41. HE44-67
    42. CK31-41
    43. HR34-44
    44. HE67-35
    45. HE35-3
    46. CK41-51
    47. HR44-64
    48. CO42-43
    49. HR64-54 check
    50. CK51-41
    51. HR54-56
    52. HR56-46
    53. CO43-42
    54. HR46-16
    55. CK41-51
    56. HR16-11 check
    57. CO42-41
    58. HE3-35
    59. CH33-54
    60. HE35-67
    61. CH54-33
    62. HR11-1
    63. CK51-42
    64. HR1-4
    65. CO41-51
    66. HE67-35
    67. CO51-52
    68. HR4-2 check
    69. CK42-41
    70. HR2-1 check
    71. CK41-42
    72. HE35-3
    73. CH33-41
    74. HR1-31 check
    75. CK42-51
    76. HR31-34
    77. CO52-42
    78. HE3-35
    79. CH41-22
    80. HR34-24
    81. CH22-43
    82. HR24-21 check
    83. CO42-31
    84. HE35-63
    85. HR21-11
    86. CK51-41
    87. HE63-46
    88. CH43-22
    89. HR11-15
    90. CO31-42
    91. HE46-74
    92. CH22-43
    93. HR15-12
    94. CH43-64
    95. HK40-50
    96. CO42-32 big
    97. HK50-60
    98. CH64-72
    99. HR12-13
    100. CO32-42
    101. HR13-83
    102. CH72-51
    103. HR83-82
    104. CO42-53
    105. HE74-46
    106. CO53-42
    107. HE46-14
    108. CO42-33
    109. HR82-22
    110. HR22-21 check
    111. CK41-42
    112. HR21-31 check
    113. CK42-52 big
    114. HK60-50
    115. CO33-32
    116. HR31-41
    117. CH51-72
    118. HE14-46
    119. CO32-42
    120. HR41-61
    121. CH72-53
    122. HE46-18
    123. CO42-32
    124. HR61-41
    125. CO32-42
    126. HE18-35 check
    127. CH53-34
    128. HR41-61
    129. CO42-43
    130. HR61-31
    131. CH34-55
    132. HK50-40
    133. HR31-33
    134. HR33-23
    135. CK52-42
    136. HR23-24
    137. CK42-41
    138. HR24-54
    139. CH55-36
    140. HR54-44
    141. CK41-42
    142. HK40-50
    143. CH36-55
    144. HE35-18
    145. CH55-36
    146. HR44-34
    147. CH36-55
    148. HR34-54
    149. CH55-67
    150. HR54-44
    151. CH67-59
    152. HK50-49
    153. CH59-78
    154. HR44-47
    155. CH78-66
    156. HR47-48
    157. HE18-46
    158. CO43-53
    159. HE46-29 check
    160. CK42-31
    161. HK49-60
    162. CK31-32
    163. HR48-38 check
    164. CK32-42
    165. HE29-57
    166. CK42-51
    167. HK60-50
    168. CK51-41 big
    169. HR38-48 check
    170. CK41-31
    171. HE57-25
    172. CO53-52
    173. HK50-60
    174. CH66-54
    175. HR48-44
    176. CH54-42
    177. HR44-43
    178. HR43-13
    179. HR13-11 check
    180. CK31-32
    181. HR11-16
    182. CK32-31
    183. HR16-56
    184. CH42-54
    185. HR56-54
    186. CO52-42
    187. HE25-57
    188. HE57-74
    189. HR54-34 check
    190. CO42-32
    191. HR34-44
    192. CO32-33
    193. HE74-46
    194. HE46-14 check
    195. CK31-32
    196. HR44-41
    197. HR41-31 mate

.. _漢車象楚象士:

漢車象楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HR11,HE41,CK42,CE57,CO32

.. janggi-moves::

    1. HK40-39
    2. CK42-33 big
    3. HK39-49
    4. CK33-42 big
    5. HK49-60
    6. CE57-34
    7. HR11-21
    8. CK42-51 big
    9. HK60-49
    10. CK51-42 big
    11. HK49-39
    12. CE34-62
    13. HE41-13
    14. CE62-85
    15. HR21-24
    16. CE85-53
    17. HR24-44 check
    18. CK42-31
    19. HR44-45
    20. HK39-49
    21. CE53-81
    22. HE13-36
    23. CO32-42
    24. HR45-75
    25. CE81-53
    26. HE36-64
    27. CO42-52
    28. HR75-45
    29. HK49-60
    30. HR45-35 check
    31. CK31-41
    32. HR35-33
    33. CE53-81
    34. HE64-81
    35. CO52-42
    36. HR33-35
    37. CO42-43
    38. HK60-50
    39. HR35-45
    40. CK41-42
    41. HE81-64
    42. CK42-33
    43. HE64-87
    44. HE87-55
    45. HE55-78
    46. HE78-46
    47. HK50-60
    48. HE46-14
    49. CO43-53
    50. HR45-25
    51. CO53-52
    52. HR25-23 check
    53. CK33-32
    54. HR23-22 check
    55. CK32-33
    56. HR22-52
    57. HR52-42 mate

.. _漢車象楚卒卒:

漢車象楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR11,HE70,CK43,CP8,CP57

.. janggi-moves::

    1. HK48-38
    2. CK43-33 big
    3. HK38-49
    4. CK33-42 big
    5. HK49-59
    6. CK42-52
    7. HR11-12 check
    8. CK52-53
    9. HK59-49
    10. CK53-43 big
    11. HK49-39
    12. CK43-33 big
    13. HE70-38
    14. CP57-58
    15. HR12-14
    16. CP8-18
    17. HR14-34 check
    18. CK33-42
    19. HE38-66
    20. CP18-28
    21. HR34-24
    22. CK42-32 big
    23. HE66-34
    24. CP28-18
    25. HK39-40
    26. CK32-42
    27. HR24-22 check
    28. CK42-31
    29. HR22-72
    30. CK31-41
    31. HR72-71 check
    32. CK41-42
    33. HR71-51 check
    34. CK42-32
    35. HK40-50
    36. CP18-28
    37. HE34-66
    38. HE66-83
    39. CP58-49 check
    40. HK50-49
    41. CP28-38 check
    42. HK49-50
    43. CP38-49 check
    44. HK50-49
    45. HE83-55 mate

.. _漢車象楚卒士:

漢車象楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HE32,CK33,CP36,CO31

.. janggi-moves::

    1. HE32-64
    2. CP36-26 big
    3. HK38-48
    4. CK33-42 big
    5. HE64-47
    6. CP26-36
    7. HR49-39
    8. CP36-46
    9. HE47-75
    10. CP46-56 big
    11. HK48-58
    12. HK58-59
    13. CP56-66
    14. HR39-36
    15. CP66-67
    16. HR36-56
    17. HR56-55
    18. HE75-52
    19. HE52-35
    20. HR55-45 check
    21. CK42-51 big
    22. HK59-49
    23. HK49-39
    24. HE35-18
    25. CK51-52
    26. HE18-46
    27. CK52-53
    28. HE46-74
    29. CP67-57
    30. HR45-65
    31. CP57-58
    32. HR65-63 check
    33. CK53-52
    34. HR63-61
    35. CP58-49 check
    36. HK39-38
    37. CK52-53
    38. HR61-31 check
    39. CK53-43
    40. HR31-42 mate

.. _漢車象楚士士:

漢車象楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR55,HE52,CK33,CO31,CO42

.. janggi-moves::

    1. HE52-75
    2. CO42-41
    3. HK48-58
    4. CK33-32
    5. HR55-35 check
    6. CK32-42
    7. HR35-45 check
    8. CK42-51 big
    9. HR45-55 check
    10. CK51-42
    11. HR55-52 check
    12. CK42-33
    13. HR52-53 check
    14. CK33-32
    15. HE75-47
    16. CO31-42
    17. HE47-64 check
    18. CK32-31
    19. HR53-23
    20. CO42-32
    21. HR23-21 check
    22. CK31-42
    23. HR21-41 check
    24. CK42-53 big
    25. HK58-49
    26. CO32-42
    27. HR41-71
    28. CK53-43 big
    29. HE64-47
    30. HR71-74
    31. HR74-44 check
    32. CK43-33
    33. HE47-15
    34. CO42-53
    35. HR44-41
    36. CO53-42
    37. HR41-61
    38. CO42-32
    39. HR61-51 check
    40. CO32-42
    41. HR51-55
    42. CO42-43
    43. HR55-65
    44. CK33-42
    45. HR65-45
    46. HR45-43 check
    47. CK42-31
    48. HE15-47
    49. HE47-64
    50. HR43-53 mate

.. _漢車兵兵士楚:

漢車兵兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HP1,HP7,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HR10-9
    7. CK41-51 big
    8. HK60-50
    9. CK51-42 big
    10. HR9-49 check
    11. CK42-33
    12. HR49-41
    13. HP7-17
    14. HP17-27
    15. HP27-37
    16. HR41-51 check
    17. CK33-43 big
    18. HP37-47
    19. HP47-46
    20. HP46-45
    21. HP45-44 mate

.. _漢車兵兵楚車:

漢車兵兵楚車
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR8,HP24,HP67,CK33,CR32

.. janggi-moves::

    1. HP24-34 check
    2. CK33-42 big
    3. HR8-48 check
    4. CK42-51
    5. HR48-58 check
    6. CK51-41 big
    7. HR58-49 check
    8. CK41-51
    9. HP34-44
    10. CR32-72
    11. HR49-47
    12. CR72-80 check
    13. HK50-49
    14. CR80-79 check
    15. HK49-40
    16. CR79-80 check
    17. HK40-39
    18. CR80-30
    19. HP67-57
    20. CR30-29 check
    21. HK39-40
    22. CR29-30 check
    23. HK40-49
    24. CR30-29 check
    25. HK49-60
    26. CR29-30 check
    27. HK60-59
    28. CR30-29 check
    29. HR47-49
    30. CR29-27
    31. HP57-56
    32. CR27-67
    33. HR49-46
    34. CR67-69 check
    35. HK59-60
    36. CR69-70 check
    37. HK60-49
    38. CR70-69 check
    39. HK49-40
    40. CR69-70 check
    41. HK40-39
    42. CR70-69 check
    43. HR46-49
    44. CR69-62
    45. HP44-43
    46. CR62-22
    47. HR49-48
    48. CR22-29 check
    49. HK39-40
    50. CR29-30 check
    51. HK40-49
    52. CR30-29 check
    53. HK49-58
    54. CR29-22
    55. HP56-55
    56. CK51-41
    57. HR48-18
    58. CK41-51
    59. HP55-54
    60. CR22-42
    61. HR18-11 check
    62. CK51-52
    63. HR11-13
    64. HP54-53 check
    65. CR42-53 check
    66. HP43-53 check
    67. CK52-51
    68. HR13-11 mate

.. _漢車兵兵楚包:

漢車兵兵楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR34,HP14,HP87,CK41,CC4

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-41 big
    5. HK49-40
    6. CC4-34
    7. HP14-24
    8. CC34-14
    9. HP24-34
    10. CK41-42
    11. HP34-44
    12. HK40-50
    13. HP87-77
    14. HP77-67
    15. HP67-57
    16. HP57-47
    17. HP47-46
    18. HP46-45
    19. HP44-54
    20. CC14-74
    21. HP54-64
    22. CC74-54
    23. HK50-60
    24. HP64-54
    25. HP54-64
    26. HP64-63
    27. HP45-55
    28. HP55-54
    29. HP63-53 check
    30. CK42-31
    31. HP53-43
    32. HP54-53
    33. HP43-42 mate

.. _漢車兵兵楚馬:

漢車兵兵楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR60,HP1,HP82,CK41,CH40

.. janggi-moves::

    1. HK48-58
    2. CH40-19
    3. HR60-59
    4. CH19-38
    5. HR59-89
    6. CH38-46 check
    7. HK58-49
    8. CH46-67 big
    9. HK49-60
    10. CK41-42
    11. HR89-49 check
    12. CK42-53 big
    13. HR49-58 check
    14. CK53-42
    15. HR58-78
    16. CK42-51 big
    17. HK60-49
    18. CK51-41 big
    19. HK49-39
    20. CK41-31 big
    21. HR78-38 check
    22. CK31-42
    23. HR38-35
    24. CH67-48
    25. HK39-49
    26. CH48-56 big
    27. HR35-45 check
    28. CK42-33
    29. HR45-65
    30. CK33-43 big
    31. HK49-59
    32. CH56-48
    33. HP1-11
    34. CK43-53 big
    35. HK59-49
    36. CH48-56
    37. HP82-72
    38. CK53-42 big
    39. HK49-59
    40. CH56-44
    41. HR65-45
    42. CK42-53 big
    43. HK59-49
    44. CH44-23
    45. HP11-21
    46. CH23-42
    47. HP21-31
    48. CH42-54
    49. HK49-40
    50. CH54-73
    51. HP31-41
    52. HP41-51
    53. HP51-61
    54. HP61-71
    55. HP72-62
    56. HR45-75
    57. CK53-42
    58. HR75-73
    59. CK42-32 big
    60. HK40-50
    61. CK32-42 big
    62. HK50-60
    63. HR73-74
    64. HR74-54
    65. HR54-52 check
    66. CK42-43
    67. HR52-51
    68. HR51-31
    69. HP62-52
    70. HR31-42 mate

.. _漢車兵兵楚象:

漢車兵兵楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR71,HP22,HP64,CK42,CE31

.. janggi-moves::

    1. HK48-38
    2. CK42-33 big
    3. HK38-49
    4. CK33-42 big
    5. HK49-60
    6. CK42-52 big
    7. HP64-54
    8. CE31-54
    9. HR71-72 check
    10. CK52-53
    11. HR72-73 check
    12. CK53-42
    13. HP22-12
    14. CE54-37 check
    15. HK60-59
    16. CK42-52 big
    17. HK59-49
    18. CK52-42 big
    19. HK49-40
    20. HR73-74
    21. CE37-69
    22. HR74-54
    23. HR54-57
    24. HR57-47 check
    25. CK42-32 big
    26. HK40-50
    27. CK32-33
    28. HK50-60
    29. HP12-22
    30. HK60-59
    31. HR47-41
    32. CE69-46
    33. HR41-46
    34. HR46-41
    35. HR41-51 check
    36. CK33-43
    37. HP22-32
    38. HR51-42 mate

.. _漢車兵兵楚卒:

漢車兵兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR5,HP56,HP7,CK31,CP15

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HP56-46
    4. CP15-5
    5. HK49-40
    6. HP46-36
    7. HP7-17
    8. CP5-15
    9. HP17-27
    10. CP15-25
    11. HP27-37
    12. HP37-47
    13. HK40-50
    14. HP47-46
    15. HP46-45
    16. HP36-46
    17. HK50-60
    18. HP46-56
    19. HP56-66
    20. CK41-42
    21. HK60-59
    22. HP66-65
    23. HP65-64
    24. HP64-63
    25. CP25-35
    26. HP45-55
    27. CK42-31
    28. HP63-53
    29. CP35-36
    30. HP53-43
    31. CP36-37
    32. HP55-54
    33. CP37-38
    34. HP54-53
    35. CP38-49 check
    36. HK59-49
    37. HP43-42 mate

.. _漢車兵兵楚士:

漢車兵兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR10,HP1,HP83,CK42,CO33

.. janggi-moves::

    1. HK48-58
    2. CK42-43
    3. HR10-50 check
    4. CK43-53 big
    5. HK58-49
    6. CK53-42 big
    7. HK49-40 check
    8. CO33-43
    9. HR50-49
    10. CK42-33 big
    11. HR49-39 check
    12. CK33-42
    13. HK40-50
    14. CO43-53 big
    15. HK50-60
    16. CK42-43
    17. HR39-49 check
    18. CK43-33
    19. HR49-29
    20. HR29-23 check
    21. CK33-42
    22. HP1-11
    23. HP11-21
    24. HP83-73
    25. HP73-63
    26. CO53-43
    27. HR23-24
    28. HK60-50
    29. HK50-40
    30. CK42-41
    31. HR24-34
    32. CO43-42
    33. HR34-54
    34. HP63-62
    35. HP62-52
    36. CO42-52
    37. HR54-52
    38. HR52-53
    39. HR53-31 mate

.. _漢車兵士士楚:

漢車兵士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR9,HP7,HO40,HO50,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HR9-49 check
    7. CK41-51 big
    8. HR49-58 check
    9. CK51-42
    10. HR58-48 check
    11. CK42-31
    12. HR48-43
    13. HP7-17
    14. HP17-27
    15. HP27-37
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. HP34-33
    20. HR43-42 mate

.. _漢車兵士楚車:

漢車兵士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR44,HP73,HO49,CK53,CR1

.. janggi-moves::

    1. HP73-63 check
    2. CK53-52
    3. HR44-54 check
    4. CK52-42
    5. HP63-53 check
    6. CK42-31 big
    7. HR54-34 check
    8. CK31-41
    9. HR34-44 check
    10. CK41-31 big
    11. HO49-38
    12. CR1-9 check
    13. HK39-40
    14. CR9-10 check
    15. HK40-49
    16. CR10-2
    17. HO38-48
    18. CR2-62
    19. HO48-58
    20. CR62-69 check
    21. HK49-48
    22. CR69-62
    23. HP53-43
    24. CR62-32
    25. HK48-49
    26. CR32-39 check
    27. HK49-60
    28. CR39-40 check
    29. HK60-59
    30. CR40-39 check
    31. HO58-49
    32. CR39-32
    33. HK59-60
    34. CR32-62
    35. HK60-50
    36. CR62-70 check
    37. HO49-60
    38. CR70-62
    39. HR44-24
    40. CR62-12
    41. HK50-49
    42. CR12-19 check
    43. HK49-58
    44. CR19-12
    45. HR24-21 check
    46. CK31-32
    47. HK58-59
    48. CR12-19 check
    49. HO60-49
    50. CR19-29
    51. HR21-41
    52. CR29-49 check
    53. HK59-49
    54. HR41-42 mate

.. _漢車兵士楚包:

漢車兵士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR68,HP42,HO59,CK53,CC2

.. janggi-moves::

    1. HP42-32
    2. CK53-43 big
    3. HK48-58
    4. CK43-33
    5. HP32-22
    6. CK33-42
    7. HR68-88
    8. HR88-87
    9. CC2-32
    10. HO59-60
    11. HK58-59
    12. CK42-52 big
    13. HK59-49
    14. CC32-62
    15. HR87-82
    16. CK52-42 big
    17. HK49-40
    18. CK42-33 big
    19. HK40-50
    20. CC62-2
    21. HR82-84
    22. HR84-54
    23. CK33-42 big
    24. HO60-49
    25. CC2-32
    26. HR54-44 check
    27. CK42-33
    28. HP22-12
    29. HR44-41
    30. HR41-31
    31. CK33-43
    32. HR31-32
    33. HR32-52
    34. HP12-22
    35. HP22-32
    36. HR52-42 mate

.. _漢車兵士楚馬:

漢車兵士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR71,HP72,HO39,CK42,CH36

.. janggi-moves::

    1. HK48-58
    2. CK42-52 big
    3. HK58-49
    4. CH36-28 check
    5. HO39-38
    6. CK52-42 big
    7. HK49-60
    8. CK42-52 big
    9. HK60-50
    10. CK52-42 big
    11. HO38-48
    12. CH28-36
    13. HO48-49
    14. CH36-57
    15. HR71-61
    16. CH57-49
    17. HR61-66
    18. CH49-57 big
    19. HR66-46 check
    20. CK42-33
    21. HR46-56
    22. CK33-42 big
    23. HK50-60
    24. CH57-69
    25. HK60-59
    26. CH69-77
    27. HR56-55
    28. CH77-69
    29. HP72-62
    30. CH69-88
    31. HR55-45 check
    32. CK42-53 big
    33. HK59-49
    34. CH88-76
    35. HR45-44
    36. CH76-57 check
    37. HK49-40
    38. CH57-65
    39. HR44-64
    40. CH65-46
    41. HR64-68
    42. CH46-25
    43. HR68-58 check
    44. CK53-42
    45. HR58-48 check
    46. CK42-53
    47. HR48-41
    48. CH25-4
    49. HR41-1
    50. CH4-16
    51. HR1-31 check
    52. CK53-43
    53. HR31-41 check
    54. CK43-53
    55. HR41-44
    56. HR44-54 check
    57. CK53-43
    58. HP62-52
    59. CH16-28 check
    60. HK40-39
    61. CH28-47 check
    62. HK39-49
    63. CH47-28 big
    64. HK49-38
    65. CH28-36
    66. HR54-14
    67. CH36-57 check
    68. HK38-39
    69. CH57-45
    70. HK39-40
    71. HR14-44 check
    72. CK43-33 big
    73. HK40-50
    74. HR44-42 mate

.. _漢車兵士楚象:

漢車兵士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR3,HP62,HO49,CK32,CE11

.. janggi-moves::

    1. HK38-48
    2. CK32-42 big
    3. HK48-58
    4. CE11-34
    5. HP62-72
    6. CK42-52 big
    7. HK58-48
    8. CK52-42 big
    9. HK48-38
    10. CE34-66 check
    11. HK38-39
    12. CK42-32 big
    13. HO49-38
    14. CE66-38
    15. HR3-7
    16. CE38-66 big
    17. HR7-37 check
    18. CK32-42
    19. HK39-40
    20. HR37-35
    21. HP72-62
    22. HR35-45 check
    23. CE66-43
    24. HR45-55
    25. CK42-31 big
    26. HK40-50
    27. CE43-75
    28. HR55-75
    29. CK31-42 big
    30. HR75-45 check
    31. CK42-53
    32. HK50-40
    33. HR45-41
    34. HR41-51 check
    35. CK53-43
    36. HP62-52
    37. HR51-42 mate

.. _漢車兵士楚卒:

漢車兵士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR12,HP7,HO39,CK41,CP27

.. janggi-moves::

    1. HO39-40
    2. CP27-28 check
    3. HK38-39
    4. CK41-31 big
    5. HK39-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. CK51-41 big
    11. HO40-49
    12. CP28-38
    13. HR12-19
    14. HP7-17
    15. HK50-40
    16. HP17-27
    17. HP27-37
    18. HP37-36
    19. HP36-35
    20. HP35-34
    21. CK41-42
    22. HR19-12 check
    23. CK42-43
    24. HO49-38
    25. HR12-32
    26. CK43-53
    27. HP34-33
    28. HR32-42 mate

.. _漢車兵士楚士:

漢車兵士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR5,HP24,HO50,CK33,CO42

.. janggi-moves::

    1. HK48-58
    2. HK58-59
    3. CK33-43
    4. HR5-45 check
    5. CK43-53 big
    6. HR45-55 check
    7. CK53-43
    8. HP24-34
    9. CO42-31
    10. HR55-52
    11. CO31-42
    12. HR52-62
    13. CK43-53 big
    14. HK59-49
    15. HK49-40
    16. HP34-44
    17. CO42-33
    18. HP44-54 check
    19. CK53-43
    20. HR62-63 check
    21. CK43-42
    22. HP54-53 check
    23. CK42-31
    24. HK40-49
    25. HK49-60
    26. HR63-64
    27. HR64-34
    28. CO33-32
    29. HP53-43
    30. HR34-24
    31. CK31-41
    32. HR24-22
    33. HR22-32
    34. CK41-51 big
    35. HK60-49
    36. HR32-42 mate

.. _漢車兵楚車士:

漢車兵楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR62,HP64,CK53,CR69,CO41

.. janggi-moves::

    1. HP64-54 check
    2. CK53-43
    3. HP54-44 check
    4. CK43-33 big
    5. HP44-34 check
    6. CK33-43
    7. HR62-69
    8. CO41-42
    9. HR69-49 check
    10. CK43-53
    11. HR49-59 check
    12. CK53-43
    13. HP34-44 check
    14. CK43-33 big
    15. HK38-48
    16. CO42-43
    17. HR59-29
    18. CO43-53
    19. HR29-22
    20. CO53-42
    21. HK48-58
    22. CO42-53
    23. HP44-34 check
    24. CK33-43
    25. HR22-23 check
    26. CK43-42
    27. HP34-33 check
    28. CK42-51
    29. HK58-49
    30. HK49-40
    31. HR23-24
    32. HR24-54
    33. CO53-52
    34. HP33-43
    35. HR54-64
    36. CK51-41
    37. HR64-62
    38. HR62-52
    39. CK41-31 big
    40. HK40-50
    41. HR52-42 mate

.. _漢車兵楚包士:

漢車兵楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK50,HR58,HP64,CK33,CC39,CO31

.. janggi-moves::

    1. HP64-54
    2. CK33-42 big
    3. HK50-40
    4. CK42-33
    5. HR58-28
    6. CO31-42
    7. HR28-38 check
    8. CC39-34 check
    9. HR38-34 check
    10. CK33-43
    11. HR34-24
    12. CK43-33 big
    13. HK40-50
    14. CK33-43 big
    15. HR24-44 check
    16. CK43-33
    17. HR44-34 check
    18. CK33-43 big
    19. HP54-44 check
    20. CK43-53
    21. HR34-35
    22. CO42-43
    23. HR35-65
    24. CO43-33
    25. HR65-62
    26. CO33-42
    27. HK50-40
    28. CO42-33
    29. HP44-54 check
    30. CK53-43
    31. HR62-63 check
    32. CK43-42
    33. HP54-53 check
    34. CK42-31
    35. HK40-50
    36. HK50-60
    37. HR63-64
    38. HR64-34
    39. CO33-32
    40. HP53-43
    41. HR34-24
    42. CK31-41
    43. HR24-22
    44. HR22-32
    45. CK41-51 big
    46. HK60-50
    47. HR32-42 mate

.. _漢車兵楚馬士:

漢車兵楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR12,HP2,CK51,CH84,CO32

.. janggi-moves::

    1. HR12-32
    2. CK51-41
    3. HR32-52
    4. CH84-72
    5. HP2-12
    6. CH72-64
    7. HR52-32
    8. CH64-45
    9. HP12-22
    10. CH45-26 check
    11. HK38-39
    12. CH26-47 check
    13. HK39-49
    14. CH47-28 big
    15. HK49-38
    16. CH28-7
    17. HK38-39
    18. CH7-15
    19. HK39-40
    20. CH15-7
    21. HR32-37
    22. CH7-28 check
    23. HK40-39
    24. CK41-42
    25. HK39-38
    26. CH28-16
    27. HR37-34
    28. HR34-44 check
    29. CK42-33 big
    30. HK38-49
    31. CH16-37 check
    32. HK49-60
    33. CH37-25
    34. HR44-24
    35. CH25-46
    36. HR24-28
    37. CH46-65
    38. HR28-38 check
    39. CK33-42
    40. HR38-48 check
    41. CK42-33
    42. HR48-41
    43. CH65-84
    44. HR41-81
    45. CH84-76
    46. HR81-51 check
    47. CK33-43
    48. HR51-41 check
    49. CK43-33
    50. HR41-44
    51. HR44-34 check
    52. CK33-43
    53. HP22-32
    54. CH76-68 check
    55. HK60-59
    56. CH68-47 check
    57. HK59-49
    58. CH47-68 big
    59. HK49-58
    60. CH68-56
    61. HR34-74
    62. CH56-37 check
    63. HK58-59
    64. CH37-45
    65. HK59-60
    66. HR74-44 check
    67. CK43-53 big
    68. HK60-50
    69. HR44-42 mate

.. _漢車兵楚象士:

漢車兵楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR26,HP45,CK33,CE51,CO31

.. janggi-moves::

    1. HP45-44
    2. CE51-74
    3. HR26-23 check
    4. CK33-32
    5. HR23-22 check
    6. CK32-33
    7. HP44-34 check
    8. CK33-43 big
    9. HK48-38
    10. CE74-51
    11. HK38-39
    12. HK39-40
    13. HR22-62
    14. CO31-42
    15. HR62-69
    16. CO42-32
    17. HR69-49 check
    18. CK43-53
    19. HP34-44
    20. CO32-33
    21. HP44-54 check
    22. CK53-52
    23. HP54-64
    24. CE51-83
    25. HP64-63
    26. CE83-55
    27. HR49-44
    28. HR44-54 check
    29. CK52-42
    30. HP63-53 check
    31. CK42-31
    32. HR54-34
    33. CO33-32
    34. HK40-50
    35. CE55-83
    36. HK50-60
    37. HR34-36
    38. CE83-51
    39. HP53-43
    40. CE51-74
    41. HR36-34
    42. CE74-51
    43. HR34-24
    44. CK31-41
    45. HP43-53
    46. CE51-34
    47. HR24-34
    48. CK41-31
    49. HP53-43
    50. HR34-24
    51. CK31-41
    52. HR24-22
    53. HR22-32
    54. CK41-51 big
    55. HK60-50
    56. HR32-42 mate

.. _漢車兵楚卒卒:

漢車兵楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HR49,HP17,CK41,CP16,CP56

.. janggi-moves::

    1. HK48-58 check
    2. CK41-51
    3. HP17-27
    4. CP56-46 big
    5. HK58-48
    6. CP46-36
    7. HR49-69
    8. CK51-41 big
    9. HK48-58
    10. CK41-51 big
    11. HK58-49
    12. HR69-66
    13. CK51-41 big
    14. HK49-60
    15. CP16-26
    16. HP27-17
    17. CK41-51 big
    18. HR66-56 check
    19. CK51-41
    20. HP17-7
    21. CP26-16
    22. HR56-36
    23. CK41-51 big
    24. HK60-50
    25. CK51-41 big
    26. HK50-40
    27. HR36-16
    28. HR16-36
    29. HR36-32
    30. HP7-17
    31. HP17-27
    32. HP27-37
    33. HP37-36
    34. HP36-35
    35. HP35-34
    36. HP34-33
    37. HR32-42 mate

.. _漢車兵楚卒士:

漢車兵楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR68,HP25,CK53,CP27,CO41

.. janggi-moves::

    1. HP25-35
    2. CP27-28 check
    3. HK38-39
    4. CO41-42
    5. HP35-34
    6. CK53-43
    7. HR68-48 check
    8. CK43-53
    9. HR48-58 check
    10. CK53-43
    11. HR58-49 check
    12. CK43-53
    13. HR49-60 check
    14. CK53-43
    15. HP34-44 check
    16. CK43-33 big
    17. HK39-49
    18. CP28-38 check
    19. HK49-50
    20. CO42-43
    21. HR60-52
    22. CO43-42
    23. HR52-55
    24. CO42-43
    25. HR55-15
    26. CO43-53
    27. HR15-12
    28. HK50-60
    29. HP44-34 check
    30. CK33-43
    31. HR12-13 check
    32. CK43-42
    33. HP34-33 check
    34. CK42-51
    35. HR13-11 check
    36. CK51-52
    37. HK60-50
    38. HR11-18
    39. HR18-38
    40. CK52-51
    41. HK50-40
    42. HR38-58
    43. CO53-52
    44. HP33-43
    45. HR58-68
    46. CK51-41
    47. HR68-62
    48. HR62-52
    49. CK41-31 big
    50. HK40-50
    51. HR52-42 mate

.. _漢車兵楚士士:

漢車兵楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR10,HP55,CK43,CO42,CO33

.. janggi-moves::

    1. HP55-54
    2. CO33-32
    3. HP54-44 check
    4. CK43-33 big
    5. HP44-34 check
    6. CK33-43
    7. HR10-50 check
    8. CK43-53
    9. HR50-60 check
    10. CK53-43
    11. HR60-49 check
    12. CK43-53
    13. HR49-58 check
    14. CK53-43
    15. HP34-44 check
    16. CK43-33 big
    17. HK38-49
    18. CO42-52
    19. HR58-52
    20. CO32-42
    21. HR52-55
    22. CO42-43
    23. HR55-25
    24. CO43-53
    25. HR25-22
    26. CO53-42
    27. HK49-60
    28. CO42-53
    29. HP44-34 check
    30. CK33-43
    31. HR22-23 check
    32. CK43-42
    33. HP34-33 check
    34. CK42-51
    35. HK60-50
    36. HK50-40
    37. HR23-24
    38. HR24-54
    39. CO53-52
    40. HP33-43
    41. HR54-64
    42. CK51-41
    43. HR64-62
    44. HR62-52
    45. CK41-31 big
    46. HK40-50
    47. HR52-42 mate

.. _漢車士士楚車:

漢車士士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR51,HO40,HO50,CK31,CR41

.. janggi-moves::

    1. HR51-33 mate

.. janggi-board::

    HK39,HR12,HO40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 check
    2. HR12-19
    3. CR9-19 mate

.. _漢車士士楚包:

漢車士士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR51,HO40,HO50,CK31,CC41

.. janggi-moves::

    1. HR51-33 mate

.. janggi-board::

    HK39,HR48,HO49,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-33 mate

.. _漢車士士楚馬:

漢車士士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR49,HO39,HO48,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢車士士楚象:

漢車士士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR51,HO40,HO50,CK31,CE41

.. janggi-moves::

    1. HR51-33 mate

.. janggi-board::

    HK48,HR58,HO49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢車士士楚卒:

漢車士士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR48,HO39,HO49,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢車士士楚士:

漢車士士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HR52,HO40,HO50,CK31,CO32

.. janggi-moves::

    1. HR52-51 mate

.. _漢車士楚車士:

漢車士楚車士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR3,HO38,CK51,CR41,CO31

.. janggi-moves::

    1. HR3-53 mate

.. _漢車士楚包包:

漢車士楚包包
~~~~~~~~~~~~

.. janggi-board::

    HK49,HR32,HO48,CK51,CC1,CC52

.. janggi-moves::

    1. HR32-31 check
    2. CC1-41 check
    3. HK49-40
    4. HR31-33 mate

.. _漢車士楚包馬:

漢車士楚包馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR2,HO38,CK43,CC42,CH1

.. janggi-moves::

    1. HR2-3 check
    2. CH1-13
    3. HR3-13 mate

.. janggi-board::

    HK48,HR2,HO49,CK31,CC41,CH34

.. janggi-moves::

    1. CH34-46 check
    2. HR2-42 check
    3. CK31-42
    4. CK42-31 mate

.. _漢車士楚包象:

漢車士楚包象
~~~~~~~~~~~~

.. janggi-board::

    HK49,HR52,HO48,CK31,CC61,CE32

.. janggi-moves::

    1. HR52-51 check
    2. CC61-41 check
    3. HK49-40
    4. CE32-64 big
    5. HR51-33 check
    6. CE64-32
    7. HR33-53 mate

.. janggi-board::

    HK38,HR49,HO48,CK41,CC31,CE1

.. janggi-moves::

    1. CE1-33 mate

.. _漢車士楚包卒:

漢車士楚包卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR5,HO38,CK32,CC42,CP24

.. janggi-moves::

    1. HR5-35 check
    2. CP24-34
    3. HR35-34 mate

.. janggi-board::

    HK50,HR38,HO49,CK42,CC41,CP37

.. janggi-moves::

    1. CP37-38
    2. HO49-48
    3. CP38-48
    4. CP48-49 mate

.. _漢車士楚包士:

漢車士楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR3,HO38,CK51,CC41,CO31

.. janggi-moves::

    1. HR3-53 mate

.. janggi-board::

    HK48,HR58,HO38,CK32,CC41,CO31

.. janggi-moves::

    1. CO31-42 mate

.. _漢車士楚馬馬:

漢車士楚馬馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR21,HO38,CK32,CH31,CH33

.. janggi-moves::

    1. HR21-22 mate

.. janggi-board::

    HK39,HR50,HO49,CK41,CH15,CH35

.. janggi-moves::

    1. CH15-27 check
    2. HK39-38
    3. CH27-46 check
    4. HK38-39
    5. CH35-27 check
    6. HK39-40
    7. CH27-19 check
    8. HK40-39
    9. CH46-27 mate

.. _漢車士楚馬象:

漢車士楚馬象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR2,HO38,CK43,CH1,CE42

.. janggi-moves::

    1. HR2-3 check
    2. CH1-13
    3. HR3-13 mate

.. janggi-board::

    HK39,HR50,HO49,CK41,CH9,CE33

.. janggi-moves::

    1. CE33-16 check
    2. HK39-38
    3. CH9-17 check
    4. HK38-39
    5. CH17-36 check
    6. HK39-40
    7. CH36-28 check
    8. HK40-39
    9. CH28-7 check
    10. HK39-40
    11. CH7-19 mate

.. _漢車士楚馬卒:

漢車士楚馬卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR66,HO58,CK51,CH46,CP4

.. janggi-moves::

    1. CH46-58 check
    2. HK39-49
    3. CH58-66
    4. HK49-60 big
    5. CK51-41
    6. CH66-54
    7. CH54-46
    8. CP4-14
    9. CK41-31
    10. CP14-24
    11. CP24-34
    12. CP34-35
    13. CP35-36
    14. CP36-37
    15. CP37-38
    16. CH46-25
    17. CH25-37
    18. CP38-49 mate

.. _漢車士楚馬士:

漢車士楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR21,HO38,CK32,CH33,CO31

.. janggi-moves::

    1. HR21-22 mate

.. janggi-board::

    HK38,HR39,HO48,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢車士楚象象:

漢車士楚象象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR61,HO38,CK31,CE41,CE32

.. janggi-moves::

    1. HR61-51
    2. CE32-64
    3. HR51-33 check
    4. CE64-32
    5. HR33-53 mate

.. janggi-board::

    HK39,HR40,HO49,CK41,CE32,CE33

.. janggi-moves::

    1. CE33-16 check
    2. HK39-38
    3. CE32-55 mate

.. _漢車士楚象卒:

漢車士楚象卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR5,HO38,CK32,CE42,CP24

.. janggi-moves::

    1. HR5-35 check
    2. CP24-34
    3. HR35-34 mate

.. janggi-board::

    HK39,HR68,HO58,CK52,CE56,CP57

.. janggi-moves::

    1. CP57-58 check
    2. HK39-40
    3. CP58-68
    4. HK40-49
    5. CE56-84
    6. HK49-59 big
    7. CK52-42
    8. HK59-49 big
    9. CK42-51
    10. CE84-52
    11. HK49-59
    12. CK51-41
    13. HK59-49 big
    14. CK41-31
    15. HK49-40 big
    16. CE52-35
    17. CK31-41
    18. CK41-51
    19. CE35-3
    20. CE3-26
    21. CP68-58
    22. HK40-39
    23. CE26-43
    24. CE43-66
    25. CP58-49 mate

.. _漢車士楚象士:

漢車士楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR3,HO38,CK51,CE41,CO31

.. janggi-moves::

    1. HR3-53 mate

.. janggi-board::

    HK38,HR39,HO48,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢車士楚卒卒:

漢車士楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR29,HO58,CK41,CP19,CP48

.. janggi-moves::

    1. CP19-29 check
    2. HK39-40
    3. CP48-58
    4. CP58-48
    5. CP48-38
    6. HK40-50 big
    7. CK41-31
    8. HK50-60
    9. CP29-39
    10. HK60-59
    11. CP38-48
    12. CP39-49 mate

.. _漢車士楚卒士:

漢車士楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR52,HO38,CK31,CP4,CO32

.. janggi-moves::

    1. HR52-51 mate

.. _漢車士楚士士:

漢車士楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HR3,HO38,CK51,CO31,CO41

.. janggi-moves::

    1. HR3-53 mate

.. _漢包包馬士楚:

漢包包馬士楚
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC41,HC8,HH33,HO50,CK31

.. janggi-moves::

    1. HO50-60
    2. HK40-49
    3. CK31-42 big
    4. HC41-44
    5. CK42-33
    6. HC44-50
    7. HO60-59
    8. HO59-58
    9. HC8-68
    10. HC68-38
    11. CK33-32
    12. HC38-60
    13. HC60-57
    14. HC57-59
    15. HC59-39
    16. HK49-48
    17. HO58-59
    18. HC39-69
    19. HC69-49
    20. HO59-60
    21. HC50-70
    22. HC70-30
    23. HO60-50
    24. HO50-40
    25. HC30-50
    26. HO40-39
    27. HC49-29
    28. HK48-49
    29. HO39-38
    30. HC29-59
    31. HC59-39 mate

.. _漢包包馬楚士:

漢包包馬楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC1,HC13,HH51,CK41,CO52

.. janggi-moves::

    1. HH51-72
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-42
    7. HH72-64
    8. CK42-53 big
    9. HK60-50
    10. CK53-42 big
    11. HH64-45
    12. CO52-51
    13. HC1-71
    14. HK50-40
    15. CK42-32 big
    16. HH45-37
    17. HK40-50
    18. CK32-42 big
    19. HK50-60
    20. CK42-52 big
    21. HH37-58
    22. CO51-42
    23. HK60-50
    24. CO42-32
    25. HK50-40
    26. CK52-42
    27. HH58-46
    28. CK42-33 big
    29. HH46-34
    30. CK33-43
    31. HC13-83
    32. HH34-46
    33. CK43-33 big
    34. HK40-50
    35. CK33-42
    36. HK50-60
    37. CK42-41
    38. HC71-11
    39. CO32-42
    40. HH46-67
    41. CK41-51 big
    42. HH67-55
    43. CO42-32
    44. HK60-50
    45. CK51-42 big
    46. HK50-40
    47. CK42-33 big
    48. HH55-34
    49. HK40-50
    50. HK50-60
    51. CK33-43
    52. HC83-3
    53. HH34-13 check
    54. CK43-42
    55. HC11-15
    56. CK42-51 big
    57. HK60-49
    58. CK51-41 big
    59. HK49-38
    60. HC3-23
    61. HH13-25
    62. CO32-42
    63. HC15-35
    64. CK41-31
    65. HC23-28
    66. CK31-32
    67. HC28-48
    68. CO42-51
    69. HK38-49
    70. CK32-42
    71. HH25-37
    72. HC35-39
    73. CO51-52
    74. HC39-59
    75. HH37-58
    76. CO52-51
    77. HH58-46 check
    78. CK42-31
    79. HH46-54
    80. CK31-41
    81. HH54-75
    82. CK41-31
    83. HC48-50
    84. HC59-39
    85. CO51-42
    86. HH75-54
    87. CK31-41
    88. HC39-89
    89. HC50-42
    90. HK49-59
    91. HC89-49
    92. HK59-58
    93. HK58-48
    94. CK41-51
    95. HH54-33
    96. HH33-14
    97. HH14-22
    98. HC42-12
    99. HH22-34
    100. HH34-53
    101. HH53-32 mate

.. _漢包包象士楚:

漢包包象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC8,HC10,HE33,HO50,CK31

.. janggi-moves::

    1. HO50-60
    2. HC10-50
    3. CK31-32
    4. HK40-49
    5. CK32-33
    6. HO60-59
    7. HO59-58
    8. HC8-68
    9. HC68-38
    10. CK33-32
    11. HC38-60
    12. HC60-57
    13. HC57-59
    14. HC59-39
    15. HK49-48
    16. HO58-59
    17. HC39-69
    18. HC69-49
    19. HO59-60
    20. HC50-70
    21. HC70-30
    22. HO60-50
    23. HO50-40
    24. HC30-50
    25. HO40-39
    26. HC49-29
    27. HK48-49
    28. HO39-38
    29. HC29-59
    30. HC59-39 mate

.. _漢包包象楚士:

漢包包象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC1,HC49,HE74,CK41,CO51

.. janggi-moves::

    1. HC1-51
    2. HC51-21
    3. HC21-71
    4. HC71-76
    5. HE74-46 check
    6. CK41-31 big
    7. HC76-36
    8. HE46-74
    9. CK31-41
    10. HC36-39
    11. HC39-34
    12. HC34-84
    13. HC84-44
    14. HK38-48
    15. HC49-47
    16. HC47-50
    17. HK48-58
    18. HC44-84
    19. HC84-54
    20. HC54-59
    21. HE74-46 check
    22. CK41-31
    23. HC59-51
    24. HC51-21
    25. HC21-41
    26. HC41-49
    27. HC49-42
    28. HE46-74
    29. HK58-48
    30. CK31-41
    31. HE74-57
    32. HE57-85
    33. HE85-53
    34. HE53-81
    35. HE81-64 check
    36. CK41-51
    37. HC42-49
    38. HE64-87
    39. HE87-70
    40. HC50-80
    41. HC80-60
    42. HE70-87
    43. HE87-55 mate

.. _漢包包兵兵楚:

漢包包兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC54,HC68,HP2,HP22,CK52

.. janggi-moves::

    1. HP22-32
    2. CK52-53
    3. HP2-12
    4. CK53-43
    5. HP12-22
    6. HK40-39
    7. HK39-38
    8. HC68-28
    9. HK38-39
    10. HK39-40
    11. HC28-21
    12. HC21-30
    13. HC30-50
    14. HC50-42
    15. HC42-49
    16. HK40-50
    17. HK50-60
    18. CK43-33
    19. HK60-59
    20. HK59-58
    21. CK33-43
    22. HC54-59
    23. HC49-42
    24. HC42-48
    25. HK58-49
    26. HC59-39
    27. HK49-38
    28. CK43-53
    29. HC39-37
    30. HC37-40
    31. HC48-28
    32. HK38-39
    33. HC28-21
    34. HC21-29
    35. HC29-49
    36. HK39-38
    37. CK53-43
    38. HC49-42
    39. HP22-12
    40. HC42-22
    41. HC22-2
    42. HP12-22
    43. HP32-42
    44. HP42-52
    45. HC2-32
    46. HC32-39
    47. CK43-53
    48. HP52-62
    49. HC39-31
    50. HK38-39
    51. HP22-32
    52. HC40-33
    53. HK39-40
    54. HC33-63
    55. HC63-61
    56. HC61-70
    57. HC70-30
    58. HC30-50
    59. CK53-43
    60. HP62-52
    61. HC50-41
    62. HP52-42 mate

.. _漢包包兵士楚:

漢包包兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC84,HC68,HP3,HO50,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HC68-38
    4. HK58-59
    5. HP3-13
    6. HP13-23
    7. CK41-42
    8. HO50-49
    9. CK42-51 big
    10. HO49-58
    11. CK51-42
    12. HC38-68
    13. HC68-28
    14. HC28-22
    15. HC22-29
    16. HC29-69
    17. HC69-39
    18. HO58-48
    19. HK59-49
    20. CK42-32
    21. HK49-40
    22. HO48-38 check
    23. CK32-42
    24. HP23-33 check
    25. CK42-51
    26. HK40-49
    27. HP33-43
    28. CK51-52
    29. HO38-48
    30. HO48-58
    31. CK52-51
    32. HP43-53
    33. HK49-60
    34. HP53-63
    35. HP63-73
    36. HP73-83
    37. HC84-82
    38. HC82-90
    39. HC90-50
    40. CK51-41
    41. HP83-73
    42. HP73-63
    43. HP63-53
    44. HO58-49 check
    45. CK41-31
    46. HP53-42 mate

.. _漢包包兵楚卒:

漢包包兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK50,HC75,HC8,HP34,CK43,CP4

.. janggi-moves::

    1. HP34-44 check
    2. CK43-42
    3. HK50-49
    4. CP4-14
    5. HK49-38
    6. CK42-31 big
    7. HK38-48
    8. CK31-41
    9. HC8-58
    10. CP14-24
    11. HK48-38
    12. HK38-39
    13. HP44-54
    14. CP24-34
    15. HP54-64
    16. CP34-44
    17. HP64-74
    18. CP44-54
    19. HK39-40
    20. HC75-73
    21. HC73-80
    22. HC80-30
    23. HC30-50
    24. CK41-42
    25. HK40-39
    26. HK39-38
    27. HC58-28
    28. HC28-78
    29. HK38-39
    30. HC78-73
    31. HC73-79
    32. HC79-29
    33. HC29-59
    34. HK39-40
    35. HC50-30
    36. HC30-80
    37. HK40-39
    38. HK39-38
    39. HC80-73
    40. CP54-55
    41. HC59-54
    42. HK38-39
    43. HC73-80
    44. HK39-40
    45. HC80-30
    46. HC30-50
    47. CP55-56
    48. HC54-84
    49. CP56-57
    50. HC84-34
    51. CP57-58
    52. HP74-64
    53. HP64-54
    54. HP54-44 check
    55. CP58-48
    56. HC50-47 check
    57. CK42-31
    58. HK40-50
    59. CK31-32
    60. HC47-43
    61. CP48-58
    62. HC34-54
    63. HC54-60
    64. HC60-57
    65. HK50-60
    66. HC43-50
    67. HP44-54
    68. HP54-53
    69. CP58-48
    70. HK60-59
    71. HC57-60
    72. CP48-38
    73. HP53-43
    74. HC60-58
    75. HC58-18
    76. HC18-68
    77. CP38-48
    78. HC50-47
    79. HC47-42
    80. CP48-38
    81. HK59-58
    82. CP38-39
    83. HC42-50
    84. HK58-48
    85. HC68-38
    86. HC38-58
    87. HP43-53
    88. CP39-40
    89. HC50-47
    90. HC47-49
    91. HC58-51
    92. HP53-42 mate

.. _漢包包兵楚士:

漢包包兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC4,HC87,HP6,CK31,CO43

.. janggi-moves::

    1. HK38-49
    2. CK31-42
    3. HP6-16
    4. CO43-53 big
    5. HK49-60
    6. CO53-52
    7. HP16-26
    8. CK42-53 big
    9. HK60-50
    10. CK53-42 big
    11. HK50-40
    12. HP26-36
    13. HP36-46
    14. HK40-49
    15. HP46-45
    16. HP45-44
    17. HC4-54
    18. HK49-38
    19. HC54-34
    20. CK42-31
    21. HP44-54
    22. HP54-64
    23. HP64-74
    24. HP74-84
    25. HC87-83
    26. HC83-88
    27. HC88-28
    28. HC28-48
    29. HK38-49
    30. HP84-74
    31. HP74-64
    32. HP64-54
    33. HP54-44
    34. HK49-58
    35. HC34-54
    36. CO52-42
    37. HC54-59
    38. HK58-49
    39. CK31-32
    40. HC48-50
    41. CK32-31
    42. HP44-34
    43. CO42-43
    44. HC50-43
    45. CK31-41
    46. HP34-44
    47. HC43-45 check
    48. CK41-51
    49. HC59-39
    50. HP44-43
    51. HC45-50
    52. CK51-52
    53. HP43-33
    54. HC39-31
    55. HP33-42 mate

.. _漢包包士士楚:

漢包包士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK39,HC29,HC49,HO40,HO50,CK41

.. janggi-moves::

    1. HK39-38
    2. HO50-60
    3. HO60-59
    4. HC49-69
    5. CK41-31 big
    6. HK38-49
    7. HK49-50
    8. HC69-49
    9. HO59-58
    10. HO58-48
    11. HK50-60
    12. HC49-47
    13. CK31-41
    14. HC47-50 check
    15. CK41-51 big
    16. HK60-49
    17. CK51-41
    18. HC29-59
    19. HK49-58 check
    20. CK41-31
    21. HO40-49
    22. HO48-38
    23. HC59-39 mate

.. _漢包包士楚車:

漢包包士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC58,HC50,HO60,CK31,CR8

.. janggi-moves::

    1. HC58-8
    2. HC8-58
    3. HK48-49
    4. HO60-59
    5. HC58-60
    6. HC60-57
    7. HO59-58
    8. HC57-59
    9. HC59-39
    10. HK49-48
    11. HO58-59
    12. HC39-69
    13. HC69-49
    14. HO59-60
    15. HC50-70
    16. HC70-30
    17. HO60-50
    18. HO50-40
    19. HC30-50
    20. HO40-39
    21. HC49-29
    22. HK48-49
    23. HO39-38
    24. HC29-59
    25. HC59-39 mate

.. _漢包包士楚包:

漢包包士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC41,HC60,HO40,CK51,CC38

.. janggi-moves::

    1. HC41-50
    2. HO40-39
    3. HO39-38
    4. HO38-39
    5. HC60-38
    6. HC38-40
    7. HC40-37
    8. HO39-38
    9. HC37-39
    10. HC39-59
    11. HK49-48
    12. HO38-39
    13. HC59-29
    14. HC29-49
    15. HO39-40
    16. HC50-30
    17. HC30-70
    18. HO40-50
    19. HO50-60
    20. HC70-50
    21. HO60-59
    22. HC49-69
    23. HK48-49
    24. HO59-58
    25. HC69-39
    26. HC39-59 mate

.. _漢包包士楚馬:

漢包包士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC58,HC50,HO60,CK31,CH8

.. janggi-moves::

    1. HC58-8
    2. HC8-58
    3. HK48-49
    4. HO60-59
    5. HC58-60
    6. HC60-57
    7. HO59-58
    8. HC57-59
    9. HC59-39
    10. HK49-48
    11. HO58-59
    12. HC39-69
    13. HC69-49
    14. HO59-60
    15. HC50-70
    16. HC70-30
    17. HO60-50
    18. HO50-40
    19. HC30-50
    20. HO40-39
    21. HC49-29
    22. HK48-49
    23. HO39-38
    24. HC29-59
    25. HC59-39 mate

.. _漢包包士楚象:

漢包包士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC68,HC50,HO38,CK51,CE40

.. janggi-moves::

    1. HO38-39
    2. HO39-40
    3. HC68-38
    4. HK48-49
    5. HO40-39
    6. HC38-40
    7. HC40-37
    8. HO39-38
    9. HC37-39
    10. HC39-59
    11. HK49-48
    12. HO38-39
    13. HC59-29
    14. HC29-49
    15. HO39-40
    16. HC50-30
    17. HC30-70
    18. HO40-50
    19. HO50-60
    20. HC70-50
    21. HO60-59
    22. HC49-69
    23. HK48-49
    24. HO59-58
    25. HC69-39
    26. HC39-59 mate

.. _漢包包士楚卒:

漢包包士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC58,HC50,HO60,CK31,CP8

.. janggi-moves::

    1. HC58-8
    2. HC8-58
    3. HK48-49
    4. HO60-59
    5. HC58-60
    6. HC60-57
    7. HO59-58
    8. HC57-59
    9. HC59-39
    10. HK49-48
    11. HO58-59
    12. HC39-69
    13. HC69-49
    14. HO59-60
    15. HC50-70
    16. HC70-30
    17. HO60-50
    18. HO50-40
    19. HC30-50
    20. HO40-39
    21. HC49-29
    22. HK48-49
    23. HO39-38
    24. HC29-59
    25. HC59-39 mate

.. _漢包包士楚士:

漢包包士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC61,HC50,HO40,CK51,CO31

.. janggi-moves::

    1. HC61-31
    2. HK48-49
    3. HO40-39
    4. HO39-38
    5. HC31-39
    6. HC39-59
    7. HK49-48
    8. HO38-39
    9. HC59-29
    10. HC29-49
    11. HO39-40
    12. HC50-30
    13. HC30-70
    14. HO40-50
    15. HO50-60
    16. HC70-50
    17. HO60-59
    18. HC49-69
    19. HK48-49
    20. HO59-58
    21. HC69-39
    22. HC39-59 mate

.. _漢包包楚包卒:

漢包包楚包卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC39,HC49,CK53,CC74,CP54

.. janggi-moves::

    1. CP54-64
    2. HK40-50
    3. CC74-54
    4. HK50-60
    5. CP64-65
    6. CK53-52
    7. CK52-51
    8. CK51-41
    9. CK41-31
    10. CP65-55 check
    11. HK60-50
    12. CC54-56
    13. CC56-51
    14. CC51-21
    15. CP55-56
    16. CP56-57
    17. CC21-41
    18. CK31-42
    19. CP57-58
    20. CP58-49 mate

.. _漢包包楚包士:

漢包包楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HC51,HC49,CK42,CC33,CO32

.. janggi-moves::

    1. CC33-31 check
    2. HC51-33
    3. CO32-33 mate

.. _漢包包楚馬馬:

漢包包楚馬馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC21,HC12,CK31,CH11,CH32

.. janggi-moves::

    1. HC21-1 mate

.. janggi-board::

    HK38,HC58,HC49,CK51,CH3,CH56

.. janggi-moves::

    1. CH56-68
    2. HK38-39
    3. CH68-60 check
    4. HK39-40
    5. CK51-41
    6. HK40-50
    7. CH60-68
    8. HC58-78
    9. CH68-76
    10. HK50-40
    11. CH3-15
    12. HC78-71
    13. CK41-51
    14. HC71-21
    15. CH76-68
    16. HC21-61
    17. CH68-89
    18. HK40-50
    19. CK51-41
    20. HK50-60
    21. CH89-70
    22. HC61-21
    23. CH70-78
    24. HK60-50
    25. CH15-36
    26. HK50-40
    27. CH78-66
    28. HK40-39
    29. CH66-58 check
    30. HK39-38
    31. CK41-31
    32. CH58-37
    33. HK38-48
    34. CH37-18 check
    35. HK48-58
    36. CH36-55
    37. HK58-48
    38. CH55-67 check
    39. HK48-38 big
    40. CK31-41
    41. HC21-61
    42. CH18-6
    43. HK38-39
    44. CH6-27 check
    45. HK39-40
    46. CH67-59
    47. HC61-21
    48. CH27-6
    49. HK40-39
    50. CH6-18 check
    51. HK39-38
    52. CK41-51
    53. HC21-61
    54. CH18-37
    55. CH59-67
    56. HK38-39
    57. CH37-58 check
    58. HK39-38
    59. CH58-46 check
    60. HK38-39
    61. CH46-27 check
    62. HK39-40
    63. CH67-59
    64. CH27-35
    65. HC61-21
    66. CK51-52
    67. HK40-39
    68. CH59-47 check
    69. HK39-38
    70. CH47-55
    71. HK38-39
    72. CH35-27 check
    73. HK39-40
    74. CH27-19 check
    75. HK40-50
    76. CH19-38 check
    77. HK50-40
    78. CH38-30
    79. CK52-51
    80. HC21-61
    81. CH55-36
    82. HK40-39
    83. CH30-18 check
    84. HK39-38
    85. CH18-6
    86. HC61-31
    87. CH6-27
    88. HC31-37
    89. CH36-55
    90. HK38-48
    91. CH55-67 check
    92. HK48-58 big
    93. CK51-41
    94. HC37-77
    95. CH27-15
    96. CH15-36
    97. HC77-57
    98. CH36-57
    99. HK58-48 big
    100. CH57-45 check
    101. HK48-38 check
    102. CH45-26 check
    103. HK38-39
    104. CH26-18 check
    105. HK39-38
    106. CH18-6
    107. HK38-39
    108. CH6-27 check
    109. HK39-40
    110. CH67-86
    111. CH86-65
    112. CH65-57
    113. CH27-6
    114. CH6-18
    115. CH18-37
    116. CH37-58
    117. CH57-36
    118. CH36-28 mate

.. _漢包包楚馬士:

漢包包楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC21,HC12,CK31,CH11,CO32

.. janggi-moves::

    1. HC21-1 mate

.. janggi-board::

    HK38,HC48,HC39,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢包包楚象士:

漢包包楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC21,HC12,CK31,CE11,CO32

.. janggi-moves::

    1. HC21-1 mate

.. janggi-board::

    HK38,HC48,HC39,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢包包楚卒卒:

漢包包楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK50,HC24,HC74,CK41,CP5,CP34

.. janggi-moves::

    1. CK41-31
    2. HC24-54
    3. CP5-15
    4. HK50-49
    5. CP34-44
    6. HK49-40 big
    7. CK31-41
    8. HK40-49
    9. CK41-51
    10. HC54-34
    11. CP15-25
    12. HK49-60 big
    13. CK51-41
    14. HK60-49
    15. CK41-31
    16. HC34-54
    17. CP25-35
    18. CK31-41
    19. CP35-45
    20. HK49-39
    21. CP45-46
    22. CP46-47
    23. HK39-38
    24. CP47-57
    25. HK38-49
    26. CP57-67
    27. CK41-51
    28. HK49-39
    29. CP67-68
    30. HK39-49
    31. CP44-54
    32. HC74-34
    33. CP54-44
    34. HK49-59 big
    35. CK51-41
    36. HK59-49
    37. CK41-31
    38. HC34-64
    39. CP68-78
    40. CP44-45
    41. CK31-41
    42. CK41-51
    43. CP45-55
    44. CP55-56
    45. CP78-68
    46. CP56-57
    47. HC64-69
    48. CP68-58 check
    49. HK49-40
    50. CP57-47
    51. HK40-39
    52. CP47-37
    53. CP58-48
    54. CP37-38 check
    55. HK39-40
    56. CP48-49 mate

.. _漢包馬馬士楚:

漢包馬馬士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC81,HH9,HH10,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. CK51-41 big
    9. HO40-49
    10. CK41-42
    11. HH9-30
    12. HH30-38
    13. HH38-59
    14. HH59-67
    15. HH67-75
    16. HH10-18
    17. HH18-26
    18. HH26-34 check
    19. CK42-31
    20. HH75-54
    21. HH34-42
    22. HH42-63
    23. HH63-51 mate

.. _漢包馬馬楚士:

漢包馬馬楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC12,HH11,HH20,CK42,CO31

.. janggi-moves::

    1. HK48-38
    2. CK42-33 big
    3. HK38-49
    4. CK33-42 big
    5. HK49-40
    6. CK42-33 big
    7. HH20-39
    8. HK40-50
    9. CK33-42 big
    10. HK50-60
    11. CK42-43
    12. HH39-58
    13. HH58-46
    14. CK43-53 big
    15. HK60-50
    16. CK53-43
    17. HK50-40
    18. HH46-34
    19. CK43-33
    20. HK40-50
    21. HK50-60
    22. CK33-43
    23. HH34-22 check
    24. CK43-33
    25. HK60-49
    26. HC12-72
    27. CO31-32
    28. HK49-60
    29. CO32-42
    30. HH11-3
    31. HH22-14 check
    32. CK33-43
    33. HC72-12
    34. CK43-53 big
    35. HK60-50
    36. CK53-43 big
    37. HK50-40
    38. CK43-53
    39. HH3-15
    40. HH14-22
    41. CO42-32
    42. HC12-32
    43. CK53-42
    44. HC32-12 check
    45. CK42-31 big
    46. HK40-50
    47. HH15-36
    48. HH36-55
    49. HH55-43 mate

.. _漢包馬象士楚:

漢包馬象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC81,HH10,HE1,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-42
    7. HH10-29
    8. HH29-50
    9. HH50-58
    10. HH58-77
    11. HH77-85
    12. HC81-89
    13. HH85-73
    14. HH73-61 check
    15. CK42-31
    16. HK60-50
    17. HO40-49
    18. HC89-39
    19. CK31-41
    20. HE1-24 check
    21. CK41-51
    22. HC39-59
    23. HO49-58 mate

.. _漢包馬象楚士:

漢包馬象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC23,HH31,HE11,CK32,CO33

.. janggi-moves::

    1. HH31-12
    2. CO33-42 big
    3. HK38-49
    4. CO42-31
    5. HH12-24
    6. CK32-42 big
    7. HK49-40
    8. CK42-32 big
    9. HH24-36
    10. HK40-50
    11. CK32-42 big
    12. HK50-60
    13. CK42-51 big
    14. HH36-55
    15. CO31-32
    16. HK60-50
    17. CK51-41 big
    18. HH55-47
    19. CK41-42
    20. HK50-40
    21. CK42-33 big
    22. HH47-35
    23. CK33-42
    24. HH35-54 check
    25. CK42-31
    26. HH54-46
    27. CO32-42 big
    28. HK40-50
    29. CK31-41
    30. HH46-25
    31. CO42-51 big
    32. HK50-60
    33. CK41-42
    34. HC23-27
    35. HH25-44
    36. HK60-49
    37. HE11-34
    38. HE34-57
    39. HC27-67
    40. HC67-47 check
    41. CK42-33
    42. HE57-89
    43. HE89-66
    44. HH44-23
    45. HC47-50
    46. HE66-38
    47. CO51-42
    48. HE38-15
    49. CK33-32
    50. HC50-42
    51. HC42-50
    52. HH23-4 check
    53. CK32-31
    54. HH4-12 mate

.. _漢包馬兵兵楚:

漢包馬兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC12,HH52,HP41,HP2,CK42

.. janggi-moves::

    1. HH52-64
    2. CK42-32 big
    3. HK38-49
    4. CK32-42 big
    5. HK49-60
    6. CK42-53 big
    7. HH64-56
    8. HP41-51
    9. HK60-50
    10. HK50-40
    11. CK53-43
    12. HH56-37
    13. CK43-33
    14. HP51-61
    15. HK40-50
    16. HH37-58
    17. HH58-46
    18. CK33-43
    19. HK50-40
    20. HH46-34
    21. CK43-33
    22. HK40-50
    23. HK50-60
    24. CK33-43
    25. HH34-22 check
    26. CK43-53 big
    27. HK60-49
    28. HK49-39
    29. HC12-32
    30. HC32-40
    31. CK53-42
    32. HH22-3
    33. HH3-24
    34. HP2-12
    35. HP12-22
    36. HP22-32 check
    37. CK42-52
    38. HH24-45
    39. HK39-49
    40. HC40-58
    41. HH45-57 mate

.. _漢包馬兵士楚:

漢包馬兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC31,HH51,HP81,HO39,CK52

.. janggi-moves::

    1. HH51-72
    2. CK52-42
    3. HC31-53
    4. HO39-49
    5. CK42-31 big
    6. HK38-48
    7. CK31-42 big
    8. HK48-58
    9. CK42-52
    10. HK58-59
    11. HK59-60
    12. HO49-58
    13. HC53-59 check
    14. CK52-42
    15. HH72-53
    16. HH53-34 check
    17. CK42-32
    18. HK60-49
    19. CK32-33
    20. HC59-39 check
    21. CK33-43 big
    22. HK49-59
    23. HC39-69
    24. HC69-49
    25. HP81-71
    26. HP71-61
    27. HP61-51
    28. HO58-48 check
    29. CK43-33
    30. HH34-13
    31. HH13-21 check
    32. CK33-32
    33. HC49-69
    34. HC69-39
    35. HO48-38 mate

.. _漢包馬兵楚卒:

漢包馬兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC1,HH36,HP62,CK31,CP35

.. janggi-moves::

    1. HH36-15
    2. CK31-42 big
    3. HK48-38
    4. CK42-33
    5. HK38-49
    6. CK33-42 big
    7. HK49-40
    8. CK42-33
    9. HH15-34
    10. CP35-36
    11. HH34-53
    12. CP36-26 big
    13. HK40-49
    14. CK33-42 big
    15. HH53-45
    16. CP26-27
    17. HK49-59
    18. CP27-28
    19. HK59-49
    20. HK49-39
    21. CK42-31 big
    22. HH45-37
    23. CK31-32
    24. HP62-52
    25. HK39-49
    26. CK32-33
    27. HH37-45 check
    28. CK33-43
    29. HK49-39
    30. HH45-66
    31. CK43-33 big
    32. HK39-49
    33. CK33-43 big
    34. HH66-47
    35. CK43-53
    36. HP52-62
    37. CK53-42
    38. HK49-39
    39. CK42-31 big
    40. HH47-35
    41. CK31-32
    42. HP62-52
    43. HK39-49
    44. CK32-33
    45. HK49-59
    46. CP28-38
    47. HH35-54 check
    48. CK33-43
    49. HP52-42
    50. HP42-32
    51. HP32-22
    52. HP22-12
    53. HP12-2
    54. HC1-4
    55. HC4-64
    56. HC64-34
    57. HP2-12
    58. HP12-22
    59. HP22-32
    60. HC34-31
    61. CP38-48
    62. HH54-42
    63. HH42-61
    64. HP32-42
    65. HC31-53
    66. HC53-13
    67. HP42-52
    68. CK43-33
    69. HH61-42
    70. CK33-32
    71. HH42-54
    72. CP48-58 check
    73. HK59-58
    74. HP52-42 mate

.. _漢包馬兵楚士:

漢包馬兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC13,HH51,HP3,CK52,CO33

.. janggi-moves::

    1. HH51-72
    2. CK52-42 big
    3. HK48-38
    4. CO33-32
    5. HH72-64
    6. CK42-33 big
    7. HK38-49
    8. CK33-42 big
    9. HK49-60
    10. CK42-51 big
    11. HH64-56
    12. HK60-50
    13. CK51-42 big
    14. HK50-40
    15. CO32-31
    16. HH56-75
    17. CK42-32 big
    18. HK40-50
    19. CK32-42 big
    20. HK50-60
    21. CK42-51 big
    22. HH75-54
    23. CK51-41
    24. HH54-46
    25. CO31-42
    26. HH46-27
    27. CK41-51 big
    28. HK60-50
    29. CO42-41
    30. HH27-35
    31. CK51-42 big
    32. HK50-60
    33. CK42-51 big
    34. HH35-54
    35. CO41-42
    36. HK60-50
    37. CO42-52
    38. HH54-35
    39. CK51-42 big
    40. HK50-60
    41. CK42-53 big
    42. HH35-54
    43. CO52-42
    44. HK60-50
    45. CO42-31
    46. HC13-73
    47. CK53-52
    48. HK50-40
    49. CK52-51
    50. HH54-75
    51. CK51-42
    52. HC73-76
    53. CK42-32 big
    54. HK40-50
    55. CK32-42 big
    56. HK50-60
    57. HH75-56
    58. HC76-46
    59. CK42-51
    60. HK60-49
    61. CO31-42
    62. HH56-44
    63. CO42-52
    64. HP3-13
    65. CO52-53
    66. HP13-23
    67. HP23-33
    68. HH44-63 check
    69. CK51-52
    70. HC46-50
    71. HK49-40
    72. HC50-30
    73. HC30-60 check
    74. CO53-42
    75. HH63-42
    76. CK52-51
    77. HH42-63 check
    78. CK51-41
    79. HP33-42 mate

.. _漢包馬士士楚:

漢包馬士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC2,HH31,HO40,HO50,CK32

.. janggi-moves::

    1. HH31-12 mate

.. _漢包馬士楚車:

漢包馬士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC39,HH13,HO49,CK33,CR42

.. janggi-moves::

    1. HH13-34 check
    2. CK33-43
    3. HH34-55 check
    4. CK43-33
    5. HO49-38 mate

.. janggi-board::

    HK40,HC49,HH60,HO50,CK42,CR52

.. janggi-moves::

    1. CK42-51
    2. HH60-48
    3. CR52-32 check
    4. HH48-36
    5. CR32-36 mate

.. _漢包馬士楚包:

漢包馬士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK39,HC19,HH13,HO48,CK51,CC42

.. janggi-moves::

    1. HO48-58
    2. CK51-41
    3. HK39-40
    4. CK41-51
    5. HH13-21
    6. CK51-52
    7. HH21-33 check
    8. CK52-53
    9. HH33-45 check
    10. CK53-52
    11. HH45-64 check
    12. CK52-51
    13. HK40-50
    14. CK51-41
    15. HK50-60
    16. CK41-31
    17. HH64-43
    18. CK31-41
    19. HH43-35
    20. CK41-31
    21. HH35-16
    22. CK31-32
    23. HH16-24 check
    24. CK32-31
    25. HK60-59
    26. HC19-69
    27. CK31-41
    28. HC69-49
    29. CK41-51
    30. HH24-43
    31. CK51-52
    32. HH43-64 check
    33. CK52-51
    34. HK59-60
    35. CK51-41
    36. HK60-50
    37. CK41-31
    38. HH64-43
    39. CK31-41
    40. HH43-62 check
    41. CK41-31
    42. HH62-74
    43. CK31-32
    44. HH74-53 check
    45. CK32-33
    46. HH53-45 check
    47. CK33-32
    48. HH45-24 check
    49. CK32-31
    50. HO58-48
    51. HC49-45
    52. HK50-49
    53. HO48-38
    54. CK31-41
    55. HH24-36
    56. HK49-40
    57. HH36-44
    58. HH44-63
    59. HK40-49
    60. CK41-31
    61. HC45-50
    62. CK31-32
    63. HH63-51 check
    64. CK32-31
    65. HK49-60
    66. HC50-70
    67. HK60-50
    68. HC70-40 check
    69. CK31-41
    70. HH51-63
    71. HC40-60
    72. HH63-84
    73. HH84-65
    74. HH65-53 check
    75. CK41-31
    76. HC60-40 mate

.. janggi-board::

    HK39,HC49,HH48,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-37 mate

.. _漢包馬士楚馬:

漢包馬士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HC2,HH11,HO38,CK52,CH31

.. janggi-moves::

    1. HH11-32 check
    2. CH31-12
    3. HC2-22 mate

.. janggi-board::

    HK38,HC48,HH49,HO39,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢包馬士楚象:

漢包馬士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC39,HH1,HO48,CK43,CE42

.. janggi-moves::

    1. HH1-22 check
    2. CK43-53
    3. HH22-34 check
    4. CK53-43
    5. HH34-55 check
    6. CK43-33
    7. HO48-38 mate

.. janggi-board::

    HK48,HC49,HH58,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢包馬士楚卒:

漢包馬士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC44,HH41,HO39,CK43,CP48

.. janggi-moves::

    1. HC44-50 check
    2. CP48-58
    3. HO39-49 check
    4. CP58-48
    5. HO49-48 mate

.. janggi-board::

    HK38,HC49,HH48,HO39,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢包馬士楚士:

漢包馬士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC39,HH13,HO48,CK33,CO42

.. janggi-moves::

    1. HH13-34 check
    2. CK33-43
    3. HH34-55 check
    4. CK43-33
    5. HO48-38 mate

.. _漢包馬楚包卒:

漢包馬楚包卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC41,HH20,CK51,CC52,CP5

.. janggi-moves::

    1. HC41-49
    2. CP5-15
    3. HH20-28
    4. CP15-25
    5. HH28-16
    6. CP25-15
    7. HH16-35
    8. CP15-25
    9. HH35-23
    10. CP25-35
    11. HH23-35
    12. HH35-23
    13. HH23-11
    14. HH11-32 mate

.. _漢包馬楚包士:

漢包馬楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC9,HH19,CK32,CC42,CO33

.. janggi-moves::

    1. HC9-39 check
    2. CO33-43
    3. HH19-38 check
    4. CO43-33
    5. HH38-57 check
    6. CO33-43
    7. HH57-36 check
    8. CO43-33
    9. HH36-24 mate

.. janggi-board::

    HK39,HC49,HH21,CK41,CC1,CO32

.. janggi-moves::

    1. CC1-31 check
    2. HH21-33
    3. CO32-33 mate

.. _漢包馬楚馬士:

漢包馬楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HC12,HH33,CK53,CH14,CO42

.. janggi-moves::

    1. HH33-45 check
    2. CK53-43
    3. HC12-19
    4. CH14-35
    5. HC19-49 check
    6. CH35-47 check
    7. HK39-40
    8. HC49-46 mate

.. janggi-board::

    HK38,HC39,HH48,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢包馬楚象士:

漢包馬楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC58,HH19,CK32,CE42,CO33

.. janggi-moves::

    1. HC58-40 check
    2. CO33-43
    3. HH19-38 check
    4. CO43-33
    5. HH38-57 check
    6. CO33-43
    7. HH57-36 check
    8. CO43-33
    9. HH36-24 mate

.. janggi-board::

    HK38,HC48,HH39,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢包馬楚卒卒:

漢包馬楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC40,HH22,CK32,CP10,CP29

.. janggi-moves::

    1. HH22-34 check
    2. CP29-39
    3. HC40-38 mate

.. janggi-board::

    HK38,HC7,HH24,CK51,CP4,CP14

.. janggi-moves::

    1. CP14-24
    2. HK38-49
    3. CP4-14
    4. HK49-60 big
    5. CK51-41
    6. HK60-50 big
    7. CK41-31
    8. CP14-15
    9. CP15-25
    10. CP25-35
    11. CP35-45
    12. CK31-41
    13. CK41-51
    14. CP45-55
    15. CP55-56
    16. CP24-34
    17. CP34-44
    18. CP56-66
    19. CP66-67
    20. CP67-68
    21. HK50-49
    22. CP44-54
    23. CP54-55
    24. CP55-56
    25. CP56-57
    26. CP68-58 check
    27. HK49-40
    28. CP58-48
    29. CP57-58
    30. CP48-49 mate

.. _漢包馬楚卒士:

漢包馬楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC22,HH33,CK53,CP28,CO42

.. janggi-moves::

    1. HH33-45 check
    2. CK53-43
    3. HC22-30
    4. CP28-38
    5. HC30-50 check
    6. CP38-49 check
    7. HK40-49
    8. HK49-40 mate

.. _漢包馬楚士士:

漢包馬楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC8,HH53,CK33,CO32,CO42

.. janggi-moves::

    1. HH53-45 check
    2. CK33-43
    3. HK48-38
    4. HC8-48 mate

.. _漢包象象士楚:

漢包象象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC62,HE61,HE40,HO60,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-59
    4. CK41-51 big
    5. HE40-57
    6. HO60-49
    7. HK59-60
    8. HK60-50
    9. HE57-89
    10. HE89-66
    11. HC62-69
    12. HC69-39
    13. HE61-84
    14. HE66-34 check
    15. CK51-41
    16. HE34-2
    17. HE2-25
    18. HE84-56
    19. HE56-73 check
    20. CK41-51
    21. HC39-59
    22. HO49-58 mate

.. _漢包象象楚士:

漢包象象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC2,HE1,HE21,CK31,CO52

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-42
    5. HE21-4
    6. CK42-53 big
    7. HK60-50
    8. CK53-42 big
    9. HK50-40
    10. CK42-31 big
    11. HE4-36
    12. CK31-42
    13. HE36-59
    14. HE59-87
    15. CK42-31 big
    16. HK40-50
    17. CK31-41 big
    18. HK50-60
    19. CK41-42
    20. HE87-55
    21. HE55-38
    22. HE38-66
    23. CK42-53 big
    24. HK60-50
    25. CK53-42 big
    26. HK50-40
    27. HE66-34
    28. HE34-57
    29. CK42-31 big
    30. HK40-50
    31. CO52-53
    32. HK50-60
    33. CO53-43
    34. HE57-34
    35. CK31-41
    36. HE34-6
    37. CK41-51 big
    38. HK60-49
    39. CK51-41
    40. HC2-8
    41. CO43-53 big
    42. HK49-38
    43. CK41-31 big
    44. HE6-34
    45. HC8-48
    46. HK38-49
    47. CK31-41
    48. HE1-24 check
    49. CK41-42
    50. HE24-47 check
    51. CK42-31
    52. HE47-75
    53. HC48-50
    54. HE75-58
    55. HE58-86
    56. HE86-54
    57. HE54-82
    58. HE82-65
    59. HE34-2
    60. HE2-25
    61. CO53-43
    62. HC50-43
    63. CK31-41
    64. HE65-42 check
    65. CK41-31
    66. HC43-50
    67. CK31-41
    68. HE25-53
    69. HE53-81
    70. HE81-64 check
    71. CK41-51
    72. HE42-74 check
    73. CK51-52
    74. HE64-47
    75. HE47-24 check
    76. CK52-53
    77. HE74-57
    78. HE57-85 mate

.. _漢包象兵兵楚:

漢包象兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC23,HE21,HP1,HP82,CK43

.. janggi-moves::

    1. HK48-58
    2. CK43-42
    3. HE21-4
    4. CK42-51 big
    5. HK58-49
    6. CK51-41 big
    7. HK49-40
    8. CK41-31 big
    9. HE4-36
    10. HK40-50
    11. HK50-60
    12. CK31-41
    13. HE36-59
    14. CK41-51
    15. HP1-11
    16. HP11-21
    17. HP21-31
    18. HP82-72
    19. HP72-62
    20. HP31-41 check
    21. CK51-42
    22. HE59-87
    23. HE87-55
    24. HE55-38
    25. HE38-66
    26. CK42-53 big
    27. HK60-50
    28. CK53-42 big
    29. HK50-40
    30. HE66-34
    31. HE34-57
    32. CK42-32 big
    33. HK40-50
    34. CK32-42 big
    35. HK50-60
    36. CK42-53
    37. HC23-63
    38. HC63-61
    39. HC61-67
    40. HK60-49
    41. HC67-47
    42. CK53-42
    43. HE57-74 check
    44. CK42-43
    45. HE74-46 check
    46. CK43-33
    47. HE46-63
    48. CK33-42
    49. HE63-86
    50. HE86-58
    51. HE58-75
    52. HP62-52 check
    53. CK42-32
    54. HC47-50
    55. HE75-47
    56. HE47-79
    57. HE79-56
    58. HP52-42 mate

.. _漢包象兵士楚:

漢包象兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC62,HE84,HP41,HO39,CK33

.. janggi-moves::

    1. HE84-52
    2. CK33-43 big
    3. HK48-38
    4. CK43-53
    5. HE52-24
    6. CK53-43
    7. HO39-40
    8. HK38-39
    9. CK43-33 big
    10. HK39-49
    11. CK33-42 big
    12. HK49-60
    13. CK42-53 big
    14. HK60-50
    15. CK53-42 big
    16. HO40-49
    17. HK50-40
    18. HC62-22
    19. HC22-29
    20. HO49-39
    21. HC29-49
    22. HO39-38
    23. HO38-48 check
    24. CK42-53
    25. HK40-39
    26. HE24-56
    27. HE56-88
    28. HE88-65
    29. HE65-37
    30. HE37-14
    31. HE14-42
    32. HE42-25 check
    33. CK53-52
    34. HC49-29
    35. HC29-59
    36. HO48-58 mate

.. _漢包象兵楚士:

漢包象兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC83,HE90,HP22,CK53,CO32

.. janggi-moves::

    1. HP22-32
    2. CK53-43 big
    3. HK48-58
    4. CK43-53 big
    5. HK58-49
    6. CK53-43 big
    7. HK49-60
    8. CK43-33
    9. HP32-22
    10. CK33-42
    11. HE90-67
    12. CK42-51 big
    13. HK60-50
    14. CK51-41 big
    15. HK50-40
    16. CK41-31 big
    17. HE67-39
    18. HK40-50
    19. HK50-60
    20. HE39-56
    21. CK31-42
    22. HE56-73
    23. CK42-51 big
    24. HK60-50
    25. CK51-42 big
    26. HK50-40
    27. HC83-23
    28. HC23-21
    29. HC21-26
    30. HE73-56
    31. CK42-31 big
    32. HK40-50
    33. CK31-41 big
    34. HK50-60
    35. HC26-66
    36. CK41-31
    37. HK60-49
    38. HC66-46
    39. CK31-42
    40. HE56-79
    41. HE79-47
    42. HE47-15
    43. HP22-32 check
    44. CK42-33
    45. HC46-50
    46. HP32-22
    47. HE15-32
    48. HE32-64
    49. HE64-41
    50. HE41-73
    51. HE73-56 mate

.. _漢包象士士楚:

漢包象士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC39,HE14,HO40,HO49,CK32

.. janggi-moves::

    1. HO49-38 mate

.. _漢包象士楚車:

漢包象士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC34,HE42,HO38,CK31,CR41

.. janggi-moves::

    1. HE42-14 check
    2. CK31-32
    3. HC34-39 mate

.. janggi-board::

    HK40,HC39,HE67,HO38,CK41,CR51

.. janggi-moves::

    1. CR51-60 check
    2. HE67-50
    3. CR60-38
    4. HE50-78
    5. CR38-60 check
    6. HE78-50
    7. CR60-58 mate

.. _漢包象士楚包:

漢包象士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC33,HE51,HO40,CK31,CC32

.. janggi-moves::

    1. HE51-74
    2. CK31-41 big
    3. HK48-38
    4. HC33-39
    5. HO40-49
    6. HO49-48
    7. HK38-49
    8. HK49-59
    9. HC39-69
    10. HC69-49 check
    11. CK41-31
    12. HE74-51
    13. HE51-83
    14. HE83-55
    15. HE55-87
    16. HE87-64
    17. HE64-41
    18. HE41-73
    19. HE73-56
    20. HE56-88
    21. HE88-65
    22. HE65-82
    23. HE82-54 mate

.. janggi-board::

    HK39,HC49,HE48,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-33 mate

.. _漢包象士楚馬:

漢包象士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC33,HE25,HO39,CK32,CH26

.. janggi-moves::

    1. HC33-40 check
    2. CH26-38
    3. HO39-38 mate

.. janggi-board::

    HK38,HC48,HE49,HO39,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢包象士楚象:

漢包象士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC34,HE42,HO38,CK31,CE41

.. janggi-moves::

    1. HE42-14 check
    2. CK31-32
    3. HC34-39 mate

.. janggi-board::

    HK48,HC49,HE58,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢包象士楚卒:

漢包象士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC40,HE33,HO39,CK32,CP28

.. janggi-moves::

    1. HE33-65 check
    2. CP28-38 check
    3. HO39-38 mate

.. janggi-board::

    HK38,HC49,HE48,HO39,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢包象士楚士:

漢包象士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC34,HE42,HO38,CK31,CO41

.. janggi-moves::

    1. HE42-14 check
    2. CK31-32
    3. HC34-39 mate

.. _漢包象楚包士:

漢包象楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE3,CK51,CC52,CO31

.. janggi-moves::

    1. HE3-31
    2. HE31-3
    3. HE3-35
    4. HE35-7
    5. HE7-24
    6. HE24-41
    7. HE41-13
    8. HE13-36
    9. HE36-8
    10. HE8-40
    11. HE40-17
    12. HE17-34 mate

.. _漢包象楚馬士:

漢包象楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC37,HE13,CK32,CH42,CO43

.. janggi-moves::

    1. HE13-36 check
    2. CO43-33
    3. HE36-4 mate

.. janggi-board::

    HK38,HC39,HE48,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢包象楚象士:

漢包象楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC58,HE11,CK32,CE42,CO33

.. janggi-moves::

    1. HC58-40 check
    2. CO33-43
    3. HE11-34 check
    4. CO43-33
    5. HE34-66 check
    6. CO33-43
    7. HE66-38 check
    8. CO43-33
    9. HE38-15 mate

.. janggi-board::

    HK38,HC39,HE48,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢包象楚卒卒:

漢包象楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC7,HE24,CK51,CP4,CP14

.. janggi-moves::

    1. CP14-24
    2. HK38-49
    3. CP4-14
    4. HK49-60 big
    5. CK51-41
    6. HK60-50 big
    7. CK41-31
    8. CP14-15
    9. CP15-25
    10. CP25-35
    11. CP35-45
    12. CK31-41
    13. CK41-51
    14. CP45-55
    15. CP55-56
    16. CP24-34
    17. CP34-44
    18. CP56-66
    19. CP66-67
    20. CP67-68
    21. HK50-49
    22. CP44-54
    23. CP54-55
    24. CP55-56
    25. CP56-57
    26. CP68-58 check
    27. HK49-40
    28. CP58-48
    29. CP57-58
    30. CP48-49 mate

.. _漢包象楚卒士:

漢包象楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE35,CK31,CP4,CO32

.. janggi-moves::

    1. HE35-3 mate

.. _漢包象楚士士:

漢包象楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HE22,CK33,CO31,CO32

.. janggi-moves::

    1. HE22-5 mate

.. _漢包兵兵士楚:

漢包兵兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC82,HP1,HP7,HO50,CK31

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HO50-40
    4. HP1-11
    5. HP11-21
    6. HP7-17
    7. HP17-27
    8. CK41-51 big
    9. HK58-49
    10. CK51-41 big
    11. HK49-39
    12. HP27-37
    13. HP37-47
    14. HP47-57
    15. HP57-56
    16. HP56-55
    17. HP55-54
    18. HO40-49
    19. HP54-64
    20. HP64-74
    21. HP74-84
    22. HK39-40
    23. HC82-89
    24. HC89-39
    25. HP84-74
    26. HP74-64
    27. HP64-54
    28. CK41-42
    29. HP54-44
    30. HP44-34
    31. HO49-38
    32. HP34-33 check
    33. CK42-41
    34. HP33-43
    35. HC39-31
    36. CK41-51
    37. HP43-53
    38. HC31-11 mate

.. _漢包兵兵楚車:

漢包兵兵楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC83,HP62,HP7,CK41,CR52

.. janggi-moves::

    1. HP62-52
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-31
    7. HP7-17
    8. CK31-32
    9. HK60-50
    10. CK32-33
    11. HP17-27
    12. CK33-43 big
    13. HK50-40
    14. CK43-33 big
    15. HP27-37
    16. CK33-43
    17. HP52-62
    18. HP62-72
    19. HP72-82
    20. HC83-81
    21. HC81-90
    22. HC90-30
    23. HC30-50
    24. HP82-72
    25. HP72-62
    26. HP37-47 check
    27. CK43-33 big
    28. HK40-49
    29. HP47-37
    30. HP62-52
    31. HP52-42
    32. HP37-36
    33. HP36-35
    34. HP35-34 mate

.. _漢包兵兵楚包:

漢包兵兵楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC31,HP73,HP77,CK51,CC34

.. janggi-moves::

    1. HC31-61
    2. CK51-42 big
    3. HK48-58
    4. CK42-51 big
    5. HK58-49
    6. CK51-42 big
    7. HK49-40
    8. CK42-32
    9. HP77-67
    10. CC34-31 big
    11. HK40-49
    12. CC31-33
    13. HC61-70
    14. CK32-42 big
    15. HK49-40
    16. CK42-32
    17. HC70-63
    18. CC33-31 big
    19. HK40-49
    20. CK32-42 big
    21. HK49-58
    22. CC31-53
    23. HC63-68
    24. CK42-43
    25. HP67-66
    26. CC53-33
    27. HC68-63
    28. HP66-56
    29. HP56-46
    30. HK58-49
    31. HK49-40
    32. CC33-53
    33. HP46-45
    34. CK43-33 big
    35. HP45-35
    36. CK33-42
    37. HP35-34
    38. CC53-31 check
    39. HP34-44
    40. CK42-32 big
    41. HK40-49
    42. CK32-42
    43. HP73-72
    44. CC31-53
    45. HK49-39
    46. CK42-31 big
    47. HP44-34
    48. CK31-42
    49. HP72-62
    50. CC53-31 check
    51. HP34-44
    52. CC31-53
    53. HK39-40
    54. HC63-61
    55. CK42-33 big
    56. HP44-34 check
    57. CK33-43
    58. HC61-70
    59. CC53-23
    60. HC70-30
    61. CC23-63
    62. HP34-44 check
    63. CK43-33 big
    64. HK40-50
    65. CK33-42
    66. HK50-60
    67. CK42-53 big
    68. HP44-54 check
    69. CK53-43
    70. HC30-70
    71. CC63-23
    72. HC70-50
    73. CK43-33
    74. HK60-49
    75. CC23-43
    76. HP62-52
    77. HC50-45
    78. HP54-44
    79. HP44-34 check
    80. CK33-32
    81. HC45-50
    82. HK49-60
    83. HC50-70
    84. CK32-31
    85. HP34-33
    86. HP52-42 mate

.. _漢包兵兵楚馬:

漢包兵兵楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC5,HP43,HP25,CK31,CH1

.. janggi-moves::

    1. HC5-35
    2. CH1-22
    3. HP43-33 check
    4. CK31-41
    5. HP25-24
    6. CH22-3
    7. HP24-34
    8. CH3-22
    9. HP33-23
    10. CH22-43
    11. HP34-44
    12. CH43-64
    13. HP44-54
    14. CH64-76
    15. HP23-33
    16. CH76-57 check
    17. HK38-39
    18. CH57-65
    19. HK39-40
    20. HC35-32
    21. CK41-51
    22. HC32-38
    23. CK51-41
    24. HP33-43
    25. CK41-31
    26. HK40-49
    27. CH65-57 check
    28. HK49-48
    29. CH57-45
    30. HP54-44
    31. CK31-41
    32. HK48-49
    33. CH45-57 check
    34. HK49-39
    35. CH57-36
    36. HP44-54
    37. CH36-24
    38. HP43-53
    39. CH24-45
    40. HP54-44
    41. HC38-40
    42. HC40-35
    43. CH45-64
    44. HP44-54
    45. CH64-56
    46. HC35-40
    47. CH56-77
    48. HP53-43
    49. CH77-65
    50. HK39-49
    51. HK49-50
    52. CK41-51
    53. HC40-60 check
    54. CK51-41
    55. HC60-30
    56. HC30-70
    57. CK41-51
    58. HP43-33
    59. CK51-52
    60. HC70-30
    61. CK52-51
    62. HK50-40
    63. HC30-50
    64. CK51-52
    65. HP33-43
    66. CK52-51
    67. HK40-49
    68. CH65-57 check
    69. HK49-58
    70. CH57-65
    71. HK58-48
    72. CH65-44
    73. HC50-44
    74. HC44-49
    75. HP43-42 mate

.. _漢包兵兵楚象:

漢包兵兵楚象
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC28,HP74,HP7,CK41,CE46

.. janggi-moves::

    1. HP74-64
    2. CE46-14
    3. HP64-54
    4. CE14-37
    5. HP54-44
    6. CE37-60
    7. HK40-39
    8. CK41-31 big
    9. HP44-34
    10. CE60-77
    11. HK39-38
    12. CE77-45
    13. HC28-58
    14. CE45-62
    15. HP34-33
    16. CE62-34
    17. HK38-49
    18. CK31-41 big
    19. HK49-60
    20. CK41-51
    21. HP33-43
    22. CK51-52
    23. HK60-49
    24. CK52-51
    25. HP43-53 check
    26. CK51-41 big
    27. HK49-60
    28. CK41-31
    29. HC58-52
    30. CK31-32
    31. HK60-49
    32. CK32-31
    33. HC52-54
    34. HK49-60
    35. CK31-32
    36. HP53-43
    37. HC54-24
    38. CE34-66
    39. HP43-53
    40. CE66-34
    41. HP7-6
    42. CK32-33
    43. HP6-16
    44. CE34-57
    45. HP16-26
    46. CE57-85
    47. HP53-63
    48. CK33-42
    49. HC24-28
    50. CK42-51 big
    51. HK60-49
    52. CK51-42 big
    53. HK49-38
    54. CE85-53
    55. HC28-68
    56. CK42-31 big
    57. HK38-49
    58. CK31-42 big
    59. HK49-60
    60. CK42-52
    61. HC68-62
    62. CE53-25 big
    63. HK60-49
    64. CK52-42 big
    65. HK49-39
    66. CE25-53
    67. HC62-66
    68. CK42-31 big
    69. HK39-49
    70. CK31-42 big
    71. HK49-60
    72. CK42-52
    73. HC66-16
    74. CE53-36 big
    75. HK60-49
    76. CK52-42 big
    77. HK49-39
    78. CE36-68
    79. HC16-46
    80. CE68-85
    81. HP26-36
    82. CE85-57
    83. HC46-26
    84. HC26-56
    85. HK39-38
    86. HP36-46
    87. CE57-85
    88. HK38-49
    89. HK49-60
    90. HP46-45
    91. CE85-53
    92. HP45-55
    93. CE53-21
    94. HC56-53
    95. CK42-52
    96. HC53-73
    97. CE21-44
    98. HC73-43
    99. CE44-76
    100. HC43-73
    101. HK60-49
    102. HK49-38
    103. HP55-45
    104. CK52-42
    105. HP45-35
    106. CE76-44
    107. HP35-34
    108. CE44-61
    109. HC73-53
    110. HP34-44
    111. HK38-49
    112. HK49-59
    113. HP44-54
    114. CK42-31
    115. HC53-73
    116. CE61-33
    117. HC73-33
    118. HP63-53
    119. HP53-43
    120. HP54-53
    121. HP43-42 mate

.. _漢包兵兵楚卒:

漢包兵兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC6,HP23,HP74,CK41,CP24

.. janggi-moves::

    1. HK48-38
    2. CK41-42
    3. HP74-64
    4. CK42-32 big
    5. HK38-49
    6. CK32-42 big
    7. HK49-60
    8. CK42-52 big
    9. HP64-54
    10. CP24-34
    11. HK60-50
    12. CK52-42 big
    13. HK50-40
    14. CP34-24
    15. HP23-13
    16. CK42-31 big
    17. HK40-50
    18. CK31-42 big
    19. HK50-60
    20. CP24-14
    21. HP13-3
    22. CP14-4
    23. HP3-2
    24. HC6-3
    25. CP4-14
    26. HC3-1
    27. CP14-24
    28. HC1-10
    29. CP24-34
    30. HC10-70
    31. CK42-52
    32. HK60-49
    33. CK52-42 big
    34. HK49-39
    35. HP2-12
    36. HP12-22
    37. HK39-40
    38. CP34-24
    39. HP54-44
    40. HC70-30
    41. CK42-31 big
    42. HK40-50
    43. CK31-42
    44. HK50-60
    45. HC30-70
    46. CK42-52 big
    47. HK60-50
    48. CK52-42
    49. HC70-40
    50. HK50-49
    51. HK49-39
    52. HP22-32 check
    53. CK42-53
    54. HK39-49
    55. HK49-50
    56. HC40-60
    57. HK50-40
    58. HC60-30
    59. HC30-50
    60. CP24-34
    61. HP44-34
    62. CK53-43
    63. HP32-22
    64. HP34-44 check
    65. CK43-33 big
    66. HK40-49
    67. CK33-42
    68. HP44-43 check
    69. CK42-52
    70. HP22-32
    71. HP32-42 mate

.. _漢包兵兵楚士:

漢包兵兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC81,HP6,HP7,CK31,CO43

.. janggi-moves::

    1. HK38-49
    2. CK31-42
    3. HP6-16
    4. CO43-53 big
    5. HK49-60
    6. HP16-26
    7. HP26-36
    8. HP36-46
    9. HK60-50
    10. HP46-45
    11. HP45-44
    12. HP7-17
    13. HP17-27
    14. HK50-40
    15. HP44-54
    16. HP54-64
    17. HP64-74
    18. HP74-84
    19. HC81-85
    20. HP84-74
    21. HP74-64
    22. HP64-54
    23. HP27-37
    24. HP37-36
    25. HP36-35
    26. HC85-25
    27. HC25-45
    28. CK42-52
    29. HK40-49
    30. HC45-50
    31. HP54-64
    32. HP64-63
    33. HP35-45
    34. CK52-42
    35. HK49-60 check
    36. CK42-52
    37. HP45-44
    38. HP44-54
    39. CO53-42
    40. HK60-49
    41. HC50-42
    42. CK52-42 big
    43. HK49-60
    44. HP63-53 check
    45. CK42-31
    46. HP53-43
    47. HP54-53
    48. HP43-42 mate

.. _漢包兵士士楚:

漢包兵士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC49,HP23,HO40,HO58,CK31

.. janggi-moves::

    1. HP23-33
    2. CK31-41
    3. HO58-48 check
    4. CK41-31
    5. HK38-39
    6. HC49-47
    7. HC47-50
    8. HO40-49
    9. HO49-38
    10. HP33-42 mate

.. _漢包兵士楚車:

漢包兵士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP33,HO49,CK41,CR43

.. janggi-moves::

    1. HP33-43
    2. CK41-31
    3. HP43-33
    4. CK31-41 big
    5. HK48-38 check
    6. CK41-31
    7. HK38-39
    8. HK39-40
    9. HO49-48
    10. HC50-47
    11. HK40-50
    12. HC47-49
    13. HP33-42 mate

.. _漢包兵士楚包:

漢包兵士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC9,HP53,HO39,CK51,CC32

.. janggi-moves::

    1. HK38-48
    2. CK51-41 big
    3. HP53-43
    4. CK41-51
    5. HO39-49
    6. CK51-52
    7. HC9-59
    8. CK52-51
    9. HP43-53 check
    10. CK51-41 big
    11. HK48-58
    12. HC59-39
    13. HK58-59
    14. HO49-48
    15. HC39-69
    16. CK41-51
    17. HC69-49
    18. HK59-60
    19. HK60-50
    20. HP53-42 mate

.. _漢包兵士楚馬:

漢包兵士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC10,HP43,HO48,CK41,CH40

.. janggi-moves::

    1. HC10-50
    2. CK41-51
    3. HP43-53
    4. CK51-41
    5. HK49-60 check
    6. CK41-51
    7. HO48-38
    8. CK51-41
    9. HO38-49 check
    10. CK41-51
    11. HO49-40
    12. CK51-41
    13. HO40-49 check
    14. CK41-51
    15. HO49-48
    16. HC50-47
    17. HK60-50
    18. HC47-49
    19. HP53-42 mate

.. _漢包兵士楚象:

漢包兵士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC42,HP33,HO48,CK41,CE59

.. janggi-moves::

    1. HC42-50 check
    2. CK41-31
    3. HO48-58
    4. CK31-41
    5. HO58-49 check
    6. CK41-31
    7. HO49-59
    8. CK31-41
    9. HO59-49 check
    10. CK41-31
    11. HK38-39
    12. HK39-40
    13. HO49-48
    14. HC50-47
    15. HK40-50
    16. HC47-49
    17. HP33-42 mate

.. _漢包兵士楚卒:

漢包兵士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP44,HO49,CK41,CP24

.. janggi-moves::

    1. HP44-43
    2. CK41-51
    3. HP43-53
    4. CK51-41 big
    5. HK48-58 check
    6. CK41-51
    7. HO49-38
    8. CP24-34
    9. HK58-48
    10. CP34-44
    11. HC50-47
    12. CP44-54
    13. HC47-49
    14. CP54-44
    15. HK48-58
    16. HK58-59
    17. HK59-60
    18. HK60-50
    19. CP44-54
    20. HO38-48
    21. CP54-44
    22. HC49-44
    23. HC44-49
    24. HP53-42 mate

.. _漢包兵士楚士:

漢包兵士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC50,HP23,HO49,CK41,CO42

.. janggi-moves::

    1. HP23-33
    2. CK41-31
    3. HC50-42
    4. CK31-41
    5. HC42-50 check
    6. CK41-31
    7. HK38-39
    8. HK39-40
    9. HO49-48
    10. HC50-47
    11. HK40-50
    12. HC47-49
    13. HP33-42 mate

.. _漢包兵楚包卒:

漢包兵楚包卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC49,HP37,CK31,CC32,CP47

.. janggi-moves::

    1. HP37-47
    2. HK40-50
    3. HK50-60
    4. HK60-59
    5. HK59-58
    6. HP47-46
    7. HP46-45
    8. HP45-44
    9. HP44-43
    10. HK58-48
    11. HP43-42 mate

.. _漢包兵楚包士:

漢包兵楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC49,HP33,CK31,CC32,CO43

.. janggi-moves::

    1. HP33-43
    2. HK40-50
    3. HK50-60
    4. HK60-59
    5. HK59-58
    6. HK58-48
    7. HP43-42 mate

.. _漢包兵楚馬卒:

漢包兵楚馬卒
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC50,HP44,CK41,CH39,CP34

.. janggi-moves::

    1. HP44-43
    2. CH39-47
    3. HC50-48
    4. CP34-44
    5. HC48-46
    6. CH47-68 check
    7. HK49-50
    8. HP43-42 mate

.. janggi-board::

    HK38,HC84,HP67,CK43,CH13,CP4

.. janggi-moves::

    1. CH13-25
    2. HK38-49 big
    3. CK43-33
    4. HK49-40 big
    5. CK33-42
    6. HK40-50 big
    7. CK42-31
    8. HK50-40 big
    9. CH25-33
    10. HP67-77
    11. CP4-14
    12. HP77-87
    13. CP14-24
    14. HC84-89
    15. CP24-34
    16. HK40-49
    17. CH33-54
    18. HC89-39 check
    19. CP34-44
    20. HK49-38 big
    21. CK31-41
    22. HP87-77
    23. CH54-46 check
    24. HK38-49
    25. CK41-31
    26. CP44-45
    27. CH46-65
    28. HP77-67
    29. CP45-46
    30. HC39-69
    31. CH65-84
    32. HK49-40 big
    33. CK31-41
    34. HK40-49
    35. CH84-76
    36. HC69-66
    37. CK41-51
    38. HP67-77
    39. CH76-84
    40. HP77-67
    41. CH84-63
    42. HK49-60 big
    43. CH63-55
    44. HP67-57
    45. CK51-41
    46. CH55-74
    47. HC66-26
    48. CH74-53
    49. HK60-50
    50. CH53-65
    51. HC26-66
    52. CH65-86
    53. HK50-49
    54. CK41-31
    55. CH86-78
    56. HP57-67
    57. CP46-47
    58. HK49-39 big
    59. CP47-37
    60. HC66-68
    61. CH78-70
    62. HP67-57
    63. CK31-41
    64. HP57-67
    65. CK41-51
    66. CP37-47
    67. HK39-38
    68. CH70-58
    69. HC68-48
    70. CH58-79
    71. HP67-77
    72. CH79-60
    73. HC48-18
    74. CP47-37 check
    75. HK38-49
    76. CH60-68 check
    77. HK49-59 big
    78. CK51-41
    79. HP77-67
    80. CH68-47 check
    81. HK59-49
    82. CK41-31
    83. CH47-55
    84. HP67-57
    85. CH55-36
    86. HP57-67
    87. CP37-47
    88. HK49-38
    89. CK31-41
    90. HP67-66
    91. CK41-51
    92. CH36-17 check
    93. HK38-49
    94. CH17-25
    95. HK49-38
    96. CP47-57
    97. CH25-33
    98. HK38-48
    99. CH33-45
    100. HC18-58 check
    101. CP57-47 check
    102. HK48-38
    103. CH45-66
    104. HK38-49
    105. CK51-41
    106. CH66-87
    107. CH87-75
    108. HK49-38
    109. CH75-67
    110. HK38-39
    111. CK41-51
    112. CP47-48
    113. HK39-40
    114. CH67-88
    115. CH88-76
    116. CP48-58
    117. CH76-57
    118. CP58-49 mate

.. _漢包兵楚馬士:

漢包兵楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP23,CK41,CH6,CO42

.. janggi-moves::

    1. HP23-33
    2. CH6-27 check
    3. HK48-49
    4. CH27-48
    5. HK49-48
    6. CO42-43
    7. HP33-43
    8. HP43-42 mate

.. _漢包兵楚象卒:

漢包兵楚象卒
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC10,HP43,CK41,CE40,CP29

.. janggi-moves::

    1. HC10-50
    2. CE40-17 check
    3. HK49-48
    4. CE17-49
    5. HK48-49
    6. CP29-39 check
    7. HK49-48
    8. CP39-49
    9. HK48-49
    10. HP43-42 mate

.. _漢包兵楚象士:

漢包兵楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC50,HP63,CK41,CE39,CO42

.. janggi-moves::

    1. HP63-53
    2. CE39-16 check
    3. HK48-49
    4. CE16-48
    5. HK49-48
    6. CO42-43
    7. HP53-43
    8. HP43-42 mate

.. _漢包兵楚卒卒:

漢包兵楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC50,HP23,CK31,CP29,CP69

.. janggi-moves::

    1. HP23-33
    2. CP29-39 check
    3. HK49-48
    4. CP39-49
    5. HK48-49
    6. CP69-59 check
    7. HK49-48
    8. CP59-49
    9. HK48-49
    10. HP33-42 mate

.. _漢包兵楚卒士:

漢包兵楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HC50,HP23,CK41,CP29,CO42

.. janggi-moves::

    1. HP23-33
    2. CP29-39 check
    3. HK49-48
    4. CP39-49
    5. HK48-49
    6. CO42-43
    7. HP33-43
    8. HP43-42 mate

.. _漢包兵楚士士:

漢包兵楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC49,HP23,CK41,CO42,CO33

.. janggi-moves::

    1. HP23-33
    2. CO42-43
    3. HP33-43
    4. HP43-42 mate

.. _漢包士士楚車:

漢包士士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC49,HO50,HO60,CK31,CR1

.. janggi-moves::

    1. CK31-41
    2. CR1-31 mate

.. _漢包士士楚包:

漢包士士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC39,HO40,HO49,CK32,CC42

.. janggi-moves::

    1. HO49-38 mate

.. janggi-board::

    HK40,HC49,HO50,HO39,CK32,CC31

.. janggi-moves::

    1. CK32-42 mate

.. _漢包士士楚馬:

漢包士士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC48,HO39,HO49,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢包士士楚象:

漢包士士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC39,HO40,HO49,CK32,CE42

.. janggi-moves::

    1. HO49-38 mate

.. janggi-board::

    HK48,HC58,HO49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢包士士楚卒:

漢包士士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HC48,HO39,HO49,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢包士楚包士:

漢包士楚包士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC33,HO38,CK32,CC42,CO31

.. janggi-moves::

    1. HC33-39 mate

.. _漢包士楚馬馬:

漢包士楚馬馬
~~~~~~~~~~~~

.. janggi-board::

    HK50,HC49,HO58,CK32,CH33,CH46

.. janggi-moves::

    1. CH46-58 check
    2. HK50-60
    3. CH58-79 check
    4. HK60-50
    5. CH79-87
    6. HK50-60
    7. CH87-75
    8. HK60-50
    9. CH75-56
    10. HK50-60
    11. CH56-48
    12. HK60-50
    13. CH48-36
    14. HK50-40
    15. CH36-15
    16. HK40-50
    17. CH33-54
    18. HK50-40 big
    19. CH54-35
    20. CK32-31
    21. HK40-39
    22. CH15-27 check
    23. HK39-40
    24. CK31-41
    25. HK40-50
    26. CH35-56
    27. HK50-60
    28. CH56-75
    29. CH75-67
    30. CH27-6
    31. CH6-25
    32. CH25-37
    33. CH67-86
    34. CH86-78
    35. CH78-57
    36. CH57-38
    37. CH37-58
    38. CH58-79 mate

.. _漢包士楚馬象:

漢包士楚馬象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC33,HO38,CK32,CH20,CE42

.. janggi-moves::

    1. HC33-40 check
    2. CH20-39
    3. HO38-39 mate

.. janggi-board::

    HK48,HC49,HO58,CK51,CH55,CE12

.. janggi-moves::

    1. CH55-67 check
    2. HK48-38
    3. CH67-46 check
    4. HK38-39
    5. CH46-58 check
    6. HK39-38
    7. CH58-46 check
    8. HK38-39
    9. CH46-27 check
    10. HK39-40
    11. CK51-41
    12. CE12-35
    13. HK40-50
    14. CK41-31
    15. HK50-40
    16. CH27-8
    17. HK40-39
    18. CH8-20 check
    19. HK39-38
    20. CH20-28
    21. HK38-48
    22. CH28-36 check
    23. HK48-58
    24. CH36-55 check
    25. HK58-48
    26. CH55-67 check
    27. HK48-38
    28. CK31-41
    29. CE35-7
    30. CE7-24
    31. CE24-56
    32. CE56-33
    33. HK38-39
    34. CE33-16 check
    35. HK39-40
    36. CH67-86
    37. HK40-50
    38. CH86-78
    39. HK50-60
    40. CE16-33
    41. CE33-61
    42. CE61-84
    43. CE84-52
    44. CE52-35
    45. HK60-50
    46. CH78-57
    47. HK50-40
    48. CE35-7
    49. CH57-38
    50. CH38-26
    51. CE7-24
    52. CE24-56
    53. CE56-84
    54. CE84-67
    55. CH26-7
    56. CH7-28 mate

.. _漢包士楚馬卒:

漢包士楚馬卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC40,HO50,CK31,CH38,CP4

.. janggi-moves::

    1. CH38-50
    2. HK48-49
    3. CH50-69
    4. HK49-39 big
    5. CK31-41
    6. HK39-49 big
    7. CK41-51
    8. HC40-58
    9. CH69-90
    10. HC58-40
    11. CH90-78
    12. HK49-60 big
    13. CH78-57
    14. HK60-50
    15. CP4-14
    16. HC40-60 check
    17. CH57-65
    18. HK50-49
    19. CH65-44
    20. CP14-24
    21. CK51-41
    22. CP24-34
    23. CP34-35
    24. CP35-45
    25. HK49-40
    26. CH44-65
    27. HC60-30
    28. CH65-57
    29. HC30-50 check
    30. CK41-51
    31. HC50-30
    32. CP45-46
    33. HK40-50
    34. CK51-41
    35. HC30-60
    36. CP46-47
    37. HC60-56
    38. CK41-31
    39. HK50-40 big
    40. CP47-37
    41. CP37-38
    42. CH57-49
    43. CH49-37
    44. CP38-49 mate

.. _漢包士楚馬士:

漢包士楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC34,HO38,CK33,CH42,CO43

.. janggi-moves::

    1. HC34-39 mate

.. janggi-board::

    HK38,HC48,HO39,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢包士楚象象:

漢包士楚象象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC33,HO39,CK32,CE42,CE15

.. janggi-moves::

    1. HC33-40 check
    2. CE15-38
    3. HO39-38 mate

.. janggi-board::

    HK50,HC49,HO38,CK32,CE15,CE88

.. janggi-moves::

    1. CE15-38
    2. HK50-40
    3. CE88-56
    4. HK40-50
    5. CK32-31
    6. HK50-60
    7. CE38-70
    8. HK60-59
    9. CE70-87 check
    10. HK59-58
    11. CE56-33
    12. CE87-64
    13. CE64-36
    14. HK58-48
    15. CE33-16 check
    16. HK48-58
    17. CK31-41
    18. CE36-53
    19. HK58-59
    20. CE53-76 check
    21. HK59-60
    22. CE16-33
    23. CE33-65
    24. CE65-88 check
    25. HK60-50
    26. CE76-53
    27. CE53-21
    28. CE21-4
    29. CE4-32
    30. CE32-55
    31. CE88-56
    32. CE56-84
    33. CE55-27 check
    34. HK50-40
    35. CE84-67
    36. CE27-4
    37. CE4-36
    38. CE36-8 mate

.. _漢包士楚象卒:

漢包士楚象卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HC33,HO39,CK32,CE42,CP28

.. janggi-moves::

    1. HC33-40 check
    2. CP28-38 check
    3. HO39-38 mate

.. janggi-board::

    HK40,HC9,HO58,CK53,CE35,CP4

.. janggi-moves::

    1. CE35-58
    2. HK40-39
    3. CE58-26
    4. HC9-49
    5. CE26-54
    6. HK39-38
    7. CE54-82
    8. HK38-48
    9. CE82-65 check
    10. HK48-58 big
    11. CK53-42
    12. HK58-59
    13. CP4-14
    14. HC49-69
    15. CP14-24
    16. HK59-49 big
    17. CK42-31
    18. HC69-39
    19. CP24-25
    20. CP25-26
    21. CE65-82
    22. CE82-54
    23. CP26-27
    24. CE54-71
    25. CE71-43
    26. CK31-41
    27. HK49-38
    28. CK41-51
    29. CE43-15
    30. HK38-49
    31. CP27-37
    32. HK49-60 big
    33. CK51-41
    34. HK60-50 big
    35. CE15-43
    36. HK50-49
    37. CP37-47
    38. CE43-26 check
    39. HK49-38
    40. CK41-51
    41. CP47-57
    42. CP57-58
    43. HC39-34
    44. CE26-54
    45. HC34-64
    46. CE54-71
    47. CE71-43
    48. HK38-39
    49. CP58-48
    50. CE43-66
    51. HC64-67
    52. CP48-38 check
    53. HK39-40
    54. CE66-43
    55. HK40-50
    56. CE43-26
    57. HK50-60 big
    58. CK51-41
    59. CP38-49 mate

.. _漢包士楚象士:

漢包士楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC33,HO38,CK32,CE42,CO31

.. janggi-moves::

    1. HC33-39 mate

.. janggi-board::

    HK38,HC48,HO39,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢包士楚卒卒:

漢包士楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC57,HO38,CK43,CP28,CP84

.. janggi-moves::

    1. CP28-38
    2. HK40-50 big
    3. CP38-48
    4. HK50-60
    5. CP48-58
    6. HK60-50 big
    7. CK43-33
    8. HK50-40 big
    9. CK33-42
    10. HK40-50 big
    11. CK42-51
    12. HK50-40
    13. CP84-74
    14. HC57-59 check
    15. CK51-41
    16. CP58-48
    17. HK40-39
    18. CP74-64
    19. CP64-54
    20. HC59-29
    21. CK41-51
    22. HC29-49
    23. CP54-55
    24. CP55-56
    25. CP56-57
    26. CP48-58
    27. CP57-47
    28. CP47-37
    29. CP58-48
    30. CP37-38 check
    31. HK39-40
    32. CP48-49 mate

.. _漢包士楚士士:

漢包士楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HC34,HO38,CK33,CO42,CO43

.. janggi-moves::

    1. HC34-39 mate

.. _漢馬馬象士楚:

漢馬馬象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH20,HH90,HE10,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. CK51-41 big
    9. HO40-49
    10. HH20-39
    11. HH39-47
    12. HH47-55
    13. HH55-63
    14. HH90-78
    15. HH78-86
    16. HH86-74
    17. HH74-62 check
    18. CK41-31
    19. HH62-43 check
    20. CK31-32
    21. HE10-38
    22. HE38-15 check
    23. CK32-33
    24. HH43-64
    25. HH64-45 mate

.. _漢馬馬象楚車:

漢馬馬象楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH32,HH72,HE73,CK42,CR51

.. janggi-moves::

    1. HH72-51
    2. CK42-33 big
    3. HK38-49
    4. CK33-42 big
    5. HK49-60
    6. HE73-56
    7. HE56-84
    8. HE84-67
    9. CK42-52 big
    10. HK60-50
    11. CK52-42 big
    12. HK50-40
    13. HE67-35
    14. HE35-58
    15. HE58-86
    16. CK42-33 big
    17. HK40-50
    18. CK33-42 big
    19. HK50-60
    20. HE86-54
    21. HE54-37
    22. CK42-52 big
    23. HK60-50
    24. CK52-42 big
    25. HK50-40
    26. CK42-33
    27. HH32-53
    28. CK33-42
    29. HH51-72
    30. CK42-31
    31. HK40-50
    32. CK31-42 big
    33. HH53-45
    34. HE37-14 check
    35. CK42-41
    36. HK50-40
    37. HH45-53 mate

.. _漢馬馬象楚包:

漢馬馬象楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH32,HH53,HE11,CK43,CC42

.. janggi-moves::

    1. HH32-44 check
    2. CK43-53 check
    3. HH44-65 check
    4. CK53-52
    5. HH65-73 check
    6. CK52-51
    7. HE11-34
    8. CK51-41
    9. HK48-58
    10. CK41-31
    11. HH73-52
    12. HK58-59
    13. CK31-32
    14. HH52-44 check
    15. CK32-33
    16. HH44-65
    17. HK59-60
    18. CK33-43
    19. HE34-11 check
    20. CK43-33
    21. HH65-53
    22. HH53-34
    23. HH34-13
    24. HH13-25 check
    25. CK33-32
    26. HH25-44 check
    27. CK32-31
    28. HH44-63
    29. HE11-34
    30. CK31-32
    31. HH63-51 check
    32. CK32-31
    33. HE34-66
    34. CK31-41
    35. HH51-63
    36. HE66-83
    37. CK41-31
    38. HE83-55
    39. CK31-41
    40. HE55-27
    41. CK41-31
    42. HE27-50
    43. CK31-32
    44. HH63-51 check
    45. CK32-31
    46. HE50-67
    47. CK31-41
    48. HH51-63
    49. CK41-31
    50. HK60-50
    51. HE67-35
    52. CK31-32
    53. HH63-51 check
    54. CK32-31
    55. HE35-3 check
    56. CK31-41
    57. HH51-63
    58. HH63-84
    59. HH84-65
    60. HE3-35
    61. HE35-7
    62. HH65-53 check
    63. CK41-31
    64. HE7-35
    65. HE35-3 mate

.. _漢馬馬象楚馬:

漢馬馬象楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HH70,HE64,CK52,CH33

.. janggi-moves::

    1. HH51-72
    2. CH33-45
    3. HE64-87
    4. CK52-42
    5. HK38-39
    6. CK42-31 big
    7. HK39-49
    8. CK31-42
    9. HK49-60
    10. CH45-66
    11. HE87-64
    12. CK42-52 big
    13. HK60-50
    14. CK52-42 big
    15. HK50-40
    16. CH66-45
    17. HE64-47
    18. CH45-66
    19. HE47-19
    20. CK42-31 big
    21. HE19-36
    22. CH66-47
    23. HK40-50
    24. CK31-42
    25. HE36-13
    26. CH47-26 big
    27. HK50-40
    28. CK42-31 big
    29. HE13-36
    30. CH26-38
    31. HE36-64
    32. CH38-57 big
    33. HK40-50
    34. CK31-42 big
    35. HK50-60
    36. CH57-45
    37. HE64-47
    38. CH45-66
    39. HE47-24
    40. CH66-45
    41. HE24-7
    42. CH45-26
    43. HE7-39
    44. CH26-47
    45. HE39-16
    46. CK42-52 big
    47. HK60-50
    48. CH47-28
    49. HE16-48
    50. CH28-36
    51. HE48-65
    52. CH36-57
    53. HE65-37
    54. CH57-45
    55. HE37-20
    56. CK52-42
    57. HK50-40
    58. CK42-31 big
    59. HK40-49
    60. CK31-42
    61. HH72-84
    62. CH45-37 big
    63. HK49-38
    64. CH37-16
    65. HE20-48
    66. CH16-28
    67. HE48-25 check
    68. CK42-31 big
    69. HK38-48
    70. CK31-41 big
    71. HK48-58
    72. CK41-51 big
    73. HE25-53
    74. CK51-52
    75. HH84-72
    76. CH28-36
    77. HK58-49
    78. CK52-42 big
    79. HK49-59
    80. CK42-52
    81. HH70-58
    82. CH36-44
    83. HE53-85
    84. CH44-65
    85. HH72-64 check
    86. CK52-42
    87. HE85-62
    88. HE62-34
    89. CK42-53
    90. HH64-45 check
    91. CK53-52
    92. HH45-33 check
    93. CK52-42
    94. HH33-54 check
    95. CK42-53
    96. HK59-49
    97. CK53-43 big
    98. HK49-40
    99. CH65-53
    100. HH58-66
    101. HH66-45
    102. CK43-42
    103. HK40-50
    104. CK42-31
    105. HE34-6
    106. CH53-74
    107. HE6-23
    108. CH74-53
    109. HK50-60
    110. CH53-34
    111. HE23-51
    112. CH34-55
    113. HE51-23
    114. CH55-67
    115. HH45-57
    116. HK60-49
    117. HE23-46
    118. CK31-41
    119. HK49-39
    120. CH67-55
    121. HH57-45
    122. CK41-31 big
    123. HK39-49
    124. HE46-14 check
    125. CK31-41
    126. HK49-40
    127. CH55-74
    128. HH54-42
    129. CK41-51
    130. HH42-63 check
    131. CK51-52
    132. HH63-44 check
    133. CK52-51
    134. HH44-32 check
    135. CK51-41
    136. HH45-64
    137. HH32-53 check
    138. CK41-51
    139. HH53-74
    140. HH74-82
    141. HH82-61
    142. HH64-72 check
    143. CK51-41
    144. HH61-53 mate

.. _漢馬馬象楚象:

漢馬馬象楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH8,HH10,HE66,CK41,CE11

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-40
    6. CK41-31 big
    7. HK40-50
    8. CK31-41 big
    9. HE66-49
    10. CE11-34
    11. HK50-40
    12. CK41-31
    13. HE49-26
    14. CE34-11 big
    15. HK40-50
    16. CK31-41 big
    17. HK50-60
    18. CK41-51 big
    19. HE26-54
    20. CE11-43
    21. HK60-50
    22. CK51-42
    23. HE54-77
    24. CE43-71 big
    25. HE77-49
    26. CE71-54
    27. HK50-60
    28. CK42-51
    29. HE49-66
    30. CE54-71 big
    31. HK60-50
    32. CK51-41 big
    33. HK50-40
    34. CK41-31 big
    35. HE66-34
    36. CE71-43
    37. HK40-50
    38. CK31-42
    39. HE34-62
    40. CE43-11 big
    41. HE62-45
    42. CE11-34
    43. HK50-40
    44. CK42-31
    45. HE45-73
    46. CE34-62 big
    47. HK40-50
    48. CK31-42 big
    49. HK50-60
    50. CK42-51 big
    51. HE73-56
    52. CE62-45
    53. HK60-50
    54. CK51-42
    55. HK50-40
    56. CK42-31 big
    57. HE56-39
    58. CE45-68 check
    59. HK40-50
    60. CK31-41 big
    61. HK50-60
    62. CE68-45
    63. HH8-27
    64. HK60-50
    65. CE45-77 big
    66. HK50-40
    67. HH27-46
    68. HH10-18
    69. HK40-50
    70. HE39-16
    71. CE77-45
    72. HE16-48
    73. HH18-37
    74. CE45-62
    75. HH46-65
    76. HH65-44
    77. HH44-63
    78. CK41-31
    79. HH37-45
    80. CE62-34
    81. HE48-65
    82. CK31-41
    83. HE65-42
    84. CE34-62
    85. HH45-33 check
    86. CK41-51
    87. HE42-25
    88. HE25-57
    89. HE57-85
    90. HE85-62
    91. HE62-34 mate

.. _漢馬馬象楚卒:

漢馬馬象楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HH32,HE39,CK31,CP36

.. janggi-moves::

    1. HE39-7
    2. CK31-42
    3. HE7-24
    4. CK42-33
    5. HE24-56 check
    6. CK33-42
    7. HE56-88
    8. CP36-37 check
    9. HK38-39
    10. CP37-38 check
    11. HK39-38
    12. CK42-33 big
    13. HK38-49
    14. CK33-42 big
    15. HK49-60
    16. CK42-52 big
    17. HE88-56
    18. CK52-42
    19. HE56-84
    20. HE84-67
    21. CK42-52 big
    22. HK60-50
    23. CK52-42 big
    24. HK50-40
    25. HE67-35
    26. HE35-58
    27. HE58-86
    28. CK42-33 big
    29. HK40-50
    30. CK33-42 big
    31. HK50-60
    32. HE86-54
    33. HE54-37
    34. CK42-52 big
    35. HK60-50
    36. CK52-42 big
    37. HK50-40
    38. CK42-33
    39. HH32-53
    40. CK33-42
    41. HH51-72
    42. CK42-31
    43. HK40-50
    44. CK31-42 big
    45. HH53-45
    46. HE37-14 check
    47. CK42-41
    48. HK50-40
    49. HH45-53 mate

.. _漢馬馬象楚士:

漢馬馬象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH41,HH52,HE83,CK33,CO42

.. janggi-moves::

    1. HE83-66
    2. HK48-49
    3. HK49-50
    4. HE66-49
    5. HE49-26
    6. HK50-60
    7. HE26-58
    8. HE58-75
    9. HK60-50
    10. CK33-32
    11. HH52-44 check
    12. CK32-31
    13. HH41-62
    14. CO42-33
    15. HH44-23 check
    16. CK31-42 big
    17. HE75-43
    18. HK50-40
    19. HE43-66
    20. HH62-54 check
    21. CK42-43
    22. HH23-15
    23. HH15-34
    24. HE66-89
    25. HE89-57
    26. HE57-25
    27. HH34-15
    28. HH15-3
    29. HH3-24 mate

.. _漢馬馬兵兵楚:

漢馬馬兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH2,HH20,HP1,HP32,CK43

.. janggi-moves::

    1. HK48-38
    2. CK43-33 big
    3. HK38-49
    4. CK33-43 big
    5. HK49-40
    6. CK43-33 big
    7. HH20-39
    8. CK33-32
    9. HH2-21
    10. CK32-31
    11. HK40-50
    12. CK31-41 big
    13. HH39-47
    14. HK50-40
    15. CK41-31 big
    16. HH47-35
    17. HK40-50
    18. CK31-41 big
    19. HH35-43
    20. HK50-60
    21. HH43-22 check
    22. CK41-31
    23. HH22-34
    24. CK31-41
    25. HH21-42
    26. CK41-51 big
    27. HK60-50
    28. HK50-40
    29. HH42-63 check
    30. CK51-41
    31. HP1-11
    32. CK41-31
    33. HH63-51
    34. HP11-21 check
    35. CK31-41
    36. HH51-43
    37. HP21-31 mate

.. _漢馬馬兵士楚:

漢馬馬兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH61,HH89,HP7,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-41 big
    3. HK48-58
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK58-48
    8. CK52-53
    9. HH89-68
    10. HK48-38
    11. HH68-56
    12. HH56-35
    13. HH35-54
    14. HP7-17
    15. HP17-27
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. HP34-33
    21. HH54-73
    22. HH73-61 check
    23. CK53-52
    24. HP33-42 mate

.. _漢馬馬兵楚車:

漢馬馬兵楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH1,HH21,HP7,CK52,CR11

.. janggi-moves::

    1. HH1-13
    2. HP7-17
    3. HP17-27
    4. HP27-37
    5. HP37-47
    6. HP47-57
    7. HP57-67
    8. HP67-66
    9. HP66-65
    10. HP65-64
    11. HP64-63
    12. HP63-62 check
    13. CK52-51
    14. HH13-32 check
    15. CK51-41
    16. HH32-11
    17. CK41-31 big
    18. HH21-33
    19. CK31-42
    20. HH33-54 check
    21. CK42-43
    22. HH11-32
    23. HH32-24 check
    24. CK43-53
    25. HH24-45 check
    26. CK53-43
    27. HP62-52
    28. HH54-35 mate

.. _漢馬馬兵楚包:

漢馬馬兵楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH82,HH80,HP81,CK52,CC33

.. janggi-moves::

    1. HH82-61
    2. CK52-51
    3. HH80-68
    4. CK51-41 big
    5. HK48-58
    6. HH68-56
    7. CK41-51
    8. HK58-49
    9. CK51-41 big
    10. HK49-40
    11. CK41-31
    12. HH56-44
    13. HH44-65
    14. CK31-32
    15. HH65-53 check
    16. CK32-31
    17. HH53-45
    18. CK31-32
    19. HH45-24 check
    20. CK32-31
    21. HK40-50
    22. CK31-41 big
    23. HH24-45
    24. HK50-60
    25. CK41-51 big
    26. HH45-53
    27. HK60-50
    28. HK50-40
    29. CC33-83
    30. HH53-74
    31. CK51-52
    32. HH74-55
    33. HH55-43
    34. HH43-64 check
    35. CK52-51
    36. HH64-83
    37. HH83-62
    38. HH62-54
    39. CK51-41
    40. HH61-42
    41. CK41-31 big
    42. HK40-50
    43. HK50-60
    44. HH42-23 check
    45. CK31-32
    46. HP81-71
    47. HP71-61
    48. HP61-51
    49. HP51-41
    50. HH23-11 mate

.. _漢馬馬兵楚馬:

漢馬馬兵楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH47,HH59,HP42,CK43,CH13

.. janggi-moves::

    1. HP42-52
    2. CK43-53
    3. HP52-62
    4. CK53-42
    5. HK48-38
    6. CH13-5
    7. HH47-35
    8. CH5-26 check
    9. HK38-39
    10. CK42-31
    11. HK39-49
    12. CK31-41 big
    13. HK49-60
    14. CK41-51
    15. HH35-43 check
    16. CK51-42
    17. HH43-64
    18. CK42-53
    19. HH64-56
    20. CH26-45
    21. HH56-75
    22. CH45-66
    23. HK60-50
    24. CK53-42 big
    25. HK50-40
    26. CH66-74
    27. HP62-72
    28. CK42-31 big
    29. HK40-50
    30. CK31-41 big
    31. HK50-60
    32. CK41-51
    33. HH75-54
    34. CH74-53
    35. HP72-62
    36. CH53-41
    37. HH54-73
    38. CK51-42
    39. HP62-52 check
    40. CK42-53
    41. HK60-50
    42. CH41-33
    43. HP52-62
    44. CK53-42 big
    45. HH59-47
    46. HK50-60
    47. CK42-51 big
    48. HH47-55
    49. HK60-50
    50. CK51-41 big
    51. HK50-40
    52. CK41-31
    53. HH55-43 check
    54. CK31-42
    55. HH43-64
    56. CK42-31
    57. HK40-50
    58. CK31-41 big
    59. HK50-60
    60. CK41-51 big
    61. HH64-56
    62. HK60-50
    63. CK51-41 big
    64. HH56-48
    65. CK41-42
    66. HH73-61 check
    67. CK42-43
    68. HK50-40
    69. CK43-53
    70. HH48-36
    71. CH33-41
    72. HP62-72 check
    73. CK53-43
    74. HH61-73
    75. HH73-54
    76. HH36-24 check
    77. CK43-53
    78. HH54-75
    79. HH75-63
    80. HK40-50
    81. HH24-32 check
    82. CK53-43 big
    83. HK50-60
    84. CK43-33
    85. HH32-51
    86. CH41-22
    87. HH63-44
    88. CK33-42
    89. HH51-32
    90. CH22-14
    91. HP72-62
    92. CH14-33
    93. HH32-13
    94. CK42-41
    95. HH13-34
    96. CK41-31
    97. HH44-63
    98. CK31-41
    99. HH34-46
    100. HK60-50
    101. HK50-40
    102. CK41-31
    103. HH46-34
    104. HH63-55
    105. HH55-43 check
    106. CK31-42
    107. HH43-64
    108. CK42-31
    109. HK40-50
    110. CK31-41 big
    111. HK50-60
    112. HH34-55
    113. CK41-42
    114. HH55-43
    115. CH33-25
    116. HP62-52 check
    117. CK42-53 big
    118. HK60-49
    119. CH25-44
    120. HK49-39
    121. HH43-22
    122. CH44-63
    123. HP52-62
    124. CK53-42
    125. HH64-45
    126. CK42-32 big
    127. HK39-49
    128. CH63-44
    129. HH22-34
    130. CK32-33
    131. HH45-37
    132. CK33-43
    133. HK49-39
    134. HH37-25
    135. CH44-63
    136. HK39-40
    137. CH63-75
    138. HP62-52
    139. CH75-63
    140. HH34-22 check
    141. CK43-53
    142. HP52-62
    143. CK53-42
    144. HH22-34 check
    145. CK42-43
    146. HH34-15
    147. CH63-55
    148. HH15-23
    149. CH55-47
    150. HH25-37
    151. CH47-59 check
    152. HH37-49
    153. CH59-67
    154. HH23-35 check
    155. CK43-42
    156. HH35-54 check
    157. CK42-43
    158. HH49-57
    159. CH67-59 check
    160. HK40-39
    161. CH59-47 check
    162. HK39-49
    163. CH47-35 big
    164. HH57-45
    165. CH35-23
    166. HH54-75
    167. HH75-63
    168. CH23-31
    169. HK49-60
    170. HH45-26
    171. CK43-33
    172. HH26-14 check
    173. CK33-43
    174. HH14-22 check
    175. CK43-53 big
    176. HK60-49
    177. CH31-23
    178. HK49-38
    179. CH23-35
    180. HH63-55
    181. CK53-42
    182. HH55-34 check
    183. CK42-32
    184. HP62-52
    185. CH35-54
    186. HK38-49
    187. CH54-73
    188. HP52-42 check
    189. CK32-33
    190. HH22-41
    191. HK49-60
    192. CK33-43
    193. HP42-32
    194. CH73-54
    195. HH34-53
    196. HH53-72
    197. CH54-73
    198. HH41-22
    199. CH73-54
    200. HH22-3
    201. CH54-73
    202. HH3-24 check
    203. CK43-33
    204. HH72-51
    205. CH73-52
    206. HH24-16
    207. CH52-64
    208. HH16-37
    209. CH64-52
    210. HH37-56
    211. HH56-35
    212. HH35-54 check
    213. CK33-43
    214. HP32-42
    215. HP42-52
    216. CK43-53
    217. HP52-62
    218. HH51-32 check
    219. CK53-43
    220. HH32-24 check
    221. CK43-53
    222. HH24-45 check
    223. CK53-43
    224. HP62-52
    225. HH54-35 mate

.. _漢馬馬兵楚象:

漢馬馬兵楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH61,HH3,HP75,CK41,CE24

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-59
    6. CK41-51 big
    7. HH61-53
    8. CE24-47
    9. HP75-65
    10. CK51-52
    11. HP65-55
    12. CK52-53
    13. HH3-22
    14. CE47-19
    15. HH22-34 check
    16. CK53-43
    17. HH34-15
    18. CE19-47
    19. HH15-36
    20. CE47-79
    21. HH36-44
    22. CE79-47
    23. HH44-65
    24. CE47-19
    25. HH65-57
    26. HP55-45
    27. CK43-42
    28. HP45-44
    29. CE19-47
    30. HH57-45
    31. CE47-19
    32. HH45-24
    33. CE19-47
    34. HH24-36
    35. CE47-79
    36. HH36-48
    37. CE79-47
    38. HH48-27
    39. HK59-49
    40. CE47-64
    41. HH27-15
    42. HH15-34 check
    43. CK42-33
    44. HP44-54
    45. CK33-43 big
    46. HK49-58
    47. CE64-81
    48. HH34-55
    49. CK43-42
    50. HP54-44
    51. CK42-32
    52. HP44-43
    53. CE81-64
    54. HH55-74
    55. CE64-36
    56. HH74-62
    57. CE36-8
    58. HH62-54
    59. CE8-25
    60. HH54-33
    61. CK32-31
    62. HH33-25
    63. HH25-46
    64. HH46-54
    65. HP43-42 mate

.. _漢馬馬兵楚卒:

漢馬馬兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH82,HH73,HP85,CK31,CP64

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HH73-54
    6. CP64-54
    7. HH82-63 check
    8. CK51-52
    9. HK60-50
    10. CP54-64
    11. HK50-40
    12. CK52-53
    13. HP85-84
    14. HP84-83
    15. HH63-84
    16. CK53-42
    17. HH84-76
    18. CK42-31 big
    19. HK40-50
    20. CK31-41 big
    21. HK50-60
    22. CK41-51 big
    23. HH76-57
    24. HP83-73
    25. CK51-52
    26. HP73-63
    27. CP64-54
    28. HK60-50
    29. CK52-42 big
    30. HK50-40
    31. HH57-45
    32. CK42-31 big
    33. HK40-50
    34. CP54-44
    35. HH45-66
    36. CK31-42
    37. HH66-74
    38. CP44-54 big
    39. HK50-60
    40. CP54-64
    41. HH74-66
    42. CK42-52 big
    43. HK60-50
    44. CK52-42 big
    45. HH66-45
    46. CK42-41
    47. HK50-60
    48. CP64-54
    49. HP63-53
    50. CP54-44
    51. HH45-64
    52. HH64-52
    53. HH52-33 check
    54. CK41-31
    55. HH33-21
    56. HP53-42 mate

.. _漢馬馬兵楚士:

漢馬馬兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HH42,HP2,CK41,CO31

.. janggi-moves::

    1. HH42-63
    2. CO31-42
    3. HH51-72
    4. CK41-31 big
    5. HK38-49
    6. CK31-32
    7. HH72-64
    8. CO42-53
    9. HH64-56
    10. CK32-42 big
    11. HK49-40
    12. CK42-52
    13. HP2-12
    14. CO53-43
    15. HH63-44
    16. CK52-53
    17. HH44-65 check
    18. CK53-52
    19. HP12-22
    20. CO43-33
    21. HH56-64 check
    22. CK52-42
    23. HH65-44
    24. CO33-43
    25. HH64-56
    26. CO43-33
    27. HH56-35
    28. CK42-53
    29. HH35-23
    30. HH44-65 check
    31. CK53-43
    32. HH23-35 check
    33. CK43-42
    34. HH35-54 check
    35. CK42-43
    36. HH54-62 check
    37. CK43-42
    38. HH65-44
    39. CK42-53
    40. HH44-23
    41. CO33-42
    42. HH62-74 check
    43. CK53-52
    44. HH23-44 check
    45. CO42-43
    46. HH74-62
    47. CK52-53
    48. HH44-65 check
    49. CK53-52
    50. HH65-73 check
    51. CK52-42
    52. HH62-54 check
    53. CK42-31 big
    54. HK40-50
    55. HK50-60
    56. HH73-81
    57. HH81-62
    58. CO43-42
    59. HH54-46
    60. HH46-34
    61. CO42-33
    62. HH62-54
    63. CO33-43
    64. HH34-13
    65. HP22-32 check
    66. CK31-41
    67. HH13-34
    68. HH34-46
    69. HK60-50
    70. CK41-51
    71. HK50-40
    72. HH46-65
    73. CK51-52
    74. HH65-84
    75. CO43-42
    76. HH54-42
    77. CK52-53
    78. HH84-72 check
    79. CK53-43
    80. HH72-51 check
    81. CK43-33 big
    82. HK40-50
    83. HH42-54 mate

.. _漢馬馬士士楚:

漢馬馬士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH11,HH53,HO40,HO50,CK31

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬馬士楚車:

漢馬馬士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HH53,HO38,CK31,CR1

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK40,HH28,HH49,HO39,CK31,CR1

.. janggi-moves::

    1. CR1-10 check
    2. HH28-20
    3. CR10-20 mate

.. _漢馬馬士楚包:

漢馬馬士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH2,HH64,HO59,CK31,CC42

.. janggi-moves::

    1. HH2-23 check
    2. CK31-32
    3. HH23-11 check
    4. CK32-31
    5. HK48-58
    6. CK31-41
    7. HH11-23
    8. CK41-51 big
    9. HH64-56
    10. HO59-60
    11. CK51-52
    12. HH23-31 check
    13. CK52-51
    14. HK58-59
    15. CK51-41
    16. HH31-23
    17. CK41-51
    18. HO60-50
    19. CK51-52
    20. HH23-31 check
    21. CK52-51
    22. HK59-60
    23. CK51-41
    24. HH31-23
    25. CK41-51
    26. HO50-40
    27. CK51-52
    28. HH23-31 check
    29. CK52-51
    30. HK60-50
    31. CK51-41
    32. HH31-23
    33. CK41-51
    34. HH23-15
    35. HH15-34
    36. HH56-64
    37. HH64-85
    38. HH85-73
    39. HH73-54
    40. HH54-33
    41. HH34-55
    42. HH55-63 mate

.. janggi-board::

    HK39,HH48,HH49,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-37 mate

.. _漢馬馬士楚馬:

漢馬馬士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH72,HH23,HO38,CK32,CH42

.. janggi-moves::

    1. HH72-51 check
    2. CK32-33
    3. HH23-2
    4. HH2-14 mate

.. janggi-board::

    HK38,HH48,HH49,HO39,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢馬馬士楚象:

漢馬馬士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH1,HH23,HO38,CK43,CE42

.. janggi-moves::

    1. HH23-31 check
    2. CK43-33
    3. HH31-12 check
    4. CK33-43
    5. HH1-22 check
    6. CK43-33
    7. HH22-34 check
    8. CK33-43
    9. HH34-55 check
    10. CK43-53
    11. HH12-31
    12. HH55-34 mate

.. janggi-board::

    HK48,HH58,HH49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢馬馬士楚卒:

漢馬馬士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HH53,HO38,CK31,CP4

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK38,HH48,HH49,HO39,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢馬馬士楚士:

漢馬馬士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH43,HH53,HO38,CK32,CO42

.. janggi-moves::

    1. HH43-24 check
    2. CK32-31
    3. HH24-12 mate

.. _漢馬馬楚馬士:

漢馬馬楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH72,HH54,CK43,CH42,CO53

.. janggi-moves::

    1. HH72-51 check
    2. CO53-52
    3. HH51-32
    4. CK43-53
    5. HH54-33
    6. CO52-51
    7. HH33-45 check
    8. CK53-52
    9. HH32-44 check
    10. CK52-53
    11. HH44-63 check
    12. CK53-43
    13. HH63-51 mate

.. janggi-board::

    HK38,HH48,HH39,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢馬馬楚象士:

漢馬馬楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH51,HH44,CK53,CE42,CO32

.. janggi-moves::

    1. HH51-72 check
    2. CK53-43
    3. HH72-64 check
    4. CK43-33
    5. HH44-52
    6. CO32-31
    7. HH64-45 check
    8. CK33-32
    9. HH52-44 check
    10. CK32-33
    11. HH44-23 check
    12. CK33-32
    13. HH23-11 mate

.. _漢馬馬楚卒卒:

漢馬馬楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH11,HH53,CK31,CP10,CP20

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK38,HH27,HH37,CK51,CP47,CP84

.. janggi-moves::

    1. CP47-37 check
    2. HK38-49
    3. CP37-27
    4. HK49-60 big
    5. CK51-41
    6. HK60-50 big
    7. CK41-31
    8. CP27-37
    9. HK50-49
    10. CP37-47
    11. CK31-41
    12. CK41-51
    13. CP47-57
    14. CP84-74
    15. CP74-64
    16. CP64-65
    17. CP65-66
    18. CP66-67
    19. CP67-68
    20. CP68-58 check
    21. HK49-40
    22. CP57-47
    23. CP47-48
    24. CP48-49 mate

.. _漢馬馬楚卒士:

漢馬馬楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH43,HH53,CK32,CP4,CO42

.. janggi-moves::

    1. HH43-24 check
    2. CK32-31
    3. HH24-12 mate

.. _漢馬馬楚士士:

漢馬馬楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH2,HH33,CK31,CO41,CO42

.. janggi-moves::

    1. HH33-12 check
    2. CK31-32
    3. HH12-24 check
    4. CK32-31
    5. HH2-23 check
    6. CK31-32
    7. HH23-4 check
    8. CK32-31
    9. HH4-12 mate

.. _漢馬象象士楚:

漢馬象象士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH30,HE10,HE90,HO40,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-39
    6. HH30-38
    7. HH38-59
    8. HH59-67
    9. HH67-75
    10. HH75-63
    11. HE10-38
    12. HE38-15
    13. HE90-67
    14. HE67-35
    15. CK41-31
    16. HK39-49
    17. HK49-60
    18. HE35-3 check
    19. CK31-41
    20. HE15-47
    21. HE47-24 mate

.. _漢馬象象楚士:

漢馬象象楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH24,HE41,HE73,CK31,CO43

.. janggi-moves::

    1. HK38-49
    2. CO43-53
    3. HK49-60
    4. HH24-12 check
    5. CK31-42
    6. HH12-4
    7. CO53-43
    8. HH4-23 check
    9. CK42-51 big
    10. HK60-50
    11. CO43-42
    12. HE41-13
    13. HK50-40
    14. HH23-15
    15. HH15-34
    16. HE73-56
    17. HE56-88
    18. HE88-65
    19. CO42-31
    20. HH34-55
    21. CO31-32
    22. HH55-63 check
    23. CK51-41
    24. HH63-44
    25. HH44-23
    26. HE13-36
    27. CK41-51
    28. HH23-44
    29. CO32-31
    30. HE36-13
    31. HH44-63 check
    32. CK51-52
    33. HE13-45
    34. HE45-17
    35. HE17-34
    36. HH63-71 check
    37. CK52-53
    38. HE34-2
    39. HE2-25 check
    40. CK53-43
    41. HH71-52
    42. HH52-31 mate

.. _漢馬象兵兵楚:

漢馬象兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH61,HE41,HP17,HP87,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41 big
    3. HK49-60
    4. CK41-51 big
    5. HH61-53
    6. CK51-52
    7. HK60-50
    8. CK52-53
    9. HK50-40
    10. HP17-27
    11. HP27-37
    12. HK40-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54 check
    19. CK53-42
    20. HP87-77
    21. HP77-67
    22. HP67-66
    23. HP66-65
    24. HP65-64
    25. HP64-63
    26. HP63-53 check
    27. CK42-31
    28. HP54-44
    29. HP44-43
    30. HP43-42 mate

.. _漢馬象兵士楚:

漢馬象兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH51,HE81,HP7,HO49,CK31

.. janggi-moves::

    1. HK38-48
    2. CK31-42 big
    3. HH51-43
    4. CK42-43 big
    5. HK48-58
    6. HK58-59
    7. HE81-64
    8. HE64-41
    9. HE41-73
    10. HE73-56
    11. HE56-88
    12. HE88-65
    13. HP7-17
    14. HP17-27
    15. HP27-37
    16. HP37-36
    17. HP36-35
    18. HP35-34
    19. HP34-33 check
    20. CK43-53 big
    21. HO49-58
    22. HE65-48
    23. HE48-25 check
    24. CK53-52
    25. HP33-42 mate

.. _漢馬象兵楚卒:

漢馬象兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH22,HE21,HP85,CK31,CP24

.. janggi-moves::

    1. HK38-49
    2. CK31-42 big
    3. HH22-43
    4. CK42-43 big
    5. HK49-60
    6. CK43-42
    7. HE21-4
    8. CK42-51 big
    9. HK60-50
    10. CK51-41 big
    11. HK50-40
    12. CK41-31 big
    13. HE4-36
    14. CP24-34
    15. HK40-50
    16. HK50-60
    17. HE36-59
    18. HP85-75
    19. CP34-44
    20. HE59-76
    21. CP44-54
    22. HP75-65
    23. HE76-48
    24. HE48-20
    25. HE20-37
    26. CP54-44
    27. HK60-50
    28. HP65-55
    29. HE37-65
    30. HE65-48
    31. HE48-16
    32. CP44-34
    33. HP55-45
    34. HE16-48
    35. HE48-80
    36. HE80-57
    37. HE57-29
    38. HE29-6
    39. CP34-24
    40. HP45-35
    41. HK50-40
    42. HE6-38
    43. HE38-70
    44. HE70-47
    45. HE47-30
    46. HE30-7
    47. CP24-14
    48. HP35-34
    49. CK31-32
    50. HE7-39
    51. CP14-15
    52. HE39-56
    53. CK32-42
    54. HE56-88
    55. CK42-51
    56. HE88-65
    57. CP15-25
    58. HK40-49
    59. HP34-44
    60. CK51-52
    61. HE65-48
    62. CP25-35
    63. HP44-43
    64. CP35-45
    65. HE48-76
    66. CK52-51
    67. HP43-53
    68. CP45-46
    69. HK49-59
    70. HE76-44
    71. HE44-16
    72. HK59-60
    73. HE16-33
    74. CP46-47
    75. HK60-50
    76. CP47-48
    77. HE33-65
    78. CP48-49 check
    79. HK50-49
    80. HP53-42 mate

.. _漢馬象兵楚士:

漢馬象兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK40,HH71,HE50,HP62,CK51,CO31

.. janggi-moves::

    1. HP62-52 check
    2. CK51-41
    3. HH71-63
    4. CO31-32
    5. HE50-78
    6. CK41-31
    7. HK40-50
    8. CO32-33
    9. HE78-46
    10. CO33-43
    11. HE46-14 check
    12. CK31-32
    13. HH63-44
    14. CK32-33
    15. HH44-25 check
    16. CK33-32
    17. HH25-13 check
    18. CK32-31
    19. HK50-60
    20. HH13-34 check
    21. CK31-32
    22. HE14-46
    23. HE46-78
    24. HE78-55
    25. HE55-87
    26. HE87-64
    27. CK32-31
    28. HH34-15
    29. CK31-32
    30. HH15-23
    31. CK32-33
    32. HE64-81
    33. HH23-2
    34. HH2-14 check
    35. CK33-32
    36. HE81-64
    37. CK32-31
    38. HH14-35
    39. CO43-33
    40. HH35-23 check
    41. CK31-41
    42. HE64-36
    43. HH23-4
    44. HH4-12
    45. CO33-32
    46. HE36-13
    47. HH12-33
    48. HH33-54
    49. CK41-31
    50. HE13-36
    51. CK31-41
    52. HE36-4
    53. CO32-31
    54. HH54-33 check
    55. CO31-32
    56. HE4-32
    57. HH33-54
    58. HP52-42 mate

.. _漢馬象士士楚:

漢馬象士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH53,HE42,HO40,HO50,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢馬象士楚車:

漢馬象士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH53,HE42,HO38,CK31,CR1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK50,HH39,HE49,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-10 check
    2. HH39-20
    3. CR10-20 mate

.. _漢馬象士楚包:

漢馬象士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH56,HE71,HO59,CK31,CC42

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HH56-64
    4. CK41-51 big
    5. HE71-54
    6. CK51-41
    7. HO59-60
    8. CK41-31
    9. HH64-72
    10. CK31-41
    11. HH72-53 check
    12. CK41-51
    13. HH53-61
    14. CK51-52
    15. HH61-73 check
    16. CK52-53
    17. HK58-59
    18. HO60-50
    19. HK59-60
    20. HH73-65 check
    21. CK53-52
    22. HH65-44 check
    23. CK52-51
    24. HH44-23
    25. HO50-40
    26. CK51-52
    27. HH23-31 check
    28. CK52-51
    29. HK60-50
    30. CK51-41
    31. HH31-23
    32. CK41-51
    33. HE54-86
    34. CK51-52
    35. HH23-31 check
    36. CK52-51
    37. HE86-58
    38. HH31-23
    39. CK51-41
    40. HO40-39
    41. HK50-40
    42. HH23-4
    43. CK41-31
    44. HH4-25
    45. CK31-32
    46. HH25-44 check
    47. CK32-31
    48. HH44-63
    49. HE58-35
    50. CK31-32
    51. HH63-51 check
    52. CK32-31
    53. HE35-3 check
    54. CK31-41
    55. HH51-63
    56. HH63-84
    57. HH84-65
    58. HE3-35
    59. HE35-7
    60. HH65-53 check
    61. CK41-31
    62. HE7-35
    63. HE35-3 mate

.. janggi-board::

    HK39,HH49,HE48,HO38,CK32,CC31

.. janggi-moves::

    1. CK32-42 mate

.. _漢馬象士楚馬:

漢馬象士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH51,HE42,HO38,CK33,CH1

.. janggi-moves::

    1. HE42-65 mate

.. janggi-board::

    HK38,HH49,HE48,HO39,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢馬象士楚象:

漢馬象士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH72,HE25,HO38,CK43,CE42

.. janggi-moves::

    1. HH72-51 check
    2. CK43-33
    3. HE25-48
    4. HK39-40
    5. HO38-39
    6. HE48-16 mate

.. janggi-board::

    HK48,HH49,HE58,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢馬象士楚卒:

漢馬象士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HE64,HO38,CK31,CP4

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK38,HH49,HE48,HO39,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢馬象士楚士:

漢馬象士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH54,HE21,HO38,CK32,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢馬象楚馬象:

漢馬象楚馬象
~~~~~~~~~~~~

.. janggi-board::

    HK50,HH21,HE81,CK32,CH17,CE42

.. janggi-moves::

    1. HE81-64 check
    2. CK32-31
    3. HH21-2
    4. CH17-29 check
    5. HK50-49
    6. CH29-37 check
    7. HK49-60
    8. HH2-23 mate

.. _漢馬象楚馬士:

漢馬象楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH73,HE52,CK43,CH42,CO33

.. janggi-moves::

    1. HE52-75 check
    2. CK43-53
    3. HH73-65 check
    4. CK53-43
    5. HH65-57 check
    6. CK43-53
    7. HH57-45 mate

.. janggi-board::

    HK38,HH39,HE48,CK41,CH45,CO31

.. janggi-moves::

    1. CH45-57 mate

.. _漢馬象楚象象:

漢馬象楚象象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH2,HE43,CK32,CE41,CE42

.. janggi-moves::

    1. HE43-15 check
    2. CK32-31
    3. HH2-23 check
    4. CK31-32
    5. HH23-4 check
    6. CK32-31
    7. HH4-12 mate

.. janggi-board::

    HK38,HH49,HE39,CK41,CE76,CE89

.. janggi-moves::

    1. CE89-66 check
    2. HK38-48 big
    3. CE66-43 check
    4. HK48-38
    5. CE43-15 mate

.. _漢馬象楚象士:

漢馬象楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH22,HE15,CK33,CE41,CO42

.. janggi-moves::

    1. HH22-14 check
    2. CK33-32
    3. HH14-35 check
    4. CK32-31
    5. HH35-23 check
    6. CK31-32
    7. HH23-4 check
    8. CK32-31
    9. HH4-12 mate

.. janggi-board::

    HK38,HH39,HE48,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢馬象楚卒卒:

漢馬象楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH53,HE42,CK31,CP10,CP20

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK38,HH25,HE52,CK42,CP15,CP74

.. janggi-moves::

    1. CP15-25
    2. HK38-49 big
    3. CK42-52
    4. HK49-60 big
    5. CK52-42
    6. HK60-50 big
    7. CK42-31
    8. CP25-35
    9. CP35-45
    10. CK31-41
    11. CK41-51
    12. CP45-55
    13. CP55-56
    14. CP56-57
    15. CP57-58
    16. CP74-64
    17. CP58-48
    18. CP64-54
    19. CP54-55
    20. CP55-56
    21. CP56-57
    22. CP57-58
    23. CP48-49 mate

.. _漢馬象楚卒士:

漢馬象楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH54,HE21,CK32,CP4,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢馬象楚士士:

漢馬象楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH22,HE15,CK33,CO41,CO42

.. janggi-moves::

    1. HH22-14 check
    2. CK33-32
    3. HH14-35 check
    4. CK32-31
    5. HH35-23 check
    6. CK31-32
    7. HH23-4 check
    8. CK32-31
    9. HH4-12 mate

.. _漢馬兵兵士楚:

漢馬兵兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH31,HP7,HP87,HO50,CK41

.. janggi-moves::

    1. HH31-43
    2. CK41-42
    3. HK48-38
    4. CK42-43
    5. HO50-40
    6. CK43-33 big
    7. HK38-49
    8. CK33-42 big
    9. HK49-60
    10. HP7-17
    11. HP17-27
    12. HP27-37
    13. CK42-51 big
    14. HK60-50
    15. HP37-47
    16. HP47-57
    17. HK50-60
    18. HP57-56
    19. HP56-55
    20. HP55-54
    21. HP54-53
    22. HP87-77
    23. HP77-67
    24. HP67-57
    25. HP53-43
    26. HP57-56
    27. HP56-55
    28. HP55-54
    29. HP54-53
    30. HP43-42 mate

.. _漢馬兵兵楚車:

漢馬兵兵楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH42,HP17,HP87,CK41,CR61

.. janggi-moves::

    1. HH42-61
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HH61-53
    8. CK51-52
    9. HK60-50
    10. CK52-53
    11. HK50-40
    12. HP17-27
    13. HP27-37
    14. HK40-50
    15. HP37-47
    16. HP47-57
    17. HK50-60
    18. HP57-56
    19. HP56-55
    20. HP55-54 check
    21. CK53-42
    22. HP87-77
    23. HP77-67
    24. HP67-66
    25. HP66-65
    26. HP65-64
    27. HP64-63
    28. HP63-53 check
    29. CK42-31
    30. HP54-44
    31. HP44-43
    32. HP43-42 mate

.. _漢馬兵兵楚包:

漢馬兵兵楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH12,HP81,HP3,CK41,CC32

.. janggi-moves::

    1. HK48-58
    2. CK41-51 big
    3. HK58-49
    4. CK51-41 big
    5. HK49-40
    6. CK41-42
    7. HP81-71
    8. HP71-61
    9. HP61-51
    10. CC32-2
    11. HH12-24
    12. CC2-4
    13. HP3-13
    14. CC4-34
    15. HH24-3
    16. CK42-32
    17. HH3-11 check
    18. CK32-33
    19. HP13-23 check
    20. CK33-42
    21. HP51-41
    22. HP23-13
    23. HH11-23 check
    24. CK42-33
    25. HK40-50
    26. CC34-32
    27. HK50-60
    28. HH23-15
    29. HP13-23 check
    30. CK33-42
    31. HH15-36
    32. CC32-37
    33. HH36-57
    34. HH57-45
    35. CK42-41
    36. HK60-50
    37. CK41-51
    38. HK50-40
    39. HH45-37
    40. HH37-45
    41. HP23-33
    42. HH45-53
    43. HH53-61
    44. HP33-42 mate

.. _漢馬兵兵楚馬:

漢馬兵兵楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH78,HP84,HP7,CK51,CH3

.. janggi-moves::

    1. HP84-74
    2. CK51-41
    3. HP74-64
    4. CH3-15
    5. HP7-17
    6. CH15-36
    7. HP17-27
    8. CH36-48
    9. HK39-38
    10. CH48-56
    11. HP64-74
    12. CK41-31 big
    13. HK38-49
    14. CH56-35
    15. HP27-17
    16. CK31-41 big
    17. HK49-60
    18. CK41-51 big
    19. HH78-57
    20. CH35-54
    21. HK60-50
    22. CK51-41 big
    23. HK50-40
    24. CK41-31 big
    25. HH57-36
    26. CH54-66
    27. HP74-84
    28. CH66-87
    29. HP17-27
    30. CH87-68
    31. HP84-74
    32. HP74-73
    33. CH68-56
    34. HK40-49
    35. CK31-42 big
    36. HK49-58
    37. CK42-52
    38. HH36-57
    39. CH56-35
    40. HP27-37
    41. CH35-54
    42. HP73-72
    43. CH54-66 check
    44. HK58-49
    45. CK52-42 big
    46. HK49-40
    47. CK42-31
    48. HH57-65
    49. CH66-45
    50. HH65-46
    51. CH45-53
    52. HP72-62
    53. CH53-45
    54. HK40-49
    55. CK31-41
    56. HP37-47
    57. CK41-42
    58. HP62-72
    59. HP72-82
    60. HK49-40
    61. CK42-31 big
    62. HP47-37
    63. CH45-26
    64. HH46-25
    65. CH26-7
    66. HK40-39
    67. CH7-28
    68. HK39-38
    69. CH28-7
    70. HK38-48
    71. CH7-28
    72. HH25-46
    73. HH46-34
    74. CK31-32
    75. HK48-38
    76. CK32-33
    77. HH34-15
    78. HP82-72
    79. HP72-62
    80. CK33-43
    81. HH15-34
    82. CH28-7
    83. HP37-47
    84. CH7-19 check
    85. HK38-49
    86. CH19-27
    87. HH34-26
    88. HP62-72
    89. CK43-42
    90. HK49-59
    91. HP47-57
    92. CK42-51
    93. HH26-47
    94. CH27-15
    95. HP72-62
    96. CK51-42
    97. HK59-58
    98. CH15-36
    99. HP57-56
    100. CH36-24
    101. HH47-55
    102. CH24-43
    103. HH55-63 check
    104. CK42-51
    105. HP62-72 check
    106. CK51-41
    107. HP56-46
    108. CH43-64
    109. HK58-49
    110. HK49-50
    111. HP72-62
    112. CK41-51
    113. HH63-44
    114. CK51-42
    115. HH44-23 check
    116. CK42-53
    117. HH23-35
    118. CH64-83
    119. HP62-72
    120. CH83-64
    121. HP72-82
    122. CH64-43
    123. HH35-23
    124. CH43-24
    125. HH23-44
    126. CK53-42
    127. HK50-60
    128. CH24-16
    129. HP46-45
    130. CH16-37
    131. HH44-23 check
    132. CK42-51 big
    133. HP45-55
    134. CH37-58
    135. HP82-72
    136. HH23-35
    137. HH35-56
    138. CH58-66
    139. HK60-50
    140. CH66-74
    141. HP55-54
    142. CK51-42 big
    143. HP54-44
    144. CH74-55
    145. HH56-75
    146. HK50-49
    147. HH75-54 check
    148. CK42-53
    149. HP72-62
    150. HH54-73
    151. CK53-42
    152. HP62-52 check
    153. CK42-53
    154. HK49-40
    155. CH55-74
    156. HP44-54 check
    157. CK53-43
    158. HH73-61
    159. CH74-55
    160. HP54-44 check
    161. CK43-33 big
    162. HK40-50
    163. CH55-36
    164. HP44-54
    165. CH36-57
    166. HH61-73
    167. CK33-43 big
    168. HK50-60
    169. HP54-44 check
    170. CK43-33
    171. HH73-54
    172. CH57-65
    173. HP44-34 check
    174. CK33-43
    175. HP52-42
    176. CK43-53
    177. HK60-50
    178. CH65-57
    179. HK50-40
    180. HP42-32
    181. CH57-45
    182. HP34-44
    183. CH45-64
    184. HH54-66
    185. HH66-74
    186. HH74-62
    187. HP44-54 check
    188. CK53-52
    189. HP54-64
    190. HP64-54
    191. HH62-74
    192. HP54-53 check
    193. CK52-51
    194. HP32-42 mate

.. _漢馬兵兵楚象:

漢馬兵兵楚象
~~~~~~~~~~~~

.. janggi-board::

    HK40,HH50,HP82,HP83,CK43,CE11

.. janggi-moves::

    1. HH50-38
    2. CK43-33
    3. HK40-50
    4. CK33-42 big
    5. HH38-46
    6. CE11-43
    7. HH46-54 check
    8. CK42-41
    9. HH54-33 check
    10. CK41-42
    11. HH33-45
    12. CE43-11
    13. HK50-60
    14. CK42-51 big
    15. HK60-49
    16. CK51-41
    17. HP83-73
    18. CE11-34
    19. HK49-38
    20. CK41-31
    21. HH45-26
    22. CE34-6 big
    23. HK38-49
    24. CK31-41 big
    25. HH26-47
    26. CE6-29
    27. HK49-60
    28. CK41-51 big
    29. HH47-55
    30. CE29-46
    31. HK60-49
    32. CK51-41
    33. HK49-38
    34. CK41-31 big
    35. HH55-34
    36. CE46-74
    37. HK38-49
    38. CK31-41 big
    39. HK49-60
    40. CK41-51 big
    41. HH34-55
    42. CE74-57
    43. HH55-63 check
    44. CK51-52
    45. HH63-44 check
    46. CK52-51
    47. HP73-63
    48. CE57-85 big
    49. HK60-50
    50. CK51-41
    51. HP82-72
    52. HP63-73
    53. CE85-53
    54. HK50-60
    55. HH44-65
    56. CK41-42
    57. HP72-62
    58. HP73-63
    59. CE53-21
    60. HH65-53
    61. CK42-51
    62. HK60-50
    63. CK51-42 big
    64. HK50-40
    65. HH53-34 check
    66. CK42-32
    67. HK40-50
    68. CK32-33
    69. HK50-60
    70. CK33-43
    71. HP62-52
    72. HH34-55 check
    73. CK43-33
    74. HH55-36
    75. HH36-44
    76. HH44-23
    77. HH23-31
    78. HP63-53
    79. HP52-42
    80. HP53-43 mate

.. _漢馬兵兵楚卒:

漢馬兵兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH12,HP41,HP87,CK32,CP54

.. janggi-moves::

    1. HK38-49
    2. CK32-42 big
    3. HK49-60
    4. CP54-64
    5. HH12-24
    6. CP64-65
    7. HP87-77
    8. CK42-52 big
    9. HK60-50
    10. CK52-42 big
    11. HK50-40
    12. CP65-66
    13. HH24-45
    14. CK42-32 big
    15. HH45-37
    16. HP41-51
    17. HP51-61
    18. HK40-50
    19. CK32-42 big
    20. HK50-60
    21. CK42-52 big
    22. HH37-58
    23. HK60-50
    24. CK52-42 big
    25. HK50-40
    26. CK42-31 big
    27. HH58-39
    28. HK40-50
    29. HH39-47
    30. CK31-42
    31. HK50-40
    32. CK42-31 big
    33. HH47-35
    34. HK40-50
    35. CK31-41 big
    36. HK50-60
    37. CK41-42
    38. HH35-54 check
    39. CK42-53
    40. HK60-50
    41. CK53-43 big
    42. HK50-40
    43. HH54-66
    44. HH66-54
    45. HH54-35 check
    46. CK43-33
    47. HP77-67
    48. HP67-57
    49. HK40-50
    50. HP57-47
    51. HP47-46
    52. HP46-45
    53. HP45-44
    54. HP44-43 check
    55. CK33-32
    56. HH35-54
    57. HP43-42 mate

.. _漢馬兵兵楚士:

漢馬兵兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH11,HP7,HP87,CK31,CO53

.. janggi-moves::

    1. HK38-49
    2. CK31-42 big
    3. HK49-60
    4. CO53-52
    5. HH11-23 check
    6. CK42-53 big
    7. HK60-50
    8. HK50-40
    9. CK53-43
    10. HH23-15
    11. HH15-34
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-36
    16. HP87-77
    17. HP77-67
    18. HP67-57
    19. HP57-56
    20. HP56-55
    21. HP55-54
    22. HH34-26
    23. HP36-35
    24. HH26-45
    25. CK43-42
    26. HP35-34
    27. CK42-31
    28. HP34-33
    29. CO52-42
    30. HP54-53
    31. CO42-32
    32. HK40-50
    33. HP33-32 check
    34. CK31-32
    35. HH45-66
    36. HH66-54
    37. HP53-42 mate

.. _漢馬兵士士楚:

漢馬兵士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH89,HP7,HO40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. CK41-51 big
    7. HK60-50
    8. HH89-77
    9. HH77-85
    10. HH85-73
    11. HH73-54
    12. HP7-17
    13. HP17-27
    14. HP27-37
    15. HP37-36
    16. HP36-35
    17. HP35-34
    18. CK51-41 big
    19. HP34-44
    20. CK41-51
    21. HP44-43
    22. CK51-52
    23. HH54-73 check
    24. CK52-51
    25. HH73-61
    26. HP43-42 mate

.. _漢馬兵士楚車:

漢馬兵士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH89,HP17,HO40,CK31,CR7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. HH89-77
    11. HH77-85
    12. HH85-73
    13. HH73-54
    14. HP7-17
    15. HP17-27
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. CK51-41 big
    21. HP34-44
    22. CK41-51
    23. HP44-43
    24. CK51-52
    25. HH54-73 check
    26. CK52-51
    27. HH73-61
    28. HP43-42 mate

.. _漢馬兵士楚包:

漢馬兵士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH31,HP7,HO40,CK32,CC41

.. janggi-moves::

    1. HH31-12
    2. CK32-42 big
    3. HK48-38
    4. CK42-32 big
    5. HK38-49
    6. CK32-42 big
    7. HK49-60
    8. CK42-51 big
    9. HK60-50
    10. CK51-42 big
    11. HO40-49
    12. CC41-49
    13. HH12-24
    14. CC49-41 big
    15. HK50-40
    16. CK42-31 big
    17. HH24-36
    18. CK31-42
    19. HH36-55
    20. CK42-31 big
    21. HH55-34
    22. CC41-21
    23. HP7-17
    24. CK31-41
    25. HP17-27
    26. CC21-51
    27. HH34-22 check
    28. CK41-42
    29. HP27-37
    30. CC51-33 check
    31. HP37-47
    32. CK42-53
    33. HP47-46
    34. CC33-63
    35. HP46-45
    36. CK53-42
    37. HK40-50
    38. CK42-33
    39. HH22-14 check
    40. CK33-32
    41. HK50-60
    42. HP45-55
    43. HP55-54
    44. CK32-42
    45. HH14-22
    46. HH22-34 check
    47. CK42-31
    48. HP54-53
    49. CK31-32
    50. HH34-46
    51. HH46-54
    52. HP53-42 mate

.. _漢馬兵士楚馬:

漢馬兵士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK40,HH69,HP53,HO49,CK41,CH10

.. janggi-moves::

    1. HH69-57
    2. CK41-31 big
    3. HO49-39
    4. CH10-18
    5. HH57-45
    6. CH18-39
    7. HP53-43
    8. CH39-47 big
    9. HP43-33
    10. CH47-28 check
    11. HK40-39
    12. CH28-47 check
    13. HK39-38
    14. CH47-55
    15. HH45-53
    16. CH55-43
    17. HK38-49
    18. CH43-51
    19. HK49-60
    20. HH53-34
    21. CH51-63
    22. HP33-43
    23. CK31-32
    24. HH34-13 check
    25. CK32-31
    26. HH13-21
    27. CK31-41
    28. HP43-53
    29. CK41-51
    30. HH21-33
    31. CH63-71
    32. HK60-50
    33. HH33-54
    34. CH71-63
    35. HP53-63
    36. CK51-52
    37. HH54-33 check
    38. CK52-42 big
    39. HH33-45
    40. HK50-60
    41. HP63-53 check
    42. CK42-31
    43. HH45-66
    44. HH66-54
    45. HP53-42 mate

.. _漢馬兵士楚象:

漢馬兵士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH5,HP65,HO40,CK53,CE47

.. janggi-moves::

    1. HH5-26
    2. CE47-79
    3. HH26-45 check
    4. CK53-42
    5. HH45-37
    6. CE79-47
    7. HH37-16
    8. CK42-31 big
    9. HK38-49
    10. CE47-19
    11. HH16-24
    12. CE19-47
    13. HH24-45
    14. CE47-15
    15. HO40-39
    16. CE15-32
    17. HK49-40
    18. CE32-4
    19. HH45-24
    20. CE4-32
    21. HH24-43 check
    22. CK31-41
    23. HH43-35
    24. HH35-23
    25. HH23-44
    26. CE32-4
    27. HP65-64
    28. CE4-21
    29. HH44-32
    30. CK41-31
    31. HH32-24
    32. CK31-41
    33. HP64-54
    34. CK41-51
    35. HH24-5
    36. CE21-4
    37. HH5-17
    38. CK51-42
    39. HH17-25
    40. CE4-27
    41. HH25-46
    42. CE27-4
    43. HH46-34 check
    44. CK42-32
    45. HH34-15
    46. CK32-33
    47. HP54-44
    48. HK40-49
    49. HO39-38
    50. HO38-48
    51. HH15-3
    52. CE4-27
    53. HP44-54
    54. CK33-42
    55. HH3-15
    56. CE27-10
    57. HH15-34 check
    58. CK42-33
    59. HP54-53
    60. CE10-27
    61. HH34-13
    62. CE27-44
    63. HH13-25 check
    64. CK33-32
    65. HH25-44 check
    66. CK32-31
    67. HH44-65
    68. HH65-73
    69. HH73-54
    70. HP53-42 mate

.. _漢馬兵士楚卒:

漢馬兵士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH41,HP7,HO49,CK42,CP24

.. janggi-moves::

    1. HH41-62
    2. CK42-31 big
    3. HK38-48
    4. CK31-42 big
    5. HK48-58
    6. CK42-51 big
    7. HH62-54
    8. CP24-34
    9. HK58-48
    10. CK51-41 big
    11. HH54-46
    12. CP34-35
    13. HK48-38
    14. CP35-45
    15. HH46-65
    16. CK41-31 big
    17. HK38-48
    18. HK48-58
    19. HK58-59
    20. HK59-60
    21. HH65-73
    22. HH73-54
    23. HP7-6
    24. HP6-16
    25. HP16-15
    26. CK31-41
    27. HP15-25
    28. CK41-51
    29. HK60-50
    30. CK51-52
    31. HP25-24
    32. CP45-46
    33. HP24-34
    34. CP46-47
    35. HP34-33
    36. CP47-48
    37. HH54-73 check
    38. CK52-51
    39. HH73-61
    40. CP48-49 check
    41. HK50-49
    42. HP33-42 mate

.. _漢馬兵士楚士:

漢馬兵士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH43,HP7,HO39,CK53,CO51

.. janggi-moves::

    1. HH43-51
    2. HH51-72 check
    3. CK53-42
    4. HH72-64
    5. HH64-45
    6. HK38-49
    7. HK49-40
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. HP37-36
    12. HP36-35
    13. HP35-34
    14. HP34-33 check
    15. CK42-41
    16. HH45-53 check
    17. CK41-51
    18. HH53-61
    19. HP33-42 mate

.. _漢馬兵楚馬士:

漢馬兵楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH81,HP23,CK43,CH80,CO33

.. janggi-moves::

    1. HH81-62 check
    2. CK43-53
    3. HP23-33
    4. CH80-68
    5. HH62-41 check
    6. CK53-52
    7. HP33-43
    8. CK52-51
    9. HH41-33
    10. CK51-52
    11. HH33-45
    12. CH68-47 check
    13. HK39-49
    14. CK52-51
    15. HP43-53
    16. CH47-28 check
    17. HK49-50
    18. CH28-36
    19. HH45-33
    20. CH36-44
    21. HH33-54
    22. CH44-23
    23. HK50-40
    24. HP53-43
    25. CK51-52
    26. HH54-73 check
    27. CK52-51
    28. HH73-61
    29. CK51-41
    30. HP43-33
    31. CK41-31
    32. HH61-53
    33. CH23-11
    34. HK40-50
    35. HH53-61
    36. CH11-23
    37. HP33-23
    38. CK31-32
    39. HH61-53 check
    40. CK32-42 big
    41. HH53-45
    42. HK50-40
    43. HP23-33 check
    44. CK42-41
    45. HH45-53 check
    46. CK41-51
    47. HH53-61
    48. HP33-42 mate

.. _漢馬兵楚象卒:

漢馬兵楚象卒
~~~~~~~~~~~~

.. janggi-board::

    HK50,HH33,HP36,CK31,CE79,CP7

.. janggi-moves::

    1. HP36-46
    2. CE79-47
    3. HK50-49
    4. CE47-15
    5. HP46-45
    6. CE15-43
    7. HP45-44
    8. CK31-42
    9. HH33-21 check
    10. CK42-53
    11. HH21-13
    12. CK53-42
    13. HH13-34 check
    14. CK42-33
    15. HH34-46
    16. CK33-42
    17. HH46-54 check
    18. CK42-53
    19. HH54-73
    20. CK53-42
    21. HH73-61 check
    22. CK42-33
    23. HH61-82
    24. CK33-42
    25. HH82-63 check
    26. CK42-53
    27. HH63-55
    28. CE43-11
    29. HH55-36
    30. CK53-42
    31. HH36-15
    32. CE11-43
    33. HH15-23 check
    34. CK42-33
    35. HH23-35
    36. CE43-71
    37. HH35-47
    38. CP7-17
    39. HH47-55
    40. CP17-27
    41. HH55-63
    42. CE71-43
    43. HH63-51
    44. CE43-75
    45. HK49-59
    46. CP27-37
    47. HP44-54
    48. CP37-38
    49. HH51-63
    50. CE75-47
    51. HH63-55
    52. CE47-19
    53. HP54-53
    54. HK59-58
    55. HH55-43
    56. CE19-36
    57. HH43-35
    58. CK33-32
    59. HP53-43
    60. CE36-8
    61. HH35-16
    62. HH16-8
    63. CK32-31
    64. HK58-59
    65. HH8-27
    66. CK31-41
    67. HH27-46
    68. CK41-51 big
    69. HP43-53
    70. HH46-54
    71. CP38-49 check
    72. HK59-49
    73. HP53-42 mate

.. _漢馬兵楚象士:

漢馬兵楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HH12,HP47,CK43,CE39,CO31

.. janggi-moves::

    1. HH12-31 check
    2. CK43-42
    3. HH31-23 check
    4. CK42-32
    5. HH23-35
    6. CE39-7
    7. HH35-16
    8. CE7-39
    9. HH16-24 check
    10. CK32-31
    11. HP47-46
    12. HH24-43 check
    13. CK31-32
    14. HH43-35
    15. CE39-7
    16. HH35-16
    17. HP46-45
    18. CE7-39
    19. HH16-37
    20. CE39-7
    21. HH37-58
    22. CK32-42
    23. HH58-66
    24. CE7-24
    25. HH66-54 check
    26. CK42-52
    27. HH54-35
    28. CE24-1
    29. HH35-14
    30. CK52-42
    31. HH14-22
    32. CE1-24
    33. HH22-34 check
    34. CK42-33
    35. HP45-55
    36. CE24-47
    37. HP55-54
    38. CE47-19
    39. HH34-55
    40. HP54-44
    41. HP44-43 check
    42. CK33-32
    43. HH55-74
    44. CE19-36
    45. HH74-62
    46. CE36-8
    47. HH62-54
    48. CE8-25
    49. HH54-33
    50. CK32-31
    51. HH33-25
    52. HH25-46
    53. HH46-54
    54. HP43-42 mate

.. _漢馬兵楚卒卒:

漢馬兵楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH21,HP86,CK52,CP14,CP24

.. janggi-moves::

    1. HP86-76
    2. CP24-34
    3. HP76-66
    4. CP14-24
    5. HP66-65
    6. CK52-53
    7. HP65-64
    8. CK53-43 big
    9. HK48-58
    10. CP34-44
    11. HP64-63
    12. CP24-14
    13. HH21-2
    14. CP14-24
    15. HH2-23
    16. CP44-54
    17. HH23-4
    18. CK43-42
    19. HH4-16
    20. CP24-34
    21. HH16-28
    22. CP54-64
    23. HH28-47
    24. CP64-65
    25. HH47-28
    26. CK42-52 big
    27. HK58-49
    28. CK52-42 big
    29. HK49-40
    30. CP34-44
    31. HH28-36
    32. CP44-54
    33. HH36-57
    34. CP65-75
    35. HH57-45
    36. CK42-31 big
    37. HK40-50
    38. CP54-44
    39. HH45-37
    40. CK31-42
    41. HH37-25
    42. CP44-54 big
    43. HK50-60
    44. CP54-64
    45. HH25-46
    46. CK42-52 big
    47. HK60-50
    48. CK52-42
    49. HK50-40
    50. CK42-33 big
    51. HH46-34
    52. CP64-54
    53. HK40-50
    54. CP54-44
    55. HH34-15
    56. CK33-43
    57. HH15-23
    58. CP44-54 big
    59. HK50-60
    60. CP54-64
    61. HH23-15
    62. CP64-54
    63. HH15-34
    64. CP54-44
    65. HH34-46
    66. CK43-42
    67. HH46-67
    68. CK42-52 big
    69. HK60-50
    70. CK52-42
    71. HH67-75
    72. CP44-54 big
    73. HK50-60
    74. CP54-64
    75. HH75-56
    76. CK42-52
    77. HK60-50
    78. CK52-42 big
    79. HK50-40
    80. CP64-54
    81. HH56-35
    82. CP54-64
    83. HH35-47
    84. CK42-31 big
    85. HH47-39
    86. CK31-42
    87. HH39-58
    88. CP64-54
    89. HH58-66
    90. CK42-31 big
    91. HK40-50
    92. CK31-41 big
    93. HK50-60
    94. CK41-42
    95. HH66-54 check
    96. CK42-52
    97. HK60-50
    98. HH54-33 check
    99. CK52-42 big
    100. HH33-45
    101. HK50-60
    102. HP63-53 check
    103. CK42-31
    104. HH45-66
    105. HH66-54
    106. HP53-42 mate

.. _漢馬兵楚卒士:

漢馬兵楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH33,HP85,CK43,CP44,CO41

.. janggi-moves::

    1. HH33-41
    2. CP44-54
    3. HH41-22 check
    4. CK43-33 big
    5. HK38-49
    6. CK33-42 big
    7. HK49-60
    8. CK42-51
    9. HP85-84
    10. CK51-52
    11. HH22-14
    12. CP54-64 big
    13. HK60-50
    14. CK52-42 big
    15. HK50-40
    16. HH14-35
    17. CK42-31
    18. HK40-50
    19. HH35-47
    20. HP84-83
    21. HP83-73
    22. HP73-63
    23. CK31-42
    24. HK50-40
    25. CK42-31 big
    26. HH47-39
    27. CK31-42
    28. HH39-58
    29. CP64-54
    30. HH58-66
    31. CK42-31 big
    32. HK40-50
    33. CK31-41 big
    34. HK50-60
    35. CK41-42
    36. HH66-54 check
    37. CK42-52
    38. HK60-50
    39. HH54-33 check
    40. CK52-42 big
    41. HH33-45
    42. HK50-60
    43. HP63-53 check
    44. CK42-31
    45. HH45-66
    46. HH66-54
    47. HP53-42 mate

.. _漢馬兵楚士士:

漢馬兵楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH12,HP44,CK52,CO31,CO43

.. janggi-moves::

    1. HH12-31 check
    2. CK52-42
    3. HH31-43
    4. HK38-49
    5. HH43-64
    6. HP44-43 check
    7. CK42-31
    8. HH64-85
    9. HH85-73
    10. HH73-54
    11. HP43-42 mate

.. _漢馬士士楚車:

漢馬士士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH27,HO40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 check
    2. HH27-19
    3. CR9-19 mate

.. _漢馬士士楚包:

漢馬士士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH48,HO49,HO38,CK32,CC31

.. janggi-moves::

    1. CC31-37 mate

.. _漢馬士士楚馬:

漢馬士士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH48,HO39,HO49,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢馬士士楚象:

漢馬士士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HH58,HO49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢馬士士楚卒:

漢馬士士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HH48,HO39,HO49,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢馬士楚馬士:

漢馬士楚馬士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HO38,CK31,CH41,CO32

.. janggi-moves::

    1. HH11-23 mate

.. _漢馬士楚象象:

漢馬士楚象象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HO38,CK31,CE41,CE32

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK38,HH49,HO39,CK41,CE76,CE89

.. janggi-moves::

    1. CE89-66 check
    2. HK38-48 big
    3. CE66-43 check
    4. HK48-38
    5. CE43-15 mate

.. _漢馬士楚象卒:

漢馬士楚象卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HH71,HO38,CK32,CE44,CP37

.. janggi-moves::

    1. CP37-38
    2. HK40-50
    3. CE44-67 check
    4. HK50-60
    5. CE67-35
    6. HK60-59
    7. CK32-31
    8. CP38-48
    9. CP48-58 check
    10. HK59-60
    11. CE35-3
    12. HK60-50
    13. CE3-26
    14. HK50-40 big
    15. CK31-41
    16. HK40-39
    17. CE26-43
    18. HH71-83
    19. CE43-66
    20. HH83-62 check
    21. CK41-51
    22. HH62-43 check
    23. CK51-42
    24. CP58-49 mate

.. _漢馬士楚象士:

漢馬士楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HO38,CK31,CE41,CO32

.. janggi-moves::

    1. HH11-23 mate

.. janggi-board::

    HK38,HH39,HO48,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢馬士楚卒卒:

漢馬士楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HH2,HO38,CK51,CP19,CP28

.. janggi-moves::

    1. CP28-38
    2. HK40-50
    3. CP19-29
    4. HK50-60 big
    5. CK51-41
    6. HK60-50 big
    7. CP38-48
    8. HK50-40
    9. CK41-51
    10. HH2-21
    11. CP48-38
    12. HK40-50
    13. CP29-39
    14. HK50-60 big
    15. CK51-41
    16. HK60-59
    17. CP38-48
    18. HH21-33 check
    19. CK41-31
    20. HH33-52 check
    21. CK31-42
    22. CP39-49 mate

.. _漢馬士楚士士:

漢馬士楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HH11,HO38,CK31,CO41,CO32

.. janggi-moves::

    1. HH11-23 mate

.. _漢象象兵兵楚:

漢象象兵兵楚
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE41,HE43,HP7,HP87,CK31

.. janggi-moves::

    1. HK38-49
    2. CK31-41
    3. HP7-17
    4. CK41-42
    5. HP17-27
    6. CK42-43 big
    7. HK49-40
    8. HP27-37
    9. HP37-47
    10. HK40-50
    11. HK50-60
    12. HP47-57
    13. HP57-56
    14. HP56-55
    15. HP55-54
    16. HP87-77
    17. HP77-67
    18. HP67-66
    19. HP66-65
    20. HP65-64
    21. HP64-63
    22. HP63-53 check
    23. CK43-33
    24. HP54-44
    25. HP44-43 check
    26. CK33-32
    27. HP53-42 mate

.. _漢象象兵士楚:

漢象象兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE84,HP1,HO50,CK32

.. janggi-moves::

    1. HE42-65
    2. CK32-31
    3. HE84-52
    4. CK31-41 big
    5. HK48-38
    6. CK41-31 big
    7. HK38-49
    8. CK31-41 big
    9. HK49-40
    10. CK41-31 big
    11. HE52-35
    12. CK31-41
    13. HE35-7
    14. CK41-31 big
    15. HE7-39
    16. CK31-41
    17. HE39-16
    18. HE16-48
    19. HK40-49
    20. HP1-11
    21. HP11-21
    22. HK49-40
    23. HE48-25
    24. HE65-88
    25. HE88-56
    26. HE56-73 check
    27. CK41-51
    28. HP21-31
    29. HP31-41 check
    30. CK51-52
    31. HE73-56
    32. HE56-84 mate

.. _漢象象兵楚卒:

漢象象兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE11,HE83,HP82,CK31,CP54

.. janggi-moves::

    1. HK38-49
    2. CK31-42 big
    3. HK49-60
    4. CP54-44
    5. HE83-55
    6. CK42-51
    7. HK60-50
    8. CK51-42
    9. HK50-40
    10. CK42-31 big
    11. HE55-38
    12. CK31-32
    13. HP82-72
    14. HP72-62
    15. CK32-42
    16. HE38-70
    17. HE70-47
    18. CK42-31 big
    19. HK40-50
    20. CK31-42
    21. HE47-75
    22. CP44-54 big
    23. HK50-60
    24. CP54-44
    25. HP62-52 check
    26. CK42-53 big
    27. HK60-50
    28. HE11-43
    29. HK50-40
    30. CP44-54
    31. HE43-15
    32. CP54-64
    33. HP52-62
    34. CK53-42
    35. HE15-47
    36. CK42-31 big
    37. HK40-50
    38. CP64-74
    39. HE75-58
    40. CK31-42
    41. HK50-60
    42. CK42-51
    43. HE47-19
    44. CP74-64
    45. HE19-36
    46. CP64-54
    47. HE36-53
    48. HE58-75
    49. CK51-42
    50. HE53-21
    51. HP62-52 check
    52. CK42-32
    53. HE75-58
    54. CK32-33
    55. HE58-26
    56. CP54-44
    57. HE21-53
    58. HE53-76
    59. CP44-34
    60. HE26-49
    61. HE49-17
    62. CP34-24
    63. HE17-40
    64. HE40-57
    65. HE76-53
    66. HE53-81
    67. HE81-64
    68. CK33-43
    69. HP52-62
    70. CK43-53
    71. HK60-50
    72. HE64-47
    73. CK53-42
    74. HK50-60
    75. CP24-14
    76. HE57-34
    77. CK42-53 big
    78. HK60-50
    79. CK53-42
    80. HK50-40
    81. CK42-32
    82. HP62-52
    83. CK32-33
    84. HE47-79
    85. HE79-56 check
    86. CK33-43
    87. HE34-6
    88. CK43-53
    89. HP52-62
    90. CP14-15
    91. HE56-84
    92. CK53-42
    93. HE84-67
    94. CK42-32 big
    95. HE6-38
    96. CP15-25
    97. HP62-52
    98. CK32-33
    99. HE67-44
    100. HE44-76
    101. HK40-50
    102. CP25-35
    103. HK50-60
    104. CP35-45
    105. HE38-66
    106. HE76-59
    107. CP45-55
    108. HE66-49
    109. CP55-45
    110. HE49-26
    111. CP45-35
    112. HE26-58
    113. CP35-25
    114. HE59-27
    115. HE27-44
    116. HE44-67
    117. CK33-43
    118. HE58-75 check
    119. CK43-33
    120. HE67-39
    121. HE39-56 check
    122. CK33-32
    123. HE56-24
    124. HE24-1
    125. CP25-35
    126. HE75-43
    127. CP35-45
    128. HE43-15 check
    129. CK32-31
    130. HE1-33
    131. CP45-55
    132. HE33-16
    133. HE16-48
    134. HE48-25
    135. HP52-42 mate

.. _漢象象兵楚士:

漢象象兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE1,HE31,HP7,CK41,CO33

.. janggi-moves::

    1. HK48-58
    2. CO33-32
    3. HE31-3
    4. CK41-51 big
    5. HK58-49
    6. CK51-42 big
    7. HK49-40
    8. CO32-31
    9. HE3-35
    10. HE1-24
    11. HE24-56
    12. CK42-32
    13. HE56-88
    14. CO31-42
    15. HE88-65
    16. CO42-43
    17. HP7-17
    18. HP17-27
    19. HP27-37
    20. HE35-3
    21. HE3-26
    22. CO43-53
    23. HE26-54
    24. HP37-36
    25. CK32-31
    26. HP36-35
    27. CK31-41
    28. HP35-34
    29. CO53-43
    30. HE54-71
    31. CO43-53
    32. HE65-48
    33. HE48-25
    34. CO53-52
    35. HE71-54
    36. HE54-86
    37. HE86-63
    38. HE63-46
    39. HE46-74
    40. HP34-33
    41. CK41-31
    42. HE25-57
    43. CK31-41
    44. HE57-29
    45. CO52-53
    46. HE29-46
    47. CO53-43
    48. HP33-43
    49. CK41-31 big
    50. HP43-33
    51. HK40-50
    52. HP33-42 mate

.. _漢象象士士楚:

漢象象士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE64,HO40,HO50,CK31

.. janggi-moves::

    1. HE42-14 mate

.. _漢象象士楚車:

漢象象士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE42,HE64,HO38,CK31,CR1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK40,HE67,HE39,HO38,CK31,CR51

.. janggi-moves::

    1. CR51-60 check
    2. HE67-50
    3. CR60-38
    4. HE50-78
    5. CR38-60 check
    6. HE78-50
    7. CR60-58 mate

.. _漢象象士楚包:

漢象象士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE72,HE5,HO59,CK32,CC42

.. janggi-moves::

    1. HE72-55 check
    2. CK32-31
    3. HK48-58
    4. CK31-41
    5. HE55-83
    6. CK41-31
    7. HO59-60
    8. HK58-59
    9. HE5-37
    10. HE37-9
    11. CK31-32
    12. HE83-55 check
    13. CK32-33
    14. HE9-26
    15. HE26-58
    16. CK33-43
    17. HE58-75 check
    18. CK43-33
    19. HE55-38
    20. CK33-32
    21. HE38-15 check
    22. CK32-31
    23. HO60-50
    24. HK59-60
    25. HO50-40
    26. HK60-50
    27. HE75-58
    28. HE58-90
    29. HE90-67
    30. HE67-84
    31. HE84-56
    32. HE56-33
    33. HE33-1
    34. HE15-38
    35. HE38-55
    36. HE55-87
    37. HE87-64
    38. HE1-33
    39. HE33-65
    40. HE65-37
    41. HE37-14 mate

.. janggi-board::

    HK39,HE48,HE49,HO38,CK32,CC31

.. janggi-moves::

    1. CK32-42 mate

.. _漢象象士楚馬:

漢象象士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE42,HE15,HO38,CK33,CH1

.. janggi-moves::

    1. HE42-65 mate

.. janggi-board::

    HK38,HE48,HE49,HO39,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢象象士楚象:

漢象象士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE42,HE64,HO38,CK31,CE1

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK48,HE58,HE49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢象象士楚卒:

漢象象士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE42,HE15,HO38,CK33,CP4

.. janggi-moves::

    1. HE42-65 mate

.. janggi-board::

    HK38,HE48,HE49,HO39,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢象象士楚士:

漢象象士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE21,HE65,HO38,CK32,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢象象楚象士:

漢象象楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HE82,HE75,CK33,CE32,CO42

.. janggi-moves::

    1. HE82-65 check
    2. CK33-43 big
    3. HE65-48 check
    4. CK43-33
    5. HE48-16 mate

.. janggi-board::

    HK38,HE48,HE39,CK41,CE43,CO31

.. janggi-moves::

    1. CE43-66 mate

.. _漢象象楚卒卒:

漢象象楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE42,HE64,CK31,CP10,CP20

.. janggi-moves::

    1. HE42-14 mate

.. janggi-board::

    HK38,HE52,HE25,CK42,CP15,CP74

.. janggi-moves::

    1. CP15-25
    2. HK38-49 big
    3. CK42-52
    4. HK49-60 big
    5. CK52-42
    6. HK60-50 big
    7. CK42-31
    8. CP25-35
    9. CP35-45
    10. CK31-41
    11. CK41-51
    12. CP45-55
    13. CP55-56
    14. CP56-57
    15. CP57-58
    16. CP74-64
    17. CP58-48
    18. CP64-54
    19. CP54-55
    20. CP55-56
    21. CP56-57
    22. CP57-58
    23. CP48-49 mate

.. _漢象象楚卒士:

漢象象楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE21,HE65,CK32,CP4,CO31

.. janggi-moves::

    1. HE21-4 mate

.. _漢象象楚士士:

漢象象楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK49,HE82,HE75,CK33,CO32,CO42

.. janggi-moves::

    1. HE82-65 check
    2. CK33-43 big
    3. HE65-48 check
    4. CK43-33
    5. HE48-16 mate

.. _漢象兵兵士楚:

漢象兵兵士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE1,HP7,HP87,HO50,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HE1-33
    4. CK31-32
    5. HK38-49
    6. CK32-33
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. CK33-42
    11. HP27-37
    12. CK42-51 big
    13. HK60-49
    14. HP37-47
    15. HP47-57
    16. HK49-60
    17. HP57-56
    18. HP56-55
    19. HP55-54
    20. HP54-53
    21. HP87-77
    22. HP77-67
    23. HP67-57
    24. HP53-43
    25. HP57-56
    26. HP56-55
    27. HP55-54
    28. HP54-53
    29. HP43-42 mate

.. _漢象兵兵楚車:

漢象兵兵楚車
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE33,HP7,HP77,CK41,CR1

.. janggi-moves::

    1. HE33-1
    2. CK41-31 big
    3. HE1-33
    4. CK31-32
    5. HK38-49
    6. CK32-33
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. CK33-42
    11. HP27-37
    12. CK42-51 big
    13. HK60-50
    14. HP37-47
    15. HP47-57
    16. HK50-60
    17. HP57-56
    18. HP56-55
    19. HP55-54
    20. HP54-53
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢象兵兵楚包:

漢象兵兵楚包
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE6,HP42,HP3,CK53,CC44

.. janggi-moves::

    1. HP42-32
    2. CK53-43
    3. HE6-38
    4. CK43-33
    5. HP32-22
    6. CK33-43
    7. HE38-55
    8. CC44-41
    9. HP22-32
    10. CC41-50
    11. HK39-40
    12. CC50-10
    13. HP3-13
    14. CC10-60
    15. HE55-38
    16. CK43-33
    17. HP32-22
    18. CC60-30
    19. HP22-12
    20. CK33-42
    21. HK40-39
    22. CK42-31
    23. HK39-49
    24. CK31-41 big
    25. HK49-60
    26. CK41-51 big
    27. HE38-55
    28. HP13-23
    29. CK51-42
    30. HE55-87
    31. CC30-90
    32. HE87-64
    33. CC90-30
    34. HE64-36
    35. HE36-59
    36. CC30-80
    37. HP12-22
    38. CC80-30
    39. HP23-13
    40. CC30-80
    41. HE59-27
    42. CC80-30
    43. HE27-44
    44. CK42-51 big
    45. HK60-50
    46. CK51-42
    47. HP22-32 check
    48. CK42-32
    49. HE44-16
    50. CK32-42 big
    51. HK50-40
    52. CK42-32 big
    53. HE16-39
    54. CK32-42
    55. HE39-56
    56. CC30-60
    57. HE56-88
    58. CC60-30
    59. HP13-23
    60. CK42-32 big
    61. HK40-50
    62. CK32-42 big
    63. HK50-60
    64. CK42-52 big
    65. HE88-56
    66. CC30-22
    67. HK60-50
    68. CK52-42 big
    69. HK50-40
    70. CK42-52
    71. HE56-88
    72. CC22-62
    73. HP23-33
    74. CC62-32 check
    75. HP33-43
    76. CC32-62
    77. HE88-65
    78. CC62-70
    79. HK40-39
    80. CC70-62
    81. HE65-48
    82. CC62-42
    83. HP43-33
    84. HE48-25
    85. CC42-62
    86. HP33-43
    87. HP43-53 check
    88. CK52-51
    89. HP53-42 mate

.. _漢象兵兵楚馬:

漢象兵兵楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK50,HE61,HP47,HP87,CK41,CH23

.. janggi-moves::

    1. HK50-60
    2. CH23-42
    3. HE61-44
    4. CH42-63
    5. HE44-27
    6. CH63-75
    7. HE27-59
    8. CH75-54
    9. HP47-57
    10. CH54-66
    11. HE59-36
    12. CH66-78
    13. HE36-13 check
    14. CK41-42
    15. HP57-56
    16. CH78-57
    17. HE13-45
    18. CH57-36
    19. HK60-49
    20. CH36-24
    21. HP56-46
    22. CH24-12
    23. HK49-58
    24. CH12-33
    25. HP87-77
    26. CH33-25
    27. HP46-36
    28. CH25-37 check
    29. HK58-48
    30. CH37-29 check
    31. HK48-38
    32. CH29-50 check
    33. HK38-49
    34. CH50-29
    35. HP36-46
    36. CH29-37 check
    37. HK49-38
    38. CH37-58
    39. HP46-56
    40. CH58-37
    41. HE45-73
    42. CH37-25
    43. HP77-76
    44. CH25-33
    45. HP56-55
    46. CH33-14
    47. HK38-39
    48. CH14-35
    49. HP55-45
    50. CH35-43
    51. HP76-75
    52. CH43-64
    53. HP45-55
    54. HK39-40
    55. CK42-31 big
    56. HK40-50
    57. CK31-42 big
    58. HK50-60
    59. CH64-76
    60. HP55-45
    61. CK42-51 big
    62. HK60-50
    63. CH76-57
    64. HK50-40
    65. HE73-56
    66. CH57-69
    67. HK40-39
    68. HE56-33
    69. CH69-57
    70. HP45-35
    71. CK51-42
    72. HE33-65 check
    73. CK42-41
    74. HP35-34
    75. CK41-51
    76. HK39-40
    77. HE65-33
    78. CK51-42
    79. HE33-61
    80. CK42-31
    81. HK40-50
    82. CK31-42 big
    83. HP34-44
    84. CH57-38 check
    85. HK50-49
    86. CH38-57 check
    87. HK49-60
    88. HP44-54
    89. CH57-76
    90. HP75-65
    91. CH76-68 check
    92. HK60-59
    93. CH68-47 check
    94. HK59-49
    95. CH47-35 big
    96. HP54-44
    97. CH35-47
    98. HP65-64
    99. CH47-68 check
    100. HK49-58
    101. CH68-87
    102. HK58-59
    103. CH87-66
    104. HP44-54
    105. CH66-47 check
    106. HK59-49
    107. CH47-35 big
    108. HK49-58
    109. HE61-44
    110. CK42-43
    111. HE44-67
    112. CH35-27
    113. HP54-44 check
    114. CK43-42
    115. HK58-49
    116. CH27-46
    117. HE67-39
    118. CH46-25
    119. HP64-54
    120. CH25-46
    121. HK49-60
    122. CH46-27
    123. HE39-7
    124. HE7-30
    125. CH27-48 check
    126. HK60-49
    127. CH48-67
    128. HE30-47
    129. CH67-46
    130. HK49-59
    131. CH46-27
    132. HP54-64
    133. CK42-52 big
    134. HK59-49
    135. CH27-35
    136. HE47-75 check
    137. CK52-42
    138. HE75-58
    139. CH35-56
    140. HP64-54
    141. CH56-37 check
    142. HK49-59
    143. CH37-45
    144. HK59-60
    145. CH45-37
    146. HE58-86
    147. CH37-56
    148. HK60-49
    149. CH56-77
    150. HE86-58
    151. CH77-56
    152. HE58-26
    153. CH56-35
    154. HK49-60
    155. HE26-49
    156. CH35-56
    157. HE49-17
    158. CH56-48 check
    159. HK60-49
    160. CH48-27
    161. HE17-34
    162. CH27-35
    163. HK49-38
    164. HE34-6
    165. CH35-56
    166. HE6-29
    167. CH56-77
    168. HE29-57
    169. CH77-65
    170. HE57-25 check
    171. CK42-52
    172. HK38-49
    173. CH65-46
    174. HE25-48
    175. CK52-42
    176. HP54-64
    177. HP64-63
    178. HE48-76
    179. CH46-65
    180. HK49-60
    181. HP44-54
    182. HE76-48
    183. CH65-46
    184. HP63-53 check
    185. CK42-41
    186. HP54-44
    187. CK41-51
    188. HK60-49
    189. HE48-25
    190. CH46-25
    191. HP44-43
    192. CH25-37 check
    193. HK49-40
    194. HP43-42 mate

.. _漢象兵兵楚象:

漢象兵兵楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE62,HP72,HP86,CK51,CE11

.. janggi-moves::

    1. HE62-45
    2. CK51-42
    3. HE45-68
    4. CE11-43
    5. HK38-39
    6. CE43-15
    7. HE68-40
    8. CK42-31 big
    9. HK39-49
    10. CK31-42 big
    11. HK49-60
    12. CE15-43
    13. HE40-8
    14. CK42-51 big
    15. HK60-50
    16. CK51-41
    17. HK50-40
    18. CK41-31 big
    19. HE8-36
    20. CE43-75
    21. HK40-50
    22. CK31-41 big
    23. HK50-60
    24. CK41-51 big
    25. HE36-59
    26. HP86-76
    27. CE75-43
    28. HK60-50
    29. CK51-41
    30. HK50-40
    31. CK41-31 big
    32. HE59-36
    33. HP72-62
    34. CK31-42
    35. HE36-8
    36. CK42-31 big
    37. HK40-50
    38. CK31-41
    39. HE8-25
    40. CE43-11 big
    41. HE25-48
    42. CK41-42
    43. HK50-40
    44. CK42-31 big
    45. HK40-49
    46. CK31-42
    47. HP62-72
    48. CE11-34
    49. HK49-39
    50. CK42-31
    51. HE48-65
    52. CE34-66 big
    53. HE65-37
    54. CE66-43
    55. HK39-49
    56. CE43-26 check
    57. HK49-60
    58. CE26-43
    59. HK60-50
    60. HE37-65
    61. CK31-41
    62. HP72-62
    63. CK41-51
    64. HK50-40
    65. HE65-33
    66. HE33-16
    67. HE16-44
    68. HK40-49
    69. HP76-66
    70. CK51-42
    71. HK49-60
    72. CE43-15
    73. HE44-21
    74. CK42-43
    75. HE21-4
    76. CE15-38
    77. HP66-56
    78. CK43-42
    79. HK60-59
    80. HK59-58
    81. HE4-36
    82. CK42-51
    83. HP62-72
    84. HE36-8
    85. CK51-42
    86. HP56-46
    87. CE38-10
    88. HE8-36
    89. CE10-38
    90. HP46-45
    91. HP45-44
    92. CE38-66
    93. HE36-8
    94. HE8-25 check
    95. CK42-31
    96. HK58-48
    97. CK31-32
    98. HP72-62
    99. CK32-33
    100. HP62-52
    101. CE66-83
    102. HE25-8
    103. CE83-55
    104. HE8-36
    105. CE55-87
    106. HK48-58
    107. CE87-55
    108. HP44-54
    109. HP54-53
    110. HE36-13
    111. CE55-23
    112. HE13-36
    113. CE23-46
    114. HP52-42
    115. HE36-4
    116. CE46-18
    117. HE4-27
    118. CE18-35 check
    119. HK58-48
    120. HE27-55
    121. CE35-3
    122. HE55-87
    123. CE3-35
    124. HE87-64
    125. CE35-67
    126. HE64-47
    127. CE67-44
    128. HK48-49
    129. HE47-75
    130. HP53-43 mate

.. _漢象兵兵楚卒:

漢象兵兵楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE90,HP83,HP15,CK41,CP24

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HE90-67
    7. HE67-84
    8. HE84-56
    9. CK41-51
    10. HK60-50
    11. CK51-41 big
    12. HK50-40
    13. CK41-31 big
    14. HE56-39
    15. HP83-73
    16. HP73-63
    17. CK31-42
    18. HP63-53 check
    19. CK42-53
    20. HE39-56
    21. CP24-34
    22. HE56-79
    23. HE79-47
    24. HP15-25
    25. HE47-15
    26. HE15-38
    27. HE38-66
    28. CP34-44
    29. HP25-35
    30. HE66-83
    31. HE83-55
    32. HE55-27
    33. HE27-50
    34. HE50-67
    35. CP44-54
    36. HK40-50
    37. HP35-45
    38. HE67-35
    39. HE35-58
    40. HE58-86
    41. CP54-64
    42. HP45-55
    43. HK50-60
    44. HE86-58
    45. HE58-75
    46. HE75-47
    47. HE47-70
    48. HE70-87
    49. CP64-74
    50. HP55-54 check
    51. CK53-42
    52. HE87-59
    53. CP74-75
    54. HE59-36
    55. HE36-8
    56. CK42-31
    57. HE8-25
    58. CP75-65
    59. HK60-49
    60. HP54-44
    61. CK31-32
    62. HE25-48
    63. CP65-55
    64. HP44-43
    65. CP55-45
    66. HE48-16
    67. CK32-31
    68. HP43-33
    69. CP45-46
    70. HK49-39
    71. HE16-48
    72. CP46-47
    73. HE48-76
    74. HE76-53
    75. HK39-40
    76. HK40-50
    77. CP47-48
    78. HE53-25
    79. CP48-49 check
    80. HK50-49
    81. HP33-42 mate

.. _漢象兵兵楚士:

漢象兵兵楚士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE69,HP62,HP7,CK31,CO41

.. janggi-moves::

    1. HP62-52
    2. CK31-32
    3. HE69-86
    4. CK32-33
    5. HE86-58
    6. CK33-43 big
    7. HK48-38
    8. CK43-33 big
    9. HK38-49
    10. CK33-43 big
    11. HK49-40
    12. CK43-33 big
    13. HE58-35
    14. CK33-43
    15. HE35-67
    16. CK43-33 big
    17. HE67-39
    18. CK33-43
    19. HE39-56
    20. CK43-53
    21. HE56-24
    22. CK53-43
    23. HE24-47
    24. HK40-50
    25. HP7-17
    26. HP17-27
    27. HP27-37
    28. HP37-36
    29. HP36-46
    30. CK43-53
    31. HE47-75
    32. CO41-31
    33. HP46-56
    34. HK50-60
    35. HP56-55
    36. HP55-54 check
    37. CK53-43
    38. HE75-47
    39. CO31-42
    40. HP52-42
    41. CK43-42
    42. HE47-19
    43. HE19-36
    44. HE36-8
    45. HE8-25 check
    46. CK42-31
    47. HP54-53
    48. CK31-32
    49. HE25-42
    50. HE42-65
    51. HP53-42 mate

.. _漢象兵士士楚:

漢象兵士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE81,HP7,HO40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HE81-64 check
    7. CK41-51 big
    8. HK60-50
    9. HE64-36
    10. HE36-4
    11. HE4-21
    12. HE21-44
    13. HE44-16
    14. HP7-17
    15. HP17-27
    16. HP27-37
    17. HP37-36
    18. HP36-35
    19. HP35-34
    20. HP34-33
    21. HE16-48
    22. CK51-41
    23. HP33-43
    24. HE48-65
    25. HP43-42 mate

.. _漢象兵士楚車:

漢象兵士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE33,HP87,HO40,CK31,CR1

.. janggi-moves::

    1. HE33-1
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. CK41-51 big
    9. HK60-50
    10. HE1-24
    11. HE24-56
    12. HK50-60
    13. HP87-77
    14. HP77-67
    15. HP67-57
    16. HE56-88
    17. HE88-65
    18. HP57-56
    19. HE65-48
    20. HE48-25
    21. HP56-55
    22. CK51-41
    23. HP55-54
    24. CK41-31
    25. HP54-53
    26. CK31-32
    27. HE25-42
    28. HE42-65
    29. HP53-42 mate

.. _漢象兵士楚包:

漢象兵士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE70,HP87,HO40,CK52,CC42

.. janggi-moves::

    1. HE70-47
    2. CC42-82
    3. HE47-24 check
    4. CK52-42
    5. HE24-56
    6. CK42-31 big
    7. HK38-49
    8. CK31-42 big
    9. HK49-59
    10. CC82-32
    11. HE56-28
    12. CC32-82
    13. HE28-45
    14. CC82-32
    15. HE45-22
    16. CK42-51 big
    17. HK59-49
    18. CK51-41 big
    19. HK49-60
    20. CK41-42
    21. HE22-5
    22. HP87-77
    23. CC32-52
    24. HE5-37
    25. CK42-53 big
    26. HK60-50
    27. CC52-55
    28. HE37-14
    29. HP77-67
    30. CC55-51
    31. HP67-66
    32. HP66-65
    33. HP65-64
    34. HO40-49
    35. HE14-31
    36. HE31-3
    37. HE3-35
    38. HE35-7
    39. HE7-24
    40. HE24-47
    41. HE47-19
    42. CK53-42
    43. HP64-54
    44. CK42-52
    45. HP54-44
    46. CK52-42
    47. HE19-47
    48. HE47-75
    49. CK42-31
    50. HP44-43
    51. CC51-21
    52. HE75-47
    53. CK31-32
    54. HK50-60
    55. CK32-31
    56. HE47-64
    57. CK31-32
    58. HE64-81
    59. HE81-53
    60. HE53-76
    61. CK32-31
    62. HE76-48
    63. CK31-41
    64. HK60-50
    65. HE48-65
    66. HP43-42 mate

.. _漢象兵士楚馬:

漢象兵士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE39,HP17,HO48,CK43,CH40

.. janggi-moves::

    1. HK38-49
    2. CH40-19
    3. HE39-56
    4. CK43-42
    5. HE56-24
    6. CK42-31
    7. HK49-58
    8. CH19-40
    9. HE24-56
    10. CK31-42
    11. HO48-49
    12. CH40-19
    13. HE56-24
    14. CK42-51 big
    15. HK58-48
    16. HE24-47
    17. CK51-41
    18. HO49-39
    19. HK48-49
    20. HK49-50
    21. HO39-49
    22. HE47-19
    23. HE19-47
    24. HE47-79
    25. HE79-56
    26. HE56-88
    27. HE88-65
    28. HP17-27
    29. HP27-37
    30. HP37-36
    31. HP36-35
    32. HP35-34
    33. CK41-51
    34. HP34-33
    35. CK51-52
    36. HE65-42
    37. HE42-25
    38. HP33-42 mate

.. _漢象兵士楚象:

漢象兵士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE6,HP87,HO40,CK31,CE30

.. janggi-moves::

    1. HE6-29
    2. CK31-41 big
    3. HK48-38
    4. HP87-77
    5. HP77-67
    6. CK41-31 big
    7. HK38-49
    8. CK31-41 big
    9. HK49-60
    10. HP67-57
    11. HP57-47
    12. HK60-50
    13. HO40-49
    14. HK50-40
    15. HP47-37
    16. HP37-27
    17. HP27-17
    18. HO49-38
    19. HE29-46
    20. HE46-63
    21. HE63-35
    22. HE35-58
    23. HE58-30
    24. HE30-7
    25. HE7-24 check
    26. CK41-31
    27. HE24-56
    28. HE56-88
    29. HE88-65
    30. HP17-27
    31. HP27-37
    32. HP37-36
    33. HP36-35
    34. CK31-41
    35. HP35-34
    36. CK41-51
    37. HP34-33
    38. CK51-52
    39. HE65-42
    40. HE42-25
    41. HP33-42 mate

.. _漢象兵士楚卒:

漢象兵士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE83,HP75,HO49,CK31,CP44

.. janggi-moves::

    1. HK38-48
    2. CK31-42
    3. HP75-85
    4. CP44-54 big
    5. HK48-58
    6. CK42-52
    7. HE83-66
    8. CP54-64 big
    9. HK58-48
    10. CK52-42 big
    11. HK48-38
    12. CK42-31 big
    13. HE66-34
    14. CP64-54
    15. HK38-48
    16. HK48-58
    17. CK31-42
    18. HK58-59
    19. CK42-52
    20. HK59-60
    21. CP54-64 big
    22. HE34-57
    23. HP85-75
    24. HO49-59
    25. HE57-85
    26. HE85-68
    27. HE68-36
    28. CP64-54
    29. HP75-74
    30. HE36-4
    31. HE4-21
    32. HP74-73
    33. HP73-63
    34. CK52-42
    35. HP63-53 check
    36. CK42-33
    37. HE21-4
    38. HE4-27
    39. HE27-50
    40. CP54-44
    41. HE50-67
    42. CP44-34
    43. HE67-84
    44. HE84-61 check
    45. CK33-32
    46. HP53-43
    47. HP43-33 check
    48. CK32-31
    49. HE61-84
    50. CK31-41
    51. HP33-43
    52. CK41-51
    53. HP43-53
    54. CK51-41
    55. HE84-56
    56. CK41-31
    57. HP53-43
    58. HP43-33
    59. CK31-41
    60. HE56-39
    61. CK41-51
    62. HE39-16
    63. CK51-52
    64. HP33-43
    65. CP34-35
    66. HE16-48
    67. CP35-45
    68. HE48-76
    69. CK52-51
    70. HP43-53
    71. CP45-46
    72. HE76-44
    73. CK51-41
    74. HE44-16
    75. CP46-47
    76. HE16-33
    77. CP47-48
    78. HE33-65
    79. CP48-49 check
    80. HO59-49
    81. CK41-51
    82. HK60-50
    83. HP53-42 mate

.. _漢象兵士楚士:

漢象兵士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE3,HP7,HO48,CK43,CO31

.. janggi-moves::

    1. HE3-31
    2. CK43-33 big
    3. HK38-49
    4. CK33-32
    5. HE31-63
    6. HE63-46
    7. HE46-14
    8. HE14-37
    9. HE37-65
    10. HP7-17
    11. HP17-27
    12. HP27-37
    13. CK32-31
    14. HP37-36
    15. CK31-41
    16. HP36-35
    17. CK41-51
    18. HP35-34
    19. CK51-52
    20. HP34-33
    21. CK52-53
    22. HO48-38
    23. HE65-48
    24. HE48-25 check
    25. CK53-52
    26. HP33-42 mate

.. _漢象兵楚象士:

漢象兵楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE35,HP43,CK52,CE23,CO33

.. janggi-moves::

    1. HP43-33 check
    2. CK52-51
    3. HP33-23
    4. CK51-42
    5. HE35-67
    6. CK42-32 big
    7. HK38-49
    8. CK32-42 big
    9. HK49-40
    10. HE67-84
    11. CK42-32 big
    12. HK40-50
    13. CK32-42 big
    14. HK50-60
    15. HE84-56
    16. HP23-33 check
    17. CK42-51
    18. HK60-50
    19. HK50-40
    20. HE56-88
    21. HE88-65
    22. CK51-52
    23. HE65-42
    24. HE42-25
    25. HP33-42 mate

.. _漢象兵楚卒卒:

漢象兵楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE10,HP85,CK41,CP30,CP24

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-39
    6. CP30-20
    7. HE10-27
    8. CK41-31 big
    9. HK39-49
    10. CK31-41 big
    11. HK49-60
    12. CK41-51 big
    13. HE27-59
    14. CP24-34
    15. HP85-75
    16. CP34-44
    17. HP75-65
    18. CP44-54
    19. HE59-36
    20. HE36-68
    21. HK60-49
    22. HE68-45
    23. HE45-77
    24. HK49-58
    25. CK51-41
    26. HE77-60
    27. CP20-30
    28. HE60-37
    29. CP54-44
    30. HP65-55
    31. CP30-40
    32. HE37-65
    33. CP40-50
    34. HE65-48
    35. CP50-60
    36. HK58-49
    37. CP60-70
    38. HE48-16
    39. CP44-54 big
    40. HP55-45
    41. HK49-48
    42. HE16-33
    43. HE33-5
    44. CK41-31
    45. HE5-37
    46. HE37-9
    47. HE9-26
    48. HE26-49
    49. HK48-58
    50. HE49-77
    51. CP54-64
    52. HP45-55
    53. HE77-45
    54. HE45-13
    55. HE13-36
    56. HE36-59
    57. CP70-80
    58. HE59-87
    59. CP64-74
    60. HK58-49
    61. HP55-45
    62. HE87-59
    63. CP74-64
    64. HP45-44
    65. CK31-42
    66. HK49-40
    67. HE59-36
    68. CP64-74
    69. HK40-49
    70. HK49-59
    71. HP44-54
    72. CK42-33
    73. HE36-8
    74. CP74-75
    75. HE8-25
    76. CP75-65
    77. HE25-48
    78. CP65-55
    79. HP54-53
    80. CK33-32
    81. HK59-49
    82. HE48-16
    83. CK32-31
    84. HP53-43
    85. CP55-45
    86. HP43-33
    87. CP45-46
    88. HK49-39
    89. HE16-48
    90. CP46-47
    91. HE48-76
    92. HE76-53
    93. CP47-57
    94. HK39-40
    95. CP80-70
    96. HE53-25
    97. CP57-58
    98. HK40-50
    99. CP70-60 check
    100. HK50-60
    101. CP58-59 check
    102. HK60-59
    103. HP33-42 mate

.. _漢象兵楚卒士:

漢象兵楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE64,HP85,CK51,CP14,CO32

.. janggi-moves::

    1. HE64-32
    2. CK51-42
    3. HE32-55
    4. CK42-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-40
    8. CP14-24
    9. HE55-38
    10. CP24-34
    11. HE38-66
    12. CP34-44
    13. HP85-75
    14. HE66-49
    15. HK40-50
    16. HP75-65
    17. CP44-54
    18. HK50-60
    19. HE49-26
    20. CP54-44
    21. HP65-55
    22. HE26-58
    23. HE58-90
    24. HE90-67
    25. HE67-50
    26. HE50-27
    27. CP44-34
    28. HP55-45
    29. HK60-50
    30. HE27-55
    31. HE55-38
    32. HE38-6
    33. CP34-24
    34. HK50-40
    35. HP45-35
    36. HE6-38
    37. HE38-70
    38. HE70-47
    39. HE47-30
    40. HE30-7
    41. CP24-14
    42. HP35-34
    43. CK41-42
    44. HE7-39
    45. CP14-15
    46. HE39-56
    47. HE56-88
    48. CK42-51
    49. HE88-65
    50. CP15-25
    51. HK40-49
    52. HP34-44
    53. CK51-52
    54. HE65-48
    55. CP25-35
    56. HP44-43
    57. CP35-45
    58. HE48-76
    59. CK52-51
    60. HP43-53
    61. CP45-46
    62. HK49-59
    63. HE76-44
    64. HE44-16
    65. HK59-60
    66. HE16-33
    67. CP46-47
    68. HK60-50
    69. CP47-48
    70. HE33-65
    71. CP48-49 check
    72. HK50-49
    73. HP53-42 mate

.. _漢象兵楚士士:

漢象兵楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE15,HP33,CK53,CO32,CO43

.. janggi-moves::

    1. HP33-43 check
    2. CK53-52
    3. HE15-32
    4. HE32-64
    5. HE64-36
    6. CK52-51
    7. HE36-8
    8. CK51-41
    9. HE8-25
    10. CK41-31 big
    11. HP43-33
    12. HK38-49
    13. HP33-42 mate

.. _漢象士士楚車:

漢象士士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE36,HO40,HO38,CK31,CR1

.. janggi-moves::

    1. CR1-9 check
    2. HE36-19
    3. CR9-19 mate

.. _漢象士士楚包:

漢象士士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK40,HE49,HO50,HO39,CK32,CC31

.. janggi-moves::

    1. CK32-42 mate

.. _漢象士士楚馬:

漢象士士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE48,HO39,HO49,CK41,CH14

.. janggi-moves::

    1. CH14-26 mate

.. _漢象士士楚象:

漢象士士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HE58,HO49,HO38,CK31,CE2

.. janggi-moves::

    1. CE2-25 mate

.. _漢象士士楚卒:

漢象士士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK38,HE48,HO39,HO49,CK31,CP36

.. janggi-moves::

    1. CP36-37 mate

.. _漢象士楚象士:

漢象士楚象士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE46,HO38,CK31,CE41,CO32

.. janggi-moves::

    1. HE46-14 mate

.. _漢象士楚卒卒:

漢象士楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HE52,HO38,CK33,CP28,CP55

.. janggi-moves::

    1. CP28-38
    2. HE52-84
    3. CK33-32
    4. CK32-31
    5. CP55-45
    6. HE84-56
    7. CP45-46
    8. HE56-79
    9. CK31-41
    10. HK40-50
    11. CP38-48
    12. HK50-60
    13. CK41-31
    14. HE79-56
    15. CP46-56
    16. CK31-41
    17. CP56-57
    18. CP57-58
    19. CP48-49 mate

.. _漢象士楚士士:

漢象士楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK39,HE46,HO38,CK31,CO41,CO32

.. janggi-moves::

    1. HE46-14 mate

.. _漢兵兵士士楚:

漢兵兵士士楚
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP7,HP87,HO40,HO39,CK41

.. janggi-moves::

    1. HK48-38
    2. CK41-31 big
    3. HK38-49
    4. CK31-41 big
    5. HK49-60
    6. HP7-17
    7. HP17-27
    8. HP27-37
    9. CK41-51 big
    10. HK60-50
    11. HP37-47
    12. HP47-57
    13. HK50-60
    14. HP57-56
    15. HP56-55
    16. HP55-54
    17. HP54-53
    18. HP87-77
    19. HP77-67
    20. HP67-57
    21. HP53-43
    22. HP57-56
    23. HP56-55
    24. HP55-54
    25. HP54-53
    26. HP43-42 mate

.. _漢兵兵士楚車:

漢兵兵士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP17,HP87,HO40,CK31,CR7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP87-77
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢兵兵士楚包:

漢兵兵士楚包
~~~~~~~~~~~~

.. janggi-board::

    HK38,HP7,HP77,HO58,CK41,CC19

.. janggi-moves::

    1. HO58-59
    2. CK41-42
    3. HO59-60
    4. CK42-31 big
    5. HK38-48
    6. CK31-41 big
    7. HK48-58
    8. CK41-51 big
    9. HK58-49
    10. CK51-41 big
    11. HK49-40
    12. CK41-31 big
    13. HK40-50
    14. HP7-6
    15. HP6-5
    16. HP5-4
    17. HP77-67
    18. CK31-42 big
    19. HO60-49
    20. CC19-59
    21. HP4-14
    22. CC59-19
    23. HP14-24
    24. CC19-69
    25. HP67-57
    26. CC69-29
    27. HP24-34
    28. CC29-59
    29. HP57-47
    30. CC59-29
    31. HP47-46
    32. HP46-45
    33. HP45-44
    34. HO49-48
    35. HP44-54
    36. HP34-44
    37. HP54-64
    38. HP44-54
    39. HP64-63
    40. HP63-53 check
    41. CK42-31
    42. HP54-44
    43. HP44-43
    44. HP43-42 mate

.. _漢兵兵士楚馬:

漢兵兵士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP17,HP87,HO40,CK31,CH7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP87-77
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢兵兵士楚象:

漢兵兵士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP17,HP87,HO40,CK31,CE7

.. janggi-moves::

    1. HP17-7
    2. CK31-41 big
    3. HK48-38
    4. CK41-31 big
    5. HK38-49
    6. CK31-41 big
    7. HK49-60
    8. HP7-17
    9. HP17-27
    10. HP27-37
    11. CK41-51 big
    12. HK60-50
    13. HP37-47
    14. HP47-57
    15. HK50-60
    16. HP57-56
    17. HP56-55
    18. HP55-54
    19. HP54-53
    20. HP87-77
    21. HP77-67
    22. HP67-57
    23. HP53-43
    24. HP57-56
    25. HP56-55
    26. HP55-54
    27. HP54-53
    28. HP43-42 mate

.. _漢兵兵士楚卒:

漢兵兵士楚卒
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP17,HP87,HO50,CK31,CP35

.. janggi-moves::

    1. HK48-58
    2. CK31-41
    3. HP17-7
    4. HK58-59
    5. CK41-51 big
    6. HK59-49
    7. HK49-40
    8. CK51-41
    9. HO50-49
    10. CK41-31
    11. HK40-50
    12. CP35-25
    13. HP87-77
    14. HP77-67
    15. HP67-57
    16. HP57-56
    17. HP56-46
    18. HP46-36
    19. HP7-17
    20. HP17-27
    21. HP27-37
    22. HP37-47
    23. HP47-57
    24. HP57-56
    25. HP56-55
    26. HP36-46
    27. HP46-56
    28. CK31-42
    29. HP55-65
    30. CP25-35
    31. HP56-55
    32. CP35-36
    33. HK50-60
    34. HP65-64
    35. HP64-63
    36. HP55-54
    37. CK42-31
    38. HP63-53
    39. CP36-37
    40. HP53-43
    41. CP37-38
    42. HP54-53
    43. CP38-49 check
    44. HK60-49
    45. HP43-42 mate

.. _漢兵兵士楚士:

漢兵兵士楚士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HP43,HP7,HO39,CK41,CO53

.. janggi-moves::

    1. HP43-53
    2. HP53-43
    3. HK38-49
    4. HP7-17
    5. HP17-27
    6. HP27-37
    7. HP37-36
    8. HP36-35
    9. HP35-34
    10. HP34-33
    11. HP43-42 mate

.. _漢兵兵楚卒卒:

漢兵兵楚卒卒
~~~~~~~~~~~~

.. janggi-board::

    HK40,HP7,HP87,CK51,CP20,CP86

.. janggi-moves::

    1. HP87-77
    2. CK51-41
    3. HP77-67
    4. CP20-30 check
    5. HK40-39
    6. CK41-31 big
    7. HK39-49
    8. CK31-41 big
    9. HK49-59
    10. CP86-76
    11. HP7-17
    12. HP17-27
    13. HP27-37
    14. CK41-51 big
    15. HK59-49
    16. HP37-47
    17. HP47-46
    18. HP67-57
    19. CP76-66
    20. HK49-39
    21. HP57-47
    22. HP46-36
    23. CP66-56
    24. HP47-37
    25. HP37-27
    26. CP56-46
    27. HP36-46
    28. HP46-36
    29. HP36-35
    30. HP35-34
    31. HP34-33
    32. HP33-43
    33. HP27-37
    34. HP37-36
    35. HP36-35
    36. HP35-34
    37. HP34-33
    38. HP43-42 mate

.. _漢兵兵楚卒士:

漢兵兵楚卒士
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP33,HP7,CK31,CP37,CO43

.. janggi-moves::

    1. HP33-43
    2. CP37-38 check
    3. HK48-58
    4. HK58-59
    5. HK59-60
    6. HK60-50
    7. HP7-17
    8. HP17-27
    9. HP27-37
    10. HP37-36
    11. HP36-35
    12. HP35-34
    13. HP34-33
    14. CP38-49 check
    15. HK50-49
    16. HP43-42 mate

.. _漢兵兵楚士士:

漢兵兵楚士士
~~~~~~~~~~~~

.. janggi-board::

    HK38,HP62,HP53,CK51,CO52,CO43

.. janggi-moves::

    1. HP62-52 check
    2. CK51-41
    3. HP53-43
    4. CK41-31 big
    5. HK38-49
    6. HP52-42 mate

.. _漢兵士士楚車:

漢兵士士楚車
~~~~~~~~~~~~

.. janggi-board::

    HK48,HP52,HO38,HO58,CK32,CR1

.. janggi-moves::

    1. CR1-41 check
    2. HP52-42 check
    3. CR41-42 mate

.. _漢兵士士楚馬:

漢兵士士楚馬
~~~~~~~~~~~~

.. janggi-board::

    HK38,HP33,HO39,HO48,CK31,CH45

.. janggi-moves::

    1. CH45-57 mate

.. _漢兵士士楚象:

漢兵士士楚象
~~~~~~~~~~~~

.. janggi-board::

    HK38,HP33,HO39,HO48,CK31,CE43

.. janggi-moves::

    1. CE43-66 mate


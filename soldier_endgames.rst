================
Soldier endgames
================

Soldier (漢兵楚)
~~~~~~~~~~~~~~~~

.. janggi-board::

    HK49,HP43,CK41

No mate exists with only one soldier.

Two soldiers (漢兵兵楚)
~~~~~~~~~~~~~~~~~~~~~~~

Two soldiers that have not ventured too far can win against a lone king.

.. janggi-board::

    HK50,HP44,HP34,CK42

.. janggi-moves::

    1. HP34-24
    2. CK42-33
    3. HP44-34
    4. CK33-42 big
    5. HK50-40
    6. HP24-23

Now the king cannot stop the soldiers from entering the palace

.. janggi-moves::

    7. HP23-33 check
    8. CK42-53
    9. HP34-44
    9. HP44-43 check
    10. CK53-52
    11. HP33-42 mate

.. janggi-board::

    HK50,HP42,HP43,CK52

Three soldiers (漢兵兵兵楚)
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two soliders cannot win if either one soldier has ventured to the second or first rank or if both soldiers have passed into the third rank. The addition of one extra soldier on the second or third rank respectively  will not win in these situations. For example

.. janggi-board::

    HP22,CK42,HP54,HP62,HK60

.. janggi-moves::

    1. HP22-32 check
    2. CK42-33
    3. HP54-53
    4. HP62-52
    5. HP52-42

Two soldiers versus chariot (漢兵兵楚車)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A chariot can easily draw versus two soliders. One way is to stop the soldiers from advancing by guarding the rank just in front of them.

.. janggi-board::

    CR12,CK42,CK60,HP56,HP66

.. janggi-moves::

    1. HP66-65
    2. CR12-14
    3. HP56-55

The rook stops the soldiers from advancing.

.. janggi-board::

    CR14,CK42,CK60,HP55,HP65


Two soldiers versus cannon (漢兵兵楚包)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If Cho can manage to get the cannon into a defensive position the endgame is a draw. For example


.. janggi-board::

    CK31,CC83,HP44,HP54,HK50


.. janggi-moves::

    1. HP54-53
    2. CK31-32

A key move. Else

.. janggi-moves::

    2. HP53-52
    3. HP44-43
    4. HP43-42 mate

.. janggi-moves::

    3. HP53-63
    4. HP63-73
    5. CC83-3

The cannon is stopping the soldiers from advancing. Another position from which Cho can draw is

.. janggi-board::

    CC51,CK42,HP34,HP54,HK40

.. janggi-moves::

    1. HP54-64
    2. HP34-44
    3. HP44-54
    4. HP64-63

The soldiers cannot enter the palace.


.. janggi-board::

    CC51,CK42,HP54,HP63,HK40

   
Two soldiers versus horse (漢兵兵楚馬)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The horse can in general stop the soldiers from mating by guarding the center of the palace.

.. janggi-board::

    CH21,CK51,HP33,HP43,HK39

In the position below Han can win if it is her turn.

.. janggi-board::

    CK42,HP23,HP34,CH90,HK39

.. janggi-moves::

    1. HP23-33 check
    2. CK42-51
    3. HP34-44
    4. CH90-78
    5. HP44-43
    6. CH78-66
    7. HP33-42 mate

If Cho moves first he will be able to save the draw.

.. janggi-moves::

    1. CH90-89
    2. HP23-33 check
    3. CH42-51

not

.. janggi-moves::

    3. CH42-41
    4. HP34-44
    5. CH78-66
    6. HP44-43
    7. CH66-54
    8. HP33-32
    9. HP43-53
    10. HP32-42 mate

enabling the soldier to block the only possible way for the horse to guard the centre.

.. janggi-moves::

    4. HP34-44
    5. CH78-66
    6. HP44-43
    7. CH66-54
    8. HP33-32
    9. CK51-52

The last moves by Cho were the only moves that could save the draw. Notive how the last move stops the soldier from blocking the horse.

.. janggi-board::

    HP32,HP43,CK52,CH54,HK39
    
Two soldiers versus elephant (漢兵兵楚象)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The elephant can keep the soldiers from mating by guarding the centre of the palace, the corner of the palace or any point the soldiers need to pass. This endgame is a draw.

.. janggi-board::

    CK42,HP73,HP54,HK59,CE88

.. janggi-moves::

    1. HP73-63
    2. CE88-65 
    3. HP63-53 check
    4. CK42-32
    5. HP54-44
    6. HP44-43

It is important for Cho to pass here. All other moves will loose the game in one move.Pass will lead to draw as none of the soldiers can mate by advancing to 42.

.. janggi-board::

    HP43,CK32,HP53,CE65,HK59

The following is a more difficult position where perfect play is required for Cho to draw.

.. janggi-board::

    CK42,HP23,HP34,CE28,HK40

.. janggi-moves::

    1. HP23-33 check
    2. CK42-52
    3. HP34-44
    4. CE28-5
    5. HP33-43
    6. CE5-37
    7. HP44-34
    8. CE37-5

The elephant blocks the rear soldier from advancing.

.. janggi-board::

    CE5,HP34,HP43,CK52,HK40


Two soldiers versus soldier (漢兵兵楚卒)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This endgame is a win in cases where the Han soldiers have safely passed
the defending soldier. If they cannot pass the endgame is a draw.

.. janggi-board::

    CK42,CP45,HP36,HP56,HK39

.. janggi-moves::

    1. HP56-66
    2. CP45-55
    3. HP36-35 
    4. HP66-76
    5. CP55-65
    6. HP76-86
    7. CP65-75

Han's second soldier cannot move without being captured.

.. janggi-board::

    CP75,HP86,HP35,CK42,HK39

If it is Han to move in the following position it is a win, but if Cho moves first it is a draw.

.. janggi-board::

    CK42,HK59,HP54,HP17,CP45

.. janggi-moves::

    1. HP17-16
    2. CP45-35
    3. HP16-15 
    4. HP15-14

The soldiers will proceed to mate the king. If instead Cho goes first

.. janggi-moves::

    1. CP45-35
    2. HP17-16
    3. CP35-25

Blocking the second Han solder. Another example that might at first seem to be a win for Han, but with good defence from Cho can actually be drawn.

.. janggi-board::

    HK42,CK59,CP55,HP46,HP26

.. janggi-moves::

    1. HP26-25
    2. HP25-35

It would now seem as if the solder on 35 keeps Cho's solider away letting the soldier on 46 to pass.

.. janggi-moves::

    3. HP46-36
    4. CK42-52

The last move was crucial. Cho is intending P55-45 with the double threat of bigjang and soldier.

.. janggi-moves::

    5. HK59-49
    6. CK52-42

Equally important move by Cho. The intention is to drive Han's general to be behind the solders. The only way for Han to avoid this perpetual bigjang threat is

.. janggi-moves::

    7. HK49-39
    8. CK42-32
    9. HP36-26
    10. CP55-45
    11. HP35-34
    12. CP45-35

And Cho succeeded in stopping the second soldier.

.. janggi-board::

    CK42,HP34,CP35,HP26,HK39

Two soldiers versus officer (漢兵兵楚士)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two soldiers cannot win against an officer. The defence is easy. It is just a matter of
keeping the officer from being captured by the soldiers without being able to recapture
with the general.

.. janggi-board::

    CK41,CO42,HP63,HP54,HK58

.. janggi-moves::

    1. HP63-53

Cho could capture the soldier and no mate would be possible. If the scoring rules are in effect
the game would then be won by Han with 2 vs 1.5. Better then to not take the exchange of soldier for officer keeping the score at 4.5 vs 4.

.. janggi-moves::

    2. CO42-51
    3. HP54-44
    4. HP44-43

Han cannot advance any further without loosing a soldier and the game is a draw. Note that although Han can force the capture of the officer it is not without Cho recapturing and scoring 1.5-0

.. janggi-board::

    CK41,CO51,HP43,HP53,CK58
